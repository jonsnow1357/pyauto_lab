#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
#import argparse
import threading
import json
import bottle

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc

STATUS_IDLE = "idle"
STATUS_RUN = "running"
TEMP_IDLE = 25.0

_webapp = bottle.Bottle()
_temperature = TEMP_IDLE
_status = STATUS_IDLE
_setPoint = TEMP_IDLE
_tempRate = 10.0  # degrees per minute
_tempRateEn = False

class EqptThread(threading.Thread):

  def __init__(self):
    super(EqptThread, self).__init__()

  def updateTemperature(self):
    global _temperature

    if (_status == STATUS_RUN):
      target = _setPoint
    else:
      target = TEMP_IDLE

    if (target != _temperature):
      if (not _tempRateEn):
        _temperature = target
      else:
        if (target > _temperature):
          _temperature += (_tempRate / 60.0)
          if (_temperature > target):
            _temperature = target
        elif (target < _temperature):
          _temperature -= (_tempRate / 60.0)
          if (_temperature < target):
            _temperature = target

  def run(self):
    while (True):
      pyauto_base.misc.waitWithPrint(1)
      #logger.info("update temperature")
      self.updateTemperature()

@_webapp.get("/errors")
def errors():
  res = []
  logger.info("errors: {}".format(res))
  return json.dumps(res)

@_webapp.get("/temperature")
def temperature():
  res = {"temperature": round(_temperature, 3)}
  logger.info("temperature: {}".format(res))
  return json.dumps(res)

@_webapp.get("/status")
def status():
  res = {"status": _status, "temp_rate_en": _tempRateEn}
  logger.info("status: {}".format(res))
  return json.dumps(res)

@_webapp.get("/setPoint")
def setPoint():
  logger.info("setPoint: {}".format(_setPoint))
  return str(_setPoint)

@_webapp.post("/setPoint")
def chgSetPoint():
  global _setPoint

  data = bottle.request.body.read()
  _setPoint = round(float(data), 0)

@_webapp.get("/tempRate")
def tempRate():
  logger.info("tempRate: {}".format(_tempRate))
  return str(_tempRate)

@_webapp.post("/tempRate")
def chgTempRate():
  global _tempRate

  data = bottle.request.body.read()
  _tempRate = float(data)

@_webapp.post("/start")
def start():
  global _status

  _status = STATUS_RUN

@_webapp.post("/stop")
def start():
  global _status

  _status = STATUS_IDLE

@_webapp.post("/tempRateEn")
def tempRateEn():
  global _tempRateEn

  data = int(bottle.request.body.read())
  if (data):
    _tempRateEn = True
  else:
    _tempRateEn = False

def mainApp():
  th1 = EqptThread()
  th1.daemon = True
  th1.start()

  bottle.debug(True)
  bottle.run(_webapp, host="localhost", port="10001")

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = ""
  #parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  #cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
