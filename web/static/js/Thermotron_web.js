////////////////////////////////////////////////////////////////////////////////
function pad0(n, len) {
  str = String(n);
  if(str.length < len) {
    return Array(len-str.length+1).join("0")+str
  }
  else {
    return str;
  }
}
////////////////////////////////////////////////////////////////////////////////
$().ready(function() {
  var d = new Date();
  strDate = d.getFullYear() + "-" + pad0(d.getMonth(), 2) + "-" + pad0(d.getDate(), 2)
  strDate += "T" + pad0(d.getHours(), 2) + ":" + pad0(d.getMinutes(), 2) + ":" + pad0(d.getSeconds(), 2)
  //console.log(strDate);
  $("#divTimestamp").html("Timestamp: <strong>"+strDate+"</strong>");

  var txt = $("#tempChamber").text();
  var temp = parseFloat(txt);
  if(temp < 1.0) {
    $("#tempChamber").css({"font-weight": "bold", "color": "blue"});
  }
  else if((temp >= 1.0) && (temp <= 40.0)) {
  }
  else {
    $("#tempChamber").css({"font-weight": "bold", "color": "red"});
  }

  var txt = $("#tempSetpoint").text();
  var temp = parseFloat(txt);
  if(temp < 1.0) {
    $("#tempSetpoint").css({"font-weight": "bold", "color": "blue"});
  }
  else if((temp >= 1.0) && (temp <= 40.0)) {
  }
  else {
    $("#tempSetpoint").css({"font-weight": "bold", "color": "red"});
  }
});
////////////////////////////////////////////////////////////////////////////////
