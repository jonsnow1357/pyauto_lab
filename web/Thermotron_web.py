#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
#import argparse
import threading
import queue
import json
import psutil
import bottle

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.equipment.Thermotron

rdFIFO = queue.Queue(maxsize=1)
wrFIFO = queue.Queue(maxsize=1)
webapp = bottle.Bottle()
localIP = None

class EqptInfo(object):

  def __init__(self):
    self.ID = None
    self.state = {}
    self.temperature = {}
    self.setpoint = None

  def toJSON(self):
    return json.dumps(self.__dict__)

  def fromJSON(self, strj):
    self.__dict__ = json.loads(strj)

  def __str__(self):
    return json.dumps(self.__dict__)

eqptInfo = EqptInfo()

def _updateInfo():
  if (not rdFIFO.empty()):
    eqptInfo.fromJSON(rdFIFO.get())
  #logger.info(eqptInfo)

@webapp.get("/static/css/<filepath:re:.*\\.css>")
def css(filepath):
  return bottle.static_file(filepath, root="static/css")

@webapp.get("/static/js/<filepath:re:.*\\.js>")
def css(filepath):
  return bottle.static_file(filepath, root="static/js")

@webapp.get("/state")
def eqptState():
  _updateInfo()
  return eqptInfo.state

@webapp.get("/temperature")
def eqptTemperature():
  _updateInfo()
  return eqptInfo.temperature

@webapp.post("/setpoint")
def eqptTemperature():
  setp = bottle.request.forms.get("txtSetPoint")
  try:
    setp = float(setp)
    while (not wrFIFO.empty()):
      wrFIFO.get()
    wrFIFO.put({"setpoint": setp})
  except ValueError:
    logger.warning("setpoint is NOT a float: {}".format(setp))
  bottle.redirect("/status")

@webapp.get("/status")
def status():
  _updateInfo()
  dictTemplate = {
      "modName": modName,
      "localIP": localIP,
      "ID": eqptInfo.ID,
      "state": eqptInfo.state["state"],
      "status": eqptInfo.state["status"],
      "alarm": eqptInfo.state["alarm"],
      "chamber": eqptInfo.temperature["chamber"],
      "setpoint": eqptInfo.temperature["setpoint"]
  }
  return bottle.template("{}.tpl".format(modName), **dictTemplate)

class EqptThread(threading.Thread):

  def __init__(self):
    super(EqptThread, self).__init__()

  def run(self):
    while (True):
      eqpt = pyauto_lab.equipment.Thermotron.Ctrl2800("ASRL8")
      eqpt.connect()

      if (not wrFIFO.empty()):
        temp = wrFIFO.get()
        #logger.info(temp)
        eqpt.setTemperature(temp["setpoint"])

      while (not rdFIFO.empty()):
        rdFIFO.get()
      info = EqptInfo()
      #info.ID = pyauto_base.misc.getRndStr(4)
      #info.state = {"status": "100", "state": "Run", "alarm": "0"}
      #info.temperature = {"chamber": "42.0 C", "setpoint": "-5.0 C"}
      info.ID = eqpt.getInfo()["ID"]
      info.state = eqpt.getStatus()
      info.temperature = eqpt.getTemperature()
      rdFIFO.put(info.toJSON())

      eqpt.showErrors()
      eqpt.disconnect()
      pyauto_base.misc.waitWithPrint(5)

def mainApp():
  global localIP

  for k, v in psutil.net_if_addrs().items():
    if (k == "Local Area Connection"):
      for nic in v:
        if (nic.family == 2):
          localIP = nic.address
  if (localIP is None):
    msg = "CANNOT determine local IP"
    logger.error(msg)
    raise RuntimeError(msg)

  th1 = EqptThread()
  th1.daemon = True
  th1.start()

  bottle.debug(True)
  #bottle.run(webapp, host="localhost", port="10001")
  bottle.run(webapp, host=localIP, port="10001")

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = ""
  #parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  #cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
