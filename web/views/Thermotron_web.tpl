<!doctype html>
<html lang="en">
  <head>
    <title>Thermotron Thermal Chamber Status</title>
    <meta http-equiv="refresh" content="10">
    <link rel="stylesheet" type="text/css" href="static/{{ modName }}.css">
    <script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
    <script src="static/{{ modName }}.js"></script>
  </head>

  <body>
    <h2>Thermotron Thermal Chamber [{{ ID }}]</h2>
    <div id="divTimestamp" style="padding: 0.2em;"></div>

    <div id="divStatus" style="padding: 0.2em;">
      <table>
        <tr>
          <td>State</td>
          <td>{{state}}</td>
        </tr>
        <tr>
          <td>Status Byte</td>
          <td>{{status}}</td>
        </tr>
        <tr>
          <td>Alarm Byte</td>
          <td>{{alarm}}</td>
        </tr>
      </table>
    </div>

    <div id="divSetPoint" style="padding: 0.2em;">
      <form id="frmSetPoint" method="post" action="setpoint">
        <label for="txtSetPoint">Setpoint:</label>
        <input type="text" id="txtSetPoint" name="txtSetPoint">
        <input type="submit" value="set">
      </form>
    </div>

    <div id="divTemperature" style="font-size: 1.5em; padding: 0.2em;">
      <table>
        <thead>
          <tr><th>Temperature ID</th><th>Value</th></tr>
        </thead>
        <tbody>
          <tr><td>chamber</td><td id="tempChamber">{{ chamber }}</td></tr>
          <tr><td>setpoint</td><td id="tempSetpoint">{{ setpoint }}</td></tr>
        </tbody>
      </table>
    </div>
  </body>
</html>
