#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_lab.config"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_lab.config
import pyauto_lab.equipment.base as labBase

class Test_LabSetupConfig(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_LabSetupConfig(self):
    cfg = pyauto_lab.config.LabSetupConfig()
    fPath = os.path.join(self.inFolder, "test_read_lab.cfg")
    cfg.loadCfgFile(fPath)
    cfg.showInfo()

    self.assertEqual(cfg.default, {"a": "4", "key": "value"})

    eq = cfg.dictEqpt["E1"]
    self.assertEqual(eq.id, "E1")
    self.assertEqual(eq.cls, "Keysight.N57xx")
    self.assertEqual(eq.conn, "10.120.67.40:5025")
    self.assertEqual(eq.params, {})
    labBase.getEqpt(eq.cls, eq.conn, eq.params)

    eq = cfg.dictEqpt["E2"]
    self.assertEqual(eq.id, "E2")
    self.assertEqual(eq.cls, "Tektronix.MSO5x")
    self.assertEqual(eq.conn, "10.120.67.69:5001")
    self.assertEqual(
        eq.params, {
            "adapter_name": "prologix",
            "adapter_gpibAddr": 24,
            "adapter_gpibEOS": "\n",
            "adapter_gpibEOM": "\n",
            "eos": "\r\n",
            "eom": "\r\n"
        })
    labBase.getEqpt(eq.cls, eq.conn, eq.params)
