#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_lab.equipment.base"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_lab.equipment.base as labBase

class Test_Equipment(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_LabEquipment(self):
    cnt = 1
    for eqptName in labBase.getEqptNames():
      eqpt = labBase.getEqpt(eqptName)
      logger.info(eqpt)
      cnt += 1
    logger.info("found {:d} equipment(s)".format(cnt))

class Test_PowerSwitch(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    eqpt = labBase.PowerSwitch("GPIB0::1::INSTR")
    self.assertEqual(eqpt.nOut, 1)
    eqpt._chkChannelId(None)
    eqpt._chkChannelId(0)
    eqpt._chkChannelId(5)

    with self.assertRaises(NotImplementedError):
      eqpt.enable()
      eqpt.disable()

    eqpt.nOut = 3
    self.assertEqual(eqpt.nOut, 3)
    eqpt._chkChannelId(1)
    eqpt._chkChannelId(2)
    eqpt._chkChannelId(3)
    with self.assertRaises(labBase.LabEqptException):
      eqpt._chkChannelId(None)
    with self.assertRaises(labBase.LabEqptException):
      eqpt._chkChannelId(0)
    with self.assertRaises(labBase.LabEqptException):
      eqpt._chkChannelId(4)
    with self.assertRaises(labBase.LabEqptException):
      eqpt._chkChannelId(5)

class Test_PowerSupply(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    eqpt = labBase.PowerSupply("GPIB0::1::INSTR")
    self.assertEqual(eqpt.nOut, 1)
    eqpt._chkChannelId(None)
    eqpt._chkChannelId(0)
    eqpt._chkChannelId(5)

    with self.assertRaises(NotImplementedError):
      eqpt.enable()
      eqpt.disable()

    eqpt.nOut = 3
    self.assertEqual(eqpt.nOut, 3)
    eqpt._chkChannelId(1)
    eqpt._chkChannelId(2)
    eqpt._chkChannelId(3)
    with self.assertRaises(labBase.LabEqptException):
      eqpt._chkChannelId(None)
    with self.assertRaises(labBase.LabEqptException):
      eqpt._chkChannelId(0)
    with self.assertRaises(labBase.LabEqptException):
      eqpt._chkChannelId(4)
    with self.assertRaises(labBase.LabEqptException):
      eqpt._chkChannelId(5)

class Test_PowerLoad(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    eqpt = labBase.PowerLoad("GPIB0::1::INSTR")
    self.assertEqual(eqpt.nOut, 1)
    eqpt._chkChannelId(None)
    eqpt._chkChannelId(0)
    eqpt._chkChannelId(5)

    with self.assertRaises(NotImplementedError):
      eqpt.enable()
      eqpt.disable()

    eqpt.nOut = 3
    self.assertEqual(eqpt.nOut, 3)
    eqpt._chkChannelId(1)
    eqpt._chkChannelId(2)
    eqpt._chkChannelId(3)
    with self.assertRaises(labBase.LabEqptException):
      eqpt._chkChannelId(None)
    with self.assertRaises(labBase.LabEqptException):
      eqpt._chkChannelId(0)
    with self.assertRaises(labBase.LabEqptException):
      eqpt._chkChannelId(4)
    with self.assertRaises(labBase.LabEqptException):
      eqpt._chkChannelId(5)
