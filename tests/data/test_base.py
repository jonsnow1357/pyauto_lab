#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_lab.data.file"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_lab.data.base

class Test_I2CFrame(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    frm = pyauto_lab.data.base.I2CFrame()

    self.assertEqual(frm.timestamp, None)
    self.assertEqual(frm.frame, "")
    self.assertEqual(frm.addr, None)
    self.assertEqual(frm.nb, 0)
    self.assertEqual(frm.rd_only, False)
    self.assertEqual(frm.wr_only, False)
    self.assertEqual(frm.rw, False)
    self.assertEqual(frm.data, [])

    frm.frame = "S 0x50_w 0x12 0x34 P"
    self.assertEqual(frm.timestamp, None)
    self.assertEqual(frm.frame, "S 0x50_w 0x12 0x34 P")
    self.assertEqual(frm.addr, "0x50")
    self.assertEqual(frm.nb, 2)
    self.assertEqual(frm.rd_only, False)
    self.assertEqual(frm.wr_only, True)
    self.assertEqual(frm.rw, False)
    self.assertEqual(frm.data, ["0x12", "0x34"])

    frm.frame = "x 0x50_r 0x55 0xAA x"
    self.assertEqual(frm.timestamp, None)
    self.assertEqual(frm.frame, "x 0x50_r 0x55 0xAA x")
    self.assertEqual(frm.addr, "0x50")
    self.assertEqual(frm.nb, 2)
    self.assertEqual(frm.rd_only, True)
    self.assertEqual(frm.wr_only, False)
    self.assertEqual(frm.rw, False)
    self.assertEqual(frm.data, ["0x55", "0xAA"])

    frm.frame = "[ 0x50_w 0x11 0x22 [ 0x50_r 0xBE 0xEF ]"
    self.assertEqual(frm.timestamp, None)
    self.assertEqual(frm.frame, "[ 0x50_w 0x11 0x22 [ 0x50_r 0xBE 0xEF ]")
    self.assertEqual(frm.addr, "0x50")
    self.assertEqual(frm.nb, 4)
    self.assertEqual(frm.rd_only, False)
    self.assertEqual(frm.wr_only, False)
    self.assertEqual(frm.rw, True)
    self.assertEqual(frm.data, ["0x11", "0x22", "0xBE", "0xEF"])
