#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_lab.data.TotalPhase"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_lab.data.TotalPhase

class Test_TotalPhase(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_readSPIdump(self):
    fPath = os.path.join(self.inFolder, "test_read_TotalPhase_SPI.csv")
    lstF = pyauto_lab.data.TotalPhase.readSPIdump(fPath)

    self.assertEqual(len(lstF), 143)
    self.assertEqual(lstF[0].fData, "11FF FFFF 01FF")
    self.assertEqual(lstF[80].fData, "10FF 04F8 0001 0000 0000")

  #@unittest.skip("")
  def test_readCANdump(self):
    fPath = os.path.join(self.inFolder, "test_read_TotalPhase_CANbus01.csv")
    lstF = pyauto_lab.data.TotalPhase.readCANdump(fPath)

    self.assertEqual(len(lstF), 436)
    self.assertEqual(lstF[14].fId, "0x08854807")
    self.assertEqual(lstF[14].fData, ["0x32", "0x30", "0x2E", "0xCD"])
    self.assertEqual(lstF[100].fId, "0x003c0000")
    self.assertEqual(lstF[100].fData,
                     ["0xFF", "0x01", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00"])
    self.assertEqual(lstF[200].fId, None)
    self.assertEqual(lstF[200].fData, None)
    self.assertEqual(lstF[200].fError, "[O] Dir=RX Pos=Tolerate Dominant Bits")

    lclDir = sys.path[0]
    fPath = os.path.join(self.inFolder, "test_read_TotalPhase_CANbus02.csv")
    lstF = pyauto_lab.data.TotalPhase.readCANdump(fPath)

    self.assertEqual(len(lstF), 689)
    self.assertEqual(lstF[90].fId, "0x09c12807")
    self.assertEqual(lstF[90].fData,
                     ["0x54", "0x20", "0x30", "0x2E", "0x30", "0x2E", "0x33", "0x3B"])
    self.assertEqual(lstF[601].fId, None)
    self.assertEqual(lstF[601].fData, None)
    self.assertEqual(lstF[601].fError, "[S] Dir=RX Pos=Data Field")

  #@unittest.skip("")
  def test_readI2Cdump(self):
    fPath = os.path.join(self.inFolder, "test_read_TotalPhase_I2C.csv")
    lstF = pyauto_lab.data.TotalPhase.readI2Cdump(fPath)

    self.assertEqual(len(lstF), 117)
    self.assertEqual(lstF[0].fData, "0x06N")
    self.assertFalse(lstF[0].bRnW)
    self.assertEqual(lstF[0].fError, None)
    self.assertEqual(lstF[33].fData, "0x48")
    self.assertFalse(lstF[33].bRnW)
    self.assertEqual(lstF[33].fError, None)

  #@unittest.skip("")
  def test_readCANdump_Optelian(self):
    fPath = os.path.join(self.inFolder, "test_read_Optelian_CAN.log")
    lstF = pyauto_lab.data.TotalPhase.readCANdump_Optelian(fPath)

    self.assertEqual(len(lstF), 281)
    self.assertEqual(lstF[0].fId, "0x003c0000")
    self.assertEqual(lstF[0].fData,
                     ["0xFF", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00", "0x00"])
    self.assertEqual(lstF[150].fId, "0x18312ac3")
    self.assertEqual(lstF[150].fData,
                     ["0xFB", "0x9E", "0x85", "0x33", "0x1E", "0xC8", "0x8D", "0xBB"])
