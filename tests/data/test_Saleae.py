#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_lab.data.TotalPhase"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_lab.data.Saleae

class Test_Saleae(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_readI2Cdump(self):
    fPath = os.path.join(self.inFolder, "test_read_Saleae_I2C.csv")
    lstF = pyauto_lab.data.Saleae.readI2Cdump(fPath)

    self.assertEqual(len(lstF), 54)

    frm = lstF[0]
    self.assertEqual(frm.timestamp, 9000)
    self.assertEqual(frm.frame, "x 0x4F_w 0x00 0x01 0x82 x")
    self.assertEqual(frm.addr, "0x4F")
    self.assertEqual(frm.nb, 3)
    self.assertEqual(frm.rd_only, False)
    self.assertEqual(frm.wr_only, True)
    self.assertEqual(frm.rw, False)
    self.assertEqual(frm.data, ["0x00", "0x01", "0x82"])

    frm = lstF[-1]
    self.assertEqual(frm.timestamp, 1095469000)
    self.assertEqual(frm.frame, "x 0x4B_r 0xBA 0xDB 0x5C x")
    self.assertEqual(frm.addr, "0x4B")
    self.assertEqual(frm.nb, 3)
    self.assertEqual(frm.rd_only, True)
    self.assertEqual(frm.wr_only, False)
    self.assertEqual(frm.rw, False)
    self.assertEqual(frm.data, ["0xBA", "0xDB", "0x5C"])
