#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_lab.data.file"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_lab.data.file

class Test_HexFile(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_read(self):
    fPath = os.path.join(self.inFolder, "test_read_HEXFile01.hex")
    hexFile = pyauto_lab.data.file.HexFile(fPath)
    hexFile.read()

    self.assertEqual(hexFile.cntBytes, 34748)
    dictTmp = {
        pyauto_lab.data.file.HEXDescData: 2173,
        pyauto_lab.data.file.HEXDescEOF: 1,
        pyauto_lab.data.file.HEXDescESAR: 0,
        pyauto_lab.data.file.HEXDescELAR: 0,
        pyauto_lab.data.file.HEXDescSSAR: 1,
        pyauto_lab.data.file.HEXDescSLAR: 0
    }
    self.assertEqual(hexFile.cntRec, dictTmp)

    fPath = os.path.join(self.inFolder, "test_read_HEXFile02.hex")
    hexFile = pyauto_lab.data.file.HexFile(fPath)
    hexFile.read()

    self.assertEqual(hexFile.cntBytes, 18208)
    dictTmp = {
        pyauto_lab.data.file.HEXDescData: 1139,
        pyauto_lab.data.file.HEXDescEOF: 1,
        pyauto_lab.data.file.HEXDescESAR: 0,
        pyauto_lab.data.file.HEXDescELAR: 0,
        pyauto_lab.data.file.HEXDescSSAR: 0,
        pyauto_lab.data.file.HEXDescSLAR: 0
    }
    self.assertEqual(hexFile.cntRec, dictTmp)

class Test_TouchstoneFile(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)
    pyauto_base.fs.mkOutFolder(self.outFolder, bClear=True)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_read(self):
    fPath = os.path.join(self.inFolder, "test_read_Touchstone.s2p")
    tFile = pyauto_lab.data.file.TouchstoneFile(fPath)
    self.assertEqual(tFile.nPorts, 2)
    tFile.read()

    self.assertEqual(tFile.nFreq, 201)
    freq = tFile.getFreq()
    self.assertEqual(len(freq), tFile.nFreq)
    self.assertEqual(freq[0], 10.0)
    self.assertEqual(freq[-1], 20000.0)
    s11 = tFile.getParam(1, 1)
    self.assertEqual(len(s11), tFile.nFreq)
    self.assertEqual(s11[0][0], -44.00026)
    self.assertEqual(s11[-1][0], -13.42100)
    s12 = tFile.getParam(1, 2)
    self.assertEqual(len(s12), tFile.nFreq)
    self.assertEqual(s12[0][0], -0.4062531)
    self.assertEqual(s12[-1][0], -3.678138)
    s21 = tFile.getParam(2, 1)
    self.assertEqual(len(s21), tFile.nFreq)
    self.assertEqual(s21[0][0], 0.2322159)
    self.assertEqual(s21[-1][0], -3.692193)
    s22 = tFile.getParam(2, 2)
    self.assertEqual(len(s22), tFile.nFreq)
    self.assertEqual(s22[0][0], -39.87220)
    self.assertEqual(s22[-1][0], -14.03813)

    fPath = os.path.join(self.inFolder, "test_read_Touchstone.s14p")
    tFile = pyauto_lab.data.file.TouchstoneFile(fPath)
    self.assertEqual(tFile.nPorts, 14)
    tFile.read()

    self.assertEqual(tFile.nFreq, 61)
    freq = tFile.getFreq()
    self.assertEqual(len(freq), tFile.nFreq)
    self.assertEqual(freq[0], 10000.0)
    self.assertEqual(freq[-1], 16000.0)
    s11 = tFile.getParam(1, 1)
    self.assertEqual(len(s11), tFile.nFreq)
    self.assertEqual(s11[0][0], 0.00571258145858686)
    self.assertEqual(s11[-1][0], 0.0141911367950095)
    s12 = tFile.getParam(1, 2)
    self.assertEqual(len(s12), tFile.nFreq)
    self.assertEqual(s12[0][0], 5.7794370315905E-05)
    self.assertEqual(s12[-1][0], 0.000155907827243765)
    s21 = tFile.getParam(2, 1)
    self.assertEqual(len(s21), tFile.nFreq)
    self.assertEqual(s21[0][0], 5.77943703159048E-05)
    self.assertEqual(s21[-1][0], 0.000155907827243766)
    s22 = tFile.getParam(2, 2)
    self.assertEqual(len(s22), tFile.nFreq)
    self.assertEqual(s22[0][0], 0.0242680717815858)
    self.assertEqual(s22[-1][0], 0.0352089100475683)

  def test_write(self):
    fPath = os.path.join(self.outFolder, "test_write_Touchstone.s2p")
    tFile = pyauto_lab.data.file.TouchstoneFile(fPath)
    self.assertEqual(tFile.nPorts, 2)
    tFile.optionLine = "# GHz S RI"

    nFreq = 10
    tFile.setFreq(range(1, (nFreq + 1)))
    tFile.setParam(1, 1, [[0.1, 0.2]] * nFreq)
    tFile.setParam(1, 2, [[-0.5, 0.6]] * nFreq)
    tFile.setParam(2, 1, [[-0.3, -0.4]] * nFreq)
    tFile.setParam(2, 2, [[0.7, -0.8]] * nFreq)
    tFile.write()

    #hashVal = [
    #    "732f365038ed6d18a7be15600c186d841b4463d922516e3e2c423cf7ab1eab1c",
    #]
    self.assertTrue(os.path.isfile(fPath), "file DOES NOT exist: {}".format(fPath))
    tmp = pyauto_base.misc.getFileHash(fPath)
    #self.assertIn(tmp, hashVal, "INCORRECT hash for {} ({})".format(fPath, tmp))

class Test_WFMFile(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_read(self):
    fPath = os.path.join(self.inFolder, "test_read_WFMFile_002.wfm")
    wfmFile = pyauto_lab.data.file.readWFMFile(fPath)

    self.assertEqual(wfmFile.nData, 500000)
    self.assertEqual(len(wfmFile.ts), wfmFile.nData)
    self.assertEqual(len(wfmFile.data), wfmFile.nData)
    logger.info(wfmFile.ts[0:10])
    logger.info(wfmFile.data[0:10])

    self.assertEqual(wfmFile.ts[0], -0.000399999989)
    self.assertEqual(wfmFile.data[0], 1.72)
    self.assertEqual(wfmFile.ts[1000], -0.000395999989)
    self.assertEqual(wfmFile.data[1000], 1.72)
    self.assertEqual(wfmFile.ts[150000], 0.000199999994)
    self.assertEqual(wfmFile.data[150000], 0.92)

    fPath = os.path.join(self.inFolder, "test_read_WFMFile_003.wfm")
    wfmFile = pyauto_lab.data.file.readWFMFile(fPath)

    self.assertEqual(wfmFile.nData, 20000)
    self.assertEqual(len(wfmFile.ts), wfmFile.nData)
    self.assertEqual(len(wfmFile.data), wfmFile.nData)
    logger.info(wfmFile.ts[0:10])
    logger.info(wfmFile.data[0:10])

    self.assertEqual(wfmFile.ts[0], -0.000089)
    self.assertEqual(wfmFile.data[0], 0.078)
    self.assertEqual(wfmFile.ts[1000], -0.000039)
    self.assertEqual(wfmFile.data[1000], 0.078)
    self.assertEqual(wfmFile.ts[19999], 0.00091095)
    self.assertEqual(wfmFile.data[19999], 3.238)
