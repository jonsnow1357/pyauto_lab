#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_lab.equipment.base"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

class Test_MultimeterCtrl(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_MultimeterCtrl(self):
    dictCLIArgs = {"driver": "Fake.Multimeter", "conn": "http://127.0.0.1", "cfg": None}
    pyauto_base.misc.runScriptMainApp("pyauto_lab.equipment_ctrl.MultimeterCtrl",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    for i, m in enumerate(labBase.lstMultimeterMode):
      dictCLIArgs = {
          "driver": "Fake.Multimeter",
          "conn": "http://127.0.0.1",
          "cfg": "{},{}".format(((i % 2) + 1), m)
      }
      pyauto_base.misc.runScriptMainApp("pyauto_lab.equipment_ctrl.MultimeterCtrl",
                                        logger,
                                        dictCLIArgs=dictCLIArgs)
