#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json
import configparser

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_base.config

class LabConfigException(Exception):
  pass

class EquipmentInfo(object):

  def __init__(self, val):
    self.id = val
    self.cls = None
    self.conn = None
    self.params = {}

  def __str__(self):
    return "{} {}, {}, {}".format(self.__class__.__name__, self._id, self.cls, self.conn)

  @property
  def id(self):
    return self._id

  @id.setter
  def id(self, val):
    if (pyauto_base.misc.isEmptyString(val)):
      msg = "CANNOT assign empty id"
      logger.error(msg)
      raise RuntimeError(msg)
    self._id = val

class LabSetupConfig(pyauto_base.config.SimpleConfig):
  """
  Assumes a DUT described in the [default] section
  and a list of LabEquipments in [equipment_???] sections.
  """

  def __init__(self):
    super(LabSetupConfig, self).__init__()
    self.manufacturing = {}
    self.dictEqpt = {}

  def _parse_sectionMfg(self):
    for s in self._cfgObj.sections():
      if (s == "manufacturing"):
        self.manufacturing = dict(self._cfgObj[s].items())

  def _parse_sectionEqpt(self):
    for s in self._cfgObj.sections():
      if (s.startswith("equipment_")):
        eqId = s[10:]
        eq = EquipmentInfo(eqId)
        eq.cls = self._cfgObj.get(s, "class")
        eq.conn = self._cfgObj.get(s, "conn")
        try:
          tmp = json.loads(self._cfgObj.get(s, "adapter"))
          if (not isinstance(tmp, dict)):
            logger.warning("equipment 'adapter' should be a dictionary")
          else:
            eq.params = dict([(("adapter_" + k), v) for k, v in tmp.items()])
        except configparser.NoOptionError:
          pass
        try:
          tmp = json.loads(self._cfgObj.get(s, "params"))
          if (not isinstance(tmp, dict)):
            logger.warning("equipment 'params' should be a dictionary")
          else:
            eq.params.update(tmp)
        except configparser.NoOptionError:
          pass

        if (eq.id in self.dictEqpt.keys()):
          logger.info("DUPLICATE equipment {}".format(eq.id))
        self.dictEqpt[eq.id] = eq

  def _showInfo_sectionEqpt(self):
    if (len(self.dictEqpt) == 0):
      return
    logger.info("-- equipment ({:d})".format(len(self.dictEqpt)))
    for k, v in self.dictEqpt.items():
      logger.info("  {}".format(v))

  def parseCfg(self):
    super(LabSetupConfig, self).parseCfg()
    self._parse_sectionMfg()
    self._parse_sectionEqpt()

  def showInfo(self):
    super(LabSetupConfig, self).showInfo()
    self._showInfo_sectionEqpt()
