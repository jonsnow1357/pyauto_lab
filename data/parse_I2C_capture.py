#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.data.TotalPhase

class AppParameters(object):

  def __init__(self):
    self.bFilter = not True
    self.filters = []

appParams = AppParameters()

def _filterFrame(frm):
  # for flt in appParams.filters:
  #   if("data" in flt.keys()):
  #     if(flt["data"] == frm.fData):
  #       return True
  #   if("regex" in flt.keys()):
  #     if(re.match(flt["regex"], frm.fData) is not None):
  #       return True
  return False

def _parseFile(lstFrames):
  if (appParams.bFilter):
    logger.info("[{}] filtering ON".format(modName))

  for frm in lstFrames:
    if (appParams.bFilter):
      if (_filterFrame(frm)):  # don't show anything that matches the filter
        continue

    frm.showInfo()

def mainApp():
  lstFrames = pyauto_lab.data.TotalPhase.readI2Cdump(cliArgs["file"])

  if (cliArgs["action"] == "parse"):
    _parseFile(lstFrames)
  elif (cliArgs["action"] == "filter"):
    appParams.bFilter = True
    _parseFile(lstFrames)

  pyauto_base.misc.copyLoggerFile("{}.log".format(os.path.basename(cliArgs["file"])[:-4]))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = ""
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("action",
                      help="action",
                      choices=("parse", "filter"),
                      nargs="?",
                      default="parse")
  parser.add_argument("file", help="SPI dump file path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
