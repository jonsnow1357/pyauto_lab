#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.data.TotalPhase

class AppParameters(object):

  def __init__(self):
    self.bHartbeatDetect = True
    self.bFilter = not True
    self.filters = [
        {
            "src": None,
            "dst": 15
        },
    ]

appParams = AppParameters()

def _showDuplicateData(lstFrames):
  allowDupe = [
      "FF 00 00 00 00 00 00 00",
      "00 00 00 00 00 00 00 00",
  ]

  frmCnt = 1
  frmPrev = lstFrames[0]
  for i in range(1, len(lstFrames)):
    frm = lstFrames[i]
    if ((frm.fId == frmPrev.fId) and (frm.fData == frmPrev.fData)):
      if ((frm.fData.upper() not in allowDupe)):
        logger.info("[{}] duplicate data detected at record {}:".format(modName, frmCnt))
        frmPrev.showInfo()
        frm.showInfo()
        frm.showInfo(bOptelian=True)
    frmPrev = frm
    frmCnt += 1

def _searchDataEvent(lstFrames):
  data1 = "FF 00 00 00 00 00 00 00"
  data2 = "02 00 FF 05 06"

  frmCnt = 0
  prevData = ""
  for frm in lstFrames:
    frmCnt += 1
    if (prevData.upper().startswith(data1) and frm.fData.upper().startswith(data2)):
      logger.info("[{}] event found at record {}:".format(modName, frmCnt))
      logger.info("  {}".format(prevData))
      logger.info("  {}".format(frm.fData))
    prevData = frm.fData

def _filterFrame(frm):
  for flt in appParams.filters:
    if (("src" in flt.keys()) and ("dst" in flt.keys())):
      if ((flt["src"] is None) and (flt["dst"] is None)):
        return False
      elif ((flt["src"] is None) and (flt["dst"] == frm.getOptelianId()[3])):
        return True
      elif ((flt["src"] == frm.getOptelianId()[2]) and (flt["dst"] is None)):
        return True
      elif ((flt["src"] == frm.getOptelianId()[2])
            and (flt["dst"] == frm.getOptelianId()[3])):
        return True

  return False

def _parseFile(lstFrames):
  if (appParams.bFilter):
    logger.info("[{}] filtering ON".format(modName))
  if (appParams.bHartbeatDetect):
    logger.info("[{}] heartbeat detection ON".format(modName))

  for frm in lstFrames:
    if (appParams.bFilter):
      if (_filterFrame(frm)):  # don't show anything that matches the filter
        continue

    frm.showInfo(bOptelian=True)

    if (appParams.bHartbeatDetect):
      if (frm.fId is None):
        continue
      if (frm.getOptelianId()[5] == "0x0000"):
        if ((frm.getOptelianId()[2] == 0) and (frm.getOptelianId()[3] == 15)):
          logger.info("  heartbeat (MGT -> all)")
        elif (frm.getOptelianId()[2] == 0):
          logger.info("  heartbeat (reply from {})".format(frm.idSrc))
        elif (frm.getOptelianId()[2] == 1):
          logger.info("  heartbeat* (reply from {})".format(frm.idSrc))

def mainApp():
  lstFrames = []
  if (cliArgs["file"].endswith("log")):
    lstFrames = pyauto_lab.data.TotalPhase.readCANdump_Optelian(cliArgs["file"])
  elif (cliArgs["file"].endswith("csv")):
    lstFrames = pyauto_lab.data.TotalPhase.readCANdump(cliArgs["file"])
  else:
    logger.error("UNSUPPORTED file: {}".format(cliArgs["file"]))
    return

  if (cliArgs["action"] == "parse"):
    _parseFile(lstFrames)
  elif (cliArgs["action"] == "filter"):
    appParams.bFilter = True
    _parseFile(lstFrames)
  elif (cliArgs["action"] == "dupe"):
    _showDuplicateData(lstFrames)
  elif (cliArgs["action"] == "trig"):
    _searchDataEvent(lstFrames)

  pyauto_base.misc.copyLoggerFile("{}.log".format(os.path.basename(cliArgs["file"])[:-4]))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = ""
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("action",
                      help="action",
                      choices=("parse", "filter", "dupe", "trig"),
                      nargs="?",
                      default="parse")
  parser.add_argument("file", help="CANbus dump file path")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
