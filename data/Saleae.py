#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_lab.data.base

def _parse_I2CFrame(lstLn):
  try:
    ts = int(float(lstLn[0]) * 1e9)
  except ValueError:
    return

  tmp = [t for t in lstLn if (t.endswith("ACK") or t.endswith("NAK"))]
  frm = pyauto_lab.data.base.I2CFrame()

  if (tmp[0].startswith("Setup Read")):
    tmp[0] = tmp[0].split()[3][1:-1] + "_r"
  elif (tmp[0].startswith("Setup Write")):
    tmp[0] = tmp[0].split()[3][1:-1] + "_w"
  else:
    raise RuntimeError
  for i in range(1, len(tmp)):
    tmp[i] = tmp[i].split()[0]
  frm.frame = "x " + " ".join(tmp) + " x"
  frm.timestamp = ts
  return frm

def readI2Cdump(fPath):
  pyauto_base.fs.chkPath_File(fPath)
  if (not fPath.lower().endswith(".csv")):
    msg = "incorrect file name: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)
  logger.info("-- reading I2C dump file: {}".format(fPath))

  tmp = []
  lstFrames = []
  with open(fPath, "r") as fIn:
    csvIn = csv.reader(fIn)
    for row in csvIn:
      #print("DBG", row)
      if (row[2].startswith("Setup")):
        frm = _parse_I2CFrame(tmp)
        if (frm is not None):
          lstFrames.append(frm)
        tmp = []
      tmp += row
  res = _parse_I2CFrame(tmp)
  if (res is not None):
    lstFrames.append(res)

  logger.info("read {:d} frame(s)".format(len(lstFrames)))
  logger.info("-- done reading file")
  return lstFrames
