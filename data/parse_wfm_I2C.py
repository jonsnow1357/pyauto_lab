#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.bin
import pyauto_lab.data.file
import pyauto_lab.data.TotalPhase

def parse_I2C_transaction(busData):
  #logger.info(len(busData), busData)
  i2cf = pyauto_lab.data.TotalPhase.I2CFrame()

  if ((busData[0] == "[") and (busData[-1] == "]")):
    i2cf.bStart = True
    i2cf.bStop = True
    busData = busData[1:-2]
  elif ((busData[0] == "[") and (busData[-1] != "]")):
    i2cf.bStart = True
    busData = busData[1:-1]

  #print len(busData),busData
  if (len(busData) == 0):
    logger.error("no bits in transaction")
    return
  if ((len(busData) % 9) != 0):
    logger.error("unexpected number of bits: {}".format(len(busData)))
    logger.error(busData)
    return

  #print len(busData),busData
  for i in range(0, len(busData), 9):
    b = busData[i:(i + 9)]
    if (b[-1] == 0):
      i2cf.addByte(pyauto_base.bin.bits2hex(b[0:8], reverse=True))
    else:
      i2cf.addByte(pyauto_base.bin.bits2hex(b[0:8], reverse=True), False)

  i2cf.showInfo()

def detect_I2C_data(ts, scl, sda):
  showI2Cdetails = True
  tGlitch = 10e-8  # ignore SCL rise-edges separated by less than tGlitch
  busState = "idle"
  busData = []  # bits on the bus
  tsS = 0.0  # timestamp for the current START
  tsP = 0.0  # timestamp for the current STOP
  tsRE = ts[1]

  if ((scl[0] != 1) or (sda[0] != 1)):
    logger.warning("BUS is not idle: SCL={} SDA={}".format(scl[0], sda[0]))
    busState = "unknown"

  for i in range(1, len(ts)):
    if (busState == "idle"):
      if (((sda[i - 1] == 1) and (sda[i] == 0))
          and (scl[i] == 1)):  # SCL = 1, SDA fall-edge
        tsS = ts[i]
        tsRE = ts[i]
        if ((tsS - tsP) > 1.0e-3):
          logger.info("======== [> 1ms break]")
        if (showI2Cdetails):
          logger.info("{: <20} @ {}".format("I2C START", tsS))
        busState = "active"
        if (busData != []):
          parse_I2C_transaction(busData)
          busData = []
        busData.append("[")
      if (((sda[i - 1] == 1) and (sda[i] == 0))
          and (scl[i] == 0)):  # SCL = 0, SDA fall-edge
        logger.warning("{: <20} @ {}".format("SDA fall-edge", ts[i]))
        busState = "unknown"
      if ((sda[i - 1] == 0) and (sda[i] == 1)):
        logger.warning("{: <20} @ {}".format("SDA rise-edge", ts[i]))
        busState = "unknown"

    if (busState == "active"):
      if (((sda[i - 1] == 0) and (sda[i] == 1))
          and (scl[i] == 1)):  # SCL = 1, SDA rise-edge
        tsP = ts[i]
        if (showI2Cdetails):
          logger.info("{: <20} @ {}".format("I2C STOP", tsP))
        busData.append("]")
        if ((len(busData) > 2)
            and (busData[-2] != 0)):  # busData may be 1 but this was not encountered yet
          logger.error("unexpected bus data")
          busState = "unknown"
        else:
          busState = "idle"
          parse_I2C_transaction(busData)
          busData = []
      if ((scl[i - 1] == 0) and (scl[i] == 1)):  # SCL rise-edge
        tmp = (ts[i] - tsRE)
        tsRE = ts[i]
        if (tmp > tGlitch):
          busData.append(sda[i])

    if (busState == "unknown"):
      if (((sda[i - 1] == 0) and (sda[i] == 1))
          and (scl[i] == 1)):  # SCL = 1, SDA rise-edge
        tsP = ts[i]
        if (showI2Cdetails):
          logger.info("{: <20} @ {}".format("I2C STOP", tsP))
        busState = "idle"

  if (busData != []):
    parse_I2C_transaction(busData)

def meas_I2C_freq(ts, scl, sda):
  showI2Cdetails = not True

  logger.info("detecting I2C frequency ...")
  tGlitch = 10e-6  # ignore edges separated by less than tGlitch
  tI2C = 1.0
  tsRE = ts[1]

  for i in range(1, len(ts)):
    tmp = 0.0
    if ((scl[i - 1] == 0) and (scl[i] == 1)):
      tmp = (ts[i] - tsRE)
      tsRE = ts[i]
      if (showI2Cdetails):
        #logger.info("{} {} {}".format(ts[i], val[i], val[i-1]))
        logger.info("{: <20} @ {}; {}".format("SCL rise-edge", tsRE, tmp))
    if ((tmp > tGlitch) and (tmp < tI2C)):
      tI2C = tmp
    if ((scl[i - 1] == 1) and (scl[i] == 0)):
      if (showI2Cdetails):
        logger.info("{: <20} @ {}".format("SCL fall-edge", ts[i]))
    if ((sda[i - 1] == 0) and (sda[i] == 1)):
      if (showI2Cdetails):
        logger.info("{: <20} @ {}".format("SDA rise-edge", ts[i]))
    if ((sda[i - 1] == 1) and (sda[i] == 0)):
      if (showI2Cdetails):
        logger.info("{: <20} @ {}".format("SDA fall-edge", ts[i]))

  I2Cfreq = 1.0 / tI2C
  logger.info("I2C frequency: {} Hz".format(I2Cfreq))
  if (I2Cfreq <= 1.0e3) or (I2Cfreq > 5.0e5):
    msg = "possible invalid I2C frequency"
    logger.error(msg)
    raise RuntimeError(msg)
  return tI2C

def mainApp():
  pyauto_base.fs.chkPath_File(cliArgs["file"][0])
  pyauto_base.fs.chkPath_File(cliArgs["file"][1])

  if (cliArgs["file"][0].endswith("csv")):
    res = pyauto_base.misc.readCSV_asCols(cliArgs["file"][0])
    #sampleInterval = res[1][1]
    SCL_ts = [float(t) for t in res[3] if t != ""]
    SCL_val = [float(t) for t in res[4] if t != ""]
  elif (cliArgs["file"][0].endswith("wfm")):
    res = pyauto_lab.data.file.readWFMFile(cliArgs["file"][0])
    SCL_ts = res.ts
    SCL_val = res.data
  else:
    msg = "UNRECOGNIZED file: {}".format(cliArgs["file"])
    logger.error(msg)
    raise RuntimeError(msg)
  if (len(SCL_ts) != len(SCL_val)):
    msg = "MISMATCH in number of SCL values: {:d} vs. {:d}".format(
        len(SCL_ts), len(SCL_val))
    logger.error(msg)
    raise RuntimeError(msg)

  if (cliArgs["file"][1].endswith("csv")):
    res = pyauto_base.misc.readCSV_asCols(cliArgs["file"][1])
    #sampleInterval = res[1][1]
    SDA_ts = [float(t) for t in res[3] if t != ""]
    SDA_val = [float(t) for t in res[4] if t != ""]
  elif (cliArgs["file"][1].endswith("wfm")):
    res = pyauto_lab.data.file.readWFMFile(cliArgs["file"][1])
    SDA_ts = res.ts
    SDA_val = res.data
  else:
    msg = "UNRECOGNIZED file: {}".format(cliArgs["file"])
    logger.error(msg)
    raise RuntimeError(msg)
  if (len(SDA_ts) != len(SDA_val)):
    msg = "MISMATCH in number of SDA values: {:d} vs. {:d}".format(
        len(SDA_ts), len(SDA_val))
    logger.error(msg)
    raise RuntimeError(msg)
  if (len(SCL_ts) != len(SDA_ts)):
    msg = "MISMATCH in number of values: {:d} vs. {:d}".format(len(SCL_ts), len(SDA_ts))
    logger.error(msg)
    raise RuntimeError(msg)

  fOutName = modName + ".csv"
  with open(fOutName, "w") as fOut:
    csvOut = csv.writer(fOut, lineterminator="\n")
    csvOut.writerow(["SCL_ts", "SDA_ts", "SCL", "SDA"])
    for i in range(0, len(SCL_ts)):
      csvOut.writerow([SCL_ts[i], SDA_ts[i], SCL_val[i], SDA_val[i]])

  min_SCL_val = min(SCL_val)
  max_SCL_val = max(SCL_val)
  min_SDA_val = min(SDA_val)
  max_SDA_val = max(SDA_val)
  logger.info("SCL range: {}".format([min_SCL_val, max_SCL_val]))
  logger.info("SDA range: {}".format([min_SDA_val, max_SDA_val]))

  Vth = 2.0
  logger.info("converting to logical values (Vth = {}) ...".format(Vth))
  for i in range(0, len(SCL_ts)):
    if (SCL_ts[i] != SDA_ts[i]):
      logger.error("mismatch in timestamps: {} vs. {}".format(SCL_ts[i], SDA_ts[i]))
      return
  ts = SCL_ts
  scl = [0 if (t < Vth) else 1 for t in SCL_val]
  sda = [0 if (t < Vth) else 1 for t in SDA_val]
  #logger.info([ts[0], scl[0], sda[0]])

  meas_I2C_freq(ts, scl, sda)
  detect_I2C_data(ts, scl, sda)

  pyauto_base.misc.copyLoggerFile("{}.log".format(
      os.path.basename(cliArgs["file"][0])[:-4]))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = ""
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("file", help="waveform file(s)", nargs=2)
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
