#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.bin
import pyauto_lab.data.file
import pyauto_lab.data.TotalPhase

def parse_CAN_transaction(busData):
  #print len(busData),busData
  if (len(busData) < 44):
    logger.error("too few bits in bus data")
    return
  if (busData[0] != 0):
    logger.error("incorrect SOF")
    return
  logger.info("bus data size: {:d}b".format(len(busData)))

  #remove stuff bits
  frmData = []  # busData without stuff bits
  cnt0 = 0
  cnt1 = 0
  for i in range(0, len(busData)):
    if (busData[i] == 0):
      cnt0 += 1
      if (cnt1 == 5):
        cnt1 = 0
        continue
      if (cnt0 == 6):
        logger.error("missing stuff bits after 0")
        return
      else:
        frmData.append(busData[i])
      cnt1 = 0
    if (busData[i] == 1):
      cnt1 += 1
      if (cnt0 == 5):
        cnt0 = 0
        continue
      if (cnt1 == 6):
        logger.info("missing stuff bits after 1 ... assuming EOF")
        break
      else:
        frmData.append(busData[i])
      cnt0 = 0

  #print len(frmData), frmData
  if (frmData[12] == 0):
    logger.info("  normal frame")
    bID = frmData[1:12]
    canID = pyauto_base.bin.bits2hex(bID, True)
    bRTR = frmData[12]
    bIDE = frmData[13]
    bDLC = frmData[15:19]
    canDLC = int(pyauto_base.bin.bits2hex(bDLC, True), 16)
    logger.info("  RTR : {}".format(bRTR))
    logger.info("  IDE : {}".format(bIDE))

    canDF = frmData[19:(19 + (canDLC * 8))]
    canData = pyauto_base.bin.bits2hex(canDF)

    canCRC = frmData[(19 + (canDLC * 8)):(19 + (canDLC * 8) + 15)]
    logger.info("  CRC : {}".format(pyauto_base.bin.bits2hex(canCRC, True)))
    canCRCd = frmData[19 + (canDLC * 8) + 15]
    logger.info("  CRCd: {}".format(canCRCd))
    canACK = frmData[19 + (canDLC * 8) + 16]
    logger.info("  ACK : {}".format(canACK))
    if (canACK != 0):
      logger.error("no ACK received")
    canACKd = frmData[19 + (canDLC * 8) + 17]
    logger.info("  ACKd: {}".format(canACKd))
  else:
    logger.info("  extended frame")
    bID = frmData[1:12] + frmData[14:32]
    canID = pyauto_base.bin.bits2hex(bID, True)
    bSRR = frmData[12]
    bIDE = frmData[13]
    bRTR = frmData[32]
    bDLC = frmData[35:39]
    canDLC = int(pyauto_base.bin.bits2hex(bDLC, True), 16)
    logger.info("  SRR : {}".format(bSRR))
    logger.info("  IDE : {}".format(bIDE))
    logger.info("  RTR : {}".format(bRTR))

    canDF = frmData[39:(39 + (canDLC * 8))]
    canData = pyauto_base.bin.bits2hex(canDF)

    canCRC = frmData[(39 + (canDLC * 8)):(39 + (canDLC * 8) + 15)]
    logger.info("  CRC : {}".format(pyauto_base.bin.bits2hex(canCRC, True)))
    canCRCd = frmData[39 + (canDLC * 8) + 15]
    logger.info("  CRCd: {}".format(canCRCd))
    canACK = frmData[39 + (canDLC * 8) + 16]
    logger.info("  ACK : {}".format(canACK))
    if (canACK != 0):
      logger.error("no ACK received")
    canACKd = frmData[39 + (canDLC * 8) + 17]
    logger.info("  ACKd: {}".format(canACKd))

  #logger.info("  ID  : {}".format(canID))
  #logger.info("  DLC : {}".format(canDLC))
  cf = pyauto_lab.data.TotalPhase.CANFrame()
  cf.addData(canID, canData, canDLC)
  cf.showInfo()

def detect_CAN_data(ts, val, tCAN):
  showCANdetails = not True
  busState = "idle"
  busData = []  # bits on the bus

  if (val[0] != 1):
    logger.warning("capture starts with 0")
    busState = "unknown"

  tsNext = ts[0]
  for i in range(1, len(ts)):
    if (busState == "idle"):
      if ((val[i - 1] == 1) and (val[i] == 0)):
        if (showCANdetails):
          logger.info("{: <20} @ {}".format("CAN SOF", ts[i]))
        busState = "active"
        tsNext = ts[i] + (tCAN / 2.0)

    if (busState == "active"):
      if (ts[i] >= tsNext):
        busData.append(val[i])
        tsNext = ts[i] + tCAN
        #if(showCANdetails):
        #  logger.info("append {} @ {}".format(val[i], ts[i]))
        #  logger.info("{}".format(busData))
        if (busData[-9:] == [1, 1, 1, 1, 1, 1, 1, 1, 1]):
          busState = "idle"
          parse_CAN_transaction(busData)
          busData = []

    if (busState == "unknown"):
      if (ts[i] >= tsNext):
        busData.append(val[i])
        tsNext = ts[i] + tCAN
        if (busData[-9:] == [1, 1, 1, 1, 1, 1, 1, 1, 1]):
          busState = "idle"
          busData = []

  if (busData != []):
    parse_CAN_transaction(busData)

def meas_CAN_freq(ts, val):
  showCANdetails = not True

  logger.info("detecting CAN frequency ...")
  tGlitch = 10e-9  # ignore edges separated by less than tGlitch
  tCAN = 100.0
  tsFE = ts[1]
  tsRE = ts[1]

  for i in range(1, len(ts)):
    tmp = 0.0
    if ((val[i - 1] == 1) and (val[i] == 0)):
      tsFE = ts[i]
      tmp = (tsFE - tsRE)
      if (showCANdetails):
        #logger.info("{} {} {}".format(ts[i], val[i], val[i-1]))
        logger.info("{: <20} @ {}; {}".format("CAN fall-edge", tsFE, tmp))
    if ((val[i - 1] == 0) and (val[i] == 1)):
      tsRE = ts[i]
      tmp = (tsRE - tsFE)
      if (showCANdetails):
        #logger.info("{} {} {}".format(ts[i], val[i], val[i-1]))
        logger.info("{: <20} @ {}; {}".format("CAN rise-edge", tsRE, tmp))
    if ((tmp > tGlitch) and (tmp < tCAN)):
      tCAN = tmp

  CANfreq = (1.0 / tCAN)
  logger.info("CAN frequency: {} Hz".format(CANfreq))
  if (CANfreq <= 1.0e2) or (CANfreq > 1.0e7):
    msg = "possible invalid CAN frequency"
    logger.error(msg)
    raise RuntimeError(msg)
  return tCAN

def mainApp():
  pyauto_base.fs.chkPath_File(cliArgs["file"])

  if (cliArgs["file"].lower().endswith("csv")):
    res = pyauto_base.misc.readCSV_asCols(cliArgs["file"])
    #sampleInterval = res[1][1]
    CAN_ts = [float(t) for t in res[3] if t != ""]
    CAN_val = [float(t) for t in res[4] if t != ""]
  elif (cliArgs["file"].endswith("wfm")):
    res = pyauto_lab.data.file.readWFMFile(cliArgs["file"])
    CAN_ts = res.ts
    CAN_val = res.data
  else:
    msg = "UNRECOGNIZED file: {}".format(cliArgs["file"])
    logger.error(msg)
    raise RuntimeError(msg)
  if (len(CAN_ts) != len(CAN_val)):
    msg = "MISMATCH in number of CAN values: {:d} vs. {:d}".format(
        len(CAN_ts), len(CAN_val))
    logger.error(msg)
    raise RuntimeError(msg)
  CAN_ts = [round(t, 6) for t in CAN_ts]  # round timestamp to us

  if (cliArgs["side"] == "high"):
    Vth = 2.2
  elif (cliArgs["side"] == "low"):
    Vth = 1.4
  else:
    logger.error("incorrect argument \"side\": {}".format(cliArgs["side"]))
    return

  logger.info("converting to logical values (Vth = {}) ...".format(Vth))
  if (cliArgs["side"] == "high"):
    CAN_val = [1 if (t > Vth) else 0 for t in CAN_val]
  elif (cliArgs["side"] == "low"):
    CAN_val = [0 if (t < Vth) else 1 for t in CAN_val]
  else:
    logger.error("incorrect argument \"side\": {}".format(cliArgs["side"]))
    return

  tCAN = meas_CAN_freq(CAN_ts, CAN_val)
  detect_CAN_data(CAN_ts, CAN_val, tCAN)

  pyauto_base.misc.copyLoggerFile("{}.log".format(os.path.basename(cliArgs["file"])[:-4]))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = ""
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("file", help="waveform file(s)")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-s",
                      "--side",
                      choices=("high", "low"),
                      help="indicates which part of the CANbus is captured")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
