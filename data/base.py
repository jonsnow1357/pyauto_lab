#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.bin

class I2CFrame(object):

  def __init__(self):
    self._frame = ""
    self._ts = None
    self.addr = None
    self.nb = 0
    self.rd_only = False
    self.wr_only = False
    self.rw = False
    self.data = []

  def __str__(self):
    return self._frame

  def _clear(self):
    self._frame = ""
    self._ts = None
    self.addr = None
    self.nb = 0
    self.rd_only = False
    self.wr_only = False
    self.rw = False
    self.data = []

  @property
  def timestamp(self):
    return self._ts

  @timestamp.setter
  def timestamp(self, val):
    if (not isinstance(val, int)):
      msg = "INCORRECT timestamp type: {}".format(type(val))
      logger.error(msg)
      raise RuntimeError(msg)

    self._ts = val

  @property
  def frame(self):
    return self._frame

  @frame.setter
  def frame(self, val):
    if (not isinstance(val, str)):
      msg = "INCORRECT frame type: {}".format(type(val))
      logger.error(msg)
      raise RuntimeError(msg)

    if (val.startswith("S") and val.endswith("P")):
      pass
    elif (val.startswith("[") and val.endswith("]")):
      # Bus Pirate syntax
      pass
    elif (val.startswith("x") and val.endswith("x")):
      # some sniffers don't actually log Start and Stop conditions
      pass
    else:
      msg = "INCORRECT frame: {}".format(val)
      logger.error(msg)
      raise RuntimeError(msg)

    self._clear()
    for t in val.split():
      if (t in ["S", "P", "[", "]", "x"]):
        continue

      if t.endswith("_r"):
        if (not pyauto_base.bin.isHexString(t[:-2], 1)):
          msg = "INCORRECT frame: {}".format(val)
          logger.error(msg)
          raise RuntimeError(msg)
        if (self.addr is None):
          self.addr = t[:-2]
        else:
          if (self.addr != t[:-2]):
            msg = "INCORRECT frame: {}".format(val)
            logger.error(msg)
            raise RuntimeError(msg)
        self.rd_only = True
        continue
      if t.endswith("_w"):
        if (not pyauto_base.bin.isHexString(t[:-2], 1)):
          msg = "INCORRECT frame: {}".format(val)
          logger.error(msg)
          raise RuntimeError(msg)
        if (self.addr is None):
          self.addr = t[:-2]
        else:
          if (self.addr != t[:-2]):
            msg = "INCORRECT frame: {}".format(val)
            logger.error(msg)
            raise RuntimeError(msg)
        self.wr_only = True
        continue

      if (pyauto_base.bin.isHexString(t, 1)):
        self.nb += 1
        self.data.append(t)
        continue

      msg = "INCORRECT frame: {}".format(val)
      logger.error(msg)
      raise RuntimeError(msg)

    if (self.addr is None):
      msg = "INCORRECT frame: {}".format(val)
      logger.error(msg)
      raise RuntimeError(msg)

    if (self.rd_only and self.wr_only):
      self.rd_only = False
      self.wr_only = False
      self.rw = True
    self._frame = val
