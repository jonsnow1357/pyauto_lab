#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import struct

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc

WFMDataType = ("WFMDATA_SCALAR_MEAS", "WFMDATA_SCALAR_CONST", "WFMDATA_VECTOR",
               "WFMDATA_PIXMAP", "WFMDATA_INVALID", "WFMDATA_WFMDB")
WFMEDimFormat = ("EXPLICIT_INT16", "EXPLICIT_INT32", "EXPLICIT_UINT32", "EXPLICIT_UINT64",
                 "EXPLICIT_FP32", "EXPLICIT_FP64", "EXPLICIT_UINT8", "EXPLICIT_INT8",
                 "EXPLICIT_INVALID_FORMAT", "EXPLICIT_INVALID_FORMAT")
WFMEDimStorage = ("EXPLICIT_SAMPLE", "EXPLICIT_MIN_MAX", "EXPLICIT_VERT_HIST",
                  "EXPLICIT_HOR_HIST", "EXPLICIT_ROW_ORDER", "EXPLICIT_COLUMN_ORDER",
                  "EXPLICIT_INVALID_STORAGE")
WFMTimeBaseSweep = ("SWEEP_ROLL", "SWEEP_SAMPLE", "SWEEP_ET", "SWEEP_INVALID")
WFMTimeBaseType = ("BASE_TIME", "BASE_SPECTRAL_MAG", "BASE_SPECTRAL_PHASE", "BASE_INVALID")

class WFMFile(object):

  def __init__(self, buff):
    if (len(buff) == 0):
      msg = "EMPTY buffer for WFMFile"
      logger.error(msg)
      raise RuntimeError(msg)
    self.fBuff = buff
    self.fSize = len(buff)
    self.offset_StaticInfo = 0
    self.offset_WfmHeader = 78
    self.offset_eDim1 = 166
    self.offset_eDim2 = 322
    self.offset_iDim1 = 478
    self.offset_iDim2 = 610
    self.offset_tb1 = 742
    self.offset_tb2 = 754
    self.offset_upd = 766
    self.offset_crv = 790
    self.offset_CurveBuff = 0
    self._fmtPack = {}
    self.ts = []
    self.data = []

  def __str__(self):
    return "{}: {:d} byte(s); {:d} data point(s)".format(self.__class__.__name__,
                                                         self.fSize, len(self.data))

  def clear(self):
    self.ts = []
    self.data = []

  def rdByteFromFile(self, idx):
    return struct.unpack(self._fmtPack["byte"], self.fBuff[idx:(idx + 1)])[0]

  def rdShortFromFile(self, idx):
    return struct.unpack(self._fmtPack["short"], self.fBuff[idx:(idx + 2)])[0]

  def rdUShortFromFile(self, idx):
    return struct.unpack(self._fmtPack["ushort"], self.fBuff[idx:(idx + 2)])[0]

  def rdIntFromFile(self, idx):
    return struct.unpack(self._fmtPack["int"], self.fBuff[idx:(idx + 4)])[0]

  def rdLongFromFile(self, idx):
    return struct.unpack(self._fmtPack["long"], self.fBuff[idx:(idx + 4)])[0]

  def rdULongFromFile(self, idx):
    return struct.unpack(self._fmtPack["ulong"], self.fBuff[idx:(idx + 4)])[0]

  def rdDoubleFromFile(self, idx):
    return struct.unpack(self._fmtPack["double"], self.fBuff[idx:(idx + 8)])[0]

  def rdString(self, buff):
    if (isinstance(buff, str)):
      return buff
    else:
      idx = buff.index(0)
      return buff[0:idx].decode("utf-8")

  def __parseStaticInfo(self):
    buff = self.fBuff[self.offset_StaticInfo:self.offset_WfmHeader]
    #logger.info("__parseStaticInfo\n{}".format(buff))

    tmp = struct.unpack("H", buff[0:2])[0]
    if (tmp == 3855):  # 0x0F0F
      logger.info("wfm: Intel byte order")
      self._fmtPack["byte"] = "<b"
      self._fmtPack["ubyte"] = "<B"
      self._fmtPack["short"] = "<h"
      self._fmtPack["ushort"] = "<H"
      self._fmtPack["int"] = "<i"
      self._fmtPack["uint"] = "<I"
      self._fmtPack["long"] = "<l"
      self._fmtPack["ulong"] = "<L"
      self._fmtPack["double"] = "<d"
    elif (tmp == 61680):  # 0xF0F0
      logger.info("wfm: PPC byte order")
      self._fmtPack["byte"] = ">b"
      self._fmtPack["ubyte"] = ">B"
      self._fmtPack["short"] = ">h"
      self._fmtPack["ushort"] = ">H"
      self._fmtPack["int"] = ">i"
      self._fmtPack["uint"] = ">I"
      self._fmtPack["long"] = ">l"
      self._fmtPack["ulong"] = ">L"
      self._fmtPack["double"] = ">d"
      logger.warning("NOT tested yet")
    else:
      msg = "INCORRECT header: {}".format(buff[0:2])
      logger.error(msg)
      raise RuntimeError(msg)

    wRev = buff[2:10].decode("utf-8")
    logger.info("wfm rev: {}".format(wRev))
    #self.revOffset1 = 0
    if (wRev == ":WFM#001"):
      pass
    elif (wRev == ":WFM#002"):
      # this format has an additional 2 bytes at offset 0x9a
      self.offset_rev = 0
      self.offset_eDim1 += 2
      self.offset_eDim2 += 2
      self.offset_iDim1 += 2
      self.offset_iDim2 += 2
      self.offset_tb1 += 2
      self.offset_tb2 += 2
      self.offset_upd += 2
      self.offset_crv += 2
    elif (wRev == ":WFM#003"):
      # this format has an additional 2 bytes at offset 0x9a
      # this format has double(8 bytes) at offsets 0x12e, 0x1ca, 0x24e, 0x2d2
      self.offset_rev = 4
      self.offset_eDim1 += 2
      self.offset_eDim2 += (2 + self.offset_rev)
      self.offset_iDim1 += (2 + (2 * self.offset_rev))
      self.offset_iDim2 += (2 + (3 * self.offset_rev))
      self.offset_tb1 += (2 + (4 * self.offset_rev))
      self.offset_tb2 += (2 + (4 * self.offset_rev))
      self.offset_upd += (2 + (4 * self.offset_rev))
      self.offset_crv += (2 + (4 * self.offset_rev))
    else:
      msg = "UNSUPPORTED format"
      logger.error(msg)
      raise NotImplementedError(msg)

    tmp = self.rdLongFromFile(11)
    if ((tmp + 15) != self.fSize):
      msg = "INCORRECT filesize in header"
      logger.error(msg)
      raise RuntimeError(msg)

    self.bytesPerDataPoint = self.rdByteFromFile(15)
    logger.info("wfm byte_p_pt: {}".format(self.bytesPerDataPoint))
    self.offset_CurveBuff = self.rdULongFromFile(16)
    lbl = self.rdString(self.fBuff[40:72])
    if (lbl != ""):
      logger.info("wfm label: {}".format(lbl))
    nFastFrames = self.rdLongFromFile(72)
    if (nFastFrames > 1):
      msg = "UNHANDLED FastFrames"
      logger.error(msg)
      raise RuntimeError(msg)

    WfmHeaderSize = self.rdLongFromFile(76)
    #print(WfmHeaderSize)
    if ((self.offset_WfmHeader + WfmHeaderSize) != self.offset_CurveBuff):
      msg = "header size MISMATCH: {} + {} != {}".format(self.offset_WfmHeader,
                                                         WfmHeaderSize,
                                                         self.offset_CurveBuff)
      logger.error(msg)
      raise RuntimeError(msg)
    if ((self.offset_crv + 30) != self.offset_CurveBuff):
      msg = "header size MISMATCH (WfmCurveObject): {} != {}".format(
          self.offset_crv, self.offset_CurveBuff)
      logger.error(msg)
      raise RuntimeError(msg)

  def __parseWaveformHeader(self):
    buff = self.fBuff[self.offset_WfmHeader:self.offset_CurveBuff]
    #logger.info("__parseWaveformHeader\n{}".format(buff))

    wType = self.rdIntFromFile(78)
    if (wType == 0):
      logger.info("wfm type: single waveform")
    else:
      msg = "UNHANDLED waveform type"
      logger.error(msg)
      raise RuntimeError(msg)

    wCnt = self.rdULongFromFile(82)
    logger.info("wfm cnt: {}".format(wCnt))
    iDimCnt = self.rdULongFromFile(114)
    eDimCnt = self.rdULongFromFile(118)
    logger.info("wfm dim cnt: {}e, {}i".format(eDimCnt, iDimCnt))
    wDataType = self.rdIntFromFile(122)
    logger.info("wfm data: {}".format(WFMDataType[wDataType]))
    if (wDataType != 2):
      msg = "UNHANDLED waveform data type"
      logger.error(msg)
      raise RuntimeError(msg)

  def __parseExplicitDimension1(self):
    buff = self.fBuff[self.offset_eDim1:self.offset_eDim2]
    #logger.info("__parseExplicitDimension1\n{}".format(buff))

    self.eDim1 = {}
    tmp = self.rdDoubleFromFile(self.offset_eDim1)
    self.eDim1["scale"] = tmp
    tmp = self.rdDoubleFromFile(self.offset_eDim1 + 8)
    self.eDim1["offset"] = tmp
    tmp = self.rdULongFromFile(self.offset_eDim1 + 16)
    self.eDim1["size"] = tmp
    tmp = self.rdString(buff[20:40])
    self.eDim1["units"] = tmp.strip(" \x00")
    tmp = self.rdIntFromFile(self.offset_eDim1 + 72)
    self.eDim1["format"] = WFMEDimFormat[tmp]
    tmp = self.rdIntFromFile(self.offset_eDim1 + 76)
    self.eDim1["storage"] = WFMEDimStorage[tmp]
    self.eDim1["null"] = buff[80:84]
    self.eDim1["over"] = buff[84:88]
    self.eDim1["under"] = buff[88:92]
    self.eDim1["high"] = buff[92:96]
    self.eDim1["low"] = buff[96:100]
    tmp = self.rdDoubleFromFile(self.offset_eDim1 + 140 + self.offset_rev)
    self.eDim1["href"] = tmp
    tmp = self.rdDoubleFromFile(self.offset_eDim1 + 148 + self.offset_rev)
    self.eDim1["trigdelay"] = tmp
    logger.info("ExplicitDim1: {}".format(self.eDim1))
    #if(self.eDim1["storage"] != "EXPLICIT_SAMPLE"):
    #  msg = "UNHANDLED waveform storage"
    #  logger.error(msg)
    #  raise RuntimeError(msg)

  def __parseExplicitDimension2(self):
    buff = self.fBuff[self.offset_eDim2:self.offset_iDim1]
    #logger.info("__parseExplicitDimension2\n{}".format(buff))

    self.eDim2 = {}
    tmp = self.rdDoubleFromFile(self.offset_eDim2)
    self.eDim2["scale"] = tmp
    tmp = self.rdDoubleFromFile(self.offset_eDim2 + 8)
    self.eDim2["offset"] = tmp
    tmp = self.rdULongFromFile(self.offset_eDim2 + 16)
    self.eDim2["size"] = tmp
    tmp = self.rdString(buff[20:40])
    self.eDim2["units"] = tmp.strip(" \x00")
    tmp = self.rdIntFromFile(self.offset_eDim2 + 72)
    print(tmp)
    self.eDim2["format"] = WFMEDimFormat[tmp]
    tmp = self.rdIntFromFile(self.offset_eDim2 + 76)
    self.eDim2["storage"] = WFMEDimStorage[tmp]
    self.eDim2["null"] = buff[80:84]
    self.eDim2["over"] = buff[84:88]
    self.eDim2["under"] = buff[88:92]
    self.eDim2["high"] = buff[92:96]
    self.eDim2["low"] = buff[96:100]
    tmp = self.rdDoubleFromFile(self.offset_eDim2 + 140 + self.offset_rev)
    self.eDim2["href"] = tmp
    tmp = self.rdDoubleFromFile(self.offset_eDim2 + 148 + self.offset_rev)
    self.eDim2["trigdelay"] = tmp
    logger.info("ExplicitDim2: {}".format(self.eDim2))
    #if(self.eDim2["storage"] != "EXPLICIT_SAMPLE"):
    #  msg = "UNHANDLED waveform storage"
    #  logger.error(msg)
    #  raise RuntimeError(msg)

  def __parseImplicitDimension1(self):
    buff = self.fBuff[self.offset_iDim1:self.offset_iDim2]
    #logger.info("__parseImplicitDimension1\n{}".format(buff))

    self.iDim1 = {}
    tmp = self.rdDoubleFromFile(self.offset_iDim1)
    self.iDim1["scale"] = tmp
    tmp = self.rdDoubleFromFile(self.offset_iDim1 + 8)
    self.iDim1["offset"] = tmp
    tmp = self.rdULongFromFile(self.offset_iDim1 + 16)
    self.iDim1["size"] = tmp
    tmp = self.rdString(buff[20:40])
    self.iDim1["units"] = tmp.strip(" \x00")
    logger.info("ImplicitDim1: {}".format(self.iDim1))

  def __parseImplicitDimension2(self):
    buff = self.fBuff[self.offset_iDim2:self.offset_tb1]
    #logger.info("__parseImplicitDimension2\n{}".format(buff))

    self.iDim2 = {}
    tmp = self.rdDoubleFromFile(self.offset_iDim2)
    self.iDim2["scale"] = tmp
    tmp = self.rdDoubleFromFile(self.offset_iDim2 + 8)
    self.iDim2["offset"] = tmp
    tmp = self.rdULongFromFile(self.offset_iDim2 + 16)
    self.iDim2["size"] = tmp
    tmp = self.rdString(buff[20:40])
    self.iDim2["units"] = tmp.strip(" \x00")
    logger.info("ImplicitDim2: {}".format(self.iDim2))

  def __parseTimeBase1(self):
    buff = self.fBuff[self.offset_tb1:self.offset_tb2]
    #logger.info("__parseTimeBase1\n{}".format(buff))

    self.tb1 = {}
    tmp = self.rdULongFromFile(self.offset_tb1)
    self.tb1["spacing"] = tmp
    tmp = self.rdIntFromFile(self.offset_tb1 + 4)
    self.tb1["sweep"] = WFMTimeBaseSweep[tmp]
    tmp = self.rdIntFromFile(self.offset_tb1 + 8)
    self.tb1["type"] = WFMTimeBaseType[tmp]
    logger.info("TimeBase1: {}".format(self.tb1))

  def __parseTimeBase2(self):
    buff = self.fBuff[self.offset_tb2:self.offset_upd]
    #logger.info("__parseTimeBase2\n{}".format(buff))

    self.tb2 = {}
    tmp = self.rdULongFromFile(self.offset_tb2)
    self.tb2["spacing"] = tmp
    tmp = self.rdIntFromFile(self.offset_tb2 + 4)
    self.tb2["sweep"] = WFMTimeBaseSweep[tmp]
    tmp = self.rdIntFromFile(self.offset_tb2 + 8)
    self.tb2["type"] = WFMTimeBaseType[tmp]
    logger.info("TimeBase2: {}".format(self.tb2))

  def __parseWfmUpdateSpec(self):
    buff = self.fBuff[self.offset_upd:self.offset_crv]
    #logger.info("__parseWfmUpdateSpec\n{}".format(buff))

  def __parseWfmCurveObject(self):
    buff = self.fBuff[self.offset_crv:self.offset_CurveBuff]
    #logger.info("__parseWfmCurveObject\n{}".format(buff))

    self.cPreStartOffset = self.rdULongFromFile(self.offset_crv + 10)
    #logger.info(self.cPreStartOffset)
    self.cDataOffset = self.rdULongFromFile(self.offset_crv + 14)
    #logger.info(self.cDataOffset)
    self.cPostStartOffset = self.rdULongFromFile(self.offset_crv + 18)
    #logger.info(self.cPostStartOffset)
    self.cPostStopOffset = self.rdULongFromFile(self.offset_crv + 22)
    #logger.info(self.cPostStopOffset)

  def __parseWfmCurveData(self):
    nPreCharge = (self.cDataOffset - self.cPreStartOffset) / self.bytesPerDataPoint
    nPostCharge = (self.cPostStopOffset - self.cPostStartOffset) / self.bytesPerDataPoint
    self.nData = self.iDim1["size"] - nPreCharge - nPostCharge
    logger.info("wfm data points: {}/{}/{}".format(nPreCharge, self.nData, nPostCharge))

    idx = self.offset_CurveBuff + self.cDataOffset
    #logger.info("__parseWfmCurveData\n{}".format(self.fBuff[idx:(idx + 10)]))
    timeVal = self.iDim1["offset"]
    nRound = 4
    while (len(self.data) < self.nData):
      if (self.eDim1["format"] == "EXPLICIT_INT8"):
        tmp = self.rdByteFromFile(idx)
        val = tmp if (tmp < 127) else (tmp - 256)
        val = round((val * self.eDim1["scale"] + self.eDim1["offset"]), nRound)
      elif (self.eDim1["format"] == "EXPLICIT_INT16"):
        val = round(
            (self.rdShortFromFile(idx) * self.eDim1["scale"] + self.eDim1["offset"]),
            nRound)
      else:
        msg = "UNSUPPORTED format: {}".format(self.eDim1["format"])
        logger.error(msg)
        raise NotImplementedError(msg)
      self.data.append(val)
      self.ts.append(round(timeVal, 12))
      idx += self.bytesPerDataPoint
      timeVal += self.iDim1["scale"]
    #logger.info("__parseWfmCurveData\n{}".format(self.data[0:10]))
    #logger.info("__parseWfmCurveData\n{}".format(self.ts[0:10]))

  def parse(self):
    self.clear()
    self.__parseStaticInfo()
    self.__parseWaveformHeader()
    self.__parseExplicitDimension1()
    self.__parseExplicitDimension2()
    self.__parseImplicitDimension1()
    self.__parseImplicitDimension2()
    self.__parseTimeBase1()
    self.__parseTimeBase2()
    self.__parseWfmUpdateSpec()
    self.__parseWfmCurveObject()

    self.__parseWfmCurveData()
    logger.info(self)

def readWFMFile(fPath):
  if (not fPath.lower().endswith(".wfm")):
    msg = "incorrect file name: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)
  pyauto_base.fs.chkPath_File(fPath)

  fBuff = b""
  #byteCnt = 0
  with open(fPath, "rb") as fIn:
    while True:
      lst_bytes = fIn.read(128)
      if (len(lst_bytes) == 0):
        break
      fBuff += lst_bytes
      #byteCnt += 1

  wfmFile = WFMFile(fBuff)
  wfmFile.parse()
  return wfmFile

HEXTypeData = "00"
HEXTypeEOF = "01"
HEXTypeESAR = "02"
HEXTypeSSAR = "03"
HEXTypeELAR = "04"
HEXTypeSLAR = "05"
HEXDescData = "data"
HEXDescEOF = "EOF"
HEXDescESAR = "ESAR"
HEXDescSSAR = "SSAR"
HEXDescELAR = "ELAR"
HEXDescSLAR = "SLAR"
HEXType2Desc = {
    HEXTypeData: HEXDescData,
    HEXTypeEOF: HEXDescEOF,
    HEXTypeESAR: HEXDescESAR,
    HEXTypeSSAR: HEXDescSSAR,
    HEXTypeELAR: HEXDescELAR,
    HEXTypeSLAR: HEXDescSLAR
}
HEXDesc2Type = {
    HEXDescData: HEXTypeData,
    HEXDescEOF: HEXTypeEOF,
    HEXDescESAR: HEXTypeESAR,
    HEXDescSSAR: HEXTypeSSAR,
    HEXDescELAR: HEXTypeELAR,
    HEXDescSLAR: HEXTypeELAR
}

class HexFile(object):

  def __init__(self, fPath):
    self.cntRec = {
        HEXDescData: 0,
        HEXDescEOF: 0,
        HEXDescESAR: 0,
        HEXDescSSAR: 0,
        HEXDescELAR: 0,
        HEXDescSLAR: 0
    }
    self.cntBytes = 0
    self._content = []
    self.fPath = fPath

  def __str__(self):
    res = "{} '{}'\n".format(self.__class__.__name__, self.fPath)
    res += "size: {} B\n".format(self.cntBytes)
    res += ("records: "
            + ", ".join(["{} {}".format(v, self.cntRec[v]) for v in HEXType2Desc.values()]))
    return res

  def clear(self):
    self.cntRec = {
        HEXDescData: 0,
        HEXDescEOF: 0,
        HEXDescESAR: 0,
        HEXDescSSAR: 0,
        HEXDescELAR: 0,
        HEXDescSLAR: 0
    }
    self.cntBytes = 0
    self._content = []

  def _parse_record(self, ln):
    if (not ln.startswith(":")):
      msg = "ILLEGAL record: {}".format(ln)
      logger.error(msg)
      raise RuntimeError(msg)

    tmp = [ln[1:3], ln[3:7], ln[7:9], ln[9:-2], ln[-2:]]
    dataLen = int(tmp[0], 16)
    addr = tmp[1]
    recType = tmp[2]
    data = [tmp[3][i:(i + 2)] for i in range(0, len(tmp[3]), 2)]
    ckSum = tmp[4]
    if (len(data) != dataLen):
      msg = "data length MISMATCH: {}".format(ln)
      logger.error(msg)
      raise RuntimeError(msg)

    ck = dataLen
    ck += int(addr[0:2], 16)
    ck += int(addr[-2:], 16)
    ck += int(recType, 16)
    for v in data:
      ck += int(v, 16)
    ck += int(ckSum, 16)
    if ((ck % 16) != 0):
      msg = "checksum: {}".format(ln)
      logger.error(msg)
      raise RuntimeError(msg)

    if (recType not in HEXType2Desc.keys()):
      msg = "INCORRECT record type: {}".format(ln)
      logger.error(msg)
      raise RuntimeError(msg)
    if (not pyauto_base.bin.isHexString(("0x" + addr), 4)):
      msg = "addr is NOT a hex number: {}".format(ln)
      logger.error(msg)
      raise RuntimeError(msg)
    if (not isinstance(data, list)):
      msg = "data is NOT a list: ".format(type(ln))
      logger.error(msg)
      raise RuntimeError(msg)
    for tmp in data:
      if (not pyauto_base.bin.isHexString(("0x" + tmp), 1)):
        msg = "data is NOT a hex number: {}".format(ln)
        logger.error(msg)
        raise RuntimeError(msg)

    if (recType == HEXTypeEOF):
      if (len(data) != 0):
        msg = "INCORRECT EOF record: {}".format(ln)
        logger.error(msg)
        raise RuntimeError(msg)
      if (self.cntRec[HEXDescEOF] != 0):
        msg = "MULTIPLE EOF records: {}".format(ln)
        logger.error(msg)
        raise RuntimeError(msg)

    if (recType == HEXTypeData):
      self.cntBytes += len(data)

    if (recType not in (HEXTypeData, HEXTypeEOF)):
      logger.info("{}: {}".format(HEXType2Desc[recType], "".join(data)))

    self.cntRec[HEXType2Desc[recType]] += 1
    self._content.append([recType, addr, data])

  def read(self):
    pyauto_base.fs.chkPath_File(self.fPath)

    self.clear()

    with open(self.fPath, "r") as fIn:
      for ln in [t.strip(" \t\n\r") for t in fIn.readlines()]:
        self._parse_record(ln)
    logger.info(self)

  '''
  def getData(self):
    addr = 0
    for rec in self._content:
      if(rec[0] == "00"):
        addr = int(rec[1], 16)
        break
    dataLen = 0
    data = []
    for rec in self._content:
      if(rec[0] == "00"):
        addr += dataLen
        dataLen = len(rec[2])
        if(addr != int(rec[1], 16)):
          logger.warning("address jump [STOPed reading data]")
          break
        data += rec[2]

    logger.info("data size: {} byte(s)".format(len(data)))
    return data
  '''

TCSTFormatDB = "dB+deg"
TCSTFormatMag = "mag+deg"
TCSTFormatRI = "re+im"
TCSTregexOpt2p = r"^# +([kKMG]?[hH][zZ])? *([SYZGH])? *(db|dB|DB|ma|MA|ri|RI)? *(R .*)?$"
TCSTregexOpt = r"^# +([kKMG]?[hH][zZ])? *([SYZ])? *(db|dB|DB|ma|MA|ri|RI)? *(R .*)?$"

class TouchstoneFile(object):

  def __init__(self, fPath):
    self.nPorts = 0
    self._option = None
    self._params = {}
    self._data = []

    ext = fPath.lower().split(".")[-1]
    if (ext.startswith("s") and ext.endswith("p")):
      self.nPorts = int(ext[1:-1])
    else:
      msg = "UNSUPORTED/INCORRECT file name: {}".format(fPath)
      logger.error(msg)
      raise RuntimeError(msg)

    self.fPath = fPath

  def __str__(self):
    return "{}: {} port(s), {} params, {}, {} ohm".format(self.__class__.__name__,
                                                          self.nPorts,
                                                          self._params["param_name"],
                                                          self._params["format"],
                                                          self._params["load"])

  def clear(self):
    self._option = None
    self._params = {}
    self._data = []

  def _data_idx(self, i, j):
    if (self.nPorts == 2):
      idx = 2 * (self.nPorts * (j - 1) + (i - 1)) + 1
    else:
      idx = 2 * (self.nPorts * (i - 1) + (j - 1)) + 1
    return [idx, (idx + 1)]

  def _parse_option(self):
    tmp = re.split(TCSTregexOpt, self._option)
    tmp = [t if (t is not None) else "" for t in tmp]
    #logger.info(tmp)
    if (len(tmp) != 6):
      msg = "INCORRECT option line: {}".format(self._option)
      logger.error(msg)
      raise RuntimeError(msg)

    if (tmp[1].lower() == ""):
      self._params["freq_mpy"] = 1e3
    elif (tmp[1].lower() == "hz"):
      self._params["freq_mpy"] = 1e-6
    elif (tmp[1].lower() == "khz"):
      self._params["freq_mpy"] = 1e-3
    elif (tmp[1].lower() == "mhz"):
      self._params["freq_mpy"] = 1.0
    elif (tmp[1].lower() == "ghz"):
      self._params["freq_mpy"] = 1e3

    if (tmp[2].lower() == ""):
      self._params["param_name"] = "S"
    else:
      self._params["param_name"] = tmp[2]

    if (tmp[3].lower() == ""):
      self._params["format"] = TCSTFormatDB
    elif (tmp[3].lower() == "db"):
      self._params["format"] = TCSTFormatDB
    elif (tmp[3].lower() == "ma"):
      self._params["format"] = TCSTFormatMag
    elif (tmp[3].lower() == "ri"):
      self._params["format"] = TCSTFormatRI

    if (tmp[4].lower() == ""):
      self._params["load"] = 50.0
    else:
      self._params["load"] = float(tmp[4].split()[-1])

  @property
  def optionLine(self):
    return self._option

  @optionLine.setter
  def optionLine(self, strVal):
    if (self.nPorts == 2):
      if (re.match(TCSTregexOpt2p, strVal) is not None):
        self._option = strVal
      else:
        msg = "INCORRECT option line: {}".format(strVal)
        logger.error(msg)
        raise RuntimeError(msg)
    else:
      if (re.match(TCSTregexOpt, strVal) is not None):
        self._option = strVal
      else:
        msg = "INCORRECT option line: {}".format(strVal)
        logger.error(msg)
        raise RuntimeError(msg)
    self._parse_option()

  @property
  def nFreq(self):
    return len(self._data)

  def _parse_data(self, ln):
    if (ln == ""):
      return

    tmp = ln.split()
    if (len(tmp) != (1 + 2 * self.nPorts * self.nPorts)):
      msg = "UNEXPECTED number of values: {}\n{}".format(len(tmp), tmp)
      logger.error(msg)
      raise RuntimeError(msg)
    tmp = [float(t) for t in tmp]
    tmp[0] *= self._params["freq_mpy"]
    self._data.append(tmp)

  def getFreq(self):
    """
    :return: list of frequencies
    """
    res = []
    for v in self._data:
      res.append(v[0])
    return res

  def setFreq(self, lstFreq):
    self._data = []
    for f in lstFreq:
      if (isinstance(f, float)):
        self._data.append([f] + [0.0] * (2 * self.nPorts * self.nPorts))
      else:
        self._data.append([float(f)] + [0.0] * (2 * self.nPorts * self.nPorts))

  def getParam(self, i, j):
    """
    :param i:
    :param j:
    :return: list of [[re, im], ...]
    """
    if ((i > self.nPorts) or (j > self.nPorts)):
      msg = "CANNOT get ({},{},{}) from {} port file".format(self._params["param_name"], i,
                                                             j, self.nPorts)
      logger.error(msg)
      raise RuntimeError(msg)

    if (self.nPorts == 2):
      idx = 2 * (self.nPorts * (j - 1) + (i - 1)) + 1
    else:
      idx = 2 * (self.nPorts * (i - 1) + (j - 1)) + 1
    #logger.info(idx)
    res = []
    for v in self._data:
      res.append([v[idx], v[idx + 1]])
    return res

  def setParam(self, i, j, lstVal):
    if (len(lstVal) != len(self._data)):
      msg = "Data SIZE MISMATCH on adding ({}, {}, {})".format(self._params["param_name"],
                                                               i, j)
      logger.error(msg)
      raise RuntimeError(msg)

    i_d, j_d = self._data_idx(i, j)
    for c in range(0, len(lstVal)):
      if (len(lstVal[c]) != 2):
        msg = "Data SIZE MISMATCH on adding ({}, {}, {}) {}".format(
            self._params["param_name"], i, j, lstVal[c])
        logger.error(msg)
        raise RuntimeError(msg)
      tmp = lstVal[c][0]
      if (isinstance(tmp, float)):
        self._data[c][i_d] = tmp
      else:
        self._data[c][i_d] = float(tmp)
      tmp = lstVal[c][1]
      if (isinstance(tmp, float)):
        self._data[c][j_d] = tmp
      else:
        self._data[c][j_d] = float(tmp)

  def read(self):
    pyauto_base.fs.chkPath_File(self.fPath)

    self.clear()

    lnData = ""
    with open(self.fPath, "r") as fIn:
      for ln in [t.strip("\t\n\r") for t in fIn.readlines()]:
        if (ln == ""):
          continue
        #logger.info(ln)
        if (ln.startswith("!")):
          continue
        if (ln.startswith("#")):
          self.optionLine = ln
          continue
        if (ln.startswith("[")):
          msg = "Touchstone File ver 2.0 or higher"
          logger.error(msg)
          raise NotImplementedError(msg)

        if (self._option is None):
          msg = "CANNOT process file without option line"
          logger.error(msg)
          raise RuntimeError(msg)
        if (ln.startswith(" ")):
          lnData += ln
        else:
          self._parse_data(lnData)
          lnData = ln
    self._parse_data(lnData)
    logger.info(self)

  def write(self):
    with open(self.fPath, "w") as fOut:
      fOut.write("! {} {}\n".format(__name__, self.__class__.__name__))
      fOut.write("! created on " + pyauto_base.misc.getTimestamp() + "\n")
      fOut.write(self._option + "\n")
      if (self.nPorts == 2):
        fOut.write("! Frequency       S11       S21       S12       S22\n")

      for v in self._data:
        fOut.write(" ".join([str(t) for t in v]) + "\n")
    logger.info("{} written".format(self.fPath))
