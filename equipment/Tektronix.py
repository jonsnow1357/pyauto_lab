#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase
from ._Tektronix_TDS import *

class PS252xG(labBase.PowerSupply):

  def __init__(self, strConnInfo, **kwargs):
    super(PS252xG, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "0,\"No error\""
    self.nOut = 3

  def setOutput(self, voltage, current, outId=None):
    self._chkChannelId(outId)

    self.write("INSTRUMENT:NSELECT {}".format(outId + 1))
    self.write("SOURCE:CURRENT {:f}".format(current))
    self.write("SOURCE:VOLTAGE {:f}".format(voltage))
    self.write("SOURCE:CURRENT:PROTECTION:STATE ON")

  def getOutput(self, outId=None):
    self._chkChannelId(outId)

    self.write("INSTRUMENT:NSELECT {}".format(outId + 1))
    res = self.query("SOURCE:CURRENT?")
    c = float(res[0])
    res = self.query("SOURCE:VOLTAGE?")
    v = float(res[0])
    return {"voltage": v, "current": c}

  def enable(self, outId=None):
    self._chkChannelId(outId)

    self.write("INSTRUMENT:NSELECT {}".format(outId + 1))
    self.write("OUTPUT:STATE ON")

  def disable(self, outId=None):
    self._chkChannelId(outId)

    self.write("INSTRUMENT:NSELECT {}".format(outId + 1))
    self.write("OUTPUT:STATE OFF")

class MSO5x(labBase.Oscilloscope):
  """
  Tektronix MSO5x series
  (https://www.tek.com/oscilloscope/5-series-mso-mixed-signal-oscilloscope)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    if ("eos" not in kwargs.keys()):
      kwargs["eos"] = "\n"
    super(MSO5x, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    #self.SCPI = False
    self.qry["err"] = "EVMSG?"
    self.replyNoErr = ""

  def _getErrors(self, TOmultiplier=2, nReplyLines=1):
    lstErr = []

    while (True):
      res = self.query(self.qry["err"])
      #logger.info(res)
      if (len(res) == 0):
        break
      elif (res[0] == "0,\"No events to report - queue empty\""):
        break
      elif (res[0] == "1,\"No events to report - new events pending *ESR?\""):
        self.ESR()
      else:
        lstErr.append(res)

    return lstErr

  def _getInfo(self):
    super(MSO5x, self)._getInfo()

    self.write("HEADER OFF")
    self._idn["opt"] = self.OPT()[0]
    if (("MSO54" in self._idn["id"]) or ("MSO64" in self._idn["id"])):
      self.nCh = 4
    elif ("MSO56" in self._idn["id"]):
      self.nCh = 6
    elif ("MSO58" in self._idn["id"]):
      self.nCh = 8
    else:
      raise NotImplementedError
    return self._idn

  def getHorizontal(self):
    self.write("HOR:DEL:MOD ON")
    res = self.query("HOR:SCA?")
    scale = float(res[0])
    res = self.query("HOR:DEL:TIM?")
    offset = float(res[0])
    return [scale, offset]

  def setHorizontal(self, scale, offset):
    self.write("HOR:DEL:MOD ON")
    self.write("HOR:SCA {}".format(scale))
    self.write("HOR:DEL:TIM {}".format(offset))

  def getVertical(self, chId):
    self._chkChannelId(chId)

    res = self.query("CH{}:SCALE?".format(chId))
    scale = float(res[0])
    res = self.query("CH{}:OFFSET?".format(chId))
    offset = float(res[0])
    return [scale, offset]

  def showCh(self, chId):
    super(MSO5x, self).showCh(chId)

    self.write("DISPLAY:GLOBAL:CH{}:STATE ON".format(chId))

  def hideCh(self, chId):
    super(MSO5x, self).hideCh(chId)

    self.write("DISPLAY:GLOBAL:CH{}:STATE OFF".format(chId))

  def getLabel(self, chId=None):
    self._chkChannelId(chId)

    res = self.query("CH{}:LABEL:NAME?".format(chId))
    return res[0].strip("\"")

  def setLabel(self, val="", chId=None):
    self._chkChannelId(chId)

    self.write("CH{}:LABEL:NAME \"{}\"".format(chId, val))

  def setTrigger(self, val):
    if (val not in labBase.lstOscTrigger):
      msg = "INCORRECT trigger sweep {}".format(val)
      logger.warning(msg)
      return

    if (val == labBase.OscTrigger_Auto):
      self.write("ACQUIRE:STOPAFTER RUNSTOP")
      self.write("TRIGGER:A:MODE AUTO")
    elif (val == labBase.OscTrigger_Trigd):
      self.write("ACQUIRE:STOPAFTER RUNSTOP")
      self.write("TRIGGER:A:MODE NORMAL")
    elif (val == labBase.OscTrigger_Single):
      self.write("ACQUIRE:STOPAFTER SEQUENCE")
      self.write("ACQUIRE:STATE ON")

  def writeImg(self, path):
    if (not path.endswith(".png")):
      msg = "INCORRECT file name: {}".format(path)
      logger.error(msg)
      raise RuntimeError(msg)

    self.write(":SAVE:IMAGE \"{}\"".format(path))
    self.waitOnCmd()
    logger.info("{} written".format(path))

class MDO3x(labBase.Oscilloscope):
  """
  Tektronix MDO3000 and MDO4000 series
  (https://www.tek.com/en/products/oscilloscopes/mdo3000)
  (https://www.tek.com/en/products/oscilloscopes/mdo4000c)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    if ("eos" not in kwargs.keys()):
      kwargs["eos"] = "\n"
    super(MDO3x, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    #self.SCPI = False
    self.qry["err"] = "EVMSG?"
    self.replyNoErr = ""

  def _getErrors(self, TOmultiplier=2, nReplyLines=1):
    lstErr = []

    while (True):
      res = self.query(self.qry["err"])
      #logger.info(res)
      if (len(res) == 0):
        break
      elif (res[0] == "0,\"No events to report - queue empty\""):
        break
      elif (res[0] == "1,\"No events to report - new events pending *ESR?\""):
        self.ESR()
      else:
        lstErr.append(res)

    return lstErr

  def _getInfo(self):
    super(MDO3x, self)._getInfo()

    self.write("HEADER OFF")
    res = self.query(":CONFIG:ANALO:NUMCHAN?")
    self.nCh = int(res[0])
    return self._idn

  def getHorizontal(self):
    self.write("HOR:DEL:MOD ON")
    res = self.query("HOR:SCA?")
    scale = float(res[0])
    res = self.query("HOR:DEL:TIM?")
    offset = float(res[0])
    return [scale, offset]

  def setHorizontal(self, scale, offset):
    self.write("HOR:DEL:MOD ON")
    self.write("HOR:SCA {}".format(scale))
    self.write("HOR:DEL:TIM {}".format(offset))

  def getVertical(self, chId):
    self._chkChannelId(chId)

    res = self.query("CH{}:SCA?".format(chId))
    scale = float(res[0])
    res = self.query("CH{}:OFFS?".format(chId))
    offset = float(res[0])
    return [scale, offset]

  def showCh(self, chId):
    super(MDO3x, self).showCh(chId)

    self.write("SEL:CH{} ON".format(chId))

  def hideCh(self, chId):
    super(MDO3x, self).hideCh(chId)

    self.write("SEL:CH{} OFF".format(chId))

  def getLabel(self, chId=None):
    self._chkChannelId(chId)

    res = self.query("CH{}:LABEL?".format(chId))
    return res[0].strip("\"")

  def setLabel(self, val="", chId=None):
    self._chkChannelId(chId)

    self.write("CH{}:LABEL \"{}\"".format(chId, val))

  def setTrigger(self, val):
    if (val not in labBase.lstOscTrigger):
      msg = "INCORRECT trigger sweep {}".format(val)
      logger.warning(msg)
      return

    if (val == labBase.OscTrigger_Auto):
      self.write("ACQUIRE:STOPAFTER RUNSTOP")
      self.write("TRIGGER:A:MODE AUTO")
    elif (val == labBase.OscTrigger_Trigd):
      self.write("ACQUIRE:STOPAFTER RUNSTOP")
      self.write("TRIGGER:A:MODE NORMAL")
    elif (val == labBase.OscTrigger_Single):
      self.write("ACQUIRE:STOPAFTER SEQUENCE")
      self.write("ACQUIRE:STATE ON")

  def writeImg(self, path):
    if (not path.endswith(".png")):
      msg = "INCORRECT file name: {}".format(path)
      logger.error(msg)
      raise RuntimeError(msg)

    self.write(":SAVE:IMAGE \"{}\"".format(path))
    self.waitOnCmd()
    logger.info("{} written".format(path))
