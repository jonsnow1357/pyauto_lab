#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv
import copy

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_comm.generic
import pyauto_lab.equipment.base as labBase

class TB5800(labBase.TrafficGen):
  """
  JDSU/Viavi T-Berd 5800 Network Tester
  (http://www.viavisolutions.com/en-us/products/t-berd-5800-handheld-network-tester)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(TB5800, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    self.SCPI = True
    self.qry["id"] = "*IDN?"
    #self.replyNoErr = None

  def connect(self):
    super(TB5800, self).connect()
    self.write("*REM")

    bertSide = []
    for side in ("PWRS", "OPPS", "BOTH"):
      res = self.query(":MOD:FUNC:LIST? {},BASE".format(side))
      if ((len(res) > 0) and (res[0] == "\"BERT\"")):
        logger.info("BERT found on {}".format(side))
        bertSide.append(side)

    if (len(bertSide) != 1):
      msg = "NO or MULTIPLE BERT module(s) found: {}".format(bertSide)
      logger.error((msg))
      raise RuntimeError(msg)

    res = self.query(":MOD:FUNC:SEL? {},BASE,\"BERT\"".format(bertSide[0]))
    if (res[0] != "ON"):
      self.write(":MOD:FUNC:SEL {},BASE,\"BERT\",ON".format(bertSide[0]))
      res = self.query(":MOD:FUNC:SEL? {},BASE,\"BERT\"".format(bertSide[0]))
      if (res[0] != "ON"):
        msg = "BERT module OFF"
        logger.error((msg))
        raise RuntimeError(msg)

    res = self.query(":MOD:FUNC:PORT? {},BASE,\"BERT\"".format(bertSide[0]))
    modPort = int(res[0])
    super(TB5800, self).disconnect()

    logger.info("using module port: {}".format(modPort))
    self._ifParams["port"] = modPort
    super(TB5800, self).connect()
    self.write("*REM")

    res = self.query(":SYSTEM:FUNCTION:READY? {},BASE,\"BERT\"".format(bertSide[0]))
    if (res[0] != "1"):
      msg = "BERT module NOT READY"
      logger.error((msg))
      raise RuntimeError(msg)

    res = self.query(":SYSTEM:FUNCTION:PORT? {},BASE,\"BERT\"".format(bertSide[0]))
    rcPort = int(res[0])
    super(TB5800, self).disconnect()

    logger.info("using remote control port: {}".format(rcPort))
    self._ifParams["port"] = rcPort
    super(TB5800, self).connect()
    self.write("*REM CURRENT CURRENT")

class _MAPModule(object):

  def __init__(self):
    self._id = ""
    self._type = ""
    self.port = 0
    self.conn = None

  @property
  def id(self):
    return self._id

  @id.setter
  def id(self, val):
    if (pyauto_base.misc.isEmptyString(val)):
      msg = "CANNOT assign empty _MAPModule id"
      logger.error(msg)
      raise RuntimeError(msg)
    self._id = val

  @property
  def type(self):
    return self._type

  @type.setter
  def type(self, val):
    if (pyauto_base.misc.isEmptyString(val)):
      msg = "CANNOT assign empty _MAPModule type"
      logger.error(msg)
      raise RuntimeError(msg)
    self._type = val

  def __str__(self):
    return "{} '{}': type '{}', port {}".format(self.__class__.__name__, self.id, self.type,
                                                self.port)

class MAP(labBase.LabEqpt):
  """
  JDSU/Viavi MAP
  (http://www.viavisolutions.com/en-us/products/multiple-application-platform-map-200)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(MAP, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "0,\"No Error\""

    self._modules = {}

  def connect(self):
    self._rsrcObj.eos = "\n"
    self._rsrcObj.eom = "\n"
    self._rsrcObj.info.timeout = 4
    super(MAP, self).connect()

    res = self.query(":SYSTEM:LAYOUT:PORT?")

    if (len(res) == 0):
      logger.warning("NO modules available")
      return
    for val in res[0].split(","):
      tmp = val.strip("\"").split()
      if (tmp[0] == "CMR"):
        continue
      if (tmp[2] == "0"):
        continue
      m_mod = _MAPModule()
      m_mod.id = tmp[0]
      m_mod.type = tmp[1]
      m_mod.port = int(tmp[2])
      self._modules[m_mod.id] = m_mod

    logger.info("found {} module(s)".format(len(self._modules)))
    for m_mod in self._modules.values():
      logger.info(m_mod)
      c_info = copy.deepcopy(self._rsrcObj.info)
      c_info.port = m_mod.port
      #print("DBG", c_info)
      c_if = pyauto_comm.generic.CommInterface()
      c_if.setInfo(c_info)
      m_mod.conn = c_if

  def build(self):
    for m_mod in self._modules.values():
      try:
        m_mod.conn.open()
      except pyauto_comm.generic.CommException:
        pass

  def disconnect(self):
    for m_mod in self._modules.values():
      m_mod.conn.close()
    super(MAP, self).disconnect()

_dict_modTypes = {
    "JITT_10G": "155M .. 11G3",
    "10G_100G": "9.813 .. 111.81G",
    "800G": "800G",
}

_cfg_PRBS_10G = "PRBS.10G"
_cfg_ETH_10G = "ETH.10G"
_cfg_PRBS_100G = "PRBS.100G"
_cfg_ETH_100G = "ETH.100G"
_cfg_PRBS_400G = "PRBS.400G"
_cfg_ETH_400G = "ETH.400G"

# yapf: disable
lstCfg = (_cfg_PRBS_10G, _cfg_ETH_10G,
          _cfg_PRBS_100G, _cfg_ETH_100G,
          _cfg_PRBS_400G, _cfg_ETH_400G)
# yapf: enable

class _ONTModule(labBase.LabEqptModule):

  def __init__(self, commIf, **kwargs):
    super(_ONTModule, self).__init__(commIf, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    # self.qry = {"id": None, "err": None}
    self.qry["err"] = ":SYST:ERR?"
    self.replyNoErr = "0, \"No error\""

    self._id = ""
    self._type = ""
    self._port = 0
    self._stack = ""
    self.user = ""
    self.protected = True

  @property
  def id(self):
    return self._id

  @id.setter
  def id(self, val):
    if (pyauto_base.misc.isEmptyString(val)):
      msg = "CANNOT assign empty _ONTPort id"
      logger.error(msg)
      raise RuntimeError(msg)
    self._id = val

  @property
  def type(self):
    return self._type

  @type.setter
  def type(self, val):
    if (pyauto_base.misc.isEmptyString(val)):
      msg = "CANNOT assign empty _ONTPort type"
      logger.error(msg)
      raise RuntimeError(msg)
    self._type = val

  @property
  def port(self):
    return self._port

  @port.setter
  def port(self, val):
    self._port = val
    self._conn.info.port = self._port

  def __str__(self):
    return "{} '{}': type '{}'".format(self.__class__.__name__, self.id, self.type)

  def _loadApp(self, appName):
    app = self.getApp()
    if (app == appName):
      logger.info("app already loaded - remove ...")
      self.write(":INST:DEL {}".format(appName))
      self.waitOnCmd(timeout=24)
    elif (app == "\"\""):
      lst_apps = self.query(":INST:LOAD?")[0].split(",")
      logger.info("apps available: {}".format(lst_apps))
      if (appName not in lst_apps):
        msg = "UNEXPECTED app: {}".format(appName)
        logger.error(msg)
        raise RuntimeError(msg)

    self.write(":INST:LOAD {}".format(appName))
    self.waitOnCmd(timeout=4)

  def startMeasurement(self):
    logger.info("start measurement")
    self.write(":INIT:IMM:ALL")
    self.waitOnCmd(timeout=4)

  def stopMeasurement(self):
    logger.info("stop measurement")
    self.write(":ABOR")
    self.waitOnCmd(timeout=4)

  def _getStack(self):
    self._stack = self.query(":INST:CONF:EDIT:LAY:STAC?")[0]

  def getApp(self):
    return self.query(":INST:CAT?")[0]

  def getPHY_RXAlarms(self):
    #res = self.query(":PHYS:LINE:CST:ALAR?")
    res = self.query(":PHYS:CST:SUM?")
    res = res[0].split(",")
    if (res[0] == "1"):
      return res[1]
    else:
      return

  def getPHY_laser(self):
    res = self.query(":OUTP:TEL:PHYS:LINE:OPT:STAT?")
    return res[0]

  def getPHYInfo(self):
    dict_res = {}

    res = self.query(":OUTP:TEL:PHYS:LINE:TYPE?")
    dict_res["TXPort"] = res[0]
    res = self.query(":INP:TEL:PHYS:LINE:TYPE?")
    dict_res["RXPort"] = res[0]

    dict_res["laser"] = self.getPHY_laser()

    res = self.query(":SOUR:DATA:TEL:PHYS:LINE:RATE?")
    dict_res["TXRate"] = res[0]
    res = self.query(":SENS:DATA:TEL:PHYS:LINE:RATE?")
    dict_res["RXRate"] = res[0]

    dict_res["RXAlarms"] = self.getPHY_RXAlarms()

    return dict_res

  def getPCSInfo(self):
    dict_res = {}

    res = self.query(":ETIM?")
    res = res[0].split(",")
    if (res[0] == "1"):
      dict_res["measTime"] = float(res[1]) / 1000
    else:
      dict_res["measTime"] = math.nan

    self._getStack()
    if (self._stack == "PHYS"):
      return dict_res

    res = self.query(":PCS:CST:SUM?")
    res = res[0].split(",")
    if (res[0] == "1"):
      dict_res["RXAlarms"] = res[1]
    else:
      dict_res["RXAlarms"] = None

    return dict_res

  def setLaserOn(self):
    self.write(":OUTP:TEL:PHYS:LINE:OPT:STAT ON")

  def setLaserOff(self):
    self.write(":OUTP:TEL:PHYS:LINE:OPT:STAT OFF")

  def showInfo(self):
    dict_res = {
        "id": self.query("*IDN?")[0],
        "modType": self.query(":INST:CONF:MOD:TYPE?")[0],
        "modInfo": self.query(":INST:CONF:MOD:INFO?")[0],
        "app": self.getApp(),
        "appStackTX": self.query(":INST:CONF:LAY:STAC:TX?")[0],
        "appStackRX": self.query(":INST:CONF:LAY:STAC:RX?")[0],
        "devMode": self.query(":INST:CONF:EDIT:DEV:MODE?")[0],
    }
    #print("DBG", self.query(":INST:CONF:PORT:CONF?"))
    #print("DBG", self.query(":STAT:OPER?"))
    for k in sorted(dict_res.keys()):
      logger.info("{}: {}".format(k, dict_res[k]))

  def cfg(self, cfgId):
    raise NotImplementedError

class _ONT_10G_JITT(_ONTModule):

  def getPHYInfo(self):
    dict_res = super(_ONT_10G_JITT, self).getPHYInfo()

    res = self.query(":PHYS:LINE:OPT:POW?")
    res = res[0].split(",")
    if (res[0] == "1"):
      dict_res["RXPwr"] = float(res[1])
    else:
      dict_res["RXPwr"] = math.nan

    res = self.query(":PHYS:LINE:FOFF?")
    res = res[0].split(",")
    if (res[0] == "1"):
      dict_res["RXOffset"] = float(res[1])
    else:
      dict_res["RXOffset"] = math.nan

    return dict_res

  def cfg(self, cfgId):
    if (cfgId not in lstCfg):
      logger.error("UNSUPPORTED mode {}".format(cfgId))
      return

    self._loadApp("\"New-Application\"")
    self.write(":INST:CONF:EDIT:OPEN ON")

    lst_cfg = cfgId.split(".")
    logger.info("configure {} ...".format(lst_cfg))
    if (lst_cfg[0] == "PRBS"):
      self.write(":INST:CONF:EDIT:DEV:MODE TERM")
      self.write(":INST:CONF:EDIT:LAY:STAC PHYS")
    elif (lst_cfg[0] == "ETH"):
      self.write(":INST:CONF:EDIT:DEV:MODE TERM")
      self.write(":INST:CONF:EDIT:LAY:STAC PHYS_PCS_MAC")

    self.write(":INST:CONF:EDIT:APPL ON")
    self.waitOnCmd(timeout=36)

    if (lst_cfg[1] == "10G"):
      self.write(":SOUR:DATA:TEL:PHYS:LINE:RATE BASE10G_LAN")
      self.write(":SENS:DATA:TEL:PHYS:LINE:RATE BASE10G_LAN")

    self.startMeasurement()

  def jittGenTest(self, iWaitTime=60):
    """
    OC192 Jitter Generation Test, this test assumes the tester has been configured for OC192 rate and traffic is up.
    :return: list of two items with max jitter values for both BW, [FullBWJitt, HighBWJitt]
    """
    FBW_Jitt = 0
    HBW_Jitt = 0
    jitterGen = []
    logger.info(f'Measuring Jitter Generation')
    cmd = f':SENS:SWE:TIME {iWaitTime}'
    rtn = self.query(cmd)
    # logger.info(rtn)
    cmd = f':INIT:IMM:ALL;*WAI'
    rtn = self.query(cmd)
    # logger.info(rtn)
    logger.info(f'waiting for {iWaitTime}s')
    pyauto_base.misc.waitWithPrint(iWaitTime)
    cmd = f':SENS:DATA:TEL:PHYS:JITT:FILT HP1_LP'
    rtn = self.query(cmd)
    # logger.info(rtn)
    cmd = f':PHYS:JITT:PEAK:PEAK:MAX?'
    rtn = self.query(cmd)
    # logger.info(rtn)
    FBW_Jitt = float(rtn[0].split(',')[1])
    cmd = f':SENS:DATA:TEL:PHYS:JITT:FILT HP2_LP'
    rtn = self.query(cmd)
    # logger.info(rtn)
    cmd = f':PHYS:JITT:PEAK:PEAK:MAX?'
    rtn = self.query(cmd)
    # logger.info(rtn)
    HBW_Jitt = float(rtn[0].split(',')[1])
    jitterGen = [FBW_Jitt, HBW_Jitt]
    logger.info(jitterGen)
    return jitterGen

  def getErrorCnt(self):
    """
    Returns the error count from the tester.
    This is an accumulative error count that only logs errors during a given test/soak
    return: Float
    """
    cmd = f':SON:PAYL:SEL:ECO:BIT?'
    rtn = self.query(cmd)
    # logger.info(rtn)
    err = float(rtn[0].split(',')[1])
    # logger.info(err)
    return err

  def getLOF(self):
    """
    returns the LOF alarm value
    return: bool
    """
    cmd = f':SON:SEL:CST:ALAR?'
    rtn = self.query(cmd)
    LOF = bool(int(rtn[0].split(',')[-1]))
    return LOF

  def getJitterRMS(self):
    """
    Returns the Jitter RMS value from the
    """
    cmd = f':PHYS:JITT:RMS?'
    rtn = self.query(cmd)
    logger.info(rtn)
    jittRMS = float(rtn[0].split(',')[1])
    logger.info(jittRMS)
    return jittRMS

  def jittGen(self):
    """
    OC192 Jitter Generation Test, this test assumes the tester has been configured for OC192 rate and traffic is up.
    :return: list of two items with max jitter values for both BW, [FullBWJitt, HighBWJitt]
    """

    FBW_Jitt = 0
    HBW_Jitt = 0
    jitterGen = []
    logger.info(f'Measuring Jitter Generation')
    cmd = f':SENS:DATA:TEL:PHYS:JITT:FILT HP1_LP'
    rtn = self.query(cmd)
    # logger.info(rtn)
    cmd = f':PHYS:JITT:PEAK:PEAK:MAX?'
    rtn = self.query(cmd)
    # logger.info(rtn)
    FBW_Jitt = float(rtn[0].split(',')[1])
    cmd = f':SENS:DATA:TEL:PHYS:JITT:FILT HP2_LP'
    rtn = self.query(cmd)
    # logger.info(rtn)
    cmd = f':PHYS:JITT:PEAK:PEAK:MAX?'
    rtn = self.query(cmd)
    # logger.info(rtn)
    HBW_Jitt = float(rtn[0].split(',')[1])
    jitterGen = [FBW_Jitt, HBW_Jitt]
    logger.info(jitterGen)
    return jitterGen

  def soak(self, iSoakTime=60):
    """
    OC192 Jitter Generation Test, this test assumes the tester has been configured for OC192 rate and traffic is up.
    :return: list of two items with max jitter values for both BW, [FullBWJitt, HighBWJitt]
    """

    FBW_Jitt = 0
    HBW_Jitt = 0
    jitterGen = []
    logger.info(f'Measuring Jitter Generation')
    cmd = f':SENS:SWE:TIME {iSoakTime}'
    rtn = self.query(cmd)
    logger.info(rtn)
    cmd = f':INIT:IMM:ALL;*WAI'
    rtn = self.query(cmd)
    logger.info(rtn)
    logger.info(f'Soaking for {iSoakTime}s')

class _ONT_10G_100G(_ONTModule):

  def getPHYInfo(self):
    dict_res = super(_ONT_10G_100G, self).getPHYInfo()

    res = self.query(":PHYS:TX:LINE:OPT:POW?")
    res = res[0].split(",")
    if (res[0] == "1"):
      dict_res["TXPwr"] = float(res[1])
    else:
      dict_res["TXPwr"] = math.nan

    res = self.query(":PHYS:LINE:OPT:POW?")
    res = res[0].split(",")
    if (res[0] == "1"):
      dict_res["RXPwr"] = float(res[1])
    else:
      dict_res["RXPwr"] = math.nan

    res = self.query(":PHYS:LINE:FOFF?")
    res = res[0].split(",")
    if (res[0] == "1"):
      dict_res["RXOffset"] = float(res[1])
    else:
      dict_res["RXOffset"] = math.nan

    return dict_res

  def cfg(self, cfgId):
    if (cfgId not in lstCfg):
      logger.error("UNSUPPORTED mode {}".format(cfgId))
      return

    self._loadApp("\"New-Application\"")
    self.write(":INST:CONF:EDIT:OPEN ON")

    lst_cfg = cfgId.split(".")
    logger.info("configure {} ...".format(lst_cfg))
    if (lst_cfg[0] == "PRBS"):
      self.write(":INST:CONF:EDIT:DEV:MODE TERM")
      self.write(":INST:CONF:EDIT:LAY:STAC PHYS")
    elif (lst_cfg[0] == "ETH"):
      self.write(":INST:CONF:EDIT:DEV:MODE TERM")
      self.write(":INST:CONF:EDIT:LAY:STAC PHYS_PCSL_MAC")

    self.write(":INST:CONF:EDIT:APPL ON")
    self.waitOnCmd(timeout=24)

    if (lst_cfg[1] == "100G"):
      self.write(":SOUR:DATA:TEL:PHYS:LINE:RATE ETH_100G")
      self.write(":SENS:DATA:TEL:PHYS:LINE:RATE ETH_100G")

    self.startMeasurement()

class _ONT_800G(_ONTModule):

  def getPHYInfo(self):
    dict_res = super(_ONT_800G, self).getPHYInfo()

    res = self.query(":PHYS:TX:ALL:PHY:OPT:POW?")
    res = res[0].split(",")
    if (res[0] == "1"):
      dict_res["TXPwr"] = float(res[1])
    else:
      dict_res["TXPwr"] = math.nan

    res = self.query(":PHYS:ALL:PHY:OPT:POW?")
    res = res[0].split(",")
    if (res[0] == "1"):
      dict_res["RXPwr"] = float(res[1])
    else:
      dict_res["RXPwr"] = math.nan

    res = self.query(":PHYS:ALL:PHY:FOFF?")
    res = res[0].split(",")
    if (res[0] == "1"):
      dict_res["RXOffset"] = float(res[1])
    else:
      dict_res["RXOffset"] = math.nan

    return dict_res

  def cfg(self, cfgId):
    if (cfgId not in lstCfg):
      logger.error("UNSUPPORTED mode {}".format(cfgId))
      return

    self._loadApp("\"New-Application\"")
    self.write(":INST:CONF:EDIT:OPEN ON")

    lst_cfg = cfgId.split(".")
    logger.info("configure {} ...".format(lst_cfg))
    if (lst_cfg[0] == "PRBS"):
      self.write(":INST:CONF:EDIT:DEV:MODE TERM")
      self.write(":INST:CONF:EDIT:LAY:STAC PHYS")
    elif (lst_cfg[0] == "ETH"):
      self.write(":INST:CONF:EDIT:DEV:MODE TERM")
      self.write(":INST:CONF:EDIT:LAY:STAC PHYS_PCS400G_MAC")

    self.write(":INST:CONF:EDIT:APPL ON")
    self.waitOnCmd(timeout=24)

    if (lst_cfg[1] == "400G"):
      self.write(":SOUR:DATA:TEL:PHYS:LINE:RATE ETH_400G")
      self.write(":SENS:DATA:TEL:PHYS:LINE:RATE ETH_400G")

    self.startMeasurement()

class ONT(labBase.LabEqpt):
  """
  JDSU/Viavi Optical Network Tester
  (https://www.viavisolutions.com/en-us/support/technical-product-support/optical-network-tester-ont)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(ONT, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.qry["err"] = ":SYST:ERR?"
    self.replyNoErr = "0, \"No error\""

    self._modules = {}

  def connect(self):
    # self._rsrcObj.eos = "\n"
    self._rsrcObj.eom = "\n"
    # self._rsrcObj.info.timeout = 4
    super(ONT, self).connect()

    res = self.query(":PRTM:TYPE?", TOmultiplier=2)
    #print("DBG", res)
    if (len(res) != 0):
      for val in res[0].split(","):
        tmp = val.strip("\"").split("=")
        c_info = copy.deepcopy(self._rsrcObj.info)
        c_if = pyauto_comm.generic.CommInterface()
        c_if.setInfo(c_info)
        if (tmp[1] == _dict_modTypes["JITT_10G"]):
          module = _ONT_10G_JITT(c_if)
        elif (tmp[1] == _dict_modTypes["10G_100G"]):
          module = _ONT_10G_100G(c_if)
        elif (tmp[1] == _dict_modTypes["800G"]):
          module = _ONT_800G(c_if)
        else:
          module = _ONTModule(c_if)
        module.id = tmp[0]
        module.type = tmp[1]
        self._modules[module.id] = module

    res = self.query(":PRTM:LIST?", TOmultiplier=2)
    #print("DBG", res)
    if (len(res) != 0):
      for val in res[0].split(","):
        tmp = val.strip("\"").split(":")
        module = self._modules[tmp[0]]
        #print("DBG", tmp)
        module.port = int(tmp[1])
        module.user = tmp[2]
        module.protected = (tmp[3] == "protected")

    logger.info("found {} port(s)".format(len(self._modules)))
    for module in self._modules.values():
      logger.info(module)
      if (module.type not in _dict_modTypes.values()):
        logger.warning("UNSUPPORTED port {}".format(module.type))
      module.open()

    dict_res = self.getInfo()
    if ("ONT-602" in dict_res["id"]):
      #dict_res["slot1CfgAvail"] = self.query(":BMOD:SLOT1:PGRP:MODE:CAT? PGRP1")[0]
      dict_res["slot2CfgAvail"] = self.query(":BMOD:SLOT2:PGRP:MODE:CAT? PGRP1")[0]
      #dict_res["slot1Cfg"] = self.query(":BMOD:SLOT1:PGRP:MODE? PGRP1")[0]
      dict_res["slot2Cfg"] = self.query(":BMOD:SLOT2:PGRP:MODE? PGRP1")[0]
      if (dict_res["slot2Cfg"] != "SINGLE_PORT_MODE"):
        self.write(":BMOD:SLOT2:PGRP:MODE PGRP1,SINGLE_PORT_MODE")

  def getModule(self, modId):
    try:
      return self._modules[modId]
    except KeyError:
      msg = "INCORRECT module {}".format(modId)
      logger.error(msg)
      return

  def getInfo(self, bUpdate=False):
    dict_res = super(ONT, self).getInfo()
    return dict_res

  def disconnect(self):
    for module in self._modules.values():
      module.close()
    super(ONT, self).disconnect()
