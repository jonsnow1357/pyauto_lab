#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class AP79xx(labBase.PowerSwitch):
  """
  APC NetShelter Switched Rack PDUs
  (https://www.apc.com/shop/us/en/categories/power-distribution/rack-power-distribution/netshelter-switched-rack-pdus/N-17k76am)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(AP79xx, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry["err"] = "NA"
    #self.replyNoErr = None
    #self.nOut = 1

  def _menu_choice(self, choice):
    logger.info("<menu choice> {}".format(choice))
    res = self.query(choice, TOmultiplier=6)
    #print("DBG", res)
    if (res[0] != choice):
      msg = "INCORRECT reply: {}".format(res)
      logger.error(msg)
      raise RuntimeError(msg)
    return res

  def connect(self):
    super(AP79xx, self).connect()

    self._rsrcObj.eom = ":"
    res = self.read(TOmultiplier=2)
    if (not res[0].startswith("User Name")):
      msg = "INCORRECT reply: {}".format(res)
      logger.error(msg)
      raise RuntimeError(msg)
    time.sleep(1)

    user = "apc"
    pswd = "apc"
    res = self.query(user, TOmultiplier=2)
    while (not res[1].startswith("Password")):
      res = self.read(TOmultiplier=2)
    self._rsrcObj.eom = "> "
    res = self.query(pswd, TOmultiplier=2)
    if ((res[0] != "***") and (not res[1].startswith("American Power Conversion"))):
      msg = "INCORRECT reply: {}".format(res)
      logger.error(msg)
      raise RuntimeError(msg)

    self._menu_choice("1")
    self._menu_choice("2")
    res = self._menu_choice("1")
    cnt = 0
    for ln in res:
      if (re.match(r"^ *[0-9]+- .*$", ln) is not None):
        if ("Master Control/Configuration" not in ln):
          cnt += 1
    if (cnt <= 0):
      msg = "CANNOT determine outlet count"
      logger.error(msg)
      raise RuntimeError
    self.nOut = cnt
    logger.info("{} reports {:d} outlet(s)".format(self.__class__.__name__, self.nOut))

  def _getInfo(self):
    res = self.query("prodInfo", TOmultiplier=2)
    for ln in res:
      if (ln.startswith("Model")):
        self._idn["model"] = ln.split(":")[1].strip()
      if (ln.startswith("Name")):
        self._idn["name"] = ln.split(":")[1].strip()
    return self._idn

  def enable(self, outId=None):
    self._chkChannelId(outId)

    logger.info("enable OUT{}".format(outId))
    self._menu_choice(str(outId))
    self._menu_choice("1")
    #self._menu_choice("1")
    raise NotImplementedError  # TODO: not tested

  def disable(self, outId=None):
    self._chkChannelId(outId)

    logger.info("disable OUT{}".format(outId))
    self._menu_choice(str(outId))
    self._menu_choice("1")
    #self._menu_choice("2")
    raise NotImplementedError  # TODO: not tested

class AP79xxB(labBase.PowerSwitch):
  """
  APC NetShelter Switched Rack PDUs
  (https://www.apc.com/shop/us/en/categories/power-distribution/rack-power-distribution/netshelter-switched-rack-pdus/N-17k76am)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "apc>"
    super(AP79xxB, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry["err"] = "NA"
    #self.replyNoErr = None
    #self.nOut = 1

  def connect(self):
    super(AP79xxB, self).connect()

    self.read(TOmultiplier=2)
    res = self.query("olStatus all")
    if (res[-2] != "E000: Success"):
      logger.error(res[-2])
      raise RuntimeError(res[-2])
    cnt = 0
    for ln in res:
      if (re.match(r"^ *[0-9]+:.*$", ln) is not None):
        cnt += 1
    if (cnt <= 0):
      msg = "CANNOT determine outlet count"
      logger.error(msg)
      raise RuntimeError
    self.nOut = cnt
    logger.info("{} reports {:d} outlet(s)".format(self.__class__.__name__, self.nOut))

  def _getInfo(self):
    res = self.query("about", TOmultiplier=2)
    for ln in res:
      if (ln.startswith("Model")):
        if ("model" not in self._idn.keys()):
          self._idn["model"] = ln.split(":")[1].strip()
      if (ln.startswith("Serial")):
        if ("SN" not in self._idn.keys()):
          self._idn["SN"] = ln.split(":")[1].strip()
      if (ln.startswith("Version")):
        if ("ver" not in self._idn.keys()):
          self._idn["ver"] = [ln.split(":")[1].strip()]
        else:
          self._idn["ver"].append(ln.split(":")[1].strip())
    return self._idn

  def enable(self, outId=None):
    self._chkChannelId(outId)

    logger.info("enable OUT{}".format(outId))
    res = self.query("olOn {}".format(outId), TOmultiplier=2)
    if (not res[1].startswith("E000")):
      logger.warning(res)

  def disable(self, outId=None):
    self._chkChannelId(outId)

    logger.info("disable OUT{}".format(outId))
    res = self.query("olOff {}".format(outId), TOmultiplier=2)
    if (not res[1].startswith("E000")):
      logger.warning(res)
