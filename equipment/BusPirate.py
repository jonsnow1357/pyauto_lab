#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

class BusPirate(labBase.IfAdapter):

  def __init__(self, strConnInfo, **kwargs):
    super(BusPirate, self).__init__(strConnInfo, speed=115200, eos="\n", eom=">")

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry = {"id": "i", "err": "NA"}
    #self.replyNoErr = None
    self._idn["I2C"] = True
    self._idn["SPI"] = True

  def _getInfo(self):
    res = self.query(self.qry["id"])
    #print("DBG", res)
    self._idn["model"] = res[1]
    self._idn["FW"] = res[2]
    self._idn["mode"] = res[-1].replace(self._params["eom"], "")
    return self._idn

  def i2cCfg(self, speed: int = 100, bPullUp: bool = False):
    self.query("m")
    self.query("4")
    if (speed == 400):
      self.query("4")
      res = 400
    else:
      self.query("3")
      res = 100
    logger.info("I2C speed: {} kHz".format(res))

    dictInfo = self.getInfo(bUpdate=True)
    if (dictInfo["mode"] != "I2C"):
      msg = "INCORRECT mode: {}".format(dictInfo["mode"])
      logger.error(msg)
      raise RuntimeError(msg)

    res = self.query("W")
    if (res[2] != "Clutch engaged!!!"):
      msg = " ".join(res)
      logger.error(msg)
      raise RuntimeError(msg)
    if (bPullUp):
      self.query("P")
    logger.info("I2C pull-up: {}".format(bPullUp))

  def i2cRd(self, i2cAddr: int, nBytes: int) -> list[int]:
    if ((i2cAddr < 0) or (i2cAddr > 127)):
      msg = "INCORRECT addr: {}".format(i2cAddr)
      logger.error(msg)
      raise RuntimeError(msg)
    if ((nBytes < 1) or (nBytes > self.maxBytes)):
      msg = "INCORRECT data length: {}".format(nBytes)
      logger.error(msg)
      raise RuntimeError(msg)

    i2c_addr_r = (i2cAddr * 2 + 1)
    cmd = "[ 0x{:02x} r:{} ]".format(i2c_addr_r, nBytes)
    #print("DBG", cmd)
    res = self.query(cmd)
    if (len(res) < 3):
      msg = "FAIL I2C: {}".format(res)
      logger.error(msg)
      raise labBase.LabEqptException(msg)

    lstB = []
    #print("DBG", res)
    for ln in res:
      if ((ln.startswith("WRITE:")) and (ln.split()[-1] == "NACK")):
        msg = "FAIL I2C: {}".format(ln)
        #logger.error(msg)
        raise labBase.LabEqptException(msg)
      if (ln.startswith("Warning")):
        msg = "FAIL I2C: {}".format(ln)
        #logger.error(msg)
        raise labBase.LabEqptException(msg)
      if (ln.startswith("READ:")):
        lstB += [int(t, 16) for t in ln.split() if t.startswith("0x")]
    if (len(lstB) != nBytes):
      msg = "FAIL I2C: {}".format(res)
      #logger.error(msg)
      raise labBase.LabEqptException(msg)
    return lstB

  def i2cWr(self, i2cAddr: int, lstBytes: list[int]):
    if ((i2cAddr < 0) or (i2cAddr > 127)):
      msg = "INCORRECT addr: {}".format(i2cAddr)
      logger.error(msg)
      raise RuntimeError(msg)
    if ((len(lstBytes) < 1) or (len(lstBytes) > self.maxBytes)):
      msg = "INCORRECT data length: {}".format(len(lstBytes))
      logger.error(msg)
      raise RuntimeError(msg)

    i2c_addr_w = (i2cAddr * 2)
    cmd = ("[ 0x{:02x} ".format(i2c_addr_w)
           + " ".join(["0x{:02x}".format(t) for t in lstBytes]) + "]")
    #print("DBG", cmd)
    res = self.query(cmd)
    if (len(res) < 3):
      msg = "FAIL I2C: {}".format(res)
      raise labBase.LabEqptException(msg)

    #print("DBG", res)
    for ln in res:
      if ((ln.startswith("WRITE:")) and (ln.split()[-1] == "NACK")):
        msg = "FAIL I2C: {}".format(ln)
        #logger.error(msg)
        raise labBase.LabEqptException(msg)
      if (ln.startswith("Warning")):
        msg = "FAIL I2C: {}".format(res)
        #logger.error(msg)
        raise labBase.LabEqptException(msg)

  def i2cWrRd(self, i2cAddr: int, lstWrBytes: list[int], nBytes: int) -> list[int]:
    if ((i2cAddr < 0) or (i2cAddr > 127)):
      msg = "INCORRECT addr: {}".format(i2cAddr)
      logger.error(msg)
      raise RuntimeError(msg)
    if ((len(lstWrBytes) < 1) or (len(lstWrBytes) > self.maxBytes)):
      msg = "INCORRECT data length: {}".format(len(lstWrBytes))
      logger.error(msg)
      raise RuntimeError(msg)
    if ((nBytes < 1) or (nBytes > self.maxBytes)):
      msg = "INCORRECT data length: {}".format(nBytes)
      logger.error(msg)
      raise RuntimeError(msg)

    i2c_addr_r = (i2cAddr * 2 + 1)
    i2c_addr_w = (i2cAddr * 2)
    cmd = ("[ 0x{:02x} ".format(i2c_addr_w)
           + " ".join(["0x{:02x}".format(t) for t in lstWrBytes])
           + " [ 0x{:02x} r:{} ]".format(i2c_addr_r, nBytes))
    #print("DBG", cmd)
    res = self.query(cmd)
    if (len(res) < 3):
      msg = "FAIL I2C: {}".format(res)
      raise labBase.LabEqptException(msg)

    lstB = []
    #print("DBG", res)
    for ln in res:
      if ((ln.startswith("WRITE:")) and (ln.split()[-1] == "NACK")):
        msg = "FAIL I2C: {}".format(ln)
        #logger.error(msg)
        raise labBase.LabEqptException(msg)
      if (ln.startswith("READ:")):
        lstB += [int(t, 16) for t in ln.split() if t.startswith("0x")]
    if (len(lstB) != nBytes):
      msg = "FAIL I2C: {}".format(res)
      raise labBase.LabEqptException(msg)
    return lstB

  def smbusRdBlock(self, i2cAddr: int, cmd: int, nBytes: int) -> list[int]:
    if ((i2cAddr < 0) or (i2cAddr > 127)):
      msg = "INCORRECT addr: {}".format(i2cAddr)
      logger.error(msg)
      raise RuntimeError(msg)
    if ((cmd < 0) or (cmd > 255)):
      msg = "INCORRECT cmd: {}".format(cmd)
      logger.error(msg)
      raise RuntimeError(msg)
    if ((nBytes < 1) or (nBytes > (self.maxBytes - 1))):
      msg = "INCORRECT nBytes: {}".format(nBytes)
      logger.error(msg)
      raise RuntimeError(msg)

    cmd = "[ 0x{:02x} 0x{:02x} [ 0x{:02x} r:{} ]".format((i2cAddr * 2), cmd,
                                                         (i2cAddr * 2 + 1), (nBytes + 1))
    #print("DBG", cmd)
    res = self.query(cmd)
    if (len(res) < 3):
      msg = "FAIL I2C: {}".format(res)
      raise labBase.LabEqptException(msg)

    lstB = []
    #print("DBG", res)
    for ln in res:
      if ((ln.startswith("WRITE:")) and (ln.split()[-1] == "NACK")):
        msg = "FAIL I2C: {}".format(res)
        raise labBase.LabEqptException(msg)
      if (ln.startswith("READ:")):
        lstB += [int(t, 16) for t in ln.split() if t.startswith("0x")]
    if (len(lstB) != (nBytes + 1)):
      msg = "FAIL I2C: {}".format(res)
      raise labBase.LabEqptException(msg)
    return lstB[1:]

  def spiCfg(self,
             speed: int = 1000,
             bCSPOL: bool = False,
             bCPOL: bool = False,
             bCPHA: bool = False,
             bMSBFirst: bool = True):
    self.query("m")
    self.query("5")
    self.query("4")
    logger.info("SPI speed: 1000 kHz")
    if (bCPOL):
      self.query("2")
    else:
      self.query("1")
    self.query("2")
    if (bCPHA):
      self.query("2")
    else:
      self.query("1")
    if (bCSPOL):
      self.query("1")
    else:
      self.query("2")
    self.query("2")

    dictInfo = self.getInfo(bUpdate=True)
    if (dictInfo["mode"] != "SPI"):
      msg = "INCORRECT mode: {}".format(dictInfo["mode"])
      logger.error(msg)
      raise RuntimeError(msg)

    res = self.query("W")
    if (res[2] != "Clutch engaged!!!"):
      msg = " ".join(res)
      logger.error(msg)
      raise RuntimeError(msg)

  def spiXfer(self, lstBytes: list[int]) -> list[int]:
    if ((len(lstBytes) < 1) or (len(lstBytes) > self.maxBytes)):
      msg = "INCORRECT data length: {}".format(len(lstBytes))
      logger.error(msg)
      raise RuntimeError(msg)

    cmd = "{" + " ".join(["0x{:02x}".format(t) for t in lstBytes]) + "]"
    #print("DBG", cmd)
    res = self.query(cmd)
    if (len(res) < 3):
      msg = "FAIL SPI: {}".format(res)
      logger.error(msg)
      raise RuntimeError(msg)

    lstB = []
    #print("DBG", res)
    for ln in res:
      if (not ln.startswith("WRITE:")):
        continue
      lstB.append(int(ln.split()[-1], 16))
    if (len(lstB) != len(lstBytes)):
      msg = "FAIL SPI: {}".format(res)
      logger.error(msg)
      raise RuntimeError(msg)
    return lstB

  def HiZ(self):
    self.query("m")
    self.query("1")

    dictInfo = self.getInfo(bUpdate=True)
    if (dictInfo["mode"] != "HiZ"):
      msg = "INCORRECT mode: {}".format(dictInfo["mode"])
      logger.error(msg)
      raise RuntimeError(msg)
