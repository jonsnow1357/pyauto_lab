#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import array

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase
try:
  import opt.aardvark_x64.aardvark_py as aa
except ImportError as ex:
  raise ModuleNotFoundError(ex)

class Aardvark(labBase.IfAdapter):

  def __init__(self, strConnInfo, **kwargs):
    super(Aardvark, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry = {"id": None, "err": "NA"}
    #self.replyNoErr = None

  def connect(self):
    (num, ports, unique_ids) = aa.aa_find_devices_ext(16, 16)
    logger.info("found {} device(s)".format(num))
    if (num == 0):
      msg = "CANNOT find Aardvark devices"
      logger.error(msg)
      raise RuntimeError(msg)
    #print("DBG", num, ports, unique_ids)
    ports = list(set(ports))  # remove duplicates
    logger.info("found Aardvark port(s): {}".format(ports))
    self._params["port"] = ports[0]

    #logger.info("connect to Aardvark on port {}".format(self._params["port"]))
    if (self._params["port"] & aa.AA_PORT_NOT_FREE):
      msg = "Aardvark in use"
      logger.error(msg)
      raise RuntimeError(msg)

    self._rsrcObj = aa.aa_open(self._params["port"])
    if (self._rsrcObj <= 0):
      msg = "CANNOT open Aardvark on port {}".format(self._params["port"])
      logger.error(msg)
      raise RuntimeError(msg)

    logger.info("connected to Aardvark on port {}".format(self._params["port"]))

  def disconnect(self):
    logger.info("disconnected from Aardvark on port {}".format(self._params["port"]))
    aa.aa_close(self._rsrcObj)
    self._params["port"] = None

  def _getInfo(self):
    self._idn["SN"] = aa.aa_unique_id(self._rsrcObj)
    res = aa.aa_version(self._rsrcObj)
    self._idn["HW"] = res[1].hardware
    self._idn["SW"] = res[1].software
    self._idn["FW"] = res[1].firmware
    res = aa.aa_features(self._rsrcObj)
    #print("DBG", res)
    self._idn["I2C"] = (res & aa.AA_FEATURE_I2C) != 0
    self._idn["SPI"] = (res & aa.AA_FEATURE_SPI) != 0
    self._idn["GPIO"] = (res & aa.AA_FEATURE_GPIO) != 0

  def i2cCfg(self, speed: int = 100, bPullUp: bool = False):
    aa.aa_configure(self._rsrcObj, aa.AA_CONFIG_SPI_I2C)
    res = aa.aa_i2c_bitrate(self._rsrcObj, int(speed))
    logger.info("I2C speed: {} kHz".format(res))
    res = aa.aa_i2c_pullup(self._rsrcObj, aa.AA_I2C_PULLUP_NONE)
    logger.info("I2C pull-up: {}".format(res))

  def i2cRd(self, i2cAddr: int, nBytes: int) -> list[int]:
    if ((i2cAddr < 0) or (i2cAddr > 127)):
      msg = "INCORRECT addr: {}".format(i2cAddr)
      logger.error(msg)
      raise RuntimeError(msg)
    if ((nBytes < 1) or (nBytes > self.maxBytes)):
      msg = "INCORRECT data length: {}".format(nBytes)
      logger.error(msg)
      raise RuntimeError(msg)

    (n_b, data) = aa.aa_i2c_read(self._rsrcObj, i2cAddr, aa.AA_I2C_NO_FLAGS, nBytes)
    #print("DBG", n_b, data)
    if (n_b == 0):
      msg = "NACK"
      raise labBase.LabEqptException(msg)
    if (n_b != nBytes):
      msg = "FAIL I2C: {}".format(n_b)
      raise labBase.LabEqptException(msg)
    return [int(t) for t in data]

  def i2cWr(self, i2cAddr: int, lstBytes: list[int]):
    if ((i2cAddr < 0) or (i2cAddr > 127)):
      msg = "INCORRECT addr: {}".format(i2cAddr)
      logger.error(msg)
      raise RuntimeError(msg)
    if ((len(lstBytes) < 1) or (len(lstBytes) > self.maxBytes)):
      msg = "INCORRECT data length: {}".format(len(lstBytes))
      logger.error(msg)
      raise RuntimeError(msg)

    tx_data = array.array('B', lstBytes)
    aa.aa_i2c_write(self._rsrcObj, i2cAddr, aa.AA_I2C_NO_FLAGS, tx_data)

  def i2cWrRd(self, i2cAddr: int, lstWrBytes: list[int], nBytes: int) -> list[int]:
    if ((i2cAddr < 0) or (i2cAddr > 127)):
      msg = "INCORRECT addr: {}".format(i2cAddr)
      logger.error(msg)
      raise RuntimeError(msg)
    if ((len(lstWrBytes) < 1) or (len(lstWrBytes) > self.maxBytes)):
      msg = "INCORRECT data length: {}".format(len(lstWrBytes))
      logger.error(msg)
      raise RuntimeError(msg)
    if ((nBytes < 1) or (nBytes > self.maxBytes)):
      msg = "INCORRECT data length: {}".format(nBytes)
      logger.error(msg)
      raise RuntimeError(msg)

    tx_data = array.array('B', lstWrBytes)
    aa.aa_i2c_write(self._rsrcObj, i2cAddr, aa.AA_I2C_NO_STOP, tx_data)
    (n_b, rx_data) = aa.aa_i2c_read(self._rsrcObj, i2cAddr, aa.AA_I2C_NO_FLAGS, nBytes)
    #print("DBG", n_b, data)
    if (n_b == 0):
      msg = "NACK"
      raise labBase.LabEqptException(msg)
    if (n_b != nBytes):
      msg = "FAIL I2C: {}".format(n_b)
      raise labBase.LabEqptException(msg)
    return [int(t) for t in rx_data]

  def spiCfg(self,
             speed: int = 500,
             bCSPOL: bool = False,
             bCPOL: bool = False,
             bCPHA: bool = False,
             bMSBFirst: bool = True):
    aa.aa_configure(self._rsrcObj, aa.AA_CONFIG_SPI_I2C)
    res = aa.aa_spi_bitrate(self._rsrcObj, int(speed))
    logger.info("SPI speed: {} kHz".format(res))
    polarity = aa.AA_SPI_POL_FALLING_RISING if (bCPOL) else aa.AA_SPI_POL_RISING_FALLING
    phase = aa.AA_SPI_PHASE_SETUP_SAMPLE if (bCPHA) else aa.AA_SPI_PHASE_SAMPLE_SETUP
    bitorder = aa.AA_SPI_BITORDER_LSB if (bMSBFirst) else aa.AA_SPI_BITORDER_MSB
    res = aa.aa_spi_configure(self._rsrcObj, polarity, phase, bitorder)
    if (res != 0):
      msg = "CANNOT configure SPI"
      logger.error(msg)
      raise RuntimeError(msg)

    sspolarity = aa.AA_SPI_SS_ACTIVE_HIGH if (bCSPOL) else aa.AA_SPI_SS_ACTIVE_LOW
    aa.aa_spi_master_ss_polarity(self._rsrcObj, sspolarity)
    #aa.aa_spi_slave_disable(self._rsrcObj)

  def spiXfer(self, lstBytes: list[int]) -> list[int]:
    if ((len(lstBytes) < 1) or (len(lstBytes) > self.maxBytes)):
      msg = "INCORRECT data length: {}".format(len(lstBytes))
      logger.error(msg)
      raise RuntimeError(msg)

    tx_data = array.array('B', lstBytes)
    rx_data = array.array('B', len(lstBytes) * [0])
    aa.aa_spi_write(self._rsrcObj, tx_data, rx_data)
    return [int(t) for t in rx_data]
