#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

class _E363xA(labBase.PowerSupply):

  def __init__(self, strConnInfo, **kwargs):
    super(_E363xA, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""

  def enable(self, outId=None):
    self.write("OUTPUT:STATE ON")
    self.waitOnCmd()
    logger.info("output(s) ON")

  def disable(self, outId=None):
    self.write("OUTPUT:STATE OFF")
    self.waitOnCmd()
    logger.info("output(s) OFF")

class E3631A(_E363xA):
  """
  Keysight E3631A Power Supply
  (https://www.keysight.com/en/pd-836433-pn-E3631A/80w-triple-output-power-supply-6v-5a-25v-1a?pm=PL&nid=-536902299.384004&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(E3631A, self).__init__(strConnInfo, **kwargs)

    self.nOut = 3

  def setOutput(self, voltage, current, outId=None):
    self._chkChannelId(outId)
    v = pyauto_base.misc.chkFloat(voltage, valId="voltage")
    c = pyauto_base.misc.chkFloat(current, valId="current", minVal=0.0)

    if (outId == 0):
      self.write("INSTRUMENT:SELECT P6V")
    elif (outId == 1):
      self.write("INSTRUMENT:SELECT P25V")
    elif (outId == 2):
      self.write("INSTRUMENT:SELECT N25V")
    self.write("VOLTAGE:LEVEL:IMMEDIATE:AMPLITUDE {:.3f}".format(v))
    self.write("SOURCE:CURRENT:LEVEL:IMMEDIATE:AMPLITUDE {:.3f}".format(c))

  def getOutput(self, outId=None):
    self._chkChannelId(outId)

    if (outId == 0):
      res = self.query("MEASURE:VOLTAGE? P6V")
      v = float(res[0])
      res = self.query("MEASURE:CURRENT? P6V")
      i = float(res[0])
      return [v, i]
    elif (outId == 1):
      res = self.query("MEASURE:VOLTAGE? P25V")
      v = float(res[0])
      res = self.query("MEASURE:CURRENT? P25V")
      i = float(res[0])
      return [v, i]
    elif (outId == 2):
      res = self.query("MEASURE:VOLTAGE? N25V")
      v = float(res[0])
      res = self.query("MEASURE:CURRENT? N25V")
      i = float(res[0])
      return [v, i]

class E3632A(_E363xA):
  """
  Keysight E3632A Power Supply
  (https://www.keysight.com/ca/en/products/dc-power-supplies/bench-power-supplies/e3630-series-bench-power-supply-80-200w.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(E3632A, self).__init__(strConnInfo, **kwargs)

  def setOutput(self, voltage, current, outId=None):
    v = pyauto_base.misc.chkFloat(voltage, valId="voltage", minVal=0.0)
    c = pyauto_base.misc.chkFloat(current, valId="current", minVal=0.0)

    if (v <= 15.0):
      self.write("VOLTAGE:RANGE P15V")
    else:
      self.write("VOLTAGE:RANGE P30V")
    self.write("APPLY {}, {}".format(v, c))
    self.waitOnCmd()

  def getOutput(self, outId=None):
    res = self.query("MEASURE:VOLTAGE?")
    v = float(res[0])
    res = self.query("MEASURE:CURRENT?")
    i = float(res[0])
    return [v, i]

class E3633A(_E363xA):
  """
  Keysight E3633A Power Supply
  (https://www.keysight.com/ca/en/products/dc-power-supplies/bench-power-supplies/e3630-series-bench-power-supply-80-200w.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(E3633A, self).__init__(strConnInfo, **kwargs)

  def setOutput(self, voltage, current, outId=None):
    v = pyauto_base.misc.chkFloat(voltage, valId="voltage", minVal=0.0)
    c = pyauto_base.misc.chkFloat(current, valId="current", minVal=0.0)

    if (v <= 8.0):
      self.write("VOLTAGE:RANGE P8V")
    else:
      self.write("VOLTAGE:RANGE P20V")
    self.write("APPLY {}, {}".format(v, c))
    self.waitOnCmd()

  def getOutput(self, outId=None):
    res = self.query("MEASURE:VOLTAGE?")
    v = float(res[0])
    res = self.query("MEASURE:CURRENT?")
    i = float(res[0])
    return [v, i]

class E3634A(_E363xA):
  """
  Keysight E3634A Power Supply
  (https://www.keysight.com/ca/en/products/dc-power-supplies/bench-power-supplies/e3630-series-bench-power-supply-80-200w.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(E3634A, self).__init__(strConnInfo, **kwargs)

  def setOutput(self, voltage, current, outId=None):
    v = pyauto_base.misc.chkFloat(voltage, valId="voltage", minVal=0.0)
    c = pyauto_base.misc.chkFloat(current, valId="current", minVal=0.0)

    if (v <= 25.0):
      self.write("VOLTAGE:RANGE P25V")
    else:
      self.write("VOLTAGE:RANGE P50V")
    self.write("APPLY {}, {}".format(v, c))
    self.waitOnCmd()

  def getOutput(self, outId=None):
    res = self.query("MEASURE:VOLTAGE?")
    v = float(res[0])
    res = self.query("MEASURE:CURRENT?")
    i = float(res[0])
    return [v, i]

class _E364xA_dual(labBase.PowerSupply):

  def __init__(self, strConnInfo, **kwargs):
    super(_E364xA_dual, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""
    self.nOut = 2

  def enable(self, outId=None):
    self.write("OUTPUT:STATE ON")
    self.waitOnCmd()
    logger.info("output(s) ON")

  def disable(self, outId=None):
    self.write("OUTPUT:STATE OFF")
    self.waitOnCmd()
    logger.info("output(s) OFF")

  def getOutput(self, outId=None):
    self._chkChannelId(outId)

    if (outId == 0):
      self.write("INSTRUMENT:NSELECT 1")
    elif (outId == 1):
      self.write("INSTRUMENT:NSELECT 2")

    res = self.query("MEASURE:VOLTAGE?")
    v = float(res[0])
    res = self.query("MEASURE:CURRENT?")
    i = float(res[0])
    return [v, i]

class E3646A(_E364xA_dual):
  """
  Keysight E3646A Power Supply
  (https://www.keysight.com/ca/en/products/dc-power-supplies/bench-power-supplies/e3640-series-bench-power-supply-30-100w.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(E3646A, self).__init__(strConnInfo, **kwargs)

  def setOutput(self, voltage, current, outId=None):
    self._chkChannelId(outId)
    v = pyauto_base.misc.chkFloat(voltage, valId="voltage")
    c = pyauto_base.misc.chkFloat(current, valId="current", minVal=0.0)

    if (outId == 0):
      self.write("INSTRUMENT:NSELECT 1")
    elif (outId == 1):
      self.write("INSTRUMENT:NSELECT 2")

    if (v <= 8.0):
      self.write("VOLTAGE:RANGE P8V")
    else:
      self.write("VOLTAGE:RANGE P20V")
    self.write("VOLTAGE:LEVEL:IMMEDIATE:AMPLITUDE {:.3f}".format(v))
    self.write("SOURCE:CURRENT:LEVEL:IMMEDIATE:AMPLITUDE {:.3f}".format(c))

class E3647A(_E364xA_dual):
  """
  Keysight E3647A Power Supply
  (https://www.keysight.com/ca/en/products/dc-power-supplies/bench-power-supplies/e3640-series-bench-power-supply-30-100w.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(E3647A, self).__init__(strConnInfo, **kwargs)

  def setOutput(self, voltage, current, outId=None):
    self._chkChannelId(outId)
    v = pyauto_base.misc.chkFloat(voltage, valId="voltage")
    c = pyauto_base.misc.chkFloat(current, valId="current", minVal=0.0)

    if (outId == 0):
      self.write("INSTRUMENT:NSELECT 1")
    elif (outId == 1):
      self.write("INSTRUMENT:NSELECT 2")

    if (v <= 35.0):
      self.write("VOLTAGE:RANGE P35V")
    else:
      self.write("VOLTAGE:RANGE P60V")
    self.write("VOLTAGE:LEVEL:IMMEDIATE:AMPLITUDE {:.3f}".format(v))
    self.write("SOURCE:CURRENT:LEVEL:IMMEDIATE:AMPLITUDE {:.3f}".format(c))

class E3648A(_E364xA_dual):
  """
  Keysight E3648A Power Supply
  (https://www.keysight.com/ca/en/products/dc-power-supplies/bench-power-supplies/e3640-series-bench-power-supply-30-100w.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(E3648A, self).__init__(strConnInfo, **kwargs)

  def setOutput(self, voltage, current, outId=None):
    self._chkChannelId(outId)
    v = pyauto_base.misc.chkFloat(voltage, valId="voltage")
    c = pyauto_base.misc.chkFloat(current, valId="current", minVal=0.0)

    if (outId == 0):
      self.write("INSTRUMENT:NSELECT 1")
    elif (outId == 1):
      self.write("INSTRUMENT:NSELECT 2")

    if (v <= 8.0):
      self.write("VOLTAGE:RANGE P8V")
    else:
      self.write("VOLTAGE:RANGE P20V")
    self.write("VOLTAGE:LEVEL:IMMEDIATE:AMPLITUDE {:.3f}".format(v))
    self.write("SOURCE:CURRENT:LEVEL:IMMEDIATE:AMPLITUDE {:.3f}".format(c))

class A_34970A(labBase.LabEqpt):
  """
  Keysight 34970A Data Acquisition / Data Logger Switch Unit
  (http://www.keysight.com/en/pd-1000001313%3Aepsg%3Apro-pn-34970A/data-acquisition-data-logger-switch-unit?nid=-33261.536881544.00&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(A_34970A, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""

  def _getInfo(self):
    super(A_34970A, self)._getInfo()

    self._idn["slots"] = {"100": None, "200": None, "300": None}
    for k in sorted(self._idn["slots"].keys()):
      res = self.query("SYSTEM:CTYPE? {}".format(k))[0]
      card = res.split(",")[1]
      if (card != "0"):
        self._idn["slots"][k] = card
        #self._idn["slot_{}".format(k)] = res

    return self._idn

  def setDateTime(self):
    t0 = datetime.datetime.now()
    self.write("SYSTEM:TIME {},{},{}".format(t0.hour, t0.minute, t0.second))
    self.write("SYSTEM:DATE {},{},{}".format(t0.year, t0.month, t0.day))

  def _config_channel_34903A(self, slotId, chId, measType):
    s = int(slotId)
    c = int(chId)
    pyauto_base.misc.chkInt(c, valId="channel", minVal=1, maxVal=20)

    if (measType == "ON"):
      self.write("ROUTE:CLOSE (@{})".format(s + c))
      self.waitOnCmd()
    elif (measType == "OFF"):
      self.write("ROUTE:CLOSE (@{})".format(s + c))
      self.waitOnCmd()
    else:
      logger.warning("UNSUPPORTED ch measurement '{}'".format(measType))
      return

  def _config_channel_34904A(self, slotId, chId, measType):
    s = int(slotId)
    c = int(chId)
    pyauto_base.misc.chkInt(c, valId="channel", minVal=1, maxVal=48)
    if (c in (9, 10, 19, 20, 29, 30, 39, 40)):
      msg = "INCORRECT channel: {}".format(c)
      logger.error(msg)
      raise RuntimeError(msg)

    if (measType == "ON"):
      self.write("ROUTE:CLOSE (@{})".format(s + c))
      self.waitOnCmd()
    elif (measType == "OFF"):
      self.write("ROUTE:CLOSE (@{})".format(s + c))
      self.waitOnCmd()
    else:
      logger.warning("UNSUPPORTED ch measurement '{}'".format(measType))
      return

  def _config_channel_34905A(self, slotId, chId, measType):
    s = int(slotId)
    c = int(chId)
    pyauto_base.misc.chkInt(c, valId="channel", minVal=11, maxVal=24)
    if (c in (15, 16, 17, 18, 19, 20)):
      msg = "INCORRECT channel: {}".format(c)
      logger.error(msg)
      raise RuntimeError(msg)

    if (measType == "ON"):
      self.write("ROUTE:CLOSE (@{})".format(s + c))
      self.waitOnCmd()
    elif (measType == "OFF"):
      self.write("ROUTE:CLOSE (@{})".format(s + c))
      self.waitOnCmd()
    else:
      logger.warning("UNSUPPORTED ch measurement '{}'".format(measType))
      return

  def _config_channel_34908A(self, slotId, chId, measType):
    s = int(slotId)
    c = int(chId)
    pyauto_base.misc.chkInt(c, valId="channel", minVal=1, maxVal=40)

    if (measType == "DC"):
      self.write("CONFIGURE:VOLTAGE:DC (@{})".format(s + c))
      self.waitOnCmd()
    elif (measType == "AC"):
      self.write("CONFIGURE:VOLTAGE:AC (@{})".format(s + c))
      self.waitOnCmd()
    elif (re.match(r"temp_[BEJKNRST]", measType) is not None):
      tmp = measType.split("_")
      self.write("CONFIGURE:TEMPERATURE TCOUPLE,{},0.1,(@{})".format(tmp[1], (s + c)))
    else:
      logger.warning("UNSUPPORTED ch measurement '{}'".format(measType))
      return

  def config_channel(self, slotId, chId, measType="DC"):
    cardId = self._idn["slots"][slotId]
    logger.info("configuring slot {} ({}) ch {} for '{}'".format(
        slotId, cardId, chId, measType))
    if (cardId == "34903A"):
      self._config_channel_34903A(slotId, chId, measType)
    elif (cardId == "34904A"):
      self._config_channel_34904A(slotId, chId, measType)
    elif (cardId == "34905A"):
      self._config_channel_34905A(slotId, chId, measType)
    elif (cardId == "34908A"):
      self._config_channel_34908A(slotId, chId, measType)
    else:
      logger.warning("UNSUPPORTED card '{}'".format(cardId))
      return

  def monitor_channel(self, slotId, chId, measType="DC"):
    self.config_channel(slotId, chId, measType)

    ch = int(slotId) + int(chId)
    self.write("ROUTE:MONITOR (@{})".format(ch))
    self.waitOnCmd()
    self.write("ROUTE:MONITOR:STATE ON".format(ch))
    self.waitOnCmd()

  def _config_slot_34903A(self, slotId, measType):
    s = int(slotId)
    chList = "(@{}:{})".format((s + 1), (s + 20))
    #logger.info(chList)
    self.write("ROUTE:SCAN (@)")
    self.waitOnCmd()

    self.write("ROUTE:OPEN {}".format(chList))
    self.waitOnCmd()

  def _config_slot_34904A(self, slotId, measType):
    s = int(slotId)
    self.write("ROUTE:SCAN (@)")
    self.waitOnCmd()

    for row in (10, 20, 30, 40):
      chList = "(@{}:{})".format((s + row + 1), (s + row + 8))
      #logger.info(chList)
      self.write("ROUTE:OPEN {}".format(chList))
      self.waitOnCmd()

  def _config_slot_34905A(self, slotId, measType):
    s = int(slotId)
    self.write("ROUTE:SCAN (@)")
    self.waitOnCmd()

    for bank in (10, 20):
      chClose = "(@{})".format(s + bank + 1)
      self.write("ROUTE:CLOSE {}".format(chClose))

  def _config_slot_34907A(self, slotId, measType):
    s = int(slotId)

    self.write("ROUTE:SCAN (@)")
    self.waitOnCmd()
    self.write("CONFIGURE:TOTALIZE READ,(@{})".format(s + 3))
    self.waitOnCmd()

  def _config_slot_34908A(self, slotId, measType):
    s = int(slotId)
    chList = "(@{}:{})".format((s + 1), (s + 40))
    #logger.info(chList)
    if (measType is None):
      measType = "DC"

    self.write("ROUTE:SCAN (@)")
    self.waitOnCmd()
    self.write("ROUTE:OPEN {}".format(chList))
    self.waitOnCmd()
    if (measType == "DC"):
      self.write("CONFIGURE:VOLTAGE:DC {}".format(chList))
      self.waitOnCmd()
    else:
      logger.warning("UNSUPPORTED ch measurement '{}'".format(measType))
      return

  def config_slot(self, slotId, measType=None):
    cardId = self._idn["slots"][slotId]
    logger.info("configure slot {} ({}) for '{}'".format(slotId, cardId, measType))
    if (cardId == "34903A"):
      self._config_slot_34903A(slotId, measType)
    elif (cardId == "34904A"):
      self._config_slot_34904A(slotId, measType)
    elif (cardId == "34905A"):
      self._config_slot_34905A(slotId, measType)
    elif (cardId == "34907A"):
      self._config_slot_34907A(slotId, measType)
    elif (cardId == "34908A"):
      self._config_slot_34908A(slotId, measType)
    else:
      logger.warning("UNSUPPORTED card '{}'".format(cardId))
      return

  def _show_config_34903A(self, slotId):
    s = int(slotId)
    chList = "(@{}:{})".format((s + 1), (s + 20))
    #logger.info(chList)
    res = self.query("ROUTE:OPEN? {}".format(chList), TOmultiplier=2)
    #logger.info(res)
    res = [int(t) for t in res[0].split(",")]
    for ch in range(1, 21):
      logger.info("  ch {}: {}".format((s + ch), "OPEN" if
                                       (res[ch - 1] == 1) else "CLOSED"))

  def _show_config_34904A(self, slotId):
    s = int(slotId)
    for row in (10, 20, 30, 40):
      chList = "(@{}:{})".format((s + row + 1), (s + row + 8))
      #logger.info(chList)
      res = self.query("ROUTE:OPEN? {}".format(chList), TOmultiplier=2)
      #logger.info(res)
      res = [int(t) for t in res[0].split(",")]
      for col in range(1, 9):
        logger.info("  ch {}: {}".format((s + row + col), "OPEN" if
                                         (res[col - 1] == 1) else "CLOSED"))

  def _show_config_34905A(self, slotId):
    s = int(slotId)
    for bank in (10, 20):
      chList = "(@{}:{})".format((s + bank + 1), (s + bank + 4))
      #logger.info(chList)
      res = self.query("ROUTE:OPEN? {}".format(chList), TOmultiplier=2)
      #logger.info(res)
      res = [int(t) for t in res[0].split(",")]
      for col in range(1, 5):
        logger.info("  ch {}: {}".format((s + bank + col), "OPEN" if
                                         (res[col - 1] == 1) else "CLOSED"))

  def _show_config_34907A(self, slotId):
    s = int(slotId)
    logger.info("  ch {}: {}".format((s + 1), "Digital IO"))
    logger.info("  ch {}: {}".format((s + 2), "Digital IO"))
    logger.info("  ch {}: {}".format((s + 3), "Totalize In"))
    logger.info("  ch {}: {}".format((s + 4), "DAC"))
    logger.info("  ch {}: {}".format((s + 5), "DAC"))

  def _show_config_34908A(self, slotId):
    s = int(slotId)
    chList = "(@{}:{})".format((s + 1), (s + 40))
    #logger.info(chList)
    res = self.query("CONFIGURE? {}".format(chList), TOmultiplier=2)
    #logger.info(res)
    res = [t.replace("\"", "") for t in res[0].split("\",\"")]
    for ch in range(1, 41):
      logger.info("  ch {}: {}".format((s + ch), res[ch - 1]))

  def show_config(self, slotId):
    cardId = self._idn["slots"][slotId]
    logger.info("configuration on slot {} ({})".format(slotId, cardId))
    if (cardId == "34903A"):
      self._show_config_34903A(slotId)
    elif (cardId == "34904A"):
      self._show_config_34904A(slotId)
    elif (cardId == "34905A"):
      self._show_config_34905A(slotId)
    elif (cardId == "34907A"):
      self._show_config_34907A(slotId)
    elif (cardId == "34908A"):
      self._show_config_34908A(slotId)
    else:
      logger.warning("UNSUPPORTED card '{}'".format(cardId))
      return

class _33x20A(labBase.FunctionGen):
  """
  Keysight 33120A Function / Arbitrary Waveform Generator, 20 MHz
  (https://www.keysight.com/main/techSupport.jspx?cc=US&lc=eng&nid=-536902324.536883183.08&pid=127539&pageMode=OV)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    super(_33x20A, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""

  def setOutput(self, mode, freq, ampl, offset, outId):
    super(_33x20A, self).setOutput(mode, freq, ampl, offset, outId)

    if (mode == labBase.Function_Sine):
      self.write("FUNC:SHAP SIN")
    elif (mode == labBase.Function_Square):
      self.write("FUNC:SHAP SQU")
    elif (mode == labBase.Function_Tri):
      self.write("FUNC:SHAP TRI")
    elif (mode == labBase.Function_Saw):
      self.write("FUNC:SHAP RAMP")
    else:
      msg = "UNSUPPORTED mode: {}".format(mode)
      logger.warning(msg)
      return

    self.write("FREQ {:d}".format(freq))
    self.write("VOLT {:f}".format(ampl))
    self.write("VOLT:OFFS {:f}".format(offset))

class A_33120A(_33x20A):
  """
  Keysight 33120A Function / Arbitrary Waveform Generator, 15 MHz
  (https://www.keysight.com/en/pd-1000001289%3Aepsg%3Apro/function-arbitrary-waveform-generator-15-mhz?nid=-536902324.536881979&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    # if ("adapter_name" not in kwargs.keys()):
    #   kwargs["adapter_name"] = "prologix"
    #   kwargs["adapter_gpibAddr"] = 10
    super(A_33120A, self).__init__(strConnInfo, **kwargs)

class A_33220A(_33x20A):
  """
  Keysight 33220A Function / Arbitrary Waveform Generator, 20 MHz
  (https://www.keysight.com/main/techSupport.jspx?cc=US&lc=eng&nid=-536902324.536883183.08&pid=127539&pageMode=OV)
  """

  def __init__(self, strConnInfo, **kwargs):
    # if ("adapter_name" not in kwargs.keys()):
    #   kwargs["adapter_name"] = "prologix"
    #   kwargs["adapter_gpibAddr"] = 10
    super(A_33220A, self).__init__(strConnInfo, **kwargs)

  def enable(self, outId):
    super(A_33220A, self).enable(outId)

    self.write("OUTPUT:STATE ON")
    self.waitOnCmd()
    logger.info("output ON")

  def disable(self, outId):
    super(A_33220A, self).disable(outId)

    self.write("OUTPUT:STATE OFF")
    self.waitOnCmd()
    logger.info("output OFF")
