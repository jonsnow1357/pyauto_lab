#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

class F4T(labBase.ThermalChamber):
  """
  Watlow F4T
  (https://www.watlow.com/products/controllers/temperature-and-process-controllers/f4t-integrated-controller)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(F4T, self).__init__(strConnInfo=strConnInfo, **kwargs)

    self._rsrcObj.eom = "\n"
    self.IEEE_488_2 = True
    self.SCPI = False
    self.qry["err"] = "NA"
    #self.replyNoErr = None

  @property
  def temperature(self):
    res = self.query("SOUR:CLO1:PVAL?")
    #print("DBG", res)
    return {"temperature": round(float(res[0]), 3)}

  @property
  def status(self):
    #res = self.query("PROG:STAT?")
    #print("DBG", res)
    return {"state": "NA"}

  @property
  def setPoint(self):
    res = self.query("SOUR:CLO1:SPO?")
    return float(res[0])

  @setPoint.setter
  def setPoint(self, val):
    self.query("SOUR:CLO1:SPO {:.3f}".format(float(val)))
    time.sleep(1)  # need a shot delay to properly read set point back

    set_point = self.setPoint
    logger.info("set point to {} C".format(set_point))

  @property
  def tempRate(self):
    res = self.query("SOUR:CLO1:RRATE?")
    return float(res[0])

  @tempRate.setter
  def tempRate(self, val, **kwargs):
    self.write("SOUR:CLO1:RRATE {}".format(val))

  def start(self):
    pass  # self.query("SOUR:CLO1:RACT BOTH")

  def stop(self):
    pass  # self.query("SOUR:CLO1:RACT OFF")

  def chkStatus(self):
    """
    checks equipment status and throws Exception if:
    - any (HW dependent) errors

    :return: [<current_temperature>, <set_point>, <at_set_point>]
    """
    res = self.query("SOUR:CLO1:ERR?")
    #print("DBG", res)
    if (res[0] != "NONE"):
      msg = "run FAILURE"
      logger.error(msg)
      self.stop()
      raise labBase.LabEqptException(msg)

    curr_temp = self.temperature["temperature"]
    set_point = self.setPoint
    _THRESHOLD = 0.3
    at_set_point = True if (math.fabs(curr_temp - set_point) < _THRESHOLD) else False

    logger.info("temperature {} C / set point {} C ({})".format(curr_temp, set_point,
                                                                at_set_point))
    return [curr_temp, set_point, at_set_point]
