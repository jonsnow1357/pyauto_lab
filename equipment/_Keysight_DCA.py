#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

class N1000(labBase.Oscilloscope):
  """
  N1000A DCA-X Wide-Bandwidth Oscilloscope Mainframe
  (https://www.keysight.com/en/pdx-2941538-pn-N1000A/dca-x-wide-bandwidth-oscilloscope-mainframe?nid=-32528.1251910.00&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    super(N1000, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "0,\"No error\""

  def _getInfo(self):
    super(N1000, self)._getInfo()

    self._idn["opt"] = self.OPT()[0]
    for i in range(8):
      res = self.query("CHANNEL{}:STATUS?".format(i + 1))
      if (res[0] == "AVA"):
        self.nCh += 1
    return self._idn

  def getHorizontal(self):
    res = self.query("TIMEBASE:SCALE?")
    scale = float(res[0])
    res = self.query("TIMEBASE:POSITION?")
    offset = float(res[0])
    return [scale, offset]

  def getVertical(self, chId):
    self._chkChannelId(chId)

    res = self.query("CHANNEL{}:YSCALE?".format(chId))
    scale = float(res[0])
    # TODO:
    return [scale, float("nan")]

  def getEmEH(self, Prob=None):
    self._checkMode("EYE")
    if Prob is None:
      self.write("MEASure:EYE:PAM:EHEight:DEFine:EOPening ZHITs")
    else:
      self.write("MEASure:EYE:PAM:EHEight:DEFine:EOPening PROBability")
      self.write("MEASure:EYE:PAM:EHEight:DEFine:EOPening:PROBability {}".format(Prob))

    self.write("MEASure:EYE:PAM:EHEight:EYE EYE0")
    EH0 = self.query("MEASure:EYE:PAM:EHEight?")
    self.write("MEASure:EYE:PAM:EHEight:EYE EYE1")
    EH1 = self.query("MEASure:EYE:PAM:EHEight?")
    self.write("MEASure:EYE:PAM:EHEight:EYE EYE2")
    EH2 = self.query("MEASure:EYE:PAM:EHEight?")
    EH = [EH0, EH1, EH2]
    return EH

  def getEmEW(self, Prob=None):
    self._checkMode("EYE")
    if Prob is None:
      self.write("MEASure:EYE:PAM:EWIDth:DEFine:EOPening ZHITs")
    else:
      self.write("MEASure:EYE:PAM:EWIDth:DEFine:EOPening PROBability")
      self.write("MEASure:EYE:PAM:EWIDth:DEFine:EOPening:PROBability {}".format(Prob))

    self.write("MEASure:EYE:PAM:EWIDth:EYE EYE0")
    EW0 = self.query("MEASure:EYE:PAM:EWIDth?")
    self.write("MEASure:EYE:PAM:EWIDth:EYE EYE1")
    EW1 = self.query("MEASure:EYE:PAM:EWIDth?")
    self.write("MEASure:EYE:PAM:EWIDth:EYE EYE2")
    EW2 = self.query("MEASure:EYE:PAM:EWIDth?")
    EW = [EW0, EW1, EW2]
    return EW

  def getJmEW(self):
    self._checkMode("JITT")

    self.write("MEASure:PEYE:EWIDth:EYE EYE0")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:EWIDth")
    EW0 = self.query("MEASure:EWIDth:EHEight?")

    self.write("MEASure:PEYE:EWIDth:EYE EYE1")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:EWIDth")
    EW1 = self.query("MEASure:PEYE:EWIDth?")

    self.write("MEASure:PEYE:EWIDth:EYE EYE2")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:EWIDth")
    EW2 = self.query("MEASure:EWIDth:EWIDth?")

    EW = [EW0, EW1, EW2]
    return EW

  def getJmEH(self):
    self._checkMode("JITT")

    self.write("MEASure:PEYE:EHEight:EYE EYE0")
    #rtn = self.query("*OPC?", TOmultiplier=10)[0]
    #rdy = int(rtn)
    #while not rdy:
    #  time.sleep(1)
    #  rtn = self.query("*OPC?", TOmultiplier=10)[0]
    #  rdy = int(rtn)
    self.waitOnCmd(timeout=10)
    self.write("MEASure:PEYE:EHEight")
    EH0 = self.query("MEASure:PEYE:EHEight?")

    self.write("MEASure:PEYE:EHEight:EYE EYE1")
    #rtn = self.query("*OPC?", TOmultiplier=10)[0]
    #rdy = int(rtn)
    #while not rdy:
    #  time.sleep(1)
    #  rtn = self.query("*OPC?", TOmultiplier=10)[0]
    #  rdy = int(rtn)
    self.waitOnCmd(timeout=10)
    self.write("MEASure:PEYE:EHEight")
    EH1 = self.query("MEASure:PEYE:EHEight?")

    self.write("MEASure:PEYE:EHEight:EYE EYE2")
    #rtn = self.query("*OPC?", TOmultiplier=10)[0]
    #rdy = int(rtn)
    #while not rdy:
    #  time.sleep(1)
    #  rtn = self.query("*OPC?", TOmultiplier=10)[0]
    #  rdy = int(rtn)
    self.waitOnCmd(timeout=10)
    self.write("MEASure:PEYE:EHEight")
    EH2 = self.query("MEASure:PEYE:EHEight?")

    EH = [EH0, EH1, EH2]
    return EH

  def getJmESkew(self):
    self._checkMode("JITT")

    self.write("MEASure:PEYE:ESKew:EYE EYE0")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:ESKew")
    ES0 = self.query("MEASure:PEYE:ESKew?")

    self.write("MEASure:PEYE:ESKew:EYE EYE1")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:ESKew")
    ES1 = self.query("MEASure:PEYE:ESKew?")

    self.write("MEASure:PEYE:ESKew:EYE EYE2")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:ESKew")
    ES2 = self.query("MEASure:PEYE:ESKew?")

    ES = [ES0, ES1, ES2]
    return ES

  def getJmPJ(self):
    self._checkMode("JITT")
    self.write("MEASure:PEYE:PJ:EYE EYE0")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:PJ")
    J0 = self.query("MEASure:PEYE:PJ?")

    self.write("MEASure:PEYE:PJ:EYE EYE1")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:PJ")
    J1 = self.query("MEASure:PEYE:PJ?")

    self.write("MEASure:PEYE:PJ:EYE EYE2")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:PJ")
    J2 = self.query("MEASure:PEYE:PJ?")

    J = [J0, J1, J2]
    return J

  def getJmPJRMS(self):
    self._checkMode("JITT")
    self.write("MEASure:PEYE:PJRMS:EYE EYE0")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:PJRMS")
    J0 = self.query("MEASure:PEYE:PJRMS?")

    self.write("MEASure:PEYE:PJRMS:EYE EYE1")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:PJRMS")
    J1 = self.query("MEASure:PEYE:PJRMS?")

    self.write("MEASure:PEYE:PJRMS:EYE EYE2")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:PJRMS")
    J2 = self.query("MEASure:PEYE:PJRMS?")

    J = [J0, J1, J2]
    return J

  def getJmDDJ(self):
    self._checkMode("JITT")
    self.write("MEASure:PEYE:DDJ:EYE EYE0")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:DDJ")
    J0 = self.query("MEASure:PEYE:DDJ?")

    self.write("MEASure:PEYE:DDJ:EYE EYE1")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:DDJ")
    J1 = self.query("MEASure:PEYE:DDJ?")

    self.write("MEASure:PEYE:DDJ:EYE EYE2")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:DDJ")
    J2 = self.query("MEASure:PEYE:DDJ?")

    J = [J0, J1, J2]
    return J

  def getJmDJ(self):
    self._checkMode("JITT")
    self.write("MEASure:PEYE:DJ:EYE EYE0")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:DJ")
    J0 = self.query("MEASure:PEYE:DJ?")

    self.write("MEASure:PEYE:DJ:EYE EYE1")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:DJ")
    J1 = self.query("MEASure:PEYE:DJ?")

    self.write("MEASure:PEYE:DJ:EYE EYE2")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:DJ")
    J2 = self.query("MEASure:PEYE:DJ?")

    J = [J0, J1, J2]
    return J

  def getJmRJ(self):
    self._checkMode("JITT")
    self.write("MEASure:PEYE:RJ:EYE EYE0")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:RJ")
    J0 = self.query("MEASure:PEYE:RJ?")

    self.write("MEASure:PEYE:RJ:EYE EYE1")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:RJ")
    J1 = self.query("MEASure:PEYE:RJ?")

    self.write("MEASure:PEYE:RJ:EYE EYE2")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:RJ")
    J2 = self.query("MEASure:PEYE:RJ?")

    J = [J0, J1, J2]
    return J

  def getJmTJ(self):
    self._checkMode("JITT")
    self.write("MEASure:PEYE:TJ:EYE EYE0")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:TJ")
    J0 = self.query("MEASure:PEYE:TJ?")

    self.write("MEASure:PEYE:TJ:EYE EYE1")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:TJ")
    J1 = self.query("MEASure:PEYE:TJ?")

    self.write("MEASure:PEYE:TJ:EYE EYE2")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:TJ")
    J2 = self.query("MEASure:PEYE:TJ?")

    J = [J0, J1, J2]
    return J

  def getJmUJ(self):
    self._checkMode("JITT")
    self.write("MEASure:PEYE:UJ:EYE EYE0")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:UJ")
    J0 = self.query("MEASure:PEYE:UJ?")

    self.write("MEASure:PEYE:UJ:EYE EYE1")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:UJ")
    J1 = self.query("MEASure:PEYE:UJ?")

    self.write("MEASure:PEYE:UJ:EYE EYE2")
    #self.query("*OPC?")
    self.waitOnCmd()
    self.write("MEASure:PEYE:UJ")
    J2 = self.query("MEASure:PEYE:UJ?")

    J = [J0, J1, J2]
    return J

  def setModeEYE(self):
    self.write("SYSTem:MODE EYE")
    self.write("ACQuire:RUN")
    #self.query("*OPC?")
    self.waitOnCmd()

  def setModeJITT(self):
    self.write("SYSTem:MODE JITTer")
    self.write("ACQuire:RUN")
    #rtn = self.query("*OPC?", TOmultiplier=60)[0]
    #logger.info(rtn)
    self.waitOnCmd(timeout=60)

  def getMode(self):
    scopeMode = self.query("SYSTem:MODE?")
    return scopeMode[0]

  def _checkMode(self, iMode):
    scopeMode = self.getMode()
    if scopeMode != iMode:
      logger.warning("Wrong scope mode, expected {}, got {} instead".format(
          iMode, scopeMode))
      logger.info("changing mode to {}".format(iMode))
      if iMode == "EYE":
        self.setModeEYE()
      elif iMode == 'JITT':
        self.setModeJITT()

  def mainVoltMeasTP4(self, iStandard='IEEE'):
    if iStandard == 'IEEE':
      vppSpec = 900E-3
      vRMSSpec = 17.5E-3
      vMaxSpec = 2.85E-3
      vMinSpec = -0.35E-3
    elif iStandard == 'CEI':
      vppSpec = 900E-3
      vRMSSpec = 17.5E-3
      vMaxSpec = 2.85E-3
      vMinSpec = -0.35E-3
    else:
      raise RuntimeError

    spec = [vppSpec, vRMSSpec, vMaxSpec, vMinSpec]

    # Differential Output Voltage Test
    self.write(':SYSTem:MODE:OSCilloscope')
    self.write(':MEASure:OSCilloscope:VPP:SOURce DIFF1')
    self.write(':MEASure: OSCilloscope:VPP')
    vpp = self.query(':MEASure: OSCilloscope:VPP?')

    # AC Common Mode Output Voltage Test
    self.write(':SYSTem:MODE OSCilloscope')
    self.write(':MEASure:OSCilloscope:VRMS:SOURce COMM3')
    self.write(':MEASure:OSCilloscope:VRMS:AREa CYCLe')
    self.write(':MEASure:OSCilloscope:VRMS:TYPe AC')
    self.write(':MEASure:OSCilloscope:VRMS')
    vRMS = self.query(':MEASure:OSCilloscope:VRMS?')

    # DC Common Mode Output Voltage Test
    self.write(':SYSTem:MODE OSCilloscope')
    self.write(':MEASure:OSCilloscope:VMAXimum:SOURce COMM3')
    self.write(':MEASure:OSCilloscope:VMAXimum')
    vMax = self.query(':MEASure:OSCilloscope:VMAXimum?')
    self.write(':SYSTem:MODE OSCilloscope')
    self.write(':MEASure:OSCilloscope:VMINimum:SOURce COMM3')
    self.write(':MEASure:OSCilloscope:VMINimum')
    vMin = self.query(':MEASure:OSCilloscope:VMINimum?')

    meas = [vpp, vRMS, vMax, vMin]

    return meas, spec

  def transitionTimeTP4(self, iStandard='IEEE'):
    if iStandard == 'IEEE':
      rtSpec = 9.5E-12
      ftSpec = 9.5E-12
    elif iStandard == 'CEI':
      rtSpec = 9.5E-12
      ftSpec = 9.5E-12

    spec = [rtSpec, ftSpec]

    # Minimum Output Rise Time
    #cmd = ':ACQuire: CDISplay'
    #cmd = ':ACQuire: RUN'
    #cmd = ':DIFF1A: DISPlay ON'
    #cmd = '*OPC?'
    #cmd = ':SYSTem: AUToscale'
    #cmd = '*OPC?'
    #cmd = ':TIMebase: UIRange 8.191000E+3'
    #cmd = ':TIMebase: FIND:SEQuence "333000"'  # Fall time sequence
    #cmd = ':TIMebase: FIND:NEXT'
    #cmd = ':MEASure: THReshold:METHod P205080'
    #cmd = ':MEASure: OSCilloscope:FALLtime'
    #cmd = ':TIMebase: FIND:SEQuence "000333"'  # Rise time sequence
    #cmd = ':TIMebase: FIND:NEXT'
    #cmd = ':MEASure: OSCilloscope:RISetime'

    rt = self.query(':MEASure:RISEtime DIFF1 000 333?')

    # Minimum Output Fall Time
    ft = self.query(':MEASure:FALLtime DIFF1 333 000?')

    meas = [rt, ft]

    return meas, spec

  def calibrate(self):
    cmd = ':CALibrate:SLOT1:STARt'
    self.write(cmd)
    cmd = ':CALibrate:SDONe?'
    rtn = self.query(cmd)
    logger.info(rtn[0])
    cmd = ':CALibrate:CONTinue'
    self.write(cmd)
    cmd = ':CALibrate:SDONe?'
    rtn = self.query(cmd, TOmultiplier=300)
    logger.info(rtn[0])
    cmd = ':CALibrate:CONTinue'
    self.write(cmd)
    #cmd = '*OPC?'
    #rtn = self.query(cmd, TOmultiplier=300)
    self.waitOnCmd(timeout=300)

  def getFallTime(self, iPattern):
    logger.info('Testing Fall Time with pattern {}'.format(iPattern))
    cmd = ':TRIGger:PLOCk ON'
    self.write(cmd)
    #cmd = '*OPC?'
    #rtn = self.query(cmd, TOmultiplier=300)
    self.waitOnCmd(timeout=300)
    cmd = ':ACQuire:CDISplay'
    self.write(cmd)
    cmd = ':ACQuire:EPATtern ON'
    self.write(cmd)
    cmd = ':SYSTem:MODE OSCilloscope'
    self.write(cmd)
    cmd = ':SYSTem:AUToscale'
    self.write(cmd)
    #cmd = '*OPC?'
    #rtn = self.query(cmd, TOmultiplier=300)
    self.waitOnCmd(timeout=300)
    cmd = ':TIMebase:FIND:SEQuence "{}"'.format(iPattern)
    self.write(cmd)
    cmd = ':TIMebase:FIND:NEXT'
    self.write(cmd)
    cmd = ':MEASure:OSCilloscope:FALLtime'
    self.write(cmd)
    cmd = ':MEASure:THReshold:METHod P205080'
    self.write(cmd)
    cmd = ':MEASure:OSCilloscope:FALLtime?'
    rtn = self.query(cmd, TOmultiplier=300)
    data = float(rtn[0])
    cmd = ':MEASure:OSCilloscope:LIST:REMove 1'
    self.write(cmd)
    return data

  def getRiseTime(self, iPattern):
    logger.info('Testing Rise Time with pattern {}'.format(iPattern))
    cmd = ':TRIGger:PLOCk ON'
    self.write(cmd)
    #cmd = '*OPC?'
    #rtn = self.query(cmd, TOmultiplier=300)
    self.waitOnCmd(timeout=300)
    cmd = ':ACQuire:CDISplay'
    self.write(cmd)
    cmd = ':ACQuire:EPATtern ON'
    self.write(cmd)
    cmd = ':SYSTem:MODE OSCilloscope'
    self.write(cmd)
    cmd = ':SYSTem:AUToscale'
    self.write(cmd)
    #cmd = '*OPC?'
    #rtn = self.query(cmd, TOmultiplier=300)
    self.waitOnCmd(timeout=300)
    cmd = ':TIMebase:FIND:SEQuence "{}"'.format(iPattern)
    self.write(cmd)
    cmd = ':TIMebase:FIND:NEXT'
    self.write(cmd)
    cmd = ':MEASure:OSCilloscope:RISEtime'
    self.write(cmd)
    cmd = ':MEASure:THReshold:METHod P205080'
    self.write(cmd)
    cmd = ':MEASure:OSCilloscope:RISEtime?'
    rtn = self.query(cmd, TOmultiplier=300)
    data = float(rtn[0])
    cmd = ':MEASure:OSCilloscope:LIST:REMove 1'
    self.write(cmd)
    return data

  def getJitter_10G(self):
    cmd = ':SYSTem:MODE EYE'
    self.write(cmd)
    # cmd = ':MEASure:EYE:LIST:REMove 1'
    # cmd = ':MTESt1:DISPlay OFF'
    # cmd = ':TRIGger:PLOCk OFF'
    # cmd = '*OPC?'
    cmd = ':TRIGger:PLOCk ON'
    self.write(cmd)
    #cmd = '*OPC?'
    #rtn = self.query(cmd, TOmultiplier=300)
    self.waitOnCmd(timeout=300)
    cmd = ':MEASure:EYE:TJ'
    self.write(cmd)
    cmd = ':MEASure:EYE:DJ'
    self.write(cmd)
    cmd = ':MEASure:EYE:RJ'
    self.write(cmd)
    cmd = ':LTESt:ACQuire:STATe ON'
    self.write(cmd)
    cmd = ':LTESt:ACQuire:CTYPe:PATTerns 5'
    self.write(cmd)
    cmd = ':ACQuire:CDISplay'
    self.write(cmd)
    cmd = ':SYSTem:AUToscale'
    self.write(cmd)
    #cmd = '*OPC?'
    #rtn = self.query(cmd, TOmultiplier=300)
    self.waitOnCmd(timeout=300)
    cmd = ':ACQuire:RUN'
    self.write(cmd)
    #cmd = '*OPC?'
    #rtn = self.query(cmd, TOmultiplier=300)
    self.waitOnCmd(timeout=300)
    cmd = ':MEASure:EYE:TJ?'
    rtn = self.query(cmd)
    Tj = float(rtn[0])
    cmd = ':MEASure:EYE:DJ?'
    rtn = self.query(cmd)
    Dj = float(rtn[0])
    cmd = ':MEASure:EYE:RJ?'
    rtn = self.query(cmd)
    Rj = float(rtn[0])
    data = {'Tj': Tj, 'Dj': Dj, 'Rj': Rj}
    cmd = ':LTESt:ACQuire:STATe OFF'
    self.write(cmd)
    return data

  def saveEyeMaskMargin_10G(self, iMask, iName):
    cmd = ':SYSTem:MODE EYE'
    self.write(cmd)
    cmd = ':MTESt1:LOAD:FNAMe "{}"'.format(iMask)
    self.write(cmd)
    cmd = ':MTESt1:LOAD'
    logger.info("MTEST load complete")
    self.write(cmd)
    cmd = ':LTESt:ACQuire:STATe ON'
    self.write(cmd)
    cmd = ':LTESt:ACQuire:CTYPe:PATTerns 5'
    self.write(cmd)
    cmd = ':MTESt1:MARGin:METHod AUTO'
    self.write(cmd)
    cmd = ':MTESt1:MARGin:AUTo:METHod HITS'
    self.write(cmd)
    cmd = ':MTESt1:MARGin:STATe ON'
    self.write(cmd)
    cmd = ':ACQuire:CDISplay'
    self.write(cmd)
    cmd = ':SYSTem:AUToscale'
    self.write(cmd)
    #cmd = '*OPC?'
    #rtn = self.query(cmd, TOmultiplier=300)
    self.waitOnCmd(timeout=300)
    cmd = ':ACQuire:RUN'
    self.write(cmd)
    #cmd = '*OPC?'
    #rtn = self.query(cmd, TOmultiplier=300)
    self.waitOnCmd(timeout=300)
    logger.info("EYE test setup done")
    pyauto_base.misc.waitWithPrint(20)
    # cmd = ':DISK:SIMage:WINDow DISPlay'
    # self.write(cmd)
    cmd = ':DISK:SIMage:FNAMe "%UDA_APPS_DIR%\\IEEE8023Test\\dataout\\{}.png"'.format(iName)
    self.write(cmd)
    cmd = ':DISK:SIMage:SAVE'
    self.write(cmd)
    #cmd = '*OPC?'
    #rtn = self.query(cmd, TOmultiplier=300)
    self.waitOnCmd(timeout=300)
    cmd = ':DISK:SIMage:FNAMe:AUPDate'
    self.write(cmd)
    #cmd = '*OPC?'
    #rtn = self.query(cmd, TOmultiplier=300)
    self.waitOnCmd(timeout=300)
    logger.info('saving image complete')
