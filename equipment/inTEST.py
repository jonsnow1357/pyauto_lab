#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv
import json

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

_STATUS_ERROR = 0x01
_STATUS_WARN = 0x02
_STATUS_BUFF_FULL = 0x04
_STATUS_RUN = 0x08
_STATUS_AT_SET_POINT = 0x10
_STATUS_SRQ_SET = 0x20

class Thermospot(labBase.ThermalChamber):
  """
  ThermoSpot Direct Contact Probe System
  (https://www.intestthermal.com/temptronic/thermospot)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    if ("eos" not in kwargs.keys()):
      kwargs["eos"] = "\r\n"
    super(Thermospot, self).__init__(strConnInfo=strConnInfo, **kwargs)

    #self._rsrcObj.timeout = 4.0
    self.IEEE_488_2 = True
    #self.SCPI = False
    self.qry["err"] = "QCE"
    self.replyNoErr = "QCE 0 0 0"

    self._set_point = 0.0
    self._temp_rate = 0.0

  def connect(self):
    super(Thermospot, self).connect()
    self.write("SI")
    self._set_point = self.setPoint

  @property
  def temperature(self):
    dict_res = {}
    res = self.query("PT1", TOmultiplier=2)
    dict_res["HEAD"] = float(res[0].split()[1])
    res = self.query("PT2", TOmultiplier=2)
    dict_res["DUTK"] = float(res[0].split()[1])
    return dict_res

  @property
  def status(self):
    """
    checks equipment status and throws Exception if:
    - any (HW dependent) errors

    :return: [<current_temperature>, <set_point>, <at_set_point>]
    """
    dict_res = {}
    res = self.query("RSA", TOmultiplier=2)
    val = int(res[0].split()[1], 16)
    dict_res["val"] = "0x{:x}".format(val)
    if ((val & (_STATUS_ERROR | _STATUS_WARN)) != 0):
      dict_res["error"] = 1
    else:
      dict_res["error"] = 0
    if ((val & _STATUS_RUN) != 0):
      dict_res["run"] = 1
    else:
      dict_res["run"] = 0
    if ((val & _STATUS_AT_SET_POINT) != 0):
      dict_res["at_set_point"] = 1
    else:
      dict_res["at_set_point"] = 0
    return dict_res

  @property
  def setPoint(self):
    res = self.query("QS", TOmultiplier=2)
    return float(res[0].split()[1])

  @setPoint.setter
  def setPoint(self, val, **kwargs):
    self._set_point = round(val, 1)
    sts = self.status
    if (sts["run"] == 1):
      self.start()
    else:
      logger.warning("set point needs start() to take effect")

  @property
  def tempRate(self):
    res = self.query("QFA37", TOmultiplier=2)
    return float(res[0].split()[1])

  @tempRate.setter
  def tempRate(self, val, **kwargs):
    self._temp_rate = round(val, 1)
    self.write("SR {}".format(self._temp_rate))
    while (True):
      if (self.tempRate == self._temp_rate):
        break

  def start(self):
    set_pt = self.setPoint
    sts = self.status
    if ((sts["run"] == 1) and (self._set_point == set_pt)):
      return

    cmd = "GT {}".format(self._set_point)
    self.write(cmd)
    while (True):
      sts = self.status
      if (sts["run"] == 1):
        break

  def stop(self):
    self.write("QU")

  def chkStatus(self):
    curr_temp = self.temperature["DUTK"]
    set_pt = self.setPoint
    sts = self.status
    at_set_pt = sts["at_set_point"]

    if (sts["error"] != 0):
      res = self.query(self.qry["err"], TOmultiplier=2)
      msg = "ERROR {}".format(res)
      logger.error(msg)
      raise RuntimeError(msg)

    logger.info("temperature {} C / set point {} C ({})".format(curr_temp, set_pt,
                                                                at_set_pt))
    return [curr_temp, set_pt, at_set_pt]
