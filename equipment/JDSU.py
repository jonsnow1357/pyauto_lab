#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class SB(labBase.LabEqpt):
  """
  JDSU SB Series Optical Switch
  """

  def __init__(self, strConnInfo, **kwargs):
    #if ("eom" not in kwargs.keys()):
    #  kwargs["eom"] = "\n"
    super(SB, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    self.qry["err"] = "LERR?"
    self.replyNoErr = "  0"
