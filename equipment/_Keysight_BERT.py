#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import warnings

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class _JITT(labBase.LabEqptModule):
  """
  Keysight M8000 Series BERT Jitter class
  """

  def __init__(self, commIf, iModule=1, iCh=0, iType=None, **kwargs):
    super(_JITT, self).__init__(commIf, **kwargs)
    self.type = iType
    self._typeId = self._getTypeId(iType)
    self._module = iModule  # if default set to module 1, if more the one module installed, we can change it.
    self.channel = iCh

  def _getTypeId(self, iType):
    if iType == 'BUj':
      return 'HFR:BUNC:'
    elif iType == 'Rj':
      return 'HFR:RAND:'
    elif iType == 'Pj1LF':
      return 'LFR:PER1:'
    elif iType == 'PjLF':
      return 'LFR:PER:'
    elif iType == 'Pj2LF':
      return 'LFR:PER2:'
    elif iType == 'Pj1HF':
      return 'HFR:PER1:'
    elif iType == 'PjHF':
      return 'HFR:PER:'
    elif iType == 'Pj2HF':
      return 'HFR:PER2:'
    else:
      return None

  def getMaxAmp(self):
    cmd = (":SOUR:JITT:{}AMPL? 'M{}.DataOut{}', MAX".format(self._typeId, self._module,
                                                            self.channel))
    rtn = self.query(cmd)
    maxAmp = float(rtn[0]) - 0.001
    return maxAmp

  def getMinAmp(self):
    cmd = (":SOUR:JITT:{}AMPL? 'M{}.DataOut{}', MIN".format(self._typeId, self._module,
                                                            self.channel))
    rtn = self.query(cmd)
    minAmp = float(rtn[0]) + 0.001
    return minAmp

  def getMinFreq(self):
    cmd = (":SOUR:JITT:{}FREQ? 'M{}.DataOut{}', MIN".format(self._typeId, self._module,
                                                            self.channel))
    rtn = self.query(cmd)
    minAmp = float(rtn[0]) + 0.001
    return minAmp

  def getMaxFreq(self):
    cmd = (":SOUR:JITT:{}FREQ? 'M{}.DataOut{}', MAX".format(self._typeId, self._module,
                                                            self.channel))
    rtn = self.query(cmd)
    maxAmp = float(rtn[0]) - 0.001
    return maxAmp

  def waitForRdy(self):
    cmd = 'stat:oper:run:cond?'
    rtn = self.query(cmd, TOmultiplier=10)
    while not int(rtn[0]):
      time.sleep(1)
      rtn = self.query(cmd, TOmultiplier=10)

  @property
  def amplitude(self):
    cmd = ("JITT:{}AMPL? 'M{}.DataOut{}'".format(self._typeId, self._module, self.channel))
    rtn = self.query(cmd)[0]
    return float(rtn)

  @amplitude.setter
  def amplitude(self, iAmplitude):
    maxAmp = self.getMaxAmp()
    if iAmplitude < 0:
      logger.warning('Minimum amplitude is 0')
      iAmplitude = 0
    elif iAmplitude > maxAmp:
      logger.warning('Maximum  amplitude is {}UI'.format(maxAmp))
      iAmplitude = maxAmp
    logger.info('Setting {} amplitude to {:2.3f}UI'.format(self.type, iAmplitude))
    cmd = ("JITT:{}AMPL 'M{}.DataOut{}', {:.3e}".format(self._typeId, self._module,
                                                        self.channel, iAmplitude))
    # self.query(cmd, TOmultiplier=2)
    self.write(cmd)
    self.waitForRdy()

  @property
  def frequency(self):
    cmd = ('JITT:{}FREQ? "M{}.DataOut{}"'.format(self._typeId, self._module, self.channel))
    rtn = self.query(cmd)[0]
    return float(rtn)

  @frequency.setter
  def frequency(self, iFreq):
    minFreq = self.getMinFreq()
    maxFreq = self.getMaxFreq()
    if iFreq < minFreq:
      logger.warning('Minimum frequency is {}Hz, setting to min'.format(minFreq))
      iFreq = minFreq
    elif iFreq > maxFreq:
      logger.warning('Maximum frequency is {}Hz, setting to max'.format(maxFreq))
      iFreq = maxFreq
    logger.info('Setting {} frequency to {}Hz'.format(self.type, iFreq))
    cmd = ('JITT:{}FREQ "M{}.DataOut{}",{:.3e}'.format(self._typeId, self._module,
                                                       self.channel, iFreq))
    self.write(cmd)
    self.waitForRdy()

  @property
  def enable(self):
    cmd = ('JITT:{}STAT? "M{}.DataOut{}"'.format(self._typeId, self._module, self.channel))
    rtn = self.query(cmd)[0]
    return rtn

  @enable.setter
  def enable(self, iEn):
    logger.info('Setting {} jitter enable output to {}'.format(self.type, iEn))
    cmd = ('JITT:{}STAT "M{}.DataOut{}",{}'.format(self._typeId, self._module, self.channel,
                                                   iEn))
    self.write(str(cmd))
    self.waitForRdy()

class _Data(labBase.LabEqptModule):

  def __init__(self, commIf, iModule=1, iCh=1, **kwargs):
    super(_Data, self).__init__(commIf, **kwargs)
    self.channel = iCh
    self._module = iModule

  def getMaxAmp(self):
    cmd = ("SOURce:VOLTage:AMPLitude? 'M{}.DataOut{}', MAX".format(
        self._module, self.channel))
    rtn = self.query(cmd)
    rtn = self.query(cmd)
    return float(rtn[0])

  def getMinAmp(self):
    cmd = ("SOURce:VOLTage:AMPLitude? 'M{}.DataOut{}', MIN".format(
        self._module, self.channel))
    rtn = self.query(cmd)
    return float(rtn[0])

  def getMaxClkFreq(self):
    cmd = (":SOURce:FREQuency? 'M{}.ClkGen', MAX".format(self._module))
    rtn = self.query(cmd)
    return float(rtn[0])

  def getMinClkFreq(self):
    cmd = (":SOURce:FREQuency? 'M{}.ClkGen', MIN".format(self._module))
    rtn = self.query(cmd, TOmultiplier=10)
    return float(rtn[0])

  def waitForRdy(self):
    cmd = 'stat:oper:run:cond?'
    rtn = self.query(cmd, TOmultiplier=10)
    while not int(rtn[0]):
      time.sleep(1)
      rtn = self.query(cmd, TOmultiplier=10)

  @property
  def coding(self):
    cmd = (":DATA:LIN:VAL? 'M{}.DataOut{}'".format(self._module, self.channel))
    rtn = self.query(cmd)[0]
    return rtn

  @coding.setter
  def coding(self, iCoding):
    validCodes = ['NRZ', 'PAM3', 'PAM4']
    if iCoding in validCodes:
      logger.info('setting coding {}'.format(iCoding))
      cmd = (":DATA:LIN:VAL 'M{}.DataOut{}', {}".format(self._module, self.channel,
                                                        iCoding))
      self.write(cmd)
      self.waitForRdy()
    else:
      logger.warning('Wrong Coding Value. supperted Codings are: {}'.format(validCodes))

  @property
  def amplitude(self):
    cmd = ("SOURce:VOLTage:AMPLitude? 'M{}.DataOut{}'".format(self._module, self.channel))
    rtn = self.query(cmd)[0]
    return rtn

  @amplitude.setter
  def amplitude(self, iAmplitude):
    minAmp = self.getMinAmp()
    maxAmp = self.getMaxAmp()
    if iAmplitude < minAmp:
      logger.warning('Minimum Data amplitude is {}V'.format(minAmp))
      iAmplitude = minAmp
    elif iAmplitude > maxAmp:
      logger.warning('Maximum Data amplitude is {}V'.format(maxAmp))
      iAmplitude = maxAmp
    logger.info('Setting data amplitude {:.1f}mV'.format(iAmplitude * 1000))
    cmd = ("SOURce:VOLTage:AMPLitude 'M{}.DataOut{}', {}".format(
        self._module, self.channel, iAmplitude))
    self.write(cmd)
    self.waitForRdy()

  @property
  def enable(self):
    cmd = (":OUTPut:STATe? 'M{}.DataOut{}'".format(self._module, self.channel))
    rtn = self.query(cmd)[0]
    return rtn

  @enable.setter
  def enable(self, iEn):
    logger.info('Setting data enable output to {}'.format(iEn))
    cmd = (":OUTPut:STATe 'M{}.DataOut{}', {}".format(self._module, self.channel, iEn))
    self.write(cmd)
    self.waitForRdy()

  @property
  def clkFreq(self):
    cmd = (":SOURce:FREQuency? 'M{}.ClkGen'".format(self._module))
    rtn = self.query(cmd)[0]
    return float(rtn)

  @clkFreq.setter
  def clkFreq(self, iVal):
    minClkFreq = self.getMinClkFreq()
    maxClkFreq = self.getMaxClkFreq()
    clkFreq = iVal
    if iVal < minClkFreq:
      logger.warning('Minimum clock frequency is {}Hz'.format(minClkFreq))
      clkFreq = minClkFreq
    elif iVal > maxClkFreq:
      logger.warning('Maximum clock frequency is {}Hz'.format(maxClkFreq))
      clkFreq = maxClkFreq
    logger.info('Setting clock frequency to {:,}Hz'.format(clkFreq))
    cmd = (":SOURce:FREQuency 'M{}.ClkGen', {}".format(self._module, clkFreq))
    self.write(cmd)
    self.waitForRdy()

  @property
  def FIR(self):
    FIR = []

    cmd = (":OUTPut:DEEMphasis:CURSor:MAGNitude0? 'M{}.DataOut{}'".format(
        self._module, self.channel))
    rtn = self.query(cmd)
    pre2 = float(rtn[0])
    FIR.append(pre2)

    cmd = (":OUTPut:DEEMphasis:CURSor:MAGNitude1? 'M{}.DataOut{}'".format(
        self._module, self.channel))
    rtn = self.query(cmd)
    pre1 = float(rtn[0])
    FIR.append(pre1)

    cmd = (":OUTPut:DEEMphasis:CURSor:MAGNitude2? 'M{}.DataOut{}'".format(
        self._module, self.channel))
    rtn = self.query(cmd)
    main = float(rtn[0])
    FIR.append(main)

    cmd = (":OUTPut:DEEMphasis:CURSor:MAGNitude3? 'M{}.DataOut{}'".format(
        self._module, self.channel))
    rtn = self.query(cmd)
    post1 = float(rtn[0])
    FIR.append(post1)

    cmd = (":OUTPut:DEEMphasis:CURSor:MAGNitude4? 'M{}.DataOut{}'".format(
        self._module, self.channel))
    rtn = self.query(cmd)
    post2 = float(rtn[0])
    FIR.append(post2)

    return FIR

  @FIR.setter
  def FIR(self, iFIR):
    FIR = iFIR
    auto = True
    if len(FIR) != 5:
      msg = 'FIR length is not 5, please check input list'
      logger.error(msg)
      raise RuntimeError(msg)

    if auto:
      cmd = (":OUTPut:DEEMphasis:CURSor:MAIN:AUTO 'M{}.DataOut{}',1".format(
          self._module, self.channel))
      self.write(cmd)
    else:
      cmd = (":OUTPut:DEEMphasis:CURSor:MAIN:AUTO 'M{}.DataOut{}',0".format(
          self._module, self.channel))
      self.write(cmd)

    cmd = (":OUTPut:DEEMphasis:CURSor:MAGNitude0 'M{}.DataOut{}', {}".format(
        self._module, self.channel, FIR[0]))
    self.write(cmd)
    self.waitForRdy()

    cmd = (":OUTPut:DEEMphasis:CURSor:MAGNitude1 'M{}.DataOut{}', {}".format(
        self._module, self.channel, FIR[1]))
    self.write(cmd)
    self.waitForRdy()

    if not auto:
      cmd = (":OUTPut:DEEMphasis:CURSor:MAGNitude2 'M{}.DataOut{}', {}".format(
          self._module, self.channel, FIR[2]))
      self.write(cmd)
      self.waitForRdy()

    cmd = (":OUTPut:DEEMphasis:CURSor:MAGNitude3 'M{}.DataOut{}', {}".format(
        self._module, self.channel, FIR[3]))
    self.write(cmd)
    self.waitForRdy()

    cmd = (":OUTPut:DEEMphasis:CURSor:MAGNitude4 'M{}.DataOut{}', {}".format(
        self._module, self.channel, FIR[4]))
    self.write(cmd)
    self.waitForRdy()

  @property
  def PRBS(self):
    cmd = ':DATA:SEQ:SET? "Generator"'
    rtn = self.query(cmd)
    return rtn.split(',')[-1]

  @PRBS.setter
  def PRBS(self, iPRBS):
    prbsList = [
        'PRBS7', 'PRBS9', 'PRBS10', 'PRBS11', 'PRBS13', 'PRBS15', 'PRBS23', 'PRBS31',
        'PRBS33', 'PRBS35', 'PRBS39', 'PRBS41', 'PRBS45', 'PRBS47', 'PRBS49', 'PRBS51'
    ]
    if iPRBS not in prbsList:
      msg = 'PRBS{} is not supported, check supported PRBS modes in documentation'.format(
          iPRBS)
      logger.error(msg)
      raise RuntimeError(msg)
    prbs = '2^{}-1'.format(iPRBS.split('S')[-1])
    logger.info('Setting PRBS mode to {}'.format(iPRBS))
    cmd = ':DATA:SEQ:SET "Generator", PRBS, "{}"'.format(prbs)
    self.write(cmd)
    self.waitForRdy()

  @property
  def coupling(self):
    cmd = ":OUTPut:COUPling? 'M1.DataOut1'"
    rtn = self.query(cmd)
    return rtn[0]

  @coupling.setter
  def coupling(self, iCoupling):
    coupList = ('AC', 'DC')
    if iCoupling not in coupList:
      msg = 'coupling {} not supported'.format(iCoupling)
      logger.error(msg)
      raise RuntimeError(msg)
    cmd = ":OUTPut:COUPling 'M1.DataOut1',{}"
    self.write(cmd)
    self.waitForRdy()

class _SJ(object):
  """
  Keysight M8000 Series BERT Jitter class
  """

  def __init__(self, iPJLF, iPJHF):
    self.SJLF = iPJLF
    self.SJHF = iPJHF
    self.primary = self.SJLF
    self.secondary = self.SJHF

  def getMaxAmp(self):
    maxAmp = self.primary.getMaxAmp()
    return maxAmp

  @property
  def amplitude(self):
    rtn = self.primary.amplitude
    return rtn

  @amplitude.setter
  def amplitude(self, iAmplitude):
    self.primary.amplitude = iAmplitude

  @property
  def frequency(self):
    rtn = self.primary.frequency
    return rtn

  @frequency.setter
  def frequency(self, iFreq):
    if self.primary == self.SJLF:
      old = self.SJLF
    else:
      old = self.SJHF
    if iFreq < self.SJHF.getMinFreq():
      self.primary = self.SJLF
      self.secondary = self.SJHF
    elif iFreq > self.SJLF.getMaxFreq():
      self.primary = self.SJHF
      self.secondary = self.SJLF
    else:
      self.SJLF.frequency = iFreq
      self.SJHF.frequency = iFreq
      SJLF_maxAmp = self.SJLF.getMaxAmp()
      SJHF_maxAmp = self.SJHF.getMaxAmp()
      if SJLF_maxAmp > SJHF_maxAmp:
        self.primary = self.SJLF
        self.secondary = self.SJHF
      else:
        self.primary = self.SJHF
        self.secondary = self.SJLF
    if old != self.primary:
      logger.info('PJ Switched occurred, please re-enable SJ')
      self.primary.amplitude = old.amplitude
      self.primary.enable = old.enable
      self.secondary.enable = 0
    self.primary.frequency = iFreq

  @property
  def enable(self):
    return self.primary.enable

  @enable.setter
  def enable(self, iEn):
    self.primary.enable = iEn
    self.secondary.enable = iEn

class _AWGData(labBase.LabEqptModule):

  def __init__(self, commIf, iModule=2, iCh=1, **kwargs):
    super(_AWGData, self).__init__(commIf, **kwargs)
    self.channel = iCh
    self._module = iModule

  def waitForRdy(self):
    cmd = 'stat:oper:run:cond?'
    rtn = self.query(cmd, TOmultiplier=10)
    while not int(rtn[0]):
      time.sleep(1)
      rtn = self.query(cmd, TOmultiplier=10)

  @property
  def enable(self):
    cmd = (":OUTPut:STATe? 'M{}.DataOut{}'".format(self._module, self.channel))
    rtn = self.query(cmd)[0]
    return rtn

  @enable.setter
  def enable(self, iEn):
    logger.info('Setting Enable as {}'.format(iEn))
    cmd = (":OUTPut:STATe 'M{}.DataOut{}', {}".format(self._module, self.channel, iEn))
    self.write(cmd)
    self.waitForRdy()

  @property
  def amplitude(self):
    cmd = (":SOURce:VOLTage:AMPLitude? 'M{}.DataOut{}'".format(self._module, self.channel))
    rtn = self.query(cmd)[0]
    return float(rtn)

  @amplitude.setter
  def amplitude(self, iAmplitude):
    if iAmplitude < 0:
      logger.warning('Minimum Data amplitude is 0V')
      iAmplitude = 0
    elif iAmplitude > 1:
      logger.warning('Maximum Data amplitude is 1V')
      iAmplitude = 1
    logger.info('setting amplitude {}'.format(iAmplitude))
    cmd = (":SOURce:VOLTage:AMPLitude 'M{}.DataOut{}', {}".format(
        self._module, self.channel, iAmplitude))
    self.write(cmd)
    self.waitForRdy()

  @property
  def RIamplitude(self):
    cmd = (":SOURce:INTerference:RANDom:AMPLitude? 'M{}.DataOut{}'".format(
        self._module, self.channel))
    rtn = self.query(cmd)[0]
    return float(rtn)

  @RIamplitude.setter
  def RIamplitude(self, iAmplitude):
    cmd = ":SOURce:INTerference:RANDom:AMPLitude? 'M{}.DataOut{}', MAX".format(
        self._module, self.channel)
    maxAmplitude = self.query(cmd)

    if iAmplitude < 0:
      logger.warning('Minimum Data amplitude is 0V')
      iAmplitude = 0
    elif iAmplitude > maxAmplitude:
      logger.warning('Maximum Data amplitude is {}V'.format(maxAmplitude))
      iAmplitude = maxAmplitude
    logger.info('setting amplitude %s' % (iAmplitude))
    cmd = ":SOURce:INTerference:RANDom:AMPLitude 'M{}.DataOut{}',{}".format(
        self._module, self.channel, iAmplitude)
    self.write(cmd)
    self.waitForRdy()

  @property
  def crestFactor(self):
    cmd = (":SOURce:INTerference:RANDom:CREStfactor? 'M{}.DataOut{}'".format(
        self._module, self.channel))
    rtn = self.query(cmd)[0]
    return float(rtn)

  @crestFactor.setter
  def crestFactor(self, iVal):
    # cmd = (":SOURce:INTerference:RANDom:CREStfactor? 'M{}.DataOut{}', MAX".format(
    #   self._module, self.channel))
    # rtn = self.query(cmd)
    # maxValue = float(rtn[0])  #return value does not match with actual max value
    maxValue = 5.6  # Hard coding it to max with amplitude = 0
    if iVal < 0:
      logger.warning('Minimum Crest Factor is 0')
      iVal = 0
    elif iVal > maxValue:
      logger.warning('Maximum Crest Factor is {}'.format(maxValue))
      iVal = maxValue
    logger.info('Setting Crest Factor to {}'.format(iVal))
    cmd = (":SOURce:INTerference:RANDom:CREStfactor 'M{}.DataOut{}', {}".format(
        self._module, self.channel, iVal))
    self.write(cmd)
    self.waitForRdy()

  @property
  def highFreq(self):
    cmd = (":SOURce:INTerference:RANDom:HFRequency? 'M{}.DataOut{}'".format(
        self._module, self.channel))
    rtn = self.query(cmd)[0]
    return float(rtn)

  @highFreq.setter
  def highFreq(self, iVal):
    if iVal < 640E3:  # Values gotten at crest factor of 4.5, may need update
      logger.warning('Minimum Frequency is 640kHz')
      iVal = 640E3
    elif iVal > 32E9:
      logger.warning('Maximum Data amplitude is 32GHz')
      iVal = 32E9
    logger.info('Setting Random Interference High Frequency to {}'.format(iVal))
    cmd = (":SOURce:INTerference:RANDom:HFRequency 'M{}.DataOut{}', {}".format(
        self._module, self.channel, iVal))
    self.write(cmd)
    self.waitForRdy()

  @property
  def lowFreq(self):
    cmd = (":SOURce:INTerference:RANDom:LFRequency? 'M{}.DataOut{}'".format(
        self._module, self.channel))
    rtn = self.query(cmd)[0]
    return float(rtn)

  @lowFreq.setter
  def lowFreq(self, iVal):
    if iVal < 160E3:  # Values gotten at crest factor of 4.5, may need update
      logger.warning('Minimum Data amplitude is 160kHz')
      iVal = 160E3
    elif iVal > 20E9:
      logger.warning('Maximum Data amplitude is 20GHz')
      iVal = 20E9
    logger.info('Setting Random Interference Low Frequency to {}'.format(iVal))
    cmd = (":SOURce:INTerference:RANDom:LFRequency 'M{}.DataOut{}', {}".format(
        self._module, self.channel, iVal))
    self.write(cmd)
    self.waitForRdy()

class _AWG(labBase.LabEqptModule):
  """
  Keysight M8054A series Bit Error Rate Tester Arbitraty Waveform Generator
  (https://www.keysight.com/en/pd-2973936-pn-M8054A/interference-source-32-ghz)

  It is assumed the AWG is installed in module 2

  """

  def __init__(self, commIf, iModule=2, **kwargs):
    super(_AWG, self).__init__(commIf, **kwargs)
    self.ch1 = _AWGData(self._conn, iModule=iModule, iCh=1)
    self.ch2 = _AWGData(self._conn, iModule=iModule, iCh=2)
    self.ch3 = _AWGData(self._conn, iModule=iModule, iCh=3)
    self.ch4 = _AWGData(self._conn, iModule=iModule, iCh=4)

class M80xx(labBase.BERT):
  """
  Keysight M8000 series Bit Error Rate Tester
  (https://www.keysight.com/ca/en/products/bit-error-ratio-testers/m8000-series-bit-error-ratio-testers.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    super(M80xx, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "0,\"No error\""
    self.ch1 = None
    self.AWG = None
    self.PJLF = None
    self.PJ1LF = None
    self.PJ2LF = None
    self.PJHF = None
    self.PJ1HF = None
    self.PJ2HF = None
    self.Rj = None
    self.BUj = None
    self.Data = None
    self.Sj = None
    self._modules = {}

  def build(self):
    """
    I will add the modules to the BERT object. supported modules:
      -M8045A: 64 Gbaud High-performance BERT
      -M8062A: 32Gb/s BERT Front End
      -M8054A: Interference Source, 32 GHz

    :return: Nothing, it adds the ch1 object
    """
    # self.ch1 = _Channel(self._rsrcObj, iCh=1)
    self._getInfo()
    for each in self._modules.keys():
      if self._modules[each] == 'M8045A':
        logger.info('Module {} was added'.format(self._modules[each]))
        self.Rj = _JITT(self._rsrcObj, iModule=each[-1], iCh=1, iType="Rj")
        self.BUj = _JITT(self._rsrcObj, iModule=each[-1], iCh=1, iType="BUj")
        self.Data = _Data(self._rsrcObj, iModule=each[-1], iCh=1)
        self.PJ1LF = _JITT(self._rsrcObj, iModule=each[-1], iCh=1, iType="Pj1LF")
        self.PJ2LF = _JITT(self._rsrcObj, iModule=each[-1], iCh=1, iType="Pj2LF")
        self.PJ1HF = _JITT(self._rsrcObj, iModule=each[-1], iCh=1, iType="Pj1HF")
        self.PJ2HF = _JITT(self._rsrcObj, iModule=each[-1], iCh=1, iType="Pj2HF")
        self.Sj = _SJ(iPJLF=self.PJ1LF, iPJHF=self.PJ1HF)
      elif self._modules[each] == 'M8062A':
        logger.info('Module {} was added'.format(self._modules[each]))
        self.Rj = _JITT(self._rsrcObj, iModule=each[-1], iCh=1, iType="Rj")
        self.BUj = _JITT(self._rsrcObj, iModule=each[-1], iCh=1, iType="BUj")
        self.Data = _Data(self._rsrcObj, iModule=each[-1], iCh=1)
        self.PJLF = _JITT(self._rsrcObj, iModule=each[-1], iCh=1, iType="PjLF")
        self.PJHF = _JITT(self._rsrcObj, iModule=each[-1], iCh=1, iType="PjHF")
        self.Sj = _SJ(iPJLF=self.PJLF, iPJHF=self.PJHF)
      elif self._modules[each] == 'M8054A':
        logger.info('Module {} was added'.format(self._modules[each]))
        self.AWG = _AWG(self._rsrcObj, iModule=each[-1])
      else:
        msg = 'BERT module {} not supported!'.format(self._modules[each])
        logger.error(msg)
        raise RuntimeError(msg)

  def addAWG(self):
    warnings.warn("Add AWG method WILL BE DEPRECATED this is now part of build method",
                  DeprecationWarning)
    """
    This adds the AWG generator object. Not all BERTs have an arbitrary waveform generator :)

    :return: Nothing, it adds the AWG object
    """
    self.AWG = _AWG(self._rsrcObj)

  def _getInfo(self):
    super(M80xx, self)._getInfo()
    if len(self.OPT()) == 0:
      msg = 'No modules detected in this BERT'
      logger.error(msg)
      raise RuntimeError(msg)
    else:
      self._idn["opt"] = self.OPT()[0]
      modules = re.findall(r"\(([^)]+)\)", self._idn["opt"])
      for each in modules:
        self._modules.update({each.split('-')[0]: each.split('-')[1].split(',')[0]})

      # self._idn = self.query('*IDN?')
      # print(self._idn)
    return self._idn

  def recallSetup(self, iSetup):
    """
    Experimental function to recall an stored setup/configuration in the BERT. currently not working :(

    :param iSetup: string with the setup name
    :return: Nothing
    """
    cmd = (":MMEMory:WORKspace:SETTings:USER:RECall '{}'".format(iSetup))
    logger.info(cmd)
    self.write(cmd)
    self.waitForRdy()

  def waitForRdy(self):
    cmd = 'stat:oper:run:cond?'
    rtn = self.query(cmd, TOmultiplier=10)
    while not int(rtn[0]):
      time.sleep(1)
      rtn = self.query(cmd, TOmultiplier=10)

  @property
  def globalEnable(self):
    cmd = ":OUTP:GLOB? 'M1.System'"
    rtn = self.query(cmd)[0]
    return rtn

  @globalEnable.setter
  def globalEnable(self, iEn):
    logger.info('Setting global enable output to {}'.format(iEn))
    cmd = ":OUTP:GLOB 'M1.System', {}".format(iEn)
    self.write(cmd)
    self.waitForRdy()

  def quickSet(self,
               iClkFreq=None,
               iCoding=None,
               iAmplitude=None,
               iSjAmp=None,
               iSJFreq=None,
               iFIR=None,
               iEN=False):
    if iClkFreq is not None:
      self.Data.clkFreq = iClkFreq
    if iCoding is not None:
      self.Data.coding = iCoding
    if iAmplitude is not None:
      self.Data.amplitude = iAmplitude
    if iFIR is not None:
      self.Data.FIR = iFIR
    if iSJFreq is not None:
      self.Sj.frequency = iSJFreq
    if iSjAmp is not None:
      self.Sj.amplitude = iSjAmp
    self.Sj.enable = True
    self.Data.enable = iEN
