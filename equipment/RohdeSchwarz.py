#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class FSP(labBase.SpectrumAnalyzer):
  """
  Rohde&Schwarz FSP Spectrum Analyzer
  (https://www.rohde-schwarz.com/ca/product/fsp-productstartpage_63493-8043.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(FSP, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    self.qry.update({
        "cf": "FREQ:CENTER",
        "span": "FREQ:SPAN",
        "ref": "DISPLAY:TRACE:Y:RLEV",
        "div": None,
        "attn": "INPUT:ATT",
        "rbw": "BAND",
        "vbw": "BAND:VID"
    })
    self.replyNoErr = "0,\"No error\""

  def _getInfo(self):
    super(FSP, self)._getInfo()

    self._idn["opt"] = self.OPT()[0]
    return self._idn

  def getPeak(self, cf, span, ref):
    #logger.info("attn: {}".format(self.attn))
    self.cf = cf
    self.span = span
    self.ref = ref
    #logger.info("  CF: {}".format(self.cf))
    #logger.info("span: {}".format(self.span))
    #logger.info(" ref: {}".format(self.ref))
    #logger.info(" RBW: {}".format(self.rbw))
    #logger.info(" VBW: {}".format(self.vbw))

    self.write("INIT")
    self.waitOnCmd()
    self.write("CALC1:MARKER:MAX")
    self.waitOnCmd()

    res = self.query("CALC1:MARKER:X?")
    freq = float(res[0])
    res = self.query("CALC1:MARKER:Y?")
    pwr = float(res[0])
    return freq, pwr
