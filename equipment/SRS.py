#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class DS345(labBase.FunctionGen):
  """
  SRS 30 MHz function/ARB generator
  (https://www.thinksrs.com/products/ds345.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("adapter_name" not in kwargs.keys()):
      kwargs["adapter_name"] = "prologix"
      kwargs["adapter_gpibAddr"] = 19
    #if ("eom" not in kwargs.keys()):
    #  kwargs["eom"] = "\n"
    super(DS345, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    #self.SCPI = False
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "1"

  def setOutput(self, mode, freq, ampl, offset, outId):
    super(DS345, self).setOutput(mode, freq, ampl, offset, outId)

    if (mode == labBase.Function_Sine):
      self.write("FUNC 0")
    elif (mode == labBase.Function_Square):
      self.write("FUNC 1")
    elif (mode == labBase.Function_Tri):
      self.write("FUNC 2")
    elif (mode == labBase.Function_Saw):
      self.write("FUNC 3")
    else:
      msg = "UNSUPPORTED mode: {}".format(mode)
      logger.warning(msg)
      return

    self.write("FREQ {:d}".format(freq))
    self.write("AMPL {:f} VP".format(ampl))
    self.write("OFFS {:f}".format(offset))
