#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
from ._base_eqpt import *

class BERT_DATA(LabEqptModule):

  def __init__(self, strConnInfo, **kwargs):
    super(BERT_DATA, self).__init__(strConnInfo, **kwargs)

  @property
  def amplitude(self):
    raise NotImplementedError

  @amplitude.setter
  def amplitude(self, val):
    raise NotImplementedError

  @property
  def enable(self):
    raise NotImplementedError

  @enable.setter
  def enable(self, val):
    raise NotImplementedError

class BERT_JITT(LabEqptModule):

  def __init__(self, strConnInfo, **kwargs):
    super(BERT_JITT, self).__init__(strConnInfo, **kwargs)

  @property
  def amplitude(self):
    raise NotImplementedError

  @amplitude.setter
  def amplitude(self, val):
    raise NotImplementedError

  @property
  def frequency(self):
    raise NotImplementedError

  @frequency.setter
  def frequency(self, val):
    raise NotImplementedError

  @property
  def enable(self):
    raise NotImplementedError

  @enable.setter
  def enable(self, val):
    raise NotImplementedError

class BERT(LabEqpt):

  def __init__(self, strConnInfo, **kwargs):
    super(BERT, self).__init__(strConnInfo, **kwargs)

  def build(self):
    raise NotImplementedError

  @property
  def enableGlobal(self):
    raise NotImplementedError

  @enableGlobal.setter
  def enableGlobal(self, val):
    raise NotImplementedError

class TrafficGen(LabEqpt):

  def __init__(self, strConnInfo, **kwargs):
    super(TrafficGen, self).__init__(strConnInfo, **kwargs)
