#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

class PM1100(labBase.PowerMeter):
  """
  EXFO PM1100 Optical Power Meter
  (http://www.exfo.com/products/discontinued/pm-1100)
  """
  dictId2Units = {"0": "dBm", "1": "W", "2": "dB", "3": "delta_W"}

  def __init__(self, strConnInfo, **kwargs):
    super(PM1100, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "0,\"\""

  def _getInfo(self):
    super(PM1100, self)._getInfo()

    res = self.TST()
    self._idn["self-test"] = res[0]
    return self._idn

  def setUnit(self, val, chId=None):
    if (val in ("dBm", "W")):
      abs_meas = True
    elif (val in ("dB", "dW")):
      abs_meas = False
    else:
      msg = "INCORRECT unit: {}".format(val)
      logger.error(msg)
      raise RuntimeError(msg)

    self.write("SENS:POW:REF:STAT {}".format("0" if abs_meas else "1"))
    self.write("SENS:POW:UNIT {}".format(val))
    self.waitOnCmd()

  def getUnit(self, chId=None):
    res = self.query("SENS:POW:UNIT?")
    return self.dictId2Units[res[0]]

  def setWavelen(self, val, chId=None):
    pyauto_base.misc.chkFloat(val, valId="wavelength", minVal=750.0, maxVal=1700.0)
    self.write("SENS:POW:WAVE {} NM".format(val))
    self.waitOnCmd()

  def getWavelen(self, chId=None):
    res = self.query("SENS:POW:WAVE?")
    return float(res[0].split()[0])

  def getPower(self, chId=None):
    res = self.query("READ:POW:DC?")
    #logger.info(res)
    if ((len(res) == 0) or (res[0].startswith("--"))):
      val = None
    else:
      val = float(res[0])
    #logger.info("power: {}".format(val))
    return val

class FVA3100(labBase.VOA):
  """
  EXFO FVA-3100 Optical Variable Attenuator
  (http://www.exfo.com/products/discontinued/fva-3100)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(FVA3100, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "0,\"\""
    self._wl = None

  def _getInfo(self):
    super(FVA3100, self)._getInfo()

    res = self.query("FIBER?")
    if (res[0] == "1"):
      self._idn["fiber"] = "SM"
    elif (res[0] == "2"):
      self._idn["fiber"] = "MM"
    else:
      self._idn["fiber"] = "??? ()".format(res[0])
    res = self.TST()
    self._idn["self-test"] = res[0]
    return self._idn

  def setWavelen(self, val, chId=None):
    pyauto_base.misc.chkFloat(val, valId="wavelength", minVal=1200.0, maxVal=1650.0)
    self.write("WVL {} NM".format(val))
    self.waitOnCmd()

  def getWavelen(self, chId=None):
    res = self.query("WVL?")
    return float(res[0])

  def setAtt(self, val, chId=None):
    pyauto_base.misc.chkFloat(val, valId="attenuation", minVal=-64.0, maxVal=0.0)
    self.write("ATT {}".format(val))
    self.waitOnCmd()

  def getAtt(self, chId=None):
    res = self.query("ATT?")
    return float(res[0])

  def enable(self, chId=None):
    self.write("D 0")
    self.waitOnCmd()

  def disable(self, chId=None):
    self.write("D 1")
    self.waitOnCmd()

  def config(self, mode="abs"):
    """
    :param mode: abs|ref
    """
    #self.disable()
    if (mode == "abs"):
      self.write("OUTPUT:APMODE 1")
      self.waitOnCmd()
    elif (mode == "ref"):
      self.write("OUTPUT:APMODE 0")
      self.waitOnCmd()
    else:
      msg = "INCORRECT mode: {}".format(mode)
      logger.error(msg)
      raise RuntimeError(msg)
    #self.enable()
