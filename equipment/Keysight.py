#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_comm.generic
import pyauto_lab.equipment.base as labBase
from ._Keysight_DCA import *
from ._Keysight_BERT import *
from ._Keysight_RF import *

class Infiniium(labBase.Oscilloscope):
  """
  Keysight Infiniium S-Series Oscilloscopes
  (https://www.keysight.com/en/pcx-x205213/infiniium-s-series-oscilloscopes?nid=-32531.0.00&cc=CA&lc=eng)
  Keysight Infiniium MXR-Series Real-Time Oscilloscopes
  (https://www.keysight.com/ca/en/products/oscilloscopes/infiniium-real-time-oscilloscopes/infiniium-mxr-series-real-time-oscilloscopes.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    super(Infiniium, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "0"
    self.nCh = 4

  def _getInfo(self):
    super(Infiniium, self)._getInfo()

    self._idn["opt"] = self.OPT()[0]
    return self._idn

  def getHorizontal(self):
    res = self.query("TIMEBASE:SCALE?")
    scale = float(res[0])
    res = self.query("TIMEBASE:POSITION?")
    offset = float(res[0])
    return [scale, offset]

  def setHorizontal(self, scale, offset):
    self.write("TIMEBASE:SCALE {}".format(scale))
    self.write("TIMEBASE:POSITION {}".format(offset))

  def getVertical(self, chId):
    self._chkChannelId(chId)

    res = self.query("CHANNEL{}:SCALE?".format(chId))
    scale = float(res[0])
    res = self.query("CHANNEL{}:OFFSET?".format(chId))
    offset = float(res[0])
    return [scale, offset]

  def showCh(self, chId):
    super(Infiniium, self).showCh(chId)

    self.write("CHANNEL{}:DISPLAY ON".format(chId))

  def hideCh(self, chId):
    super(Infiniium, self).hideCh(chId)

    self.write("CHANNEL{}:DISPLAY OFF".format(chId))

  def getLabel(self, chId=None):
    self._chkChannelId(chId)

    res = self.query("CHANNEL{}:LABEL?".format(chId))
    return res[0].strip("\"")

  def setLabel(self, val="", chId=None):
    super(Infiniium, self).setLabel(val, chId)

    self.write("CHANNEL{}:LABEL \"{}\"".format(chId, val))

  def setTrigger(self, val):
    if (val not in labBase.lstOscTrigger):
      msg = "INCORRECT trigger sweep {}".format(val)
      logger.warning(msg)
      return

    if (val == labBase.OscTrigger_Auto):
      self.write("TRIGGER:SWEEP AUTO")
      self.write(":RUN")
    elif (val == labBase.OscTrigger_Trigd):
      self.write("TRIGGER:SWEEP TRIG")
      self.write(":RUN")
    elif (val == labBase.OscTrigger_Single):
      self.write(":SINGLE")

  def writeImg(self, path):
    if (not path.endswith(".png")):
      msg = "INCORRECT file name: {}".format(path)
      logger.error(msg)
      raise RuntimeError(msg)

    self.write(":DISK:SAVE:IMAGE \"{}\",PNG,SCR".format(path))
    cnt = 0
    while (True):
      try:
        res = self.query(":PDER?", TOmultiplier=2)
        if (len(res[0]) == 0):
          self.read(TOmultiplier=2)
        elif (res[0] == "+1"):
          break
      except pyauto_comm.generic.CommTimeoutException:
        pass
      cnt += 1
      if (cnt > 10):
        logger.warning("timeout")
        break
      time.sleep(1)

    logger.info("{} written".format(path))

  def writeCSV(self, path):
    if (not path.endswith(".csv")):
      msg = "INCORRECT file name: {}".format(path)
      logger.error(msg)
      raise RuntimeError(msg)

    self.write(":DISK:SAVE:WAV ALL,\"{}\",CSV,ON".format(path))
    cnt = 0
    while (True):
      try:
        res = self.query(":PDER?", TOmultiplier=2)
        if (len(res[0]) == 0):
          self.read(TOmultiplier=2)
        elif (res[0] == "+1"):
          break
      except pyauto_comm.generic.CommTimeoutException:
        pass
      cnt += 1
      if (cnt > 10):
        logger.warning("timeout")
        break
      time.sleep(1)

    logger.info("{} written".format(path))

class _InfiniiVision(labBase.Oscilloscope):
  """
  Keysight
  (https://www.keysight.com/ca/en/products/oscilloscopes/infiniivision-2-4-channel-digital-oscilloscopes/infiniivision-2000-x-series-oscilloscopes.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    super(_InfiniiVision, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""
    self.nCh = 4

  def _getInfo(self):
    super(_InfiniiVision, self)._getInfo()

    self._idn["opt"] = self.OPT()[0]
    return self._idn

  def getHorizontal(self):
    res = self.query("TIMEBASE:SCALE?")
    scale = float(res[0])
    res = self.query("TIMEBASE:POSITION?")
    offset = float(res[0])
    return [scale, offset]

  def getVertical(self, chId):
    self._chkChannelId(chId)

    res = self.query("CHANNEL{}:SCALE?".format(chId))
    scale = float(res[0])
    res = self.query("CHANNEL{}:OFFSET?".format(chId))
    offset = float(res[0])
    return [scale, offset]

class DSOX(_InfiniiVision):
  pass

class MSOX(_InfiniiVision):
  pass

class N57xx(labBase.PowerSupply):
  """
  Keysight N5700 Series Power Supply
  (https://www.keysight.com/en/pc-856707/1500w-dc-system-power-supplies-gpib-single-output?nid=-35683.0.00&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    super(N57xx, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""

  def _getInfo(self):
    super(N57xx, self)._getInfo()

    self._idn["opt"] = self.OPT()[0]
    return self._idn

  def setOutput(self, voltage, current, outId=None):
    v = pyauto_base.misc.chkFloat(voltage, valId="voltage", minVal=0.0)
    c = pyauto_base.misc.chkFloat(current, valId="current", minVal=0.0)

    self.write("SOURCE:VOLTAGE {}".format(v))
    self.write("SOURCE:CURRENT {}".format(c))

  def getOutput(self, outId=None):
    res = self.query("MEASURE:VOLTAGE?")
    v = float(res[0])
    res = self.query("MEASURE:CURRENT?")
    i = float(res[0])
    return {"voltage": v, "current": i}

  def enable(self, outId=None):
    logger.info("enable OUT")
    self.write("OUTPUT:STATE ON")

  def disable(self, outId=None):
    logger.info("disable OUT")
    self.write("OUTPUT:STATE OFF")

class K_344xxA(labBase.Multimeter):
  """
  Keysight 34401A Digital Multimeter
  (https://www.keysight.com/en/pd-1000001295%3Aepsg%3Apro-pn-34401A/digital-multimeter-6-digit?nid=-31895.536880933&cc=CA&lc=eng)

  Keysight 34450A Digital Multimeter
  (https://www.keysight.com/en/pdx-2905537-pn-34450A/digital-multimeter-5-digit-oled-display?nid=-31940.1242734&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(K_344xxA, self).__init__(strConnInfo, **kwargs)

    if (hasattr(self._rsrcObj, "type")):
      if (self._rsrcObj.type == pyauto_comm.generic.ifVISA):
        self._rsrcObj.eos = "\n"
    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""

    self.nOut = 1

  def setChannel(self, mode, chId=None):
    if (mode == labBase.MultimeterMode_DCV):
      self.write("FUNCTION \"VOLTAGE:DC\"")
    elif (mode == labBase.MultimeterMode_ACV):
      self.write("FUNCTION \"VOLTAGE:AC\"")
    elif (mode == labBase.MultimeterMode_DCI):
      self.write("FUNCTION \"CURRENT:DC\"")
    elif (mode == labBase.MultimeterMode_ACI):
      self.write("FUNCTION \"CURRENT:AC\"")
    elif (mode == labBase.MultimeterMode_RES2W):
      self.write("FUNCTION \"RESISTANCE\"")
    elif (mode == labBase.MultimeterMode_FREQ):
      self.write("FUNCTION \"FREQUENCY\"")
    else:
      msg = "INCORRECT mode: {}".format(mode)
      logger.error(msg)
      raise RuntimeError(msg)
    self.waitOnCmd()

  def getChannel(self, chId=None):
    res = self.query("FUNCTION?")
    mode = res[0]

    # self.write("INITIATE")
    res = self.query("READ?", TOmultiplier=4)
    val = float(res[0])

    return {"mode": mode, "val": val}

class EL3xxxxA(labBase.PowerLoad):
  """
  Keysight EL30000 Series Bench Electronic Loads
  (https://www.keysight.com/us/en/products/dc-electronic-loads/el30000-series-bench-electronic-loads.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    super(EL3xxxxA, self).__init__(strConnInfo, **kwargs)
    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""

    self.nOut = 2

  def setChannel(self, mode, value, chId=None):
    self._chkChannelId(chId)
    self.write("CHANNEL:SELECT CH{}".format(chId))

    if (mode == labBase.PwrLoad_CC):
      self.write("MODE CURRENT")
      self.write("CURRENT {:.3f}".format(value))
    elif (mode == labBase.PwrLoad_CR):
      self.write("MODE RESISTANCE")
      self.write("RESISTANCE {:.3f}".format(value))
    elif (mode == labBase.PwrLoad_CP):
      self.write("MODE POWER")
      self.write("POWER {:.3f}".format(value))
    elif (mode == labBase.PwrLoad_CV):
      self.write("MODE VOLTAGE")
      self.write("VOLTAGE {:.3f}".format(value))
    else:
      msg = "INCORRECT mode: {}".format(mode)
      logger.error(msg)
      raise RuntimeError(msg)

  def getChannel(self, chId=None):
    self._chkChannelId(chId)
    self.write("CHANNEL:SELECT CH{}".format(chId))

    res = self.query("MODE?")
    mode = res[0]
    val = float("nan")
    if (mode == "CURR"):
      mode = labBase.PwrLoad_CC
      res = self.query("CURRENT?")
      val = float(res[0])
    elif (mode == "RES"):
      mode = labBase.PwrLoad_CR
      res = self.query("RESISTANCE?")
      val = float(res[0])
    elif (mode == "POW"):
      mode = labBase.PwrLoad_CP
      res = self.query("POWER?")
      val = float(res[0])
    elif (mode == "VOLT"):
      mode = labBase.PwrLoad_CV
      res = self.query("VOLTAGE?")
      val = float(res[0])
    else:
      mode = "???"
    return {"mode": mode, "val": val}

  def enable(self, chId=None):
    self._chkChannelId(chId)
    self.write("CHANNEL:SELECT CH{}".format(chId))
    self.write("INPUT:STATE ON")

  def disable(self, chId=None):
    self._chkChannelId(chId)
    self.write("CHANNEL:SELECT CH{}".format(chId))
    self.write("INPUT:STATE OFF")

class B29xxA(labBase.LabEqpt):
  """
  Keysight B2902A Precision Source/Measure Unit
  (https://www.keysight.com/en/pd-1983585-pn-B2902A/precision-source-measure-unit-2-ch-100-fa-210-v-3-a-dc-105-a-pulse?nid=-33504.978795.00&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(B29xxA, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""

  def _getInfo(self):
    super(B29xxA, self)._getInfo()

    self._idn["opt"] = self.OPT()[0]
    return self._idn
