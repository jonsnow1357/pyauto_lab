#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class N90xx(labBase.SpectrumAnalyzer):
  """
  Keysight X-Series Signal Analyzers
  (https://www.keysight.com/en/pcx-x205200/x-series-signal-analyzers?nid=-32508.0.00&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    super(N90xx, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    self.qry.update({
        "cf": "FREQ:RF:CENTER",
        "span": "FREQ:SPAN",
        "ref": "DISPLAY:WIND:TRACE:Y:RLEV",
        "div": None,
        "attn": "POWER:RF:ATT",
        "rbw": "SENSE:BAND:RES"
    })
    self.replyNoErr = "+0,\"No error\""

  def getPeak(self, cf, span, ref):
    #logger.info("attn: {}".format(self.attn))
    self.cf = cf
    self.span = span
    self.ref = ref
    #logger.info("  CF: {}".format(self.cf))
    #logger.info("span: {}".format(self.span))
    #logger.info(" ref: {}".format(self.ref))
    #logger.info(" RBW: {}".format(self.rbw))
    #logger.info(" VBW: {}".format(self.vbw))

    self.write("INIT")
    self.waitOnCmd()
    self.write("CALC:MARKER1:MAX")
    self.waitOnCmd()

    res = self.query("CALC:MARKER1:X?")
    freq = float(res[0])
    res = self.query("CALC:MARKER1:Y?")
    pwr = float(res[0])
    return freq, pwr

  def getPhaseNoise(self, cf, span0, span1, meas0, meas1, ref):
    self.write("LPLOT:METHOD PN")
    self.waitOnCmd()

    self.write("FREQ:CARRIER {}".format(cf))
    self.write("CALC:LPLOT:DECADE:TABLE ON")
    self.write("LPLOT:FREQ:OFFSET:START {}".format(span0))
    self.write("LPLOT:FREQ:OFFSET:STOP {}".format(span1))
    self.write("DISPLAY:LPLOT:VIEW:WINDOW:TRACE:Y:RLEV {}".format(ref))
    self.write("FREQ:CARRIER:TRACK ON")
    self.write("CALC:LPLOT:MARKER:BAND:LEFT {}".format(meas0))
    self.write("CALC:LPLOT:MARKER:BAND:RIGHT {}".format(meas1))
    self.write("CALC:LPLOT:MARKER:RMSN:MODE JITTER")
    self.write("POWER:RANGE:OPTIMIZE IMMEDIATE")
    self.write("LPLOT:AVERAGE:COUNT 16")
    self.write("LPLOT:AVERAGE ON")
    self.write("LPLOT:ODRIVE ON")
    self.write("LPLOT:ODRIVE:ATT:ZERO ON")
    self.write("LPLOT:CANCELLATION OFF")
    self.waitOnCmd()

    self.write("LPLOT:METHOD DANL")
    self.waitOnCmd()

    self.write("INIT")
    self.waitOnCmd(timeout=10)

    self.write("TRACE:LPLOT:COPY TRACE2,TRACE3")

    self.write("LPLOT:METHOD PN")
    self.waitOnCmd()

    self.write("LPLOT:CANCELLATION ON")
    self.write("INIT")
    self.waitOnCmd(timeout=16)

    res = self.query("CALC:LPLOT:MARKER1:Y?")
    pn = float(res[0])
    return pn

class ENA(labBase.NetworkAnalyzer):
  """
  Keysight ENA Vector Network Analyzer
  (https://www.keysight.com/ca/en/products/network-analyzers/ena-vector-network-analyzers.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(ENA, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""

  def writeTouchstone(self, path, frmt="auto"):
    if (frmt == "lin"):
      self.write("MMEM:STOR:TRAC:FORM:SNP MA")
    elif (frmt == "log"):
      self.write("MMEM:STOR:TRAC:FORM:SNP DB")
    elif (frmt == "ri"):
      self.write("MMEM:STOR:TRAC:FORM:SNP RI")

    path_d = os.path.dirname(path)
    logger.info("create folder '{}'".format(path_d))
    self.write("MMEM:MDIR '{}'".format(path_d))

    if (path.endswith("s2p")):
      self.write("CALC:MEAS2:DATA:SNP:PORTs:Save '1,2','{}'".format(path))
      self.waitOnCmd(timeout=4)
    elif (path.endswith("s3p")):
      self.write("CALC:MEAS2:DATA:SNP:PORTs:Save '1,2,3','{}'".format(path))
      self.waitOnCmd(timeout=4)
    elif (path.endswith("s4p")):
      self.write("CALC:MEAS2:DATA:SNP:PORTs:Save '1,2,3,4','{}'".format(path))
      self.waitOnCmd(timeout=4)
    else:
      msg = "INCORRECT file name: {}".format(path)
      logger.error(msg)
      raise RuntimeError(msg)
    logger.info("{} written".format(path))

class PNA(labBase.NetworkAnalyzer):
  """
  Keysight PNA Vector Network Analyzer
  (https://www.keysight.com/us/en/products/network-analyzers/pna-network-analyzers.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(PNA, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""

  def writeTouchstone(self, path, frmt="auto"):
    if (frmt == "lin"):
      self.write("MMEM:STOR:TRAC:FORM:SNP MA")
    elif (frmt == "log"):
      self.write("MMEM:STOR:TRAC:FORM:SNP DB")
    elif (frmt == "ri"):
      self.write("MMEM:STOR:TRAC:FORM:SNP RI")

    path_d = os.path.dirname(path)
    logger.info("create folder '{}'".format(path_d))
    self.write("MMEM:MDIR '{}'".format(path_d))

    if (path.endswith("s2p")):
      self.write("CALC:MEAS2:DATA:SNP:PORTs:Save '1,2','{}'".format(path))
      self.waitOnCmd(timeout=4)
    elif (path.endswith("s3p")):
      self.write("CALC:MEAS2:DATA:SNP:PORTs:Save '1,2,3','{}'".format(path))
      self.waitOnCmd(timeout=4)
    elif (path.endswith("s4p")):
      self.write("CALC:MEAS2:DATA:SNP:PORTs:Save '1,2,3,4','{}'".format(path))
      self.waitOnCmd(timeout=4)
    else:
      msg = "INCORRECT file name: {}".format(path)
      logger.error(msg)
      raise RuntimeError(msg)
    logger.info("{} written".format(path))

  def readState(self, path):
    if (not path.endswith(".csa")):
      msg = "INCORRECT file name: {}".format(path)
      logger.error(msg)
      raise RuntimeError(msg)

    logger.info("loading {}".format(path))
    self.write("MMEM:LOAD:CSAR '{}'".format(path))
    self.waitOnCmd(timeout=4)

  def writeState(self, path):
    path_d = os.path.dirname(path)
    logger.info("create folder '{}'".format(path_d))
    self.write("MMEM:MDIR '{}'".format(path_d))

    self.write("MMEM:STORE:STATE '{}'".format(path))
    self.waitOnCmd(timeout=4)

class SSA_X(labBase.SpectrumAnalyzer):
  """
  Keysight E5055A SSA-X Signal Source Analyzer
  (https://www.keysight.com/us/en/product/E5055A/e5055a-ssa-x-signal-source-analyzer-1-mhz-to-8-ghz.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(SSA_X, self).__init__(strConnInfo, **kwargs)

    self._rsrcObj.eom = "\n"
    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""

  def writeImg(self, path):
    if ((not path.endswith(".png")) and (not path.endswith(".jpg"))):
      msg = "INCORRECT file name: {}".format(path)
      logger.error(msg)
      raise RuntimeError(msg)

    cmd = "HCOPY:FILE \"{}\"".format(path)
    #print("DBG", cmd)
    self.write(cmd)
    self.waitOnCmd()
    logger.info("{} written".format(path))

  def writeCSV(self, path, frmt="auto"):
    if (not path.endswith(".csv")):
      msg = "INCORRECT file name: {}".format(path)
      logger.error(msg)
      raise RuntimeError(msg)

    # yapf: disable
    cmd = "MMEMORY:STORE:DATA \"{}\",\"CSV Formatted Data\",\"Displayed\",\"DB\",-1".format(path)
    # yapf: enable
    #print("DBG", cmd)
    self.write(cmd)
    self.waitOnCmd()
    logger.info("{} written".format(path))
