#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase
import pyauto_lab.data.file

class E4418B(labBase.PowerMeter):
  """
  Keysight E4418B EPM Series Single-Channel Power Meter
  (http://www.keysight.com/en/pd-1000002798%3Aepsg%3Apro-pn-E4418B/epm-series-single-channel-power-meter?pm=PL&nid=-536902899.536881895&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(E4418B, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""

  def _getInfo(self):
    super(E4418B, self)._getInfo()

    self._idn["sensors"] = [None]
    res = self.query("SERV:SENS:TYPE?")
    if (len(res) != 0):
      self._idn["sensors"][0] = res[0]

    return self._idn

  def setUnit(self, val, chId=None):
    if (val not in ("dBm", "W")):
      msg = "INCORRECT unit: {}".format(val)
      logger.error(msg)
      raise RuntimeError(msg)

    self.write("UNIT:POWER {}".format(val))
    self.waitOnCmd()

  def getUnit(self, chId=None):
    res = self.query("UNIT:POWER?")
    return res[0]

  def setFreq(self, val, chId=None):
    pyauto_base.misc.chkFloat(val, valId="frequency", minVal=0.1, maxVal=110000.0)
    self.write("SENS:FREQ:CW {}".format(val))
    self.waitOnCmd()

  def getFreq(self, chId=None):
    res = self.query("SENS:FREQ:CW?")
    return float(res[0].split()[0]) / 1e6

  def getPower(self, chId=None):
    res = self.query("FETCH?")
    #logger.info(res)
    val = float(res[0])
    #logger.info("power: {}".format(val))
    return val

class N1912A(labBase.PowerMeter):
  """
  Keysight N1912A P-Series Dual Channel Power Meter
  (http://www.keysight.com/en/pd-414280-pn-N1912A/p-series-dual-channel-power-meter?pm=PL&nid=-536902903.536894475&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(N1912A, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""

  def _getInfo(self):
    super(N1912A, self)._getInfo()

    self._idn["sensors"] = [None, None]
    res = self.query("SERV:SENS:TYPE?")
    if (len(res) != 0):
      self._idn["sensors"][0] = res[0]
    res = self.query("SERV:SENS2:TYPE?")
    if (len(res) != 0):
      self._idn["sensors"][1] = res[0]

    return self._idn

  def setUnit(self, val, chId=None):
    if (val not in ("dBm", "W")):
      msg = "INCORRECT unit: {}".format(val)
      logger.error(msg)
      raise RuntimeError(msg)

    if (chId in (1, 2)):
      self.write("UNIT{}:POWER {}".format(chId, val))
      self.waitOnCmd()
    else:
      raise NotImplementedError

  def getUnit(self, chId=None):
    if (chId in (1, 2)):
      res = self.query("UNIT{}:POWER?".format(chId))
      return res[0]
    else:
      raise NotImplementedError

  def setFreq(self, val, chId=None):
    pyauto_base.misc.chkFloat(val, valId="frequency", minVal=0.1, maxVal=110000.0)

    if (chId in (1, 2)):
      self.write("SENS{}:FREQ:CW {}".format(chId, val))
      self.waitOnCmd()
    else:
      raise NotImplementedError

  def getFreq(self, chId=None):
    if (chId in (1, 2)):
      res = self.query("SENS{}:FREQ:CW ?".format(chId))
      return float(res[0].split()[0]) / 1e6
    else:
      raise NotImplementedError

  def getPower(self, chId=None):
    if (chId in (1, 2)):
      res = self.query("FETCH{}?".format(chId))
      #logger.info(res)
      val = float(res[0])
      #logger.info("power: {}".format(val))
      return val
    else:
      raise NotImplementedError

class A_8375x(labBase.SignalGenerator):
  """
  Keysight 83752A Synthesized Sweeper
  (http://www.keysight.com/en/pd-1000001888%3Aepsg%3Apro-pn-83752A/synthesized-sweeper-001-20-ghz?pm=PL&nid=-32468.536879681&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("adapter_name" not in kwargs.keys()):
      kwargs["adapter_name"] = "prologix"
      kwargs["adapter_gpibAddr"] = 7
    super(A_8375x, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""

  def getUnit(self):
    res = self.query("UNIT:POWER?")
    return res[0]

  def setUnit(self, val):
    if (val not in ("dBm", "V")):
      msg = "INCORRECT unit: {}".format(val)
      logger.error(msg)
      raise RuntimeError(msg)

    self.write("UNIT:POWER {}".format(val))
    self.waitOnCmd()

  def enable(self, outId=None):
    self.write("POWER:STATE 1")
    res = self.query("POWER:STATE?")
    logger.info("output: {}".format(res[0]))

  def disable(self, outId=None):
    self.write("POWER:STATE 0")
    res = self.query("POWER:STATE?")
    logger.info("output: {}".format(res[0]))

  def CW(self, freq, pwr):
    f = pyauto_base.misc.chkFloat(freq, valId="frequency", minVal=1e7)
    p = pyauto_base.misc.chkFloat(pwr, valId="power", maxVal=10.0)

    self._output = False
    self.write("FREQUENCY:MODE CW")
    self.write("FREQUENCY:CW {}".format(f))

    self.write("POWER:MODE FIXED")
    self.write("POWER:LEVEL {}".format(p))

    res = self.query("FREQUENCY:CW?")
    f = float(res[0])
    res = self.query("POWER:LEVEL?")
    p = float(res[0])
    logger.info("CW: {} dB @ {} MHz".format(p, (f / 1e6)))

    self.waitOnCmd()
    self._output = True

  def sweepFreq(self, freq1, freq2, pwr, npoints=10, tsweep=1e-3):
    f1 = pyauto_base.misc.chkFloat(freq1, valId="min frequency", minVal=1e7)
    f2 = pyauto_base.misc.chkFloat(freq2, valId="max frequency", minVal=1e7)
    p = pyauto_base.misc.chkFloat(pwr, valId="power", maxVal=10.0)
    np = pyauto_base.misc.chkInt(npoints, valId="points", minVal=2)
    t = pyauto_base.misc.chkFloat(tsweep, valId="time", minVal=10e-6)

    if (f1 >= f2):
      msg = "INCORRECT sweep freq: {} >= {}".format(f1, f2)
      logger.error(msg)
      raise RuntimeError(msg)

    self._output = False
    self.write("FREQUENCY:MODE SWEEP")
    self.write("FREQUENCY:START {}".format(f1))
    self.write("FREQUENCY:STOP {}".format(f2))

    self.write("POWER:MODE FIXED")
    self.write("POWER:LEVEL {}".format(p))

    self.write("SWEEP:POINTS {}".format(np))
    res = self.query("SWEEP:TIME:LLIMIT?")
    tlim = float(res[0])
    if (t < tlim):
      logger.warning("INCORRECT sweep time: {} < {}".format(t, tlim))
    else:
      self.write("SWEEP:TIME {}us".format(t * 1e6))
    self.write("SWEEP:GENERATION STEP")
    self.write("SWEEP:MODE AUTO")
    #self.write("SWEEP:POINTS:TRIGGER:SOURCE IMMEDIATE")

    res = self.query("FREQUENCY:START?")
    f1 = float(res[0])
    res = self.query("FREQUENCY:STOP?")
    f2 = float(res[0])
    res = self.query("POWER:LEVEL?")
    p = float(res[0])
    logger.info("sweep freq: {} dB @ [{}, {}] MHz".format(p, (f1 / 1e6), (f2 / 1e6)))

    self.waitOnCmd()
    self.write("INITIATE:CONTINUOUS ON")
    self._output = True

  def sweepPower(self, freq, pwr1, pwr2, npoints=10, tsweep=1e-3):
    f = pyauto_base.misc.chkFloat(freq, valId="frequency", minVal=1e7)
    p1 = pyauto_base.misc.chkFloat(pwr1, valId="min power", minVal=10.0)
    p2 = pyauto_base.misc.chkFloat(pwr2, valId="max power", maxVal=10.0)
    np = pyauto_base.misc.chkInt(npoints, valId="points", minVal=2)
    t = pyauto_base.misc.chkFloat(tsweep, valId="time", minVal=10e-6)

    if (pwr1 >= pwr2):
      msg = "INCORRECT sweep pwr: {} >= {}".format(pwr1, pwr2)
      logger.error(msg)
      raise RuntimeError(msg)

    self._output = False
    self.write("FREQUENCY:MODE CW")
    self.write("FREQUENCY:CW {}".format(f))

    self.write("POWER:MODE SWEEP")
    self.write("POWER:START {}".format(p1))
    self.write("POWER:STOP {}".format(p2))

    self.write("SWEEP:POINTS {}".format(np))
    res = self.query("SWEEP:TIME:LLIMIT?")
    tlim = float(res[0])
    if (t < tlim):
      logger.warning("INCORRECT sweep time: {} < {}".format(t, tlim))
    else:
      self.write("SWEEP:TIME {}us".format(t * 1e6))
    self.write("SWEEP:GENERATION STEP")
    self.write("SWEEP:MODE AUTO")
    #self.write("SWEEP:POINTS:TRIGGER:SOURCE IMMEDIATE")

    res = self.query("FREQUENCY:CW?")
    f = float(res[0])
    res = self.query("POWER:START?")
    p1 = float(res[0])
    res = self.query("POWER:STOP?")
    p2 = float(res[0])
    logger.info("sweep pwr: [{}, {}] dB @ {} MHz".format(p1, p2, (f / 1e6)))

    self.waitOnCmd()
    self.write("INITIATE:CONTINUOUS ON")
    self._output = True

class MXG(labBase.SignalGenerator):
  """
  Keysight MXG X-Series Microwave Analog Signal Generator
  (http://www.keysight.com/en/pcx-x205194/x-series-signal-generators-mxg-exg?nid=-32490.0&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    if ("eos" not in kwargs.keys()):
      kwargs["eos"] = "\n"
    super(MXG, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True  # supports IEEE 488.2 but does not recommend using *OPC?
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error\""

  def _getInfo(self):
    super(MXG, self)._getInfo()

    self._idn["capabilities"] = self.query("SYSTEM:CAPABILITY?")[0]
    return self._idn

  def getOutputState(self, outId=None):
    res = self.query("OUTPUT:STATE?")[0]
    if (res == "0"):
      dictRes = {"0": False}
    elif (res == "1"):
      dictRes = {"0": True}
    else:
      msg = "UNEXPECTED reply: {}".format(res)
      logger.error(msg)
      raise RuntimeError(msg)

    for k in sorted(dictRes):
      logger.info("Output {}: {}".format(k, "ON" if (dictRes[k]) else "OFF"))
    return dictRes

  def setOutputState(self, outId=None, bON=False):
    if (bON):
      self.write("OUTPUT:STATE ON")
    else:
      self.write("OUTPUT:STATE OFF")

  def getUnit(self, unit="pwr"):
    if (unit == "pwr"):
      res = self.query("UNIT:POWER?")[0]
    else:
      msg = "UNEXPECTED unit: {}".format(unit)
      logger.error(msg)
      raise RuntimeError(msg)

    #logger.info("{} unit: {}".format(unit, res))
    return res

  def getFreqCW(self):
    return float(self.query("SOURCE:FREQUENCY:CW?")[0]) / 1e6

  def setFreqCW(self, val):
    self.write("SOURCE:FREQUENCY:CW {:f}MHz".format(val))

  def getPowerCW(self):
    return float(self.query("SOURCE:POWER:LEVEL:IMMEDIATE:AMPLITUDE?")[0])

  def setPowerCW(self, val):
    self.write("SOURCE:POWER:LEVEL:IMMEDIATE:AMPLITUDE {:.2f}".format(val))

  def setCW(self, freq, amp):
    self.write("SOURCE:FREQUENCY:MODE CW")
    #self.setRFOutputState()
    self.setFreqCW(freq)
    self.setPowerCW(amp)
    #self.setRFOutputState(bON=True)

    f = self.getFreqCW()
    p = self.getPowerCW()
    u = self.getUnit()
    #res = self.getErrors()
    #res = [t for t in res if(not t.startswith("-420"))]
    #for ln in res:
    #  logger.warning("error: {}".format(ln))
    logger.info("CW mode: {}{} @ {}MHz".format(p, u, f))

class A_8565EC(labBase.SpectrumAnalyzer):
  """
  Keysight 8565EC Portable Spectrum Analyzer
  (http://www.keysight.com/en/pd-1000002102%3Aepsg%3Apro-pn-8565EC/portable-spectrum-analyzer-9-khz-to-50-ghz?nid=-32441.536880941&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(A_8565EC, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry = {
        "id": "ID?",
        "err": "ERR?",
        "cf": "CF",
        "span": "SP",
        "ref": "RL",
        "div": "LG",
        "attn": "AT",
        "rbw": "RB",
        "vbw": "VB"
    }
    self.replyNoErr = "0"

  def _getInfo(self):
    res = self.IDN()
    tmp = res[0].split(",")
    self._idn = {"id": tmp[0], "opts": tmp[1]}
    return self._idn

class A_87xxES(labBase.NetworkAnalyzer):
  """
  Keysight 8753ES S-parameter Network Analyzer
  http://www.keysight.com/en/pd-1000002292%3Aepsg%3Apro-pn-8753ES/s-parameter-network-analyzer?pm=PL&nid=-32437.536881991&cc=CA&lc=eng
  """

  def __init__(self, strConnInfo, **kwargs):
    super(A_87xxES, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry.update({
        "id": "IDN?",
        "err": "OUTPERRO;",
        "stf": "STAR",
        "spf": "STOP",
        "div": "SCAL",
        "attn": None
    })
    self.replyNoErr = "    0 ,\"NO ERRORS\""

  def writeTouchstone(self, path, frmt="auto"):
    self.write("FORM4;")
    stf = float(self.stf)
    spf = float(self.spf)
    res = self.query("POIN?;")
    n_pts = int(float(res[0]))

    freq_step = (spf - stf) / (n_pts - 1)
    lstFreq = []
    for i in range(0, n_pts):
      lstFreq.append(stf + i * freq_step)

    tFile = pyauto_lab.data.file.TouchstoneFile(path)
    tFile.optionLine = "# Hz S RI R 50"
    tFile.setFreq(lstFreq)

    for cmd in ("S11;", "S12;", "S21;", "S22;"):
      logger.info("== {} {} point(s)".format(cmd, n_pts))
      self.write(cmd)
      self.write("SMIC;")
      self.write("OPC?;SING;")
      self.waitOnCmd(timeout=40)  # lots of points sometimes ...

      tmp = self._rsrcObj.gpibEOM
      self._rsrcObj.gpibEOM = None
      for i in range(0, 4):
        logger.info("read values ({})".format(i))
        res = self.query("OUTPFORM;", TOmultiplier=4)
        res = [t.split(",") for t in res]
        if (len(res) == n_pts):
          break
      if (i == 2):
        raise RuntimeError
      self._rsrcObj.gpibEOM = tmp

      tFile.setParam(int(cmd[1]), int(cmd[2]), res)
    tFile.write()
    logger.info("{} written".format(path))
    self.write("CONT;")
