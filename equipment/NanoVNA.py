#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library for NanoVNA"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import typing

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

class NanoVNA(labBase.NetworkAnalyzer):

  def __init__(self, strConnInfo, **kwargs):
    super(NanoVNA, self).__init__(strConnInfo, speed=115200, eos="\r", eom="> ")

    self.IEEE_488_2 = False
    self.SCPI = False
    self.qry = {"id": "info", "err": "NA"}
    #self.replyNoErr = None

  def _getInfo(self):
    self._idn = {}
    res = self.query(self.qry["id"])
    #print("DBG", res)
    for ln in res:
      if (ln.startswith("Board: ")):
        self._idn["id"] = ln[7:].strip()
      if (ln.startswith("Version: ")):
        self._idn["ver"] = ln[9:].strip()
      if (ln.startswith("Platform: ")):
        self._idn["platform"] = ln[10:].strip()
