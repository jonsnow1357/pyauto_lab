#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

class Ctrl2800(labBase.ThermalChamber):
  """
  Thermotron 2800 Thermal Chamber
  """

  def __init__(self, strConnInfo, **kwargs):
    super(Ctrl2800, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry = {"id": "DID", "err": "DEC"}
    self.replyNoErr = "+0000"

    self.tempMax = 175
    self.tempMin = -70
    self.tempTol = 1.5
    self.tempRamp = 2.0  # C/minute

  def setState(self, val):
    if (val == "stop"):
      self.write("S")
    elif (val == "run"):
      self.write("RM")
    else:
      logger.warning("UNRECOGNIZED state: {}".format(val))
      return
    pyauto_base.misc.waitWithPrint(2)

  def getStatus(self):
    dictRes = {}
    dictState = {
        0: "STOP",
        1: "Starting",
        2: "RUN Manual",
        3: "RUN Program",
        4: "HOLD",
        5: "Programming",
        6: "Set Up",
        7: "Delayed Start"
    }
    res = self.query("DST")
    temp = int(res[0])
    dictRes["status"] = "b{:0>8b}".format(temp)
    dictRes["state"] = dictState[temp % 8]
    res = self.query("DAL")
    dictRes["alarm"] = "b{:0>8b}".format(int(res[0]))
    #logger.info(dictRes)
    return dictRes

  def showStatus(self):
    logger.info("Thermotron status: {}".format(self.getStatus()))

  def _getInfo(self):
    super(Ctrl2800, self)._getInfo()

    self._idn.update(self.getStatus())
    #self._idn.update(self.getTemperature())
    return self._idn

  @property
  def temperature(self):
    dictTemp = {}
    res = self.query("DTV")
    dictTemp["chamber"] = "{:.1f} C".format(float(res[0]))
    res = self.query("DTS")
    dictTemp["setpoint"] = "{:.1f} C".format(float(res[0]))
    #logger.info(dictTemp)
    return dictTemp

  def setTemperature(self, temp):
    if ((temp < self.tempMin) or (temp > self.tempMax)):
      logger.warning("setpoint OUT OF RANGE: {} <> [{}, {}]".format(
          temp, self.tempMin, self.tempMax))
      return
    self.write("LTS{:+.1f}".format(temp))

  def setTemperature_Wait(self, temp, t_settle=120):
    """
    :param temp: target temperature
    :param t_settle: settle time at target temperature
    """
    logger.info("-- set temperature {}C @ {}".format(temp, pyauto_base.misc.getTimestamp()))
    dictTemp = self.temperature
    temp_ch = float(dictTemp["chamber"][:-2])
    t_change = 60 * (int(math.fabs(temp - temp_ch) / self.tempRamp) + 2)
    logger.info("-- estimated time: {} change + {} settle (from {}C)".format(
        datetime.timedelta(seconds=t_change), datetime.timedelta(seconds=t_settle),
        temp_ch))
    t_timeout = t_change + t_settle

    self.setTemperature(temp)
    t_st = datetime.datetime.now()
    t_done = None
    while (True):
      if ((t_done is None) and (math.fabs(temp_ch - temp) < self.tempTol)):
        t_done = datetime.datetime.now()
        logger.info("-- temperature in range {}C @ {}".format(
            temp_ch, pyauto_base.misc.getTimestamp()))
      elif ((t_done is not None) and (math.fabs(temp_ch - temp) >= self.tempTol)):
        t_done = None
        logger.info("-- temperature OUT OF range {}C @ {}".format(
            temp_ch, pyauto_base.misc.getTimestamp()))
      #logger.info("{}, t_done: {}".format(dictTemp, t_done))

      if (t_done is not None):
        dt = (datetime.datetime.now() - t_done)
        if (dt.total_seconds() > t_settle):
          logger.info("-- settle time: {}".format(dt))
          break
      pyauto_base.misc.waitWithPrint(4, to=5)

      dt = (datetime.datetime.now() - t_st)
      if (dt.total_seconds() > t_timeout):
        msg = "CANNOT set temperature in {}s".format(dt.total_seconds())
        logger.error(msg)
        raise RuntimeError(msg)
      dictTemp = self.temperature
      temp_ch = float(dictTemp["chamber"][:-2])

    logger.info("-- temperature is {}C @ {}".format(temp_ch,
                                                    pyauto_base.misc.getTimestamp()))

C8STS_RUNP = "Run Program"
C8STS_RUNM = "Run Manual"
C8STS_HOLDP = "Hold Program"
C8STS_HOLDM = "Hold Manual"
C8STS_SUSPEND = "Suspend Program"
C8STS_NONE = "Undefined"

class Ctrl8x00(labBase.ThermalChamber):
  """
  Thermotron Thermal Chamber with 8200/8800 controller
  (https://thermotron.com/resources/8800-controller/)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(Ctrl8x00, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry = {"id": "*IDN?", "err": "IERR?"}
    self.replyNoErr = "0"
    # self._erroCode = {
    #     "00": "No Error",
    #     "01": "Serial interface error",
    #     "02": "Input buffer overflow",
    #     "03": "Output buffer overflow",
    #     "04": "Unknown command",
    #     "05": "Number parser error",
    #     "06": "Value loaded was too high",
    #     "07": "Value loaded was too low",
    #     "08": "Incorrect channel number",
    #     "09": "Bad command syntax",
    #     "10": "Illegal program command",
    #     "11": "Illegal interval number sequence",
    #     "12": "Not enough program memory",
    #     "13": "Illegal stop command",
    #     "14": "Illegal hold command",
    #     "15": "Illegal run manual command",
    #     "16": "Incorrect operating mode",
    #     "17": "Run program error",
    #     "18": "Resume command error",
    #     "19": "Options not configured",
    #     "21": "Control module not present"
    # }

    self.targetCh = 1
    self.tempReadLim = [-50.0, 120.0]

  @property
  def temperature(self):
    res = self.query("PVAR{}?".format(self.targetCh))
    return float(res[0])

  @property
  def status(self):
    res = self.query("STAT?")
    sts = int(res[0])
    if (sts == 1):
      return C8STS_RUNP
    elif (sts == 2):
      return C8STS_HOLDP
    elif (sts == 4):
      return C8STS_SUSPEND
    elif (sts == 16):
      return C8STS_RUNM
    elif (sts == 32):
      return C8STS_HOLDM
    else:
      return C8STS_NONE

  @property
  def setPoint(self):
    res = self.query("SETP{}?".format(self.targetCh))
    return float(res[0])

  @setPoint.setter
  def setPoint(self, val):
    res = self.query("SETP{},{}".format(self.targetCh, val))
    if (res[0] != "0"):
      msg = "cmd ERROR"
      logger.error(msg)
      raise RuntimeError(msg)

  @property
  def tempRate(self):
    res = self.query("MRMP{}?".format(self.targetCh))
    return float(res[0])

  @tempRate.setter
  def tempRate(self, val):
    res = self.query("MRMP{},{}".format(self.targetCh, val))
    if (res[0] != "0"):
      msg = "cmd ERROR"
      logger.error(msg)
      raise RuntimeError(msg)

  def showChannels(self):
    for i in range(8):
      res = self.query("CNAM{}?".format(i + 1))
      logger.info("CH{}: {}".format((i + 1), res[0]))

  def start(self):
    sts = self.status
    if (sts.startswith("Run")):
      return

    res = self.query("RUNM")
    if (res[0] != "0"):
      msg = "cmd ERROR"
      logger.error(msg)
      raise RuntimeError(msg)

    logger.info("wait for start ...")
    tlim = datetime.timedelta(seconds=(30 * 60))
    t0 = datetime.datetime.now()
    dt = datetime.datetime.now() - t0
    while (dt < tlim):
      sts = self.status
      if (sts.startswith("Run")):
        logger.info("started")
        return
      pyauto_base.misc.waitWithPrint(10)
    msg = "start TIMEOUT"
    logger.error(msg)
    raise RuntimeError(msg)

  def stop(self):
    sts = self.status
    if (sts == C8STS_NONE):
      return

    res = self.query("STOP")
    if (res[0] != "0"):
      msg = "cmd ERROR"
      logger.error(msg)
      raise RuntimeError(msg)

    logger.info("wait for stop ...")
    tlim = datetime.timedelta(seconds=(30 * 60))
    t0 = datetime.datetime.now()
    dt = datetime.datetime.now() - t0
    while (dt < tlim):
      sts = self.status
      if (sts == C8STS_NONE):
        logger.info("stopped")
        return
      pyauto_base.misc.waitWithPrint(10)
    msg = "stop TIMEOUT"
    logger.error(msg)
    raise RuntimeError(msg)

  def chkStatus(self):
    set_point = self.setPoint
    curr_temp = self.temperature
    if ((curr_temp < self.tempReadLim[0]) or (curr_temp > self.tempReadLim[1])):
      msg = "temp FAILURE {} C".format(curr_temp)
      logger.error(msg)
      self.stop()
      raise labBase.LabEqptException(msg)

    logger.info("temperature {} C / set point {} C ({})".format(curr_temp, set_point,
                                                                self.status))
    return [curr_temp, set_point]

  def tempRateEn(self):
    pass

  def tempRateDis(self):
    pass
