#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import ctypes

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.bin
import pyauto_lab.equipment.base as labBase

class ML4062(labBase.IfAdapter):

  def __init__(self, strConnInfo, **kwargs):
    super(ML4062, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry = {"id": None, "err": "NA"}
    #self.replyNoErr = None
    self._idn["I2C"] = True
    self.maxBytes = 32

  def connect(self):
    arch = platform.architecture()[0]
    if (not sys.platform.startswith("win")):
      msg = "UNSUPPORTED architecture: {} {}".format(sys.platform, arch)
      logger.error(msg)
      raise RuntimeError(msg)
    if (arch == "32bit"):
      self._params["dllPath"] = os.path.abspath(
          os.path.join(os.path.dirname(__file__), "../../opt/ml4062/QSFP_DD(x32).dll"))
    elif (arch == "64bit"):
      self._params["dllPath"] = os.path.abspath(
          os.path.join(os.path.dirname(__file__), "../../opt/ml4062/QSFP_DD(x64).dll"))
    else:
      msg = "UNSUPPORTED architecture: {} {}".format(sys.platform, arch)
      logger.error(msg)
      raise RuntimeError(msg)
    pyauto_base.fs.chkPath_File(self._params["dllPath"])
    logger.info("using: {}".format(self._params["dllPath"]))
    self._params["dll"] = ctypes.CDLL(self._params["dllPath"])
    self._params["inst"] = ctypes.c_uint16(0)

    res = self._params["dll"].ConnectToHost(self._params["inst"])
    if (res == 0):
      msg = "CANNOT find MultiLane devices"
      logger.error(msg)
      raise RuntimeError(msg)
    logger.info("connected to MultiLane instance {:d}".format(self._params["inst"].value))

    pin_ms = ctypes.c_bool(True)
    res = self._params["dll"].MODSEL(self._params["inst"], pin_ms)
    if (res == 0):
      raise RuntimeError

  def disconnect(self):
    res = self._params["dll"].Disconnect(self._params["inst"])
    if (res == 0):
      msg = "CANNOT find MultiLane devices"
      logger.error(msg)
      raise RuntimeError(msg)
    logger.info("disconnected from MultiLane")

  def showPower(self):
    vcc = ctypes.c_double(0.0)
    res = self._params["dll"].GetVCC(self._params["inst"], ctypes.byref(vcc))
    if (res == 0):
      raise RuntimeError
    i = ctypes.c_double(0.0)
    res = self._params["dll"].P3V3_Current_Monitor(self._params["inst"], ctypes.byref(i))
    if (res == 0):
      raise RuntimeError
    logger.info("VCC: {:.3f} V / {:.3f} A".format(vcc.value, i.value))

  def showPins(self):
    pin_prs = ctypes.c_bool(False)
    res = self._params["dll"].MODPRS(self._params["inst"], ctypes.byref(pin_prs))
    if (res == 0):
      raise RuntimeError
    pin_int = ctypes.c_bool(False)
    res = self._params["dll"].IntL(self._params["inst"], ctypes.byref(pin_int))
    if (res == 0):
      raise RuntimeError

    # yapf: disable
    dict_pins = {
        "MODPRS_L": 1 if pin_prs else 0,
        "INT_L": 1 if pin_int else 0
    }
    # yapf: enable
    logger.info(dict_pins)

  def showInfo(self):
    super(ML4062, self).showInfo()
    self.showPower()
    self.showPins()

  def modStop(self):
    pin_rst = ctypes.c_bool(True)
    res = self._params["dll"].RESET(self._params["inst"], pin_rst)
    if (res == 0):
      raise RuntimeError
    pin_lpm = ctypes.c_bool(True)
    res = self._params["dll"].LPMODE(self._params["inst"], pin_lpm)
    if (res == 0):
      raise RuntimeError

    # yapf: disable
    dict_pins = {
        "RESET_L": 0 if pin_rst else 1,
        "LPMODE": 1 if pin_lpm else 0,
    }
    # yapf: enable
    logger.info(dict_pins)

  def modStart(self):
    pin_rst = ctypes.c_bool(False)
    res = self._params["dll"].RESET(self._params["inst"], pin_rst)
    if (res == 0):
      raise RuntimeError
    pin_lpm = ctypes.c_bool(True)
    res = self._params["dll"].LPMODE(self._params["inst"], pin_lpm)
    if (res == 0):
      raise RuntimeError

    # yapf: disable
    dict_pins = {
        "RESET_L": 0 if pin_rst else 1,
        "LPMODE": 1 if pin_lpm else 0,
    }
    # yapf: enable
    logger.info(dict_pins)

  def modPStart(self):
    pin_rst = ctypes.c_bool(False)
    res = self._params["dll"].RESET(self._params["inst"], pin_rst)
    if (res == 0):
      raise RuntimeError
    pin_lpm = ctypes.c_bool(False)
    res = self._params["dll"].LPMODE(self._params["inst"], pin_lpm)
    if (res == 0):
      raise RuntimeError

    # yapf: disable
    dict_pins = {
        "RESET_L": 0 if pin_rst else 1,
        "LPMODE": 1 if pin_lpm else 0,
    }
    # yapf: enable
    logger.info(dict_pins)

  def i2cCfg(self, speed: int = 100, bPullUp: bool = False):
    pass

  def smbusWr8(self, i2cAddr: int, cmd: int, data: int):
    if ((i2cAddr < 1) or (i2cAddr > 126)):
      msg = "INCORRECT I2C addr: {}".format(i2cAddr)
      logger.error(msg)
      raise RuntimeError(msg)
    i2cAddr *= 2  # API expects 8 bit I2C address

    i2c_addr = ctypes.c_uint8(i2cAddr)
    reg_addr = ctypes.c_uint8(cmd)
    data = ctypes.c_uint8(data)

    res = self._params["dll"].I2CWrite(self._params["inst"], i2c_addr, reg_addr, data)
    if (res == 0):
      raise RuntimeError

  def smbusRdBlock(self, i2cAddr: int, cmd: int, nBytes: int) -> list[int]:
    if ((i2cAddr < 1) or (i2cAddr > 256)):
      msg = "INCORRECT I2C addr: {}".format(i2cAddr)
      logger.error(msg)
      raise RuntimeError(msg)
    i2cAddr *= 2  # API expects 8 bit I2C address
    if ((nBytes < 1) or (nBytes > self.maxBytes)):
      msg = "INCORRECT I2C nBytes: {}".format(nBytes)
      logger.error(msg)
      raise RuntimeError(msg)

    i2c_addr = ctypes.c_uint8(i2cAddr)
    reg_addr = ctypes.c_uint8(cmd)
    buff = (ctypes.c_uint8 * nBytes)(0)
    nb = ctypes.c_uint8(nBytes)

    res = self._params["dll"].I2CRead(self._params["inst"], i2c_addr, reg_addr,
                                      ctypes.byref(buff), nb)
    if (res == 0):
      raise RuntimeError
    lstBytes = [t for t in buff]
    #logger.info(lstBytes)
    return lstBytes

class ML4064(labBase.IfAdapter):

  def __init__(self, strConnInfo, **kwargs):
    super(ML4064, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry = {"id": None, "err": "NA"}
    #self.replyNoErr = None
    self._idn["I2C"] = True
    self.maxBytes = 32

  def connect(self):
    arch = platform.architecture()[0]
    if (not sys.platform.startswith("win")):
      msg = "UNSUPPORTED architecture: {} {}".format(sys.platform, arch)
      logger.error(msg)
      raise RuntimeError(msg)
    if (arch == "32bit"):
      self._params["dllPath"] = os.path.abspath(
          os.path.join(os.path.dirname(__file__), "../../opt/ml4064/ML4064_API(x86).dll"))
    elif (arch == "64bit"):
      self._params["dllPath"] = os.path.abspath(
          os.path.join(os.path.dirname(__file__), "../../opt/ml4064/ML4064_API(x64).dll"))
    else:
      msg = "UNSUPPORTED architecture: {} {}".format(sys.platform, arch)
      logger.error(msg)
      raise RuntimeError(msg)
    pyauto_base.fs.chkPath_File(self._params["dllPath"])
    logger.info("using: {}".format(self._params["dllPath"]))
    self._params["dll"] = ctypes.CDLL(self._params["dllPath"])
    self._params["inst"] = ctypes.c_uint16(0)

    res = self._params["dll"].ConnectToHost(self._params["inst"])
    if (res == 0):
      msg = "CANNOT find MultiLane devices"
      logger.error(msg)
      raise RuntimeError(msg)
    logger.info("connected to MultiLane instance {:d}".format(self._params["inst"].value))

  def disconnect(self):
    res = self._params["dll"].Disconnect(self._params["inst"])
    if (res == 0):
      msg = "CANNOT find MultiLane devices"
      logger.error(msg)
      raise RuntimeError(msg)
    logger.info("disconnected from MultiLane")

  def showPower(self):
    vcc = ctypes.c_double(0.0)
    res = self._params["dll"].GetVCC(self._params["inst"], ctypes.byref(vcc))
    if (res == 0):
      raise RuntimeError
    i = ctypes.c_uint16(0)
    res = self._params["dll"].P3V3_Current_Monitor(self._params["inst"], ctypes.byref(i))
    if (res == 0):
      raise RuntimeError
    logger.info("VCC: {:.3f} V / {:d} mA".format(vcc.value, i.value))

  def showPins(self):
    pin_prs = ctypes.c_bool(False)
    res = self._params["dll"].MODPRSn(self._params["inst"], ctypes.byref(pin_prs))
    if (res == 0):
      raise RuntimeError
    pin_int = ctypes.c_bool(False)
    res = self._params["dll"].IntL(self._params["inst"], ctypes.byref(pin_int))
    if (res == 0):
      raise RuntimeError

    # yapf: disable
    dict_pins = {
        "PRS_L": 1 if pin_prs else 0,
        "INT_L": 1 if pin_int else 0
    }
    # yapf: enable
    logger.info(dict_pins)

  def showInfo(self):
    super(ML4064, self).showInfo()
    self.showPower()
    self.showPins()

  def modStop(self):
    pin_rst = ctypes.c_bool(False)
    res = self._params["dll"].RESETn(self._params["inst"], pin_rst)
    if (res == 0):
      raise RuntimeError
    pin_lpw = ctypes.c_bool(False)
    res = self._params["dll"].LPMODEn(self._params["inst"], pin_lpw)
    if (res == 0):
      raise RuntimeError

    # yapf: disable
    dict_pins = {
        "RESET_L": 1 if pin_rst else 0,
        "LPMODE_L": 1 if pin_lpw else 0,
    }
    # yapf: enable
    logger.info(dict_pins)

  def modStart(self):
    pin_rst = ctypes.c_bool(True)
    res = self._params["dll"].RESETn(self._params["inst"], pin_rst)
    if (res == 0):
      raise RuntimeError
    pin_lpw = ctypes.c_bool(False)
    res = self._params["dll"].LPMODEn(self._params["inst"], pin_lpw)
    if (res == 0):
      raise RuntimeError

    # yapf: disable
    dict_pins = {
        "RESET_L": 1 if pin_rst else 0,
        "LPMODE_L": 1 if pin_lpw else 0,
    }
    # yapf: enable
    logger.info(dict_pins)

  def modPStart(self):
    pin_rst = ctypes.c_bool(True)
    res = self._params["dll"].RESETn(self._params["inst"], pin_rst)
    if (res == 0):
      raise RuntimeError
    pin_lpw = ctypes.c_bool(True)
    res = self._params["dll"].LPMODEn(self._params["inst"], pin_lpw)
    if (res == 0):
      raise RuntimeError

    # yapf: disable
    dict_pins = {
        "RESET_L": 1 if pin_rst else 0,
        "LPMODE_L": 1 if pin_lpw else 0,
    }
    # yapf: enable
    logger.info(dict_pins)

  def i2cCfg(self, speed: int = 100, bPullUp: bool = False):
    pass

  def smbusWr8(self, i2cAddr: int, cmd: int, data: int):
    if ((i2cAddr < 1) or (i2cAddr > 126)):
      msg = "INCORRECT I2C addr: {}".format(i2cAddr)
      logger.error(msg)
      raise RuntimeError(msg)
    i2cAddr *= 2  # API expects 8 bit I2C address

    i2c_addr = ctypes.c_uint8(i2cAddr)
    reg_addr = ctypes.c_uint8(cmd)
    data = ctypes.c_uint8(data)

    res = self._params["dll"].I2CWrite(self._params["inst"], i2c_addr, reg_addr, data)
    if (res == 0):
      raise RuntimeError

  def smbusRdBlock(self, i2cAddr: int, cmd: int, nBytes: int) -> list[int]:
    if ((i2cAddr < 1) or (i2cAddr > 256)):
      msg = "INCORRECT I2C addr: {}".format(i2cAddr)
      logger.error(msg)
      raise RuntimeError(msg)
    i2cAddr *= 2  # API expects 8 bit I2C address
    if ((nBytes < 1) or (nBytes > self.maxBytes)):
      msg = "INCORRECT I2C nBytes: {}".format(nBytes)
      logger.error(msg)
      raise RuntimeError(msg)

    i2c_addr = ctypes.c_uint8(i2cAddr)
    reg_addr = ctypes.c_uint8(cmd)
    buff = (ctypes.c_uint8 * nBytes)(0)
    nb = ctypes.c_uint8(nBytes)

    res = self._params["dll"].I2CRead(self._params["inst"], i2c_addr, reg_addr,
                                      ctypes.byref(buff), nb)
    if (res == 0):
      raise RuntimeError
    lstBytes = [t for t in buff]
    #logger.info(lstBytes)
    return lstBytes
