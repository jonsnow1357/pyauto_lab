#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class HHF1000(labBase.LabEqpt):
  """
  OMEGA HHF1000 Wireless Handheld Air Temperature & Velocity Meter
  (https://www.omega.com/en-us/test-inspection/handheld-meters/anemometers/p/HHF1000-Series)
  """

  def __init__(self, strConnInfo, **kwargs):
    #if ("eom" not in kwargs.keys()):
    #  kwargs["eom"] = "\n"
    super(HHF1000, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry["err"] = "NA"
    #self.replyNoErr = None
    #self.nOut = 1

  def _getInfo(self):
    res = self.query("vr")
    self._idn = {"id": res[0]}
    return self._idn

  def getStats(self, nSamples=10):
    lstAF = []
    lstT = []
    logger.info("reading {} sample(s)".format(nSamples))
    for i in range(nSamples):
      res = self.query("rv")
      tmp = res[0].split(";")
      v1 = float(tmp[0][4:])
      lstAF.append(v1)
      v2 = float(tmp[1][3:]) / 10
      v2 = (v2 - 32) / 1.8  # convert to Celsius
      lstT.append(v2)
    res = {
        "airflow_min": min(lstAF),
        "airflow_avg": (sum(lstAF) / nSamples),
        "airflow_max": max(lstAF),
        "temp_min": min(lstT),
        "temp_avg": (sum(lstT) / nSamples),
        "temp_max": max(lstT)
    }
    return res
