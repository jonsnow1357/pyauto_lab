#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
from ._base_eqpt import *
from ._base_power import *

u_dBm = "dBm"
u_dBmV = "dBmV"
u_dBuV = "dBuV"
u_W = "W"
u_V = "V"

lstUnits = (u_dBm, u_dBmV, u_dBuV, u_W, u_V)

class PowerMeter(LabEqpt):

  def __init__(self, strConnInfo, **kwargs):
    super(PowerMeter, self).__init__(strConnInfo, **kwargs)
    self.nOut = 1

  def _chkChannelId(self, outId):
    chkChannelId(outId, self.nOut)

  def setUnit(self, val, chId=None):
    raise NotImplementedError

  def getUnit(self, chId=None):
    raise NotImplementedError

  def setFreq(self, val, chId=None):
    raise NotImplementedError

  def getFreq(self, chId=None):
    raise NotImplementedError

  def setWavelen(self, val, chId=None):
    raise NotImplementedError

  def getWavelen(self, chId=None):
    raise NotImplementedError

  def getPower(self, chId=None):
    raise NotImplementedError

class SpectrumAnalyzer(LabEqpt):

  def __init__(self, strConnInfo, **kwargs):
    super(SpectrumAnalyzer, self).__init__(strConnInfo, **kwargs)
    self.qry.update({
        "cf": None,
        "span": None,
        "ref": None,
        "div": None,
        "attn": None,
        "rbw": None,
        "vbw": None
    })

    self._cf = None
    self._span = None
    self._ref = None
    self._div = None
    self._attn = None
    self._rbw = None
    self._vbw = None

  @property
  def cf(self):
    res = self.query(self.qry["cf"] + "?")
    self._cf = res[0]
    return self._cf

  @cf.setter
  def cf(self, val):
    self.write(self.qry["cf"] + " " + val)
    self._cf = None

  @property
  def span(self):
    res = self.query(self.qry["span"] + "?")
    self._span = res[0]
    return self._span

  @span.setter
  def span(self, val):
    self.write(self.qry["span"] + " " + val)

  @property
  def ref(self):
    res = self.query(self.qry["ref"] + "?")
    self._ref = res[0]
    return self._ref

  @ref.setter
  def ref(self, val):
    self.write(self.qry["ref"] + " " + val)

  @property
  def div(self):
    res = self.query(self.qry["div"] + "?")
    self._div = res[0]
    return self._div

  @div.setter
  def div(self, val):
    raise NotImplementedError

  @property
  def attn(self):
    res = self.query(self.qry["attn"] + "?")
    self._attn = res[0]
    return self._attn

  @attn.setter
  def attn(self, val):
    self.write(self.qry["attn"] + " " + val)

  @property
  def rbw(self):
    res = self.query(self.qry["rbw"] + "?")
    self._rbw = res[0]
    return self._rbw

  @rbw.setter
  def rbw(self, val):
    raise NotImplementedError

  @property
  def vbw(self):
    res = self.query(self.qry["vbw"] + "?")
    self._vbw = res[0]
    return self._vbw

  @vbw.setter
  def vbw(self, val):
    raise NotImplementedError

  def getPeak(self, cf, span, ref):
    """
    :param str cf: center frequency
    :param str span: span
    :param str ref: reference level (top of screen)
    :return: [frequency (in Hz), power (in dB)]
    """
    raise NotImplementedError

  def writeImg(self, path):
    raise NotImplementedError

  def writeCSV(self, path, frmt="auto"):
    raise NotImplementedError

class NetworkAnalyzer(LabEqpt):

  def __init__(self, strConnInfo, **kwargs):
    super(NetworkAnalyzer, self).__init__(strConnInfo, **kwargs)
    self._stf = None
    self._spf = None
    self._ref = None
    self._div = None
    self._attn = None

  @property
  def stf(self):
    res = self.query(self.qry["stf"] + "?")
    self._stf = res[0]
    return self._stf

  @stf.setter
  def stf(self, val):
    raise NotImplementedError

  @property
  def spf(self):
    res = self.query(self.qry["spf"] + "?")
    self._spf = res[0]
    return self._spf

  @spf.setter
  def spf(self, val):
    raise NotImplementedError

  @property
  def ref(self):
    res = self.query(self.qry["ref"] + "?")
    self._ref = res[0]
    return self._ref

  @ref.setter
  def ref(self, val):
    raise NotImplementedError

  @property
  def div(self):
    res = self.query(self.qry["div"] + "?")
    self._div = res[0]
    return self._div

  @div.setter
  def div(self, val):
    raise NotImplementedError

  @property
  def attn(self):
    res = self.query(self.qry["attn"] + "?")
    self._attn = res[0]
    return self._attn

  @attn.setter
  def attn(self, val):
    raise NotImplementedError

  def readState(self, path):
    raise NotImplementedError

  def writeState(self, path):
    raise NotImplementedError

  def writeTouchstone(self, path, frmt="auto"):
    """
    :param str path: file path
    :param str frmt: file format:
      * "auto" - default settings from equipment
      * "lin" - linear magnitude/degrees
      * "log" - log magnitude/degrees
      * "ri" - real/imaginary
    """
    raise NotImplementedError

class SignalGenerator(LabEqpt):

  def __init__(self, strConnInfo, **kwargs):
    super(SignalGenerator, self).__init__(strConnInfo, **kwargs)
    self._unit = None
    self._output = None
    self.nOut = 1

  def _chkChannelId(self, outId):
    chkChannelId(outId, self.nOut)

  def enable(self, outId=None):
    raise NotImplementedError

  def disable(self, outId=None):
    raise NotImplementedError

  def getUnit(self):
    raise NotImplementedError

  def setUnit(self, val):
    raise NotImplementedError

  def getFreq(self, outId=None):
    raise NotImplementedError

  def setFreq(self, val, outId=None):
    raise NotImplementedError

  def getPwr(self, outId=None):
    raise NotImplementedError

  def setPwr(self, val, outId=None):
    raise NotImplementedError

  def CW(self, freq, ampl, outId=None):
    raise NotImplementedError

  def sweepFreq(self, freq1, freq2, pwr, npoints=10, tsweep=1e-3):
    raise NotImplementedError

  def sweepPower(self, freq, pwr1, pwr2, npoints=10, tsweep=1e-3):
    raise NotImplementedError
