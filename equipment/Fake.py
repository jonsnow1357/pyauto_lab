#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json
import random
import requests

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

class Multimeter(labBase.Multimeter):

  def __init__(self, strConnInfo, **kwargs):
    super(Multimeter, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = False
    self.SCPI = False
    self.qry = {"id": None, "err": ""}
    self.replyNoErr = ""

    self.nOut = 2

  def _getInfo(self):
    self._idn = {"id": "fake Multimeter ({} x CH)".format(self.nOut)}
    return self._idn

  def _getErrors(self, TOmultiplier=2):
    return []

  def setChannel(self, mode, chId=None):
    self._chkChannelId(chId)
    logger.info("set CH{}: {}".format(chId, mode))

  def getChannel(self, chId=None):
    self._chkChannelId(chId)
    logger.info("get CH{}".format(chId))
    return {"mode": random.choice(labBase.lstMultimeterMode), "val": random.random()}

class PowerSupply(labBase.PowerSupply):

  def __init__(self, strConnInfo, **kwargs):
    super(PowerSupply, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = False
    self.SCPI = False
    self.qry = {"id": None, "err": ""}
    self.replyNoErr = ""

    self.nOut = 2

  def _getInfo(self):
    self._idn = {"id": "fake PowerLoad ({} x OUT)".format(self.nOut)}
    return self._idn

  def _getErrors(self, TOmultiplier=2):
    return []

  def setOutput(self, voltage, current, outId=None):
    self._chkChannelId(outId)
    logger.info("set OUT{}: {} V, {} A".format(outId, voltage, current))

  def setProtection(self, voltage, current, outId=None):
    self._chkChannelId(outId)
    logger.info("set protection OUT{}: {} V, {} A".format(outId, voltage, current))

  def getOutput(self, outId=None):
    self._chkChannelId(outId)
    logger.info("get OUT{}".format(outId))
    return {"voltage": random.random(), "current": random.random()}

  def enable(self, outId=None):
    self._chkChannelId(outId)
    logger.info("enable OUT{}".format(outId))

  def disable(self, outId=None):
    self._chkChannelId(outId)
    logger.info("disable OUT{}".format(outId))

class PowerLoad(labBase.PowerLoad):

  def __init__(self, strConnInfo, **kwargs):
    super(PowerLoad, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = False
    self.SCPI = False
    self.qry = {"id": None, "err": ""}
    self.replyNoErr = ""

    self.nOut = 2

  def _getInfo(self):
    self._idn = {"id": "fake PowerLoad ({} x CH)".format(self.nOut)}
    return self._idn

  def _getErrors(self, TOmultiplier=2):
    return []

  def setChannel(self, mode, value, chId=None):
    self._chkChannelId(chId)
    logger.info("set CH{}: {}, {}".format(chId, mode, value))

  def getChannel(self, chId=None):
    self._chkChannelId(chId)
    logger.info("get CH{}".format(chId))
    return {"mode": random.choice(labBase.lstPwrLoadModes), "val": random.random()}

  def enable(self, chId=None):
    self._chkChannelId(chId)
    logger.info("enable CH{}".format(chId))

  def disable(self, chId=None):
    self._chkChannelId(chId)
    logger.info("disable CH{}".format(chId))

class ThermalChamber(labBase.ThermalChamber):

  def __init__(self, strConnInfo, **kwargs):
    super(ThermalChamber, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = False
    self.SCPI = False
    self.qry = {"id": None, "err": "/errors"}
    self.replyNoErr = ""

  def _getInfo(self):
    self._idn = {"id": "fake ThermalChamber"}
    return self._idn

  def _getErrors(self, TOmultiplier=2):
    res = self._rsrcObj.get(url=self.qry["err"])
    return json.loads(res)

  @property
  def temperature(self):
    res = self._rsrcObj.get(url="/temperature")
    return json.loads(res)

  @property
  def status(self):
    res = self._rsrcObj.get(url="/status")
    return json.loads(res)

  @property
  def setPoint(self):
    res = self._rsrcObj.get(url="/setPoint")
    return float(res)

  @setPoint.setter
  def setPoint(self, val, **kwargs):
    self._rsrcObj.post(url="/setPoint", data=str(val))

  @property
  def tempRate(self):
    res = self._rsrcObj.get(url="/tempRate")
    return float(res)

  @tempRate.setter
  def tempRate(self, val, **kwargs):
    self._rsrcObj.post(url="/tempRate", data=str(val))

  def start(self):
    self._rsrcObj.post(url="/start")

  def stop(self):
    self._rsrcObj.post(url="/stop")

  def chkStatus(self):
    curr_temp = self.temperature["temperature"]
    set_point = self.setPoint
    at_set_point = True if (curr_temp == set_point) else False

    logger.info("temperature {} C / set point {} C ({})".format(curr_temp, set_point,
                                                                at_set_point))
    return [curr_temp, set_point, at_set_point]

  def tempRateEn(self):
    self._rsrcObj.post(url="/tempRateEn", data="1")

  def tempRateDis(self):
    self._rsrcObj.post(url="/tempRateEn", data="0")
