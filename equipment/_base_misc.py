#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
from ._base_eqpt import *

class VOA(LabEqpt):

  def __init__(self, strConnInfo, **kwargs):
    super(VOA, self).__init__(strConnInfo, **kwargs)

  def setWavelen(self, val, chId=None):
    raise NotImplementedError

  def getWavelen(self, chId=None):
    raise NotImplementedError

  def setAtt(self, val, chId=None):
    raise NotImplementedError

  def getAtt(self, chId=None):
    raise NotImplementedError

  def enable(self, chId=None):
    raise NotImplementedError

  def disable(self, chId=None):
    raise NotImplementedError
