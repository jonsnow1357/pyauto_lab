#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class Paragon_Neo(labBase.TrafficGen):
  """
  Calnex Paragon-neo
  (https://www.calnexsol.com/en/product-detail/1295-paragon-neo-4)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(Paragon_Neo, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry = {"err": ""}
    self.replyNoErr = ""
    self._api_base = self._rsrcObj.info.URL + "/api"

  def _make_URL(self, path):
    return "{}{}{}".format(self._api_base, path, "?format=json")

  def _getInfo(self):
    res = self._rsrcObj.get(url=self._make_URL("/instrument/information"), bJson=True)
    self._idn = {"id": res["HwType"]}
    self._idn = {"SN": res["SerialNumber"]}
    self._idn = {"opt": res["HwCapabilities"]}

  def _getErrors(self, TOmultiplier=2, nReplyLines=5):
    res = self._rsrcObj.get(url=self._make_URL("/instrument/errorlog"), bJson=True)
    return res
