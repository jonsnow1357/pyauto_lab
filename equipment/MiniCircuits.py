#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class SwitchMatrix(labBase.LabEqpt):
  """
  Mini-Circits RC-2SPDT-40
  (https://www.minicircuits.com/pdfs/RC-2SPDT-40.pdf)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(SwitchMatrix, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry["err"] = "NA"
    #self.replyNoErr = None

    self.timeout = 0.2
    self.nSwitches = 0
    self.nPositions = 0

  def _getInfo(self):
    res = self._rsrcObj.query("MN?")
    self._idn["model"] = res[0][3:]
    res = self._rsrcObj.query("SN?")
    self._idn["SN"] = res[0][3:]

    if (self._idn["model"].startswith("RC-2SPDT")):
      self.nSwitches = 2
      self.nPositions = 2
    elif (self._idn["model"].startswith("RC-2SP4T")):
      self.nSwitches = 2
      self.nPositions = 4
    self._idn["matrix"] = "{}x{}".format(self.nSwitches, self.nPositions)

  def set(self, swId, posId):
    if (swId > self.nSwitches):
      logger.warning("INCORRECT switch id: {}".format(swId))
      return
    if (posId > self.nPositions):
      logger.warning("INCORRECT position id: {}".format(posId))
      return

    if (self.nPositions == 2):
      logger.info("SPDT {} COM-{}".format(chr(65 + swId), posId))
      res = self._rsrcObj.query("SET{}={}".format(chr(65 + swId), posId), TOmultiplier=2)
      if (res[0] != "1"):
        msg = "CANNOT CONNECT SPDT {} COM-{}".format(chr(65 + swId), posId)
        logger.error(msg)
        raise RuntimeError(msg)
    elif (self.nPositions == 4):
      logger.info("SP4T {} COM-{}".format(chr(65 + swId), posId))
      res = self._rsrcObj.query("SP4T{}:STATE:{}".format(chr(65 + swId), posId),
                                TOmultiplier=2)
      if (res[0] != "1"):
        msg = "CANNOT CONNECT SPDT {} COM-{}".format(chr(65 + swId), posId)
        logger.error(msg)
        raise RuntimeError(msg)
    else:
      logger.warning("switch {}/{} NOT SUPPORTED".format(swId, posId))
