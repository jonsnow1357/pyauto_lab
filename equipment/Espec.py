#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

class SCP_220(labBase.ThermalChamber):
  """
  Espec SCP-220 Thermal Chamber Web controller
  (https://espec.com/na/products/controllers/scp_220)

  NOTE: These web APIs are not documented by ESPEC,
  just discovered by inspecting the javascript of the actual web interface.

  Below is a list of known supported API GET-able objects (not all are incorporated in this driver):
     chamber_name
     temp
     mode
     time
     time_signals
     timer_use
     temp_ptc
     temp_ptc_const
     humi
     humi_const
     temp_const
     set
     set_const
     relay
     relay_const
     const_graphic
     config
     prgm_set
     prgm_mon
     alarms
     keyprotect
     current_prgm_data_all
  """

  def __init__(self, strConnInfo, **kwargs):
    super(SCP_220, self).__init__(strConnInfo=strConnInfo, **kwargs)

    self._rsrcObj.timeout = 4.0
    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry["err"] = "NA"
    #self.replyNoErr = None
    #self.address = strConnInfo
    self._api_base = "{}/app/app.app".format(self._rsrcObj.info.URL)

  def _getInfo(self):
    res = self._rsrcObj.get(url="{}?chamber_name=[]".format(self._api_base))
    res = json.loads(res)
    self._idn["chamber_name"] = res["chamber_name"]
    #get_opts = {"url": "{}?prgm_mon=[]".format(self._api_base)}
    #res = self._rsrcObj.get(get_opts)
    #res = json.loads(res)
    #print("DBG", res)

  @property
  def temperature(self):
    res = self._rsrcObj.get(url="{}?temp=[]".format(self._api_base))
    res = json.loads(res)
    return res["temp"]

  @property
  def status(self):
    res = self._rsrcObj.get(url="{}?mode=[]".format(self._api_base))
    res = json.loads(res)
    dictRes = res
    res = self._rsrcObj.get(url="{}?config=[]".format(self._api_base))
    res = json.loads(res)
    dictRes.update(res["config"])
    res = self._rsrcObj.get(url="{}?alarms=[]".format(self._api_base))
    res = json.loads(res)
    dictRes["alarms"] = res["alarms"]["alarms"]
    return dictRes

  @property
  def setPoint(self):
    res = self.temperature
    return res["target"]

  @setPoint.setter
  def setPoint(self, val, **kwargs):
    self._rsrcObj.post(url=self._api_base,
                       data="save=save&temp_setpoint={0:.2f}".format(float(val)))

  def start(self):
    self._rsrcObj.post(url=self._api_base, data="save=save&start_constant=true")

  def stop(self):
    self._rsrcObj.post(url=self._api_base, data="save=save&stop_operation=true")

  '''
  def _web_post(self, post_data):
    # res = self._rsrcObj.get({"url": "home.html"})
    key = "url"
    value = "app/app.app"
    res = self._rsrcObj.get({key: value})
    post_url = "http://{0}/app/app.app".format(self.address)
    r = requests.post(post_url, data=post_data)
    # TODO: Add verification logic based on the response.
    # print r.json()
    self._rsrcObj.post()

  def _web_get(self, query_string):
    get_url = "http://{0}/app/app.app?{1}".format(self.address, query_string)
    self._rsrcObj.get()
    return requests.get(get_url).json()

  def set_temp_rate(self, value, rate_target):
    now = int(time.time())
    interval = now
    wait_time = (rate_target / 0.5)
    print("test")
    print('wait timew is :' + str(wait_time))
    while True:
      if time.time() > interval:
        temps = self.get_temp()
        old_target = temps['target']

        if abs(old_target - value) < 0.5:  # If we are within 1 deg of setpoint, we're there
          print("Target already at {}".format(value))
          print("Monitored value: {}".format(temps['monitored']))
          return

        if old_target < value:
          # ramping up
          # new_target = min(old_target + rate, value)
          new_target = old_target + 0.5
        else:
          # ramping down
          # new_target = max(old_target - rate, value)
          new_target = old_target - 0.5
        print("Product at {}C.".format(old_target))
        print("Product at {}C, Setting temp to {}".format(old_target, new_target))
        self.set_temp(new_target)

        interval += wait_time
      continue

    post_data = "save=save&temp_setpoint={0:.2f}".format(float(value))
    self._web_post(post_data)

  def start_program(self, program_num, program_step):
    self._web_post("save=save&start_program=true&program_index={}&program_step={}".format(
        program_num, program_step))

  def pause_program(self):
    self._web_post("save=save&pause_program=true")

  def continue_program(self):
    self._web_post("save=save&continue_program=true")

  def next_step(self):
    self._web_post("save=save&step_program=true")
  '''
