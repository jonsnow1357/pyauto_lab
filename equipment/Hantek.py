#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class DSO(labBase.Oscilloscope, labBase.FunctionGen):
  """
  Hantek DSO Series
  (http://hantek.com/Products?pid=3)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    if ("eos" not in kwargs.keys()):
      kwargs["eos"] = "\n"
    super(DSO, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "0, \"No error\""

  def _getInfo(self):
    super(DSO, self)._getInfo()

    if (self._idn["id"].startswith("Hantek, DSO4104C")):
      self.nCh = 4
      self.nOut = 1
    else:
      logger.warning("UNRECOGNIZED id: {}".format(self._idn["id"]))

  def _getErrors(self, TOmultiplier=2):
    self._rsrcObj.eom = ""
    res = super(DSO, self)._getErrors()
    self._rsrcObj.eom = "\n"
    return res

  def getHorizontal(self):
    res = self.query("TIM:SCAL?")
    scale = float(res[0])
    res = self.query("TIM:POS?")
    offset = float(res[0])
    return [scale, offset]

  def getVertical(self, chId):
    self._chkChannelId(chId)

    res = self.query("CHANNEL{}:SCALE?".format(chId))
    scale = float(res[0])
    res = self.query("CHANNEL{}:OFFSET?".format(chId))
    offset = float(res[0])
    return [scale, offset]

  def showCh(self, chId):
    super(DSO, self).showCh(chId)

    self.write("CHANNEL{}:DISPLAY ON".format(chId))

  def hideCh(self, chId):
    super(DSO, self).hideCh(chId)

    self.write("CHANNEL{}:DISPLAY OFF".format(chId))

  def setTrigger(self, val):
    if (val == labBase.OscTrigger_Trigd):
      self.write("TRIG:SWE NORM")
    elif (val == labBase.OscTrigger_Auto):
      self.write("TRIG:SWE AUTO")
    elif (val == labBase.OscTrigger_Single):
      self.write("TRIG:SWE SING")
    else:
      logger.warning("INCORRECT trigger mode: {}".format(val))

  def getPlot(self, path):
    self._rsrcObj.eom = ""
    res = self.query("WAV:DATA:ALL?", TOmultiplier=4)
    self._rsrcObj.eom = "\n"
    print("DBG", res)

  def setOutput(self, mode, freq, ampl, offset, outId):
    super(DSO, self).setOutput(mode, freq, ampl, offset, outId)

    if (mode == labBase.Function_Sine):
      self.write("DDS:TYPE SINE")
    elif (mode == labBase.Function_Square):
      self.write("DDS:TYPE SQUA")
    elif (mode == labBase.Function_Tri):
      self.write("DDS:TYPE RAMP")
    elif (mode == labBase.Function_Saw):
      self.write("DDS:TYPE EXP")
    else:
      msg = "UNSUPPORTED mode: {}".format(mode)
      logger.warning(msg)
      return

    self.write("DDS:FREQ {:d}".format(freq))
    self.write("DDS:AMP {:f}".format(ampl))
    self.write("DDS:OFFS {:f}".format(offset))

  def enable(self, outId):
    super(DSO, self).enable(outId)

    self.write("DDS:SWIT 1")
    logger.info("output ON")

  def disable(self, outId):
    super(DSO, self).disable(outId)

    self.write("DDS:SWIT 0")
    logger.info("output OFF")
