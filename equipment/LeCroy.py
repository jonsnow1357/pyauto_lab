#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class WavePro(labBase.Oscilloscope):
  """
  Keysight Infiniium S-Series Oscilloscopes
  (https://www.keysight.com/en/pcx-x205213/infiniium-s-series-oscilloscopes?nid=-32531.0.00&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(WavePro, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    #self.SCPI = False
    self.qry["err"] = "ALL_STATUS?"
    self.replyNoErr = "ALST STB,000000,ESR,000000,INR,000000,DDR,000000,CMR,000000,EXR,000000,URR,000000"

  def _getErrors(self, TOmultiplier=2, nReplyLines=1):
    lstErr = []

    while (True):
      res = self.query(self.qry["err"])
      #logger.info(res)
      if (len(res) == 0):
        break
      elif (res[0].startswith("ALST")):
        tmp = res[0].split(",")
        tmp[5] = "000000"
        tmp = ",".join(tmp)
        if (tmp == self.replyNoErr):
          break
      else:
        lstErr.append(res)

    return lstErr

  def _getInfo(self):
    super(WavePro, self)._getInfo()

    self._idn["opt"] = self.OPT()[0]
    return self._idn
