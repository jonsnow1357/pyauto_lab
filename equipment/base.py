#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.misc
from ._base_eqpt import *
from ._base_power import *
from ._base_rf import *
from ._base_traffic import *
from ._base_misc import *

MultimeterMode_DCV = "DCV"
MultimeterMode_ACV = "ACV"
MultimeterMode_DCI = "DCI"
MultimeterMode_ACI = "ACI"
MultimeterMode_RES2W = "RES2W"
MultimeterMode_RES4W = "RES4W"
MultimeterMode_CAP = "CAP"
MultimeterMode_DIO = "DIO"
MultimeterMode_CONT = "CONT"
MultimeterMode_TEMP = "TEMP"
MultimeterMode_FREQ = "FREQ"
lstMultimeterMode = (MultimeterMode_DCV, MultimeterMode_ACV, MultimeterMode_DCI,
                     MultimeterMode_ACI, MultimeterMode_RES2W, MultimeterMode_RES4W,
                     MultimeterMode_CAP, MultimeterMode_DIO, MultimeterMode_CONT,
                     MultimeterMode_TEMP, MultimeterMode_FREQ)

class Multimeter(LabEqpt):

  def __init__(self, strConnInfo, **kwargs):
    super(Multimeter, self).__init__(strConnInfo, **kwargs)
    self.nOut = 1

  def _chkChannelId(self, chId):
    chkChannelId(chId, self.nOut)

  def setChannel(self, mode, chId=None):
    raise RuntimeError

  def getChannel(self, chId=None):
    """
    :param chId:
    :return: dictionary {"mode": MultimeterMode, "val": ...}
    :rtype: dict
    """
    raise RuntimeError

OscTrigger_Auto = "auto"
OscTrigger_Trigd = "trig"
OscTrigger_Single = "single"
lstOscTrigger = (OscTrigger_Auto, OscTrigger_Trigd, OscTrigger_Single)

class Oscilloscope(LabEqpt):

  def __init__(self, strConnInfo, **kwargs):
    super(Oscilloscope, self).__init__(strConnInfo, **kwargs)
    self.nCh = 1

  def _chkChannelId(self, chId):
    chkChannelId(chId, self.nCh)

  def getHorizontal(self):
    """
    :return: [<scale>, <offset>] in seconds
    :rtype: list
    """
    raise NotImplementedError

  def setHorizontal(self, scale, offset):
    """
    :param scale: in seconds
    :param offset: in seconds
    """
    raise NotImplementedError

  def getVertical(self, chId):
    """
    :param chId:
    :return: [<scale>, <offset>] in volts
    """
    raise NotImplementedError

  def setVertical(self, scale, offset):
    """
    :param scale: in volts
    :param offset: in volts
    """
    raise NotImplementedError

  def showCh(self, chId):
    self._chkChannelId(chId)

  def hideCh(self, chId):
    self._chkChannelId(chId)

  def getLabel(self, chId):
    raise NotImplementedError

  def setLabel(self, val="", chId=None):
    self._chkChannelId(chId)

  def setTrigger(self, val):
    raise NotImplementedError

  def getPlot(self, path):
    raise NotImplementedError

  def writeImg(self, path):
    raise NotImplementedError

  def writeCSV(self, path):
    raise NotImplementedError

Function_Sine = "sine"
Function_Square = "square"
Function_Tri = "triangular"
Function_Saw = "sawtooth"
lstFuntionGenOut = (Function_Sine, Function_Square, Function_Tri, Function_Saw)

class FunctionGen(LabEqpt):

  def __init__(self, strConnInfo, **kwargs):
    super(FunctionGen, self).__init__(strConnInfo, **kwargs)
    self.nOut = 1

  def _chkChannelId(self, outId):
    chkChannelId(outId, self.nOut)

  def setOutput(self, mode, freq, ampl, offset, outId):
    """
    :param mode:
    :param freq: frequency in Hz
    :param ampl: amplitude in Vpp
    :param offset: offset in V
    :param outId:
    """
    self._chkChannelId(outId)

    if (mode not in lstFuntionGenOut):
      msg = "INCORRECT mode: {}".format(mode)
      logger.error(msg)
      raise RuntimeError(msg)

    pyauto_base.misc.chkInt(freq, valId="frequency", minVal=0)
    pyauto_base.misc.chkFloat(ampl, valId="amplitude", minVal=0.0)
    pyauto_base.misc.chkFloat(offset, valId="offset", minVal=0.0)

  def enable(self, outId):
    self._chkChannelId(outId)

  def disable(self, outId):
    self._chkChannelId(outId)

class ThermalChamber(LabEqpt):

  def __init__(self, strConnInfo, **kwargs):
    super(ThermalChamber, self).__init__(strConnInfo, **kwargs)

  @property
  def temperature(self):
    """
    :return: dictionary
    """
    raise NotImplementedError

  @property
  def status(self):
    """
    :return: dictionary
    """
    raise NotImplementedError

  @property
  def setPoint(self):
    """
    :return: value
    """
    raise NotImplementedError

  @setPoint.setter
  def setPoint(self, val, **kwargs):
    raise NotImplementedError

  @property
  def tempRate(self):
    """
    Setting to > 0 enables ramping.
    Setting to <= 0 disables ramping.

    :return: value
    """
    raise NotImplementedError

  @tempRate.setter
  def tempRate(self, val, **kwargs):
    raise NotImplementedError

  def start(self):
    """
    May return instantaneously or take some time.
    Some chambers have to be started to actually change the set point.
    """
    raise NotImplementedError

  def stop(self):
    """
    May return instantaneously or take some time.
    """
    raise NotImplementedError

  def chkStatus(self):
    """
    checks equipment status and throws Exception if:
    - any (HW dependent) errors

    :return: [<current_temperature>, <set_point>, <at_set_point>]
    """
    raise NotImplementedError

  def tempRateEn(self):
    raise NotImplementedError

  def tempRateDis(self):
    raise NotImplementedError

class IfAdapter(LabEqpt):
  """
  Adapter/dongle to I2C/SPI/GPIO
  """

  def __init__(self, strConnInfo, **kwargs):
    super(IfAdapter, self).__init__(strConnInfo, **kwargs)
    self._idn = {"I2C": False, "SPI": False, "GPIO": False}
    self.maxBytes = 256

  def i2cCfg(self, speed: int = 100, bPullUp: bool = False):
    """
    :param speed: in kHz
    :param bPullUp:
    """
    raise NotImplementedError

  def i2cRd(self, i2cAddr: int, nBytes: int) -> list[int]:
    """
    I2C transaction: S addr+R byte byte ... P
    """
    raise NotImplementedError

  def i2cWr(self, i2cAddr: int, lstBytes: list[int]):
    """
    I2C transaction: S addr+W byte byte ... P
    """
    raise NotImplementedError

  def i2cWrRd(self, i2cAddr: int, lstWrBytes: list[int], nBytes: int) -> list[int]:
    """
    I2C transaction: S addr+W wByte wByte ... S addr+R rByte rByte ... P
    """
    raise NotImplementedError

  def smbusRd8(self, i2cAddr: int, cmd: int) -> int:
    raise NotImplementedError

  def smbusWr8(self, i2cAddr: int, cmd: int, data: int):
    raise NotImplementedError

  def smbusRdBlock(self, i2cAddr: int, cmd: int, nBytes: int) -> list[int]:
    raise NotImplementedError

  def smbusWrBlock(self, i2cAddr: int, cmd: int, lstBytes: list[int]):
    if ((i2cAddr < 0) or (i2cAddr > 127)):
      msg = "INCORRECT addr: {}".format(i2cAddr)
      logger.error(msg)
      raise RuntimeError(msg)
    if ((cmd < 0) or (cmd > 255)):
      msg = "INCORRECT cmd: {}".format(cmd)
      logger.error(msg)
      raise RuntimeError(msg)
    if ((len(lstBytes) < 1) or (len(lstBytes) > (self.maxBytes - 2))):
      msg = "INCORRECT data length: {}".format(len(lstBytes))
      logger.error(msg)
      raise RuntimeError(msg)

    self.i2cWr(i2cAddr, [cmd, len(lstBytes)] + lstBytes)

  def spiCfg(self,
             speed: int = 1000,
             bCSPOL: bool = False,
             bCPOL: bool = False,
             bCPHA: bool = False,
             bMSBFirst: bool = True):
    """
    :param speed: in kHz
    :param bCSPOL:
    :param bCPOL:
    :param bCPHA:
    :param bMSBFirst:
    """
    raise NotImplementedError

  def spiXfer(self, lstBytes: list[int]) -> list[int]:
    raise NotImplementedError
