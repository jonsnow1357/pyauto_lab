#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class DSG8xx(labBase.SignalGenerator):
  """
  Rigol DSG800 Series
  (https://www.rigolna.com/products/rf-signal-generators/dsg800/dsg836a/products/)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(DSG8xx, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    self.qry["err"] = ":STATUS:OPERATION?"
    self.replyNoErr = "0"

  def enable(self, outId=None):
    self.write(":OUTPUT:STATE 1")

  def disable(self, outId=None):
    self.write(":OUTPUT:STATE 0")

  def setUnit(self, val):
    if (val not in labBase.lstUnits):
      logger.warning("INCORRECT UNIT {}".format(self._unit))
      return

    self._unit = val
    self.write(":UNIT:POWER {}".format(self._unit.upper()))

  def getUnit(self):
    res = self.query(":UNIT:POWER?")
    for u in labBase.lstUnits:
      if (res[0] == u.upper()):
        self._unit = u
        return self._unit

    msg = "UNRECOGNIZED UNIT {}".format(res)
    logger.error(msg)
    raise RuntimeError(msg)

  def getFreq(self, outId=None):
    self._chkChannelId(outId)

    res = self.query(":SOURCE:FREQ?")
    return float(res[0])

  def setFreq(self, val, outId=None):
    self._chkChannelId(outId)

    self.write(":SOURCE:FREQ {}".format(val))

  def getPwr(self, outId=None):
    self._chkChannelId(outId)

    res = self.query(":SOURCE:LEVEL?")
    return float(res[0])

  def setPwr(self, val, outId=None):
    self._chkChannelId(outId)

    self.write(":SOURCE:LEVEL {}".format(val))

  def CW(self, freq, ampl, outId=None):
    self._chkChannelId(outId)

    self.setFreq(freq)
    self.setPwr(ampl)
