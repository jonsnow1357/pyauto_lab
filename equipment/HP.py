#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

class HP_8157A(labBase.VOA):
  """
  8157A Optical Attenuator
  (http://www.keysight.com/en/pd-8157A%3Aepsg%3Apro-pn-8157A/optical-attenuator?cc=CA&lc=eng&lsrch=true&searchT=8157A)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(HP_8157A, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    #self.SCPI = False
    self.qry["err"] = "ERR?"
    self.replyNoErr = "0"

    self._wl = None

  def setWavelen(self, val, chId=None):
    pyauto_base.misc.chkFloat(val, valId="wavelength", minVal=1300.0, maxVal=1550.0)
    self.write("WVL {} NM".format(val))
    #self.waitOnCmd()

  def getWavelen(self, chId=None):
    res = self.query("WVL?")
    return float(res[0]) * 1e9

  def setAtt(self, val, chId=None):
    pyauto_base.misc.chkFloat(val, valId="attenuation", minVal=0.0, maxVal=64.0)
    self.write("ATT {}".format(val))
    #self.waitOnCmd()

  def getAtt(self, chId=None):
    res = self.query("ATT?")
    return float(res[0])

  def enable(self, chId=None):
    self.write("D 0")
    #self.waitOnCmd()

  def disable(self, chId=None):
    self.write("D 1")
    #self.waitOnCmd()

class HP_8648(labBase.SignalGenerator):
  """
  8648C Synthesized RF Signal Generator, 9 kHz to 3200 MHz
  (https://www.keysight.com/us/en/product/8648C/synthesized-rf-signal-generator-9-khz-to-3200-mhz.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(HP_8648, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "+0,\"No error; \""

  @property
  def frequency(self):
    res = self.query("FREQ:CW?")
    freq = float(res[0])
    return freq

  @frequency.setter
  def frequency(self, val):
    freq = pyauto_base.misc.chkFloat(val, valId="frequency", minVal=0.0)
    self.write("FREQ:CW {}".format(freq))

  def setAtt(self, val, chId=None):
    ampl = pyauto_base.misc.chkFloat(val, valId="amplitude")
    self.write("POWER:AMPLITUDE {}DBM".format(ampl))

  @property
  def amplitude(self):
    res = self.query("POWER:AMPLITUDE?")
    ampl = float(res[0])
    return ampl

  def enable(self, chId=None):
    self.write("OUTPUT:STATE ON")
    # self.waitOnCmd()
    logger.info("output ON")

  def disable(self, chId=None):
    self.write("OUTPUT:STATE OFF")
    #self.waitOnCmd()
    logger.info("output OFF")
