#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import importlib
import itertools

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_base.bin
import pyauto_comm.generic

class _LabAdapter(object):

  def __init__(self, commIf, **kwargs):
    if (not isinstance(commIf, pyauto_comm.generic.CommInterface)):
      msg = "CANNOT use '{}'".format(type(commIf))
      logger.error(msg)
      raise RuntimeError(msg)
    self._conn = commIf

  def open(self):
    raise NotImplementedError

  def close(self):
    raise NotImplementedError

  def read(self, TOmultiplier=1):
    raise NotImplementedError

  def write(self, msg):
    raise NotImplementedError

  def query(self, msg, TOmultiplier=1):
    raise NotImplementedError

class _Prologix(_LabAdapter):

  def __init__(self, commIf, **kwargs):
    super(_Prologix, self).__init__(commIf, **kwargs)
    self.gpibAddr = None
    self.gpibEOS = None
    self.gpibEOM = None

  def _set_gpib_addr(self, addr=None):
    res = self._conn.query("++addr")
    cur_addr = res[0]

    if ((addr is None) and (self.gpibAddr is None)):
      pass
    elif ((addr is None) and (self.gpibAddr is not None)):
      self._conn.write("++addr {}".format(self.gpibAddr))
      cur_addr = self.gpibAddr
    else:
      self._conn.write("++addr {}".format(addr))
      cur_addr = addr
    logger.info("adapter GPIB address: {}".format(cur_addr))

  def open(self):
    self._conn.open()

    res = self._conn.query("++ver")
    if ((res != []) and ("Prologix" in res[0])):
      logger.info("adapter: {}".format(res[0]))
    else:
      msg = "INCORRECT adapter: {}".format(res)
      logger.error(msg)
      raise RuntimeError(msg)

    self._set_gpib_addr()

    # set mode to controller
    res = self._conn.query("++mode")
    if (res[0] != "1"):
      self._conn.write("++mode 1")
    # enable EOI assert with last character
    res = self._conn.query("++eoi")
    if (res[0] != "1"):
      self._conn.write("++eoi 1")
    # eos for GPIB to instrument
    if (self.gpibEOS is None):
      eosVal = "0"
    elif (self.gpibEOS == "\r"):
      eosVal = "1"
    elif (self.gpibEOS == "\n"):
      eosVal = "2"
    else:
      msg = "UNEXPECTED gpibEOS: '{}'".format(self.gpibEOS)
      logger.error(msg)
      raise RuntimeError(msg)
    res = self._conn.query("++eos")
    if (res[0] != eosVal):
      self._conn.write("++eos {}".format(eosVal))
    # disable read-after-write
    res = self._conn.query("++auto")
    if (res[0] != "0"):
      self._conn.write("++auto 0")
    self._conn.write("++read_tmo_ms 500")

  def close(self):
    self._conn.write("++loc")

    self._conn.close()

  def read(self, TOmultiplier=1):
    if (self.gpibEOM is None):
      res = self._conn.communicate(("++read eoi" + self._conn.eos),
                                   strEndRead=self._conn.eom,
                                   TOmultiplier=TOmultiplier)
    else:
      res = self._conn.communicate(
          ("++read {:d}".format(ord(self.gpibEOM)) + self._conn.eos),
          strEndRead=self.gpibEOM,
          TOmultiplier=TOmultiplier)
    if (len(res) > 0):
      if (res[-1] == "*"):
        res = res[:-1]
      elif (res[0].startswith("*")):
        res[0] = res[0][1:]
    #logger.info(res)
    return res

  def write(self, msg):
    self._conn.communicate((msg + self.gpibEOS), strEndRead=None)

  def query(self, msg, TOmultiplier=1):
    self.write(msg)
    res = self.read(TOmultiplier=TOmultiplier)
    #logger.info(res)
    return res

  def scan(self):
    lstAddr = []
    for addr in range(1, 31):
      self._set_gpib_addr(addr)
      try:
        res = self.query("*IDN?")
      except pyauto_comm.generic.CommTimeoutException:
        continue
      lstAddr.append(addr)
      logger.info(res)
    logger.info("GPIB addres(es) found: {}".format(lstAddr))
    return lstAddr

class LabEqptModule(object):

  def __init__(self, commIf, **kwargs):
    if (not isinstance(commIf, pyauto_comm.generic.CommInterface)):
      msg = "CANNOT use '{}'".format(type(commIf))
      logger.error(msg)
      raise RuntimeError(msg)
    self._conn = commIf

    self.IEEE_488_2 = False
    self.SCPI = False
    self.qry = {"id": None, "err": None}
    self.replyNoErr = None

  def open(self):
    return self._conn.open()

  def close(self):
    return self._conn.close()

  def read(self, TOmultiplier=1):
    return self._conn.read(TOmultiplier=TOmultiplier)

  def write(self, msg):
    return self._conn.write(msg)

  def query(self, msg, TOmultiplier=1):
    return self._conn.query(msg, TOmultiplier=TOmultiplier)

  def OPC(self, bQuery=True, TOmultiplier=1):
    """Operation Complete command/query
    :param bQuery: True|False
    :param TOmultiplier:
    """
    if (self.IEEE_488_2):
      if (bQuery):
        return self.query("*OPC?", TOmultiplier=TOmultiplier)
      else:
        self.write("*OPC")
    else:
      raise NotImplementedError

  def _waitOnCmd(self, timeout=2):
    """
    do a while loop since the parameter passed here is timeout and self.read() need a TOmultiplier
    :param timeout:
    """
    logger.info("wait (true - {})".format(timeout))
    t0 = datetime.datetime.now()
    while (True):
      res = ""
      try:
        res = self.read()
      except pyauto_comm.generic.CommTimeoutException:
        pass
      #logger.info(res)
      if ((len(res) > 0) and (res[0] == "1")):
        #logger.info("_waitOnCmd (2) ...")
        return
      delta = datetime.datetime.now() - t0
      if (delta.seconds > timeout):
        msg = "waitOnCmd TIMEOUT: {} > {}".format(delta.seconds, timeout)
        logger.error(msg)
        raise RuntimeError(msg)

  def _waitOnCmd_OPC(self, timeout=2):
    logger.info("wait (OPC - {})".format(timeout))
    t0 = datetime.datetime.now()
    bBlocked = False
    while (True):
      try:
        if (bBlocked):
          res = self.read()
        else:
          res = self.OPC()
        if (int(res[0]) != 0):
          return
      except pyauto_comm.generic.CommTimeoutException:
        bBlocked = True
      delta = datetime.datetime.now() - t0
      if (delta.seconds > timeout):
        msg = "waitOnCmd TIMEOUT: {} > {}".format(delta.seconds, timeout)
        logger.error(msg)
        raise RuntimeError(msg)

  def waitOnCmd(self, timeout=2):
    if (self.IEEE_488_2):
      self._waitOnCmd_OPC(timeout=timeout)
      return
    self._waitOnCmd(timeout=timeout)

class LabEqptException(Exception):
  pass

class LabEqpt(object):
  """
  :var _rsrcObj: obj to communicate with the equipment
  :var _ifParams: dictionary of interface parameter
  :var _params: dictionary of configuration parameters (NOT interface related)
  :var _adapter: adapter object (if an additional adapter is used to talk to the equipment)
  :var _idn: dictionary of identification information from instrument. This is used to minimize ID related queries.
  :var qry: dictionary for mapping equipment queries to standard names
  """

  def __init__(self, strConnInfo, **kwargs):
    self._rsrcObj = None
    self._params = {}
    self._idn = {}

    self.IEEE_488_2 = False
    self.SCPI = False
    self.qry = {"id": None, "err": None}
    self.replyNoErr = None

    # get supported kwargs
    tmpAdapter = {}
    for k, v in kwargs.items():
      if (k.startswith("adapter_")):
        tmpAdapter[k[8:]] = v
      if (k in ("eos", "eom")):
        self._params[k] = v

    if (strConnInfo == "ctypes"):
      return
    cInfo = pyauto_comm.generic.CommInterfaceInfo(strConnInfo)
    #logger.info(cInfo)
    cIf = pyauto_comm.generic.CommInterface()
    cIf.setInfo(cInfo)

    # process adapter
    if (len(tmpAdapter) > 0):
      if (tmpAdapter["name"] == "prologix"):
        self._rsrcObj = _Prologix(cIf)
        for k in ("gpibAddr", "gpibEOS", "gpibEOM"):
          if (k in tmpAdapter):
            setattr(self._rsrcObj, k, tmpAdapter[k])
        if (self._rsrcObj.gpibEOS is None):
          self._rsrcObj.gpibEOS = "\n"
        if (self._rsrcObj.gpibEOM is None):
          self._rsrcObj.gpibEOM = "\n"
      else:
        msg = "UNSUPPORTED adapter: {}".format(tmpAdapter["name"])
        logger.error(msg)
        raise NotImplementedError(msg)
    else:
      self._rsrcObj = cIf
    #logger.info(self._rsrcObj)
    #logger.info(self._params)

  def __str__(self):
    res = "{: <20}: {: <20} [{}] 488.2, [{}] SCPI, " \
          "{} qry, [{}] replyNoErr".format(self.__class__.__name__,
                                           self._rsrcObj.__class__.__name__,
                                           "x" if (self.IEEE_488_2) else " ",
                                           "x" if (self.SCPI) else " ",
                                           len([v for v in self.qry.values() if (v is not None)]),
                                           "x" if (self.replyNoErr is not None) else " ",)
    return res

  @property
  def timeout(self):
    if (isinstance(self._rsrcObj, pyauto_comm.generic.CommInterface)):
      return self._rsrcObj.info.timeout
    else:
      raise RuntimeError

  @timeout.setter
  def timeout(self, val):
    if (isinstance(self._rsrcObj, pyauto_comm.generic.CommInterface)):
      self._rsrcObj.info.timeout = val
    else:
      raise RuntimeError

  def read(self, TOmultiplier=1):
    return self._rsrcObj.read(TOmultiplier=TOmultiplier)

  def write(self, msg):
    return self._rsrcObj.write(msg)

  def query(self, msg, TOmultiplier=1):
    return self._rsrcObj.query(msg, TOmultiplier=TOmultiplier)

  def connect(self):
    if (isinstance(self._rsrcObj, pyauto_comm.generic.CommInterface)):
      if ("eos" in self._params.keys()):
        self._rsrcObj.eos = self._params["eos"]
      if ("eom" in self._params.keys()):
        self._rsrcObj.eom = self._params["eom"]
    self._rsrcObj.open()

  def disconnect(self):
    self._rsrcObj.close()

  def CLS(self):
    """Clear Status command"""
    if (self.IEEE_488_2):
      self.write("*CLS")
    else:
      raise NotImplementedError

  def ESR(self, TOmultiplier=1):
    """Event Status Register query"""
    if (self.IEEE_488_2):
      return self.query("*ESR?", TOmultiplier=TOmultiplier)
    else:
      raise NotImplementedError

  def IDN(self, TOmultiplier=2):
    """Identification query"""
    if (self.IEEE_488_2):
      self.qry["id"] = "*IDN?"
    elif (self.qry["id"] is None):
      msg = "id query NOT DEFINED"
      logger.error(msg)
      raise RuntimeError(msg)

    return self.query(self.qry["id"], TOmultiplier=TOmultiplier)

  def OPC(self, bQuery=True, TOmultiplier=1):
    """Operation Complete command/query
    :param bQuery: True|False
    :param TOmultiplier:
    """
    if (self.IEEE_488_2):
      if (bQuery):
        return self.query("*OPC?", TOmultiplier=TOmultiplier)
      else:
        self.write("*OPC")
    else:
      raise NotImplementedError

  def OPT(self, TOmultiplier=1):
    """Options query"""
    if (self.IEEE_488_2):
      return self.query("*OPT?", TOmultiplier=TOmultiplier)
    else:
      raise NotImplementedError

  def RST(self):
    """Reset command"""
    if (self.IEEE_488_2):
      self.write("*RST")
    else:
      raise NotImplementedError

  def STB(self, TOmultiplier=1):
    """Status Byte query"""
    if (self.IEEE_488_2):
      return self.query("*STB?", TOmultiplier=TOmultiplier)
    else:
      raise NotImplementedError

  def TST(self, TOmultiplier=1):
    """Self-Test query"""
    if (self.IEEE_488_2):
      return self.query("*TST?", TOmultiplier=TOmultiplier)
    else:
      raise NotImplementedError

  def showTST(self):
    res = self.TST()
    if (len(res) == 0):
      return

    if (res[0] == "0"):
      logger.info("TST: OK")
    else:
      logger.info("TST: FAIL ({})".format(res))

  def showESR(self):
    res = self.ESR()
    if (len(res) == 0):
      return

    try:
      res = hex(int(res[0]))
    except ValueError:
      res = hex(int(float(res[0])))
    logger.info("ESR: {}".format(res))

    flags = [
        "PON (Pwr On)", "URQ (User Request)", "CME (Command ERR)", "EXE (Execution ERR)",
        "DDE (Device ERR)", "QYE (Query ERR)", "RQC (Request Control)",
        "OPC (Operation Complete)"
    ]
    bits = pyauto_base.bin.hex2bits(res)
    for t in itertools.compress(flags[::-1], bits):
      if (t is not None):
        logger.info("  {}".format(t))

  def showSTB(self):
    res = self.STB()
    if (len(res) == 0):
      return

    res = hex(int(res[0]))
    logger.info("STB: {}".format(res))

    flags = [
        None, "MSS (Master Status)", "ESB (Event Status)", "MAV (MSG Available)",
        "TSB (Test Status)", None, None, None
    ]
    bits = pyauto_base.bin.hex2bits(res)
    for t in itertools.compress(flags[::-1], bits):
      if (t is not None):
        logger.info("  {}".format(t))

  def _waitOnCmd(self, timeout=2):
    """
    do a while loop since the parameter passed here is timeout and self.read() need a TOmultiplier
    :param timeout:
    """
    logger.info("wait (true - {})".format(timeout))
    t0 = datetime.datetime.now()
    while (True):
      res = ""
      try:
        res = self.read()
      except pyauto_comm.generic.CommTimeoutException:
        pass
      #logger.info(res)
      if ((len(res) > 0) and (res[0] == "1")):
        #logger.info("_waitOnCmd (2) ...")
        return
      delta = datetime.datetime.now() - t0
      if (delta.seconds > timeout):
        msg = "waitOnCmd TIMEOUT: {} > {}".format(delta.seconds, timeout)
        logger.error(msg)
        raise RuntimeError(msg)

  def _waitOnCmd_OPC(self, timeout=2):
    logger.info("wait (OPC - {})".format(timeout))
    t0 = datetime.datetime.now()
    bBlocked = False
    while (True):
      try:
        if (bBlocked):
          res = self.read()
        else:
          res = self.OPC()
        if (int(res[0]) != 0):
          return
      except (IndexError, pyauto_comm.generic.CommTimeoutException):
        bBlocked = True
      delta = datetime.datetime.now() - t0
      if (delta.seconds > timeout):
        msg = "waitOnCmd TIMEOUT: {} > {}".format(delta.seconds, timeout)
        logger.error(msg)
        raise RuntimeError(msg)

  def waitOnCmd(self, timeout=2):
    if (self.IEEE_488_2):
      self._waitOnCmd_OPC(timeout=timeout)
      return

    self._waitOnCmd(timeout=timeout)

  def _getErrors(self, TOmultiplier=2):
    """
    :param int TOmultiplier:
    :return: list of [<error>, ...]
    """
    lstErr = []

    while (True):
      reply = self.query(self.qry["err"], TOmultiplier=TOmultiplier)
      #logger.info(reply)
      if (len(reply) == 0):
        break
      elif (len(reply) == 1):
        reply = reply[0]
      else:
        reply = "".join(reply)
      #logger.info(reply)
      if (reply == self.replyNoErr):
        break
      else:
        lstErr.append(reply)

    return lstErr

  def getErrors(self):
    if (self.qry["err"] == "NA"):
      return []

    if (self.qry["err"] is None):
      if (self.SCPI):
        self.qry["err"] = ":SYSTEM:ERROR?"
      elif ((not self.SCPI) and self.IEEE_488_2):
        self.qry["err"] = "*STB?"
      else:
        msg = "error query NOT DEFINED"
        logger.error(msg)
        raise RuntimeError(msg)

    if (self.replyNoErr is None):
      msg = "error reply NOT DEFINED"
      logger.error(msg)
      raise RuntimeError(msg)

    #print("DBG", self.qry["err"], self.replyNoErr )
    return self._getErrors()

  def showErrors(self):
    res = self.getErrors()
    for ln in res:
      logger.warning("{}".format(ln))

  def _getInfo(self):
    if (self.qry["id"] == ""):
      return self._idn
    elif ((self.qry["id"] is None) and (self.IEEE_488_2 is False)):
      msg = "id query NOT DEFINED"
      logger.error(msg)
      raise RuntimeError(msg)

    if (self.IEEE_488_2 or self.SCPI):
      res = self.IDN()
      self._idn = {"id": res[0]}
    else:
      res = self.query(self.qry["id"])
      self._idn = {"id": res[0]}
    return self._idn

  def getInfo(self, bUpdate=False):
    if ((not bUpdate) and (len(self._idn) > 0)):
      return self._idn

    return self._getInfo()

  def showInfo(self):
    self.getInfo(bUpdate=True)
    for k in sorted(self._idn.keys()):
      logger.info("{}: {}".format(k, self._idn[k]))

  def chkID(self, regexID=".*"):
    res = self.IDN()[0]
    if (re.match(regexID, res) is None):
      msg = "INCORRECT ID: {} <> {}".format(res, format(regexID))
      logger.error(msg)
      raise RuntimeError(msg)

def _getModNames():
  res = []
  path = os.path.dirname(__file__)

  for f in os.listdir(path):
    if (not f.endswith(".py")):
      continue
    if (f.startswith("_")):
      continue
    if (f.startswith("base")):
      continue
    res.append(f[:-3])

  return res

def _getEqptObj(modName, eqptName, strConnInfo, dictExtra=None):
  mod = importlib.import_module("pyauto_lab.equipment.{}".format(modName))

  try:
    if (not isinstance(dictExtra, dict)):
      eqpt = getattr(mod, eqptName)(strConnInfo)
    else:
      eqpt = getattr(mod, eqptName)(strConnInfo, **dictExtra)
    if (isinstance(eqpt, LabEqpt)):
      return eqpt
  except (TypeError, AttributeError):
    pass

def getEqptNames():
  res = []

  for modName in _getModNames():
    try:
      mod = importlib.import_module("pyauto_lab.equipment.{}".format(modName))
    except ModuleNotFoundError:
      logger.warning("IGNORING possible equipments {}".format(modName))
      continue
    #modName = mod.__name__.split(".")[-1]
    for t in dir(mod):
      if (t.startswith("_") or t.startswith("pyauto_")):
        continue
      if (t in ("sys", "os", "platform", "logging", "re", "time", "datetime", "math", "csv",
                "json", "ctypes", "logger", "labBase")):
        continue

      try:
        eqpt = getattr(mod, t)(strConnInfo="127.0.0.1")
        if (isinstance(eqpt, LabEqpt)):
          res.append(modName + "." + t)
      except TypeError:
        pass
      except IOError:
        logger.warning("CANNOT instantiate '{}'".format(mod))

  return sorted(res)

def getEqpt(eqptName, strConnInfo="127.0.0.1", dictExtra=None):
  tmp = eqptName.split(".")
  if (len(tmp) == 2):
    return _getEqptObj(tmp[0], tmp[1], strConnInfo, dictExtra)
  elif (len(tmp) == 1):
    for modName in _getModNames():
      eqpt = _getEqptObj(modName, eqptName, strConnInfo, dictExtra)
      if (eqpt is not None):
        return eqpt
  else:
    raise RuntimeError

def chkChannelId(chId, maxCh):
  """
  checks if chId is between 1 and maxCh
  :raises RuntimeError:
  """
  if (maxCh < 1):
    raise LabEqptException
  elif (maxCh == 1):
    return
  else:
    if (not isinstance(chId, int)):
      msg = "INCORRECT chId {}/{}".format(chId, maxCh)
      logger.error(msg)
      raise LabEqptException
    if ((chId < 1) or (chId > maxCh)):
      msg = "INCORRECT chId {}/{}".format(chId, maxCh)
      logger.error(msg)
      raise LabEqptException
