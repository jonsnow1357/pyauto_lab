#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase
from ._Agilent_RF import *
from ._Agilent_misc import *

class A_86100C(labBase.Oscilloscope):
  """
  Keysight 86100C Infiniium DCA-J Wideband Oscilloscope
  (http://www.keysight.com/en/pd-318628-pn-86100C/infiniium-dca-j-wideband-oscilloscope-mainframe?nid=-32418.536883555.00&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(A_86100C, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    #self.replyNoErr = None

  def _getInfo(self):
    super(A_86100C, self)._getInfo()

    self._idn["opt"] = self.OPT()[0]
    return self._idn

class N1092A(labBase.Oscilloscope):
  """
  Keysight N1092A DCA-M Sampling Oscilloscope
  (http://www.keysight.com/en/pdx-2669072-pn-N1092A/dca-m-sampling-oscilloscope-single-optical-channel?nid=-32528.1169713.00&cc=CA&lc=eng)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(N1092A, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    #self.replyNoErr = None

  def _getInfo(self):
    super(N1092A, self)._getInfo()

    self._idn["opt"] = self.OPT()[0]
    return self._idn
