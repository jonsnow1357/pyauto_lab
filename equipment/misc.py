#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class EFMSFP(labBase.LabEqpt):
  """
  Silabs EFM32 eval board
  (https://www.silabs.com/products/mcu/32-bit/Pages/efm32lg-dk3650.aspx)
  used as SFP host board
  """

  def __init__(self, strConnInfo, **kwargs):
    super(EFMSFP, self).__init__(strConnInfo, speed=115200)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry["err"] = "info"
    self.replyNoErr = ""

  def query(self, msg, TOmultiplier=1):
    res = self._rsrcObj.sendIfCmd(msg, strEndRead=None)

    try:
      res = [t.strip("\n\r\t") for t in res]  # strip special characters
      if ((len(res) > 1) and (res[0] == msg)):  # first line can echo
        res = res[1:]
      if ((len(res) > 1) and (res[0] == res[1])):  # first line can be duplicate
        res = res[1:]
      if ((len(res) > 1) and (res[-1] == res[-2])):  # last line can be duplicate
        res = res[:-1]
      # if last line in reply contains the eom or strEndRead remove it
      if (self._rsrcObj.eom in res[-1]):
        res = res[:-1]
      else:
        logger.warning("sendCmd: {}".format(msg.strip(" \n\r")))
        msg = "missing eom: ({}), {}".format(self._rsrcObj.eom, res)
        logger.warning(msg)
        raise RuntimeError(msg)
    except IndexError:
      logger.warning("query: {}".format(msg.strip(" \n\r")))
      msg = "INCORRECT reply from board: {}".format(res)
      logger.warning(msg)
      raise RuntimeError(msg)

    return res

  def connect(self):
    #self._ifParams["timeout"] = 4 * pyauto_comm.generic.defaultUartTO
    super(EFMSFP, self).connect()
    self._rsrcObj.getIf().interBytePause = 0.02  # TODO: remove after fixing fw
    self._rsrcObj._eos = "\n"
    self._rsrcObj.eom = "#"

  def _getErrors(self, TOmultiplier=2, nReplyLines=1):
    lstErr = []

    res = self.query(self.qry["err"])
    for ln in res:
      if (ln.startswith("error")):
        lstErr.append(ln)

    if ((len(lstErr) == 1) and (lstErr[0] == "errors: 0x0000")):
      return []
    else:
      return lstErr

  def showInfo(self):
    res = self.query("info")
    for ln in res[1:]:
      logger.info(ln)
