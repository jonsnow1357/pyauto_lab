#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class _JITT(labBase.LabEqptModule):
  """
  Anritsu M1900A
  """

  def __init__(self, commIf, iType=None, **kwargs):
    super(_JITT, self).__init__(commIf, **kwargs)
    self.type = iType
    self._typeId = self._getTypeId(iType)

  def _getTypeId(self, iType):
    if iType == 'BUj':
      return 'BUJ:'
    elif iType == 'Rj':
      return 'RJ:'
    elif iType == 'Sj':
      return 'SJ:'
    elif iType == 'Sj2':
      return 'SJ2:'
    elif iType == 'SSC':
      return 'SSC:'
    else:
      return None

  def getMaxAmp(self):
    if self.type == 'Sj':
      freq = self.frequency
      if freq <= (0.1e6):
        return 2000
      elif (freq > (0.1e6)) and (freq <= (1e6)):
        return 200
      elif (freq > (1e6)) and (freq <= (10e6)):
        return 16
      elif (freq > (10e6)) and (freq <= (250e6)):
        return 1
      else:
        return None
    elif self.type == 'Sj2':
      freq = self.frequency
      if self.frequency <= (1e6):
        return 50
      elif (freq > (1e6)) and (freq <= (10e6)):
        return 10
      elif (freq > (10e6)) and (freq <= (250e6)):
        return 0.55
      else:
        return None
    else:
      return None

  def setModule(self):
    cmd = ":UENTry:ID 1"
    self.write(cmd)
    cmd = ":MODule:ID 4"
    self.write(cmd)

  @property
  def amplitude(self):
    self.setModule()
    cmd = (":SOUR:JITT:{}AMPL?".format(self._typeId))
    rtn = self.query(cmd)
    return rtn

  @amplitude.setter
  def amplitude(self, iAmplitude):
    self.setModule()
    maxAmp = self.getMaxAmp()
    if iAmplitude > maxAmp:
      iAmplitude = maxAmp
      logger.warning("Maximuma amplitude is {}".format(maxAmp))
    logger.info('Setting amplitude to {:2.3f}UI'.format(iAmplitude))
    cmd = (":SOUR:JITT:{}AMPL {:.3f}".format(self._typeId, iAmplitude))
    self.write(cmd)

  @property
  def frequency(self):
    self.setModule()
    cmd = (':SOURce:JITTer:{}FREQuency?'.format(self._typeId))
    rtn = float(self.query(cmd, TOmultiplier=5)[-1])
    return rtn

  @frequency.setter
  def frequency(self, iFreq):
    self.setModule()
    logger.info('Setting frequency to {}Hz'.format(iFreq))
    cmd = (':SOURce:JITTer:{}FREQuency {}'.format(self._typeId, int(iFreq)))
    self.write(cmd)

  @property
  def enable(self):
    self.setModule()
    cmd = (':SOURce:JITTer:{}ENABle?'.format(self._typeId))
    print(cmd)
    rtn = self.query(cmd)
    return rtn[0]

  @enable.setter
  def enable(self, iEn):
    self.setModule()
    if iEn:
      enStr = 'ON'
    else:
      enStr = 'OFF'
    logger.info('Setting jitter output to ' + str(enStr))
    cmd = (':SOURce:JITTer:{}ENABle {}'.format(self._typeId, enStr))
    self.write(cmd)

class _Data(labBase.LabEqptModule):

  def __init__(self, commIf, **kwargs):
    super(_Data, self).__init__(commIf, **kwargs)

  def setModule(self):
    cmd = ":UENTry:ID 1"
    self.write(cmd)
    cmd = ":MODule:ID 1"
    self.write(cmd)

  @property
  def modulation(self):
    self.setModule()
    cmd = (":SOURce:MODulation:TYPE?")
    rtn = self.query(cmd)
    return rtn

  @modulation.setter
  def modulation(self, iMod):
    self.setModule()
    validCodes = ['NRZ', 'PAM4']
    if iMod in validCodes:
      logger.info('setting coding {}'.format(iMod))
      cmd = (":SOURce:MODulation:TYPE {}".format(iMod))
      self.write(cmd)
      #time.sleep(1)  # Allowing 1s to set the Modulation type, change to OPC?
      self.waitOnCmd()
    else:
      logger.warning(
          'Wrong Modulation Value. supported Modulations are: {}'.format(validCodes))

  @property
  def amplitude(self):
    self.setModule()
    cmd = (":OUTPut:PAM4:TOTal:AMPLitude?")
    rtn = self.query(cmd)[-1]
    return rtn

  @amplitude.setter
  def amplitude(self, iAmplitude):
    self.setModule()
    if iAmplitude < 70E-3:
      logger.warning('Minimum Data amplitude is 50mV')
      iAmplitude = 70E-3
    elif iAmplitude > 800E-3:
      logger.warning('Maximum Data amplitude is 800mV')
      iAmplitude = 800E-3
    logger.info('setting amplitude {}'.format(iAmplitude))
    cmd = (":OUTPut:PAM4:TOTal:AMPLitude {}".format(iAmplitude))
    self.write(cmd)

  @property
  def enable(self):
    self.setModule()
    cmd = (":OUTPut:DATA:OUTPut?")
    rtn = self.query(cmd)[-1]
    return rtn

  @enable.setter
  def enable(self, iEn):
    self.setModule()
    cmd = (":OUTPut:DATA:OUTPut {}".format(iEn))
    self.write(cmd)

class M1900A(labBase.BERT):
  """
  Keysight M8000 series Bit Error Rate Tester
  (https://www.keysight.com/ca/en/products/bit-error-ratio-testers/m8000-series-bit-error-ratio-testers.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    super(M1900A, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "0,\"No error\""
    self.Sj = None
    self.Sj2 = None
    self.SSC = None
    self.Rj = None
    self.BUj = None
    self.Data = None

  def build(self):
    self.Sj = _JITT(self._rsrcObj, iType="Sj")
    self.Sj2 = _JITT(self._rsrcObj, iType="Sj2")
    self.SSC = _JITT(self._rsrcObj, iType="SSC")
    self.Rj = _JITT(self._rsrcObj, iType="Rj")
    self.BUj = _JITT(self._rsrcObj, iType="BUj")
    self.Data = _Data(self._rsrcObj)

  def _getInfo(self):
    super(M1900A, self)._getInfo()

    self._idn["opt"] = self.OPT()[0]
    return self._idn

  @property
  def globalEnable(self):
    cmd = (":SOURce:OUTPut:ASET?")
    rtn = self.query(cmd)[-1]
    return rtn

  @globalEnable.setter
  def globalEnable(self, iEn):
    cmd = (":SOURce:OUTPut:ASET {}".format(iEn))
    self.write(cmd)
