#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

class T3DMM(labBase.Multimeter):
  """
  Teledyne T3DMM Series Digital Multimeters
  (https://teledynelecroy.com/digital-multimeters/detail.aspx?modelid=11032)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    super(T3DMM, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "0,\"No error\""

    self.nOut = 1

  def setChannel(self, mode, chId=None):
    if (mode == labBase.MultimeterMode_DCV):
      self.write("CONF:VOLT:DC")
    elif (mode == labBase.MultimeterMode_DCI):
      self.write("CONF:CURR:DC")
    elif (mode == labBase.MultimeterMode_ACV):
      self.write("CONF:VOLT:AC")
    elif (mode == labBase.MultimeterMode_ACI):
      self.write("CONF:CURR:AC")
    elif (mode == labBase.MultimeterMode_RES2W):
      self.write("CONF:RES")
    elif (mode == labBase.MultimeterMode_RES4W):
      self.write("CONF:FRES")
    elif (mode == labBase.MultimeterMode_CAP):
      self.write("CONF:CAP")
    elif (mode == labBase.MultimeterMode_DIO):
      self.write("CONF:DIOD")
    elif (mode == labBase.MultimeterMode_CONT):
      self.write("CONF:CONT")
    elif (mode == labBase.MultimeterMode_TEMP):
      self.write("CONF:TEMP")
    elif (mode == labBase.MultimeterMode_FREQ):
      self.write("CONF:FREQ")
    else:
      msg = "INCORRECT mode: {}".format(mode)
      logger.error(msg)
      raise RuntimeError
    self.waitOnCmd()

  def getChannel(self, chId=None):
    res = self.query("CONF?")
    #print("DBG", res[0])
    res = res[0].strip("\"")
    if (res.startswith("VOLT ")):
      mode = labBase.MultimeterMode_DCV
    elif (res.startswith("CURR ")):
      mode = labBase.MultimeterMode_DCI
    elif (res.startswith("VOLT:AC ")):
      mode = labBase.MultimeterMode_ACV
    elif (res.startswith("CURR:AC ")):
      mode = labBase.MultimeterMode_ACI
    elif (res.startswith("RES ")):
      mode = labBase.MultimeterMode_RES2W
    elif (res.startswith("FRES ")):
      mode = labBase.MultimeterMode_RES4W
    elif (res.startswith("CAP ")):
      mode = labBase.MultimeterMode_CAP
    elif (res.startswith("DIOD")):
      mode = labBase.MultimeterMode_DIO
    elif (res.startswith("CONT")):
      mode = labBase.MultimeterMode_CONT
    elif (res.startswith("TEMP")):
      mode = labBase.MultimeterMode_TEMP
    elif (res.startswith("FREQ ")):
      mode = labBase.MultimeterMode_FREQ
    else:
      raise RuntimeError

    self.write("INIT")
    res = self.query("READ?", TOmultiplier=4)
    #print("DBG", res)
    val = float(res[0])

    return {"mode": mode, "val": val}

class T3PS(labBase.PowerSupply):
  """
  Teledyne T3PS Series Power Supplies
  (https://teledynelecroy.com/power-supplies/detail.aspx?modelid=11037)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    super(T3PS, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    self.SCPI = True
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "No Error"

  def _getInfo(self):
    super(T3PS, self)._getInfo()

    if (self._idn["id"].startswith("TELEDYNE TEST TOOLS,T3PS43203P")):
      self.nOut = 4
    elif (self._idn["id"].startswith("TELEDYNE TEST TOOLS,T3PS30063P")):
      self.nOut = 3
    else:
      logger.warning("UNRECOGNIZED id: {}".format(self._idn["id"]))

  def setOutput(self, voltage, current, outId=None):
    self._chkChannelId(outId)

    v = pyauto_base.misc.chkFloat(voltage, valId="voltage", minVal=0.0)
    c = pyauto_base.misc.chkFloat(current, valId="current", minVal=0.0)

    self.write("SOURCE{}:VOLTAGE {}".format(outId, v))
    self.write("SOURCE{}:CURRENT {}".format(outId, c))

  def getOutput(self, outId=None):
    self._chkChannelId(outId)

    res = self.query("MEASURE{}:VOLTAGE?".format(outId))
    v = float(res[0].replace("V", ""))
    res = self.query("MEASURE{}:CURRENT?".format(outId))
    i = float(res[0].replace("A", ""))
    return {"voltage": v, "current": i}

  def setProtection(self, voltage, current, outId=None):
    self._chkChannelId(outId)

    self.write("OUTPUT{}:OVP {}".format(outId, voltage))
    self.write("OUTPUT{}:OVP:STATE 1")
    self.waitOnCmd()
    self.write("OUTPUT{}:OCP {}".format(outId, current))
    self.write("OUTPUT{}:OCP:STATE 1")
    self.waitOnCmd()

  def enable(self, outId=None):
    self._chkChannelId(outId)

    logger.info("enable OUT{}".format(outId))
    self.write("OUTPUT{}:STATE ON".format(outId))

  def disable(self, outId=None):
    self._chkChannelId(outId)

    logger.info("disable OUT{}".format(outId))
    self.write("OUTPUT{}:STATE OFF".format(outId))

class T3EL(labBase.PowerLoad):
  """
  Teledyne T3EL Series Power Loads
  (https://www.teledynelecroy.com/electronic-loads/default.aspx)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    super(T3EL, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    #self.SCPI = False
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "0"

  def setChannel(self, mode, value, chId=None):
    self._chkChannelId(chId)

    if (mode == labBase.PwrLoad_CC):
      self.write("FUNC CURRENT")
      self.write("CURRENT {:.3f}".format(value))
    elif (mode == labBase.PwrLoad_CR):
      self.write("FUNC RESISTANCE")
      self.write("RESISTANCE {:.3f}".format(value))
    elif (mode == labBase.PwrLoad_CP):
      self.write("FUNC POWER")
      self.write("POWER {:.3f}".format(value))
    elif (mode == labBase.PwrLoad_CV):
      self.write("FUNC VOLTAGE")
      self.write("VOLTAGE {:.3f}".format(value))
    else:
      msg = "INCORRECT mode: {}".format(mode)
      logger.error(msg)
      raise RuntimeError(msg)

  def getChannel(self, chId=None):
    self._chkChannelId(chId)

    res = self.query("FUNC?")
    mode = res[0]
    val = float("nan")
    if (mode == "CURRENT"):
      mode = labBase.PwrLoad_CC
      res = self.query("CURRENT?")
      val = float(res[0])
    elif (mode == "RESISTANCE"):
      mode = labBase.PwrLoad_CR
      res = self.query("RESISTANCE?")
      val = float(res[0])
    elif (mode == "POWER"):
      mode = labBase.PwrLoad_CP
      res = self.query("POWER?")
      val = float(res[0])
    elif (mode == "VOLTAGE"):
      mode = labBase.PwrLoad_CV
      res = self.query("VOLTAGE?")
      val = float(res[0])
    else:
      mode = "???"
    return {"mode": mode, "val": val}

  def enable(self, chId=None):
    self._chkChannelId(chId)
    self.write("INPUT:STATE ON")

  def disable(self, chId=None):
    self._chkChannelId(chId)
    self.write("INPUT:STATE OFF")

class T3SA(labBase.SpectrumAnalyzer):
  """
  Teledyne T3SA Series Spectrum Analyzers
  (https://www.teledynelecroy.com/spectrum-analyzers/)
  """

  def __init__(self, strConnInfo, **kwargs):
    if ("eom" not in kwargs.keys()):
      kwargs["eom"] = "\n"
    super(T3SA, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    #self.SCPI = False
    #self.qry = {"id": None, "err": None}
    self.replyNoErr = "0"

  def writeImg(self, path):
    if (not path.endswith(".png")):
      msg = "INCORRECT file name: {}".format(path)
      logger.error(msg)
      raise RuntimeError(msg)

    self.write(":MMEMORY:STORE PNG,\"{}\"".format(path))
    self.waitOnCmd()
    logger.info("{} written".format(path))
