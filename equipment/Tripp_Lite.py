#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class PDUx(labBase.PowerSwitch):
  """
  Tripp Lite Switched PDUs
  (https://www.tripplite.com/products/power-distribution-units-pdus-switched~15-70)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(PDUx, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry["err"] = "NA"
    #self.replyNoErr = None
    #self.nOut = 1

  def _menu_choice(self, choice):
    logger.info("<menu choice> {}".format(choice))
    res = self.query(choice, TOmultiplier=6)
    #print("DBG", res)
    if (res[0] != choice):
      msg = "INCORRECT reply: {}".format(res)
      logger.error(msg)
      raise RuntimeError(msg)
    return res

  def _getInfo(self):
    self._menu_choice("M")
    self._menu_choice("1")
    res = self._menu_choice("2")
    for ln in res:
      tmp = ln.strip().split()
      if ("Product" in tmp):
        self._idn["Model"] = tmp[-1]
      if ("Firmware" in tmp):
        self._idn["Version"] = tmp[-1]
      if ("Serial" in tmp):
        self._idn["Serial"] = tmp[-1]
    return self._idn

  def connect(self):
    super(PDUx, self).connect()

    self._rsrcObj.eos = "\n"
    self._rsrcObj.eom = ">> "
    res = self.read(TOmultiplier=2)
    if (not res[1].startswith("Tripp Lite")):
      msg = "INCORRECT reply: {}".format(res)
      logger.error(msg)
      raise RuntimeError(msg)

    self._menu_choice("M")
    self._menu_choice("1")
    res = self._menu_choice("2")
    cnt = 0
    for ln in res:
      ln = ln.strip(" ")
      if (ln.startswith("Loads Controllable")):
        cnt = int(ln.split()[-1])
    if (cnt <= 0):
      msg = "CANNOT determine outlet count"
      logger.error(msg)
      raise RuntimeError
    self.nOut = cnt
    logger.info("{} reports {:d} outlet(s)".format(self.__class__.__name__, self.nOut))

  def enable(self, outId=None):
    self._chkChannelId(outId)

    self._menu_choice("M")
    self._menu_choice("1")
    self._menu_choice("5")
    self._menu_choice("1")

    res = self._menu_choice(str(outId))
    bContinue = False
    for ln in res:
      if (ln.strip() == "3- Turn Load On"):
        bContinue = True
        break
    if (bContinue):
      self._menu_choice("3")
      self._menu_choice("Y")
      logger.info("enable OUT{}".format(outId))

  def disable(self, outId=None):
    self._chkChannelId(outId)

    self._menu_choice("M")
    self._menu_choice("1")
    self._menu_choice("5")
    self._menu_choice("1")

    res = self._menu_choice(str(outId))
    bContinue = False
    for ln in res:
      if (ln.strip() == "3- Turn Load Off"):
        bContinue = True
        break
    if (bContinue):
      self._menu_choice("3")
      self._menu_choice("Y")
      logger.info("disable OUT{}".format(outId))
