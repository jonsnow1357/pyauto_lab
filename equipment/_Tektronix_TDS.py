#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class TDS200(labBase.Oscilloscope):
  """
  Tektronix TDS200 series
  (http://www.tek.com/datasheet/tds200-series)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(TDS200, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    #self.SCPI = False
    self.qry["err"] = "custom"
    #self.replyNoErr = None

  def _getErrors(self, TOmultiplier=2, nReplyLines=1):
    lstErr = []

    res = self.query("ERRLOG:FIRST?", TOmultiplier=TOmultiplier)
    #logger.info(res)
    if (len(res) == 0):
      return lstErr
    elif (res[0] == "\"\""):
      return lstErr
    else:
      lstErr.append(res)

    while (True):
      res = self.query("ERRLOG:NEXT?", TOmultiplier=TOmultiplier)
      #logger.info(res)
      if (len(res) == 0):
        break
      elif (res[0] == "\"\""):
        break
      else:
        lstErr.append(res)

    return lstErr

  def _getInfo(self):
    super(TDS200, self)._getInfo()

    res = self.query("CALIBRATE:STATUS?")
    self._idn["CAL"] = res[0]
    res = self.query("DIAG:RESULT:FLAG?")
    self._idn["DIAG"] = res[0]
    return self._idn

class TDS600(labBase.Oscilloscope):
  """
  Tektronix TDS600 series
  (http://www.tek.com/datasheet/tds694c-tds684c)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(TDS600, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    #self.SCPI = False
    self.qry["err"] = "EVMSG?"
    self.replyNoErr = ""

  def _getErrors(self, TOmultiplier=2, nReplyLines=1):
    lstErr = []

    while (True):
      res = self.query(self.qry["err"], TOmultiplier=TOmultiplier)
      #logger.info(res)
      if (len(res) == 0):
        break
      elif (res[0] == "0,\"No events to report - queue empty\""):
        break
      elif (res[0] == "1,\"No events to report - new events pending *ESR?\""):
        self.ESR()
      else:
        lstErr.append(res)

    return lstErr

  def _getInfo(self):
    super(TDS600, self)._getInfo()

    self._idn["opt"] = self.OPT()[0]
    return self._idn

class TDS2000(labBase.Oscilloscope):
  """
  Tektronix TDS2000 series
  (https://www.tek.com/oscilloscope/tds2000-digital-storage-oscilloscope)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(TDS2000, self).__init__(strConnInfo, **kwargs)

    self.IEEE_488_2 = True
    # self.SCPI = False
    self.qry["err"] = "EVMSG?"
    self.replyNoErr = ""

  def _getErrors(self, TOmultiplier=2, nReplyLines=1):
    lstErr = []

    while (True):
      res = self.query(self.qry["err"], TOmultiplier=TOmultiplier)
      #logger.info(res)
      if (len(res) == 0):
        break
      elif (res[0] == "0,\"No events to report - queue empty\""):
        break
      elif (res[0] == "1,\"No events to report - new events pending *ESR?\""):
        self.ESR()
      else:
        lstErr.append(res)

    return lstErr
