#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_lab.equipment.base as labBase

class WebRelay(labBase.PowerSwitch):
  """
  Xytronics electrical relay
  (https://www.controlbyweb.com/webrelay/specs.html)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(WebRelay, self).__init__(strConnInfo, **kwargs)

    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry["err"] = "NA"
    #self.replyNoErr = None
    #self.nOut = 1

  def _getInfo(self):
    res = self._rsrcObj.get(url="/home.html")
    res = re.sub(r"<[^>]*>", "\n", res).split("\n")  # strip tags an split
    res = [t.strip() for t in res]  # strip spaces
    res = [t for t in res if (t != "")]  # remove empty lines
    for ln in res:
      if (ln.startswith("Model: ")):
        self._idn["model"] = ln[7:]
      if (ln.startswith("Serial Number: ")):
        self._idn["SN"] = ln[15:]
      if (ln.startswith("Product Revision: ")):
        self._idn["ver"] = ln[18:]

  def enable(self, outId=None):
    logger.info("enable OUT")
    self._rsrcObj.get(url="/stateFull.xml?relayState=1")

  def disable(self, outId=None):
    logger.info("disable OUT")
    self._rsrcObj.get(url="/stateFull.xml?relayState=0")
