#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

class MaxTC(labBase.ThermalChamber):
  """
  Mechanical Devices MaxTC controller,
  (https://mechanical-devices.com/portfolio-posts/maxtc-high-power-temperature-forcing-system/)
  """

  def __init__(self, strConnInfo, **kwargs):
    super(MaxTC, self).__init__(strConnInfo=strConnInfo, **kwargs)

    self._rsrcObj.eos = ""
    #self.IEEE_488_2 = False
    #self.SCPI = False
    self.qry = {"id": None, "err": "NA"}
    #self.replyNoErr = None

    self._tempSetLim = [-15.0, 115.0]
    self.tempReadLim = [-20.0, 120.0]
    self._pSetPointAddr = [751, 752, 753, 754, 755, 756, 757, 758, 759, 760]
    self._pRateAddr = [761, 762, 763, 764, 765, 766, 767, 768, 769, 770]
    self._pSoakAddr = [771, 772, 773, 774, 775, 776, 777, 778, 779, 780]
    self._pStepAddr = [107, 108, 109, 110, 112, 113, 114, 125, 128, 129]
    self._pAddr = [27, 130, 131]

  @property
  def temperature(self):
    res = self.query("MI0006?")
    return int(res[0].split(",")[1]) / 10.0

  @property
  def status(self):
    """
    Reads registers play (MB0020), pause (MB0327) and converge (MB0083)
    :return: dictionary {"play": int, "pause": int, "converge": int}
    """
    res = self.query("MB0020?")
    play = res[0].split(",")[1]
    res = self.query("MB0327?")
    pause = res[0].split(",")[1]
    res = self.query("MB0083?")
    conv = res[0].split(",")[1]
    res = self.query("MB0074?")
    temp_rate = res[0].split(',')[1]
    return {
        "play": int(play),
        "pause": int(pause),
        "converge": int(conv),
        "rate": int(temp_rate)
    }

  @property
  def setPoint(self):
    res = self.query("MI0699?")
    return int(res[0].split(",")[1]) / 10.0

  @setPoint.setter
  def setPoint(self, val):
    if (val < self._tempSetLim[0]):
      logger.warning("set point of {} C NOT allowed".format(val))
      set_point = self._tempSetLim[0]
    elif val > self._tempSetLim[1]:
      logger.warning("set point of {} C NOT allowed".format(val))
      set_point = self._tempSetLim[1]
    else:
      set_point = float(val)

    res = self.query("MI0699,{:0>4}".format(int(set_point * 10)))
    if (res[0] != "OK"):
      msg = "CANNOT change set point"
      logger.error(msg)
      raise RuntimeError(msg)

    set_point = self.setPoint
    logger.info("set point to {} C".format(set_point))

  @property
  def tempRate(self):
    res = self.query("MI0045?")
    return int(res[0].split(',')[1])

  @tempRate.setter
  def tempRate(self, val):
    res = self.query("MI0045,{:0>4}".format(val))
    if (res[0] != "OK"):
      msg = "CANNOT set rate"
      logger.error(msg)
      raise RuntimeError(msg)

  def _getInfo(self):
    self._idn["model"] = "MaxTC"

  def tempRateEn(self):
    res = self.query("MB0074,1")
    logger.info("temp rate enabled")
    if (res[0] != "OK"):
      msg = "CANNOT en/dis rate"
      logger.error(msg)
      raise RuntimeError(msg)

  def tempRateDis(self):
    res = self.query("MB0074,0")
    logger.info("temp rate disabled")
    if (res[0] != "OK"):
      msg = "CANNOT en/dis rate"
      logger.error(msg)
      raise RuntimeError(msg)

  def chkStatus(self):
    """
    checks equipment status and throws Exception if:
    - the play bit is not set anymore (some failures do that)
    - the temperature is outside expected limits

    :return: [<current_temperature>, <set_point>, <at_set_point>]
    """
    sts = self.status
    if (sts["play"] != 0):
      msg = "run FAILURE"
      logger.error(msg)
      self.stop()
      raise labBase.LabEqptException(msg)

    set_point = self.setPoint
    curr_temp = self.temperature
    if ((curr_temp < self.tempReadLim[0]) or (curr_temp > self.tempReadLim[1])):
      msg = "temp FAILURE {} C".format(curr_temp)
      logger.error(msg)
      self.stop()
      raise labBase.LabEqptException(msg)

    logger.info("temperature {} C / set point {} C ({})".format(curr_temp, set_point,
                                                                sts["converge"]))
    return [curr_temp, set_point, sts["converge"]]

  def start(self):
    res = self.status
    if (res["play"] == 0):
      return

    res = self.query("MB0074,1")
    if (res[0] != "OK"):
      msg = "CANNOT enable rate"
      logger.error(msg)
      raise RuntimeError(msg)
    res = self.query("MB0020,0")
    if (res[0] != "OK"):
      msg = "CANNOT start"
      logger.error(msg)
      raise RuntimeError(msg)

    logger.info("wait for start ...")
    tlim = datetime.timedelta(seconds=(30 * 60))
    t0 = datetime.datetime.now()
    dt = datetime.datetime.now() - t0
    while (dt < tlim):
      sts = self.status
      if (sts["play"] != 0):
        msg = "start FAILURE"
        logger.error(msg)
        raise RuntimeError(msg)
      if (sts["converge"] != 0):
        logger.info("started")
        return
      pyauto_base.misc.waitWithPrint(10)
      set_point = self.setPoint
      res = self.temperature
      logger.info("temperature {} C / set point {} C ({})".format(
          res, set_point, sts["converge"]))
    msg = "start TIMEOUT"
    logger.error(msg)
    raise RuntimeError(msg)

  def stop(self):
    res = self.status
    if (res["play"] == 1):
      return

    res = self.query("MB0020,1")
    if (res[0] != "OK"):
      msg = "CANNOT stop"
      logger.error(msg)
      raise RuntimeError(msg)
    logger.info("stopped")

# @property
# def pause(self):
#   res = self.query("MB0327?")
#   oPause = bool(int(res[0].split(",")[1]))
#   return oPause
#
# @pause.setter
# def pause(self, iEnable):
#   res = self.query("MB0327,{}".format(iEnable))
#   if (res[0] != "OK"):
#     msg = "CANNOT pause"
#     logger.error(msg)
#     raise RuntimeError(msg)

  def getProfileStat(self, iProfile):
    if (iProfile not in range(1, 4)):
      logger.warning("Invalid profile {}, valid options are 1, 2, 3".format(iProfile))
      return -1

    res = self.query("MB{:0>4}?".format(self._pAddr[0]))
    oP1 = bool(int(res[0].split(",")[1]))
    return oP1

  def enableProfile(self, iProfile, iVal):
    if (iProfile not in range(1, 4)):
      logger.error("Invalid profile {}, valid options are 1, 2, and 3".format(iProfile))
      return -1

    res = self.query("MB{:0>4},{}".format(self._pAddr[iProfile - 1], int(bool(iVal))))
    if (res[0] != "OK"):
      msg = "CANNOT enable profile"
      logger.error(msg)
      raise RuntimeError(msg)

  def setProfile(self,
                 iProfile=1,
                 iSetPoint=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                 iRate=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                 iSoak=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]):
    if (iProfile == 1):
      for each in range(len(iSetPoint)):
        res = self.query("MI{:0>4},{:0>4}".format(self._pSetPointAddr[each],
                                                  iSetPoint[each] * 10))
        if (res[0] != "OK"):
          msg = "Error while setting value {} in profile {}".format(
              iSetPoint[each] * 10, iProfile)
          logger.error(msg)
          raise RuntimeError(msg)

      for each in range(len(iRate)):
        res = self.query("MI{:0>4},{:0>4}".format(self._pRateAddr[each], iRate[each]))
        if (res[0] != "OK"):
          msg = "Error while setting value {} in profile {}".format(
              iSetPoint[each] * 10, iProfile)
          logger.error(msg)
          raise RuntimeError(msg)

      for each in range(len(iSoak)):
        res = self.query("MI{:0>4},{:0>4}".format(self._pSoakAddr[each], iSoak[each]))
        if (res[0] != "OK"):
          msg = "Error while wetting value {} in profile {}".format(
              iSetPoint[each] * 10, iProfile)
          logger.error(msg)
          raise RuntimeError(msg)
    else:
      logger.error("Only profile 1 is supported")
