#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
from ._base_eqpt import *

class PowerSwitch(LabEqpt):

  def __init__(self, strConnInfo, **kwargs):
    super(PowerSwitch, self).__init__(strConnInfo, **kwargs)
    self._restartDelay = 4
    self.nOut = 1

  def _chkChannelId(self, outId):
    chkChannelId(outId, self.nOut)

  def enable(self, outId=None):
    raise NotImplementedError

  def disable(self, outId=None):
    raise NotImplementedError

  def restart(self, outId=None):
    self.disable(outId)
    time.sleep(self._restartDelay)
    self.enable(outId)

class PowerSupply(LabEqpt):

  def __init__(self, strConnInfo, **kwargs):
    super(PowerSupply, self).__init__(strConnInfo, **kwargs)
    self._restartDelay = 4
    self.nOut = 1

  def _chkChannelId(self, outId):
    chkChannelId(outId, self.nOut)

  def setOutput(self, voltage, current, outId=None):
    """
    :param voltage:
    :type voltage: float
    :param current:
    :type current: float
    :param outId:
    :type outId: None|int
    """
    raise NotImplementedError

  def setProtection(self, voltage, current, outId=None):
    """
    :param voltage:
    :type voltage: float
    :param current:
    :type current: float
    :param outId:
    :type outId: None|int
    """
    raise NotImplementedError

  def getOutput(self, outId=None):
    """
    :param outId:
    :type outId: None|int
    :return: {"voltage": ..., "current": ...}
    :rtype: dict
    """
    raise NotImplementedError

  def enable(self, outId=None):
    raise NotImplementedError

  def disable(self, outId=None):
    raise NotImplementedError

  def restart(self, outId=None):
    self.disable(outId)
    time.sleep(self._restartDelay)
    self.enable(outId)

PwrLoad_CC = "CC"
PwrLoad_CR = "CR"
PwrLoad_CP = "CP"
PwrLoad_CV = "CV"
lstPwrLoadModes = (PwrLoad_CC, PwrLoad_CR, PwrLoad_CP, PwrLoad_CV)

class PowerLoad(LabEqpt):

  def __init__(self, strConnInfo, **kwargs):
    super(PowerLoad, self).__init__(strConnInfo, **kwargs)
    self.nOut = 1

  def _chkChannelId(self, chId):
    chkChannelId(chId, self.nOut)

  def setChannel(self, mode, value, chId=None):
    """
    :param mode:
    :type mode: str
    :param value:
    :type value: float
    :param chId:
    :type chId: None|int
    """
    raise NotImplementedError

  def getChannel(self, chId=None):
    """
    :param chId:
    :type chId: None|int
    :return: {"mode": ..., "val": ...}
    :rtype: dict
    """
    raise NotImplementedError

  def enable(self, chId=None):
    raise NotImplementedError

  def disable(self, chId=None):
    raise NotImplementedError
