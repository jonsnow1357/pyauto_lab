#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase
import pyauto_lab.equipment.Agilent

nRuns = 1
nMeas = 1
eqptInfo = None

def _test_channel1(eqpt):
  dictInfo = eqpt.getInfo()
  if (dictInfo["sensors"][0] is None):
    logger.warning("MISSING sensor1")
    return

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    for fq in (500, 1000, 5000, 10000, 20000):
      eqpt.setFreq(fq, chId=1)
      logger.info("freq1: {}".format(eqpt.frequency))
      for u in ("W", "dBm"):
        eqpt.setUnit(u, chId=1)
        logger.info("pwr1: {} {}".format(eqpt.getPower(chId=1), eqpt.getUnit(chId=1)))

def _test_channel2(eqpt):
  dictInfo = eqpt.getInfo()
  if (dictInfo["sensors"][1] is None):
    logger.warning("MISSING sensor2")
    return

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    for fq in (500, 1000, 5000, 10000, 20000):
      eqpt.setFreq(fq, chId=2)
      logger.info("freq1: {}".format(eqpt.frequency))
      for u in ("W", "dBm"):
        eqpt.setUnit(u, chId=2)
        logger.info("pwr1: {} {}".format(eqpt.getPower(chId=2), eqpt.getUnit(chId=2)))

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()

  eqpt.write("INIT:CONT 1")
  eqpt.waitOnCmd()
  eqpt.write("OUTP:ROSC 1")
  eqpt.waitOnCmd()

  _test_channel1(eqpt)
  _test_channel2(eqpt)

  eqpt.write("OUTP:ROSC 0")
  eqpt.waitOnCmd()

def mainApp():
  dictArgs = {}
  if (cliArgs["adapter"] == "prologix"):
    dictArgs["adapter_name"] = cliArgs["adapter"]
    dictArgs["adapter_gpibAddr"] = 10
  eqpt = pyauto_lab.equipment.Agilent.N1912A(cliArgs["conn"], eqptGPIBAddr=10)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for N1912A"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
