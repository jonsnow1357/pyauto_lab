#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase
import pyauto_lab.equipment.Viavi

nRuns = 1
nMeas = 1

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()

  res = eqpt.query(":SYSTEM:LEGACY:MODE?")
  logger.info("GPIB legacy mode: {}".format(res[0]))
  res = eqpt.query(":SYSTEM:LAYOUT?")
  logger.info("slots: {}".format(res[0]))

  #res = eqpt.query(":SYSTEM:CARD:INFORMATION? 1,2")

  eqpt.write(":SYSTEM:INTLOCK 1")  # enable chassis laser

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    # switch LCS on slot 2
    #for out in (0, 1, 2, 3, 4):
    #  eqpt.write(":ROUTE:CLOSE 1,2,1,1,{}".format(out))
    #  eqpt.waitOnCmd(timeout=2.0)
    #  pyauto_base.misc.waitWithPrint(2)

    # set VOA on slot 3
    #eqpt.write(":OUTPUT:BBLOCK 1,3,1,0")  # enable card laser
    #for wl in (1310, 1490, 1550):
    #  eqpt.write(":OUTPUT:WAVELENGTH 1,3,1,{}".format(wl))
    #  eqpt.waitOnCmd(timeout=10.0)
    #  for atten in (0, 5, 10, 15, 20):
    #    eqpt.write(":OUTPUT:ATTENUATION 1,3,1,{}".format(atten))
    #    eqpt.waitOnCmd(timeout=10.0)
    #eqpt.write(":OUTPUT:BBLOCK 1,3,1,1")  # disable card laser

  eqpt.write(":SYSTEM:INTLOCK 0")  # disable chassis laser

def mainApp():
  dictArgs = {}
  if (cliArgs["adapter"] == "prologix"):
    dictArgs["adapter_name"] = cliArgs["adapter"]
    dictArgs["adapter_gpibAddr"] = 13
  eqpt = pyauto_lab.equipment.Viavi.MAP(cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for MAP"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
