#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase
import pyauto_lab.equipment.Viavi

nRuns = 1
nMeas = 1

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()

  #res = eqpt.query(":SYSTEM:APPLICATION:APPLICATIONS?")
  #logger.info(res)
  res = eqpt.query(":SYSTEM:APPLICATION:CAPPLICATIONS?")
  lstCApps = sorted(res[0].split(","))
  logger.info("current applications: {}".format(lstCApps))

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    for appId in lstCApps:
      eqpt.write(":SYSTEM:APPLICATION:SELECT {}".format(appId))
      eqpt.showErrors()

      eqpt.write(":SESSION:CREATE")
      res = eqpt.query(":SYSTEM:ERROR?")
      if (len(res) == 0):
        msg = "EMPTY reply"
        logger.error(msg)
        raise RuntimeError(msg)

      res = eqpt.query(":SENSE:DATA? CSTATUS:PHYSICAL:SIGNAL")
      sig = res[0]
      res = eqpt.query(":SENSE:DATA? ACOUNT:PHYSICAL:SIGNAL:LOSS")
      sigLossCnt = res[0]
      res = eqpt.query(":SENSE:DATA? ASECOND:PHYSICAL:SIGNAL:LOSS")
      sigLossTime = res[0]
      res = eqpt.query(":SENSE:DATA? CSTATUS:PCS:PHY:LINK:ACTIVE")
      link = res[0]
      res = eqpt.query(":SENSE:DATA? ECOUNT:PCS:PHY:LINK:LOSS")
      linkLossCnt = res[0]
      res = eqpt.query(":SENSE:DATA? ESECOND:PCS:PHY:LINK:LOSS")
      linkLossTime = res[0]
      logger.info("{}: signal {}, signal loss {}/{}s, "
                  "link {}, link loss {}/{}s".format(appId, sig, sigLossCnt, sigLossTime,
                                                     link, linkLossCnt, linkLossTime))

      res = eqpt.query(":SENSE:DATA? ECOUNT:PCS:PHY:SYMBOL")
      rxsym_err = res[0]
      res = eqpt.query(":SENSE:DATA? CSTATUS:MAC:ETH:FRAME:DETECT")
      rxfrm = res[0]
      res = eqpt.query(":SENSE:DATA? ECOUNT:MAC:ETH:FRAME")
      rxfrm_err = res[0]
      logger.info("{}: RXSymErr {}, RXFrm {}, RXFrmErr {}".format(
          appId, rxsym_err, rxfrm, rxfrm_err))

      eqpt.write(":SESSION:END")
      res = eqpt.query(":SYSTEM:ERROR?")
      if (len(res) == 0):
        msg = "EMPTY reply"
        logger.error(msg)
        raise RuntimeError(msg)
      #eqpt.showErrors()

def mainApp():
  dictArgs = {}
  #dictArgs["TOmultiplier"] = 4
  eqpt = pyauto_lab.equipment.Viavi.TB5800(cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for TB5800"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
