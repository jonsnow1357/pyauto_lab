#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase
import pyauto_lab.equipment.Keysight

nRuns = 1
nMeas = 1

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()
  eqpt.build()
  eqpt.addAWG()
  eqpt.AWG.ch1.enable = 1
  eqpt.AWG.ch1.crestFactor = 2
  eqpt.AWG.ch1.highFreq = 14E9
  eqpt.AWG.ch1.lowFreq = 1E9
  eqpt.waitOnCmd(timeout=10)
  logger.info('AWG ch1 is enabled? %s ' % eqpt.AWG.ch1.enable)

  # BER = eqpt.JTOL(ATL)
  # BER = eqpt.JitterTolerance(ATL)
  # logger.info(BER)
  #_test(eqpt)
  #eqpt.showErrors()
  PJ1LFampl_max = eqpt.ch1.PJ1LF.getMaxAmp()
  PJ1HFampl_max = eqpt.ch1.PJ1HF.getMaxAmp()
  logger.info('PJ1LF Max Amplitude is %f' % PJ1LFampl_max)
  logger.info('PJ1HF Max Amplitude is %f' % PJ1HFampl_max)

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    eqpt.ch1.Data.setEnable(True)
    eqpt.ch1.Data.setEnable(False)
    eqpt.ch1.Data.setEnable(True)

def mainApp():
  dictArgs = {}
  eqpt = pyauto_lab.equipment.Keysight.M80xx(cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    #_test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for M80xx"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
