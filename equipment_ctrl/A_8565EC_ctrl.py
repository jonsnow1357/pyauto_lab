#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.plot
import pyauto_lab.equipment.base as labBase
import pyauto_lab.equipment.Agilent

nRuns = 1
nMeas = 1

def _plot(eqpt):
  res = eqpt.query("AUNITS?")
  if (res[0] != "DBM"):
    msg = "UNEXPECTED amplitude units: {}".format(res[0])
    logger.error(msg)
    raise RuntimeError(msg)
  res = eqpt.query("TDF?")
  if (res[0] != "P"):
    msg = "UNEXPECTED trace data format: {}".format(res[0])
    logger.error(msg)
    raise RuntimeError(msg)

  dictInfo = eqpt.getInfo()
  fc = float(eqpt.cf)
  span = float(eqpt.span)
  div = round(float(eqpt.div), 2)
  ref = round(float(eqpt.ref), 2)
  attn = float(eqpt.attn)
  rbw = float(eqpt.rbw)
  vbw = float(eqpt.vbw)
  mid = ref - (5 * div)
  #logger.info([fc, span, ref, div, attn, rbw, vbw])

  res = eqpt.query("TRA?", TOmultiplier=2)
  data = [float(f) for f in res[0].split(",")]
  #print(data)

  ts = pyauto_base.misc.getDateTime()
  plot = pyauto_base.plot.GridPlot(ndivX=10, ndivY=10)
  plot.setFreq(fc, span, mode="center_span")
  plot.addData(data, center=mid, scale=div, unit="dBm")
  plot.txt_ur = "RBW={} kHz, VBW={} kHz".format(rbw / 1000, vbw / 1000)
  plot.txt_ul = "ATTN={} dB".format(attn)
  #plot.txt_ll = "lower left"

  if (cliArgs["mkr"]):
    marker = []
    tmp0 = eqpt.query("MKF?")[0]
    #logger.info(tmp0)
    tmp1 = eqpt.query("MKA?")[0]
    #logger.info(tmp1)
    tmp2 = eqpt.query("MKD?")[0]
    #logger.info(tmp2)
    marker.append([float(tmp0), float(tmp1)])
    #logger.info(marker)
    for mkr in marker:
      plot.addMarker(mkr[0], mkr[1])

  plot.ts = ts
  plot.title = "CH1"
  plot.show()

  dirOut = pyauto_base.fs.mkOutFolder()
  plot.fPath = os.path.join(dirOut, "{}_{}".format(dictInfo["id"], ts))
  plot.write()

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()

  if (cliArgs["plot"]):
    _plot(eqpt)
    return

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))

def mainApp():
  dictArgs = {}
  if (cliArgs["adapter"] == "prologix"):
    dictArgs["adapter_name"] = cliArgs["adapter"]
    dictArgs["adapter_gpibAddr"] = 18
  eqpt = pyauto_lab.equipment.Agilent.A_8565EC(cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for 8565EC"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("--plot", action="store_true", default=False, help="get screenshot")
  parser.add_argument("--mkr", action="store_true", default=False, help="get markers")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
