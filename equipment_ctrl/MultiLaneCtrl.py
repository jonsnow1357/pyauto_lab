#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse
import random
import json

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

nRuns = 1
nMeas = 1

def _dump(eqpt):
  dump = []
  eqpt.smbusWr8(0x50, 0x7F, 0x00)
  data = []
  for addr in range(16):
    res = eqpt.smbusRd(0x50, (addr * 16), 16)
    data += res
  dump.append({"page": "A0", "data": data})

  eqpt.smbusWr8(0x50, 0x7F, 13)
  data = []
  for addr in range(16):
    res = eqpt.smbusRd(0x50, (addr * 16), 16)
    data += res
  dump.append({"page": "A0x13", "data": data})

  eqpt.smbusWr8(0x50, 0x7F, 14)
  data = []
  for addr in range(16):
    res = eqpt.smbusRd(0x50, (addr * 16), 16)
    data += res
  dump.append({"page": "A0x14", "data": data})

  out_path = os.path.join(pyauto_base.fs.mkOutFolder(), "module_regs.json")
  with open(out_path, "w") as f_out:
    json.dump(dump, f_out)
  logger.info("{} written".format(out_path))

def _test(eqpt):
  eqpt.i2cCfg()

  if (cliArgs["act"] == "stop"):
    eqpt.modStop()
    return
  elif (cliArgs["act"] == "start"):
    eqpt.modStart()
    return
  elif (cliArgs["act"] == "pstart"):
    eqpt.modPStart()
    return

  if (cliArgs["dump"] is True):
    _dump(eqpt)
    return

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))

    res = eqpt.smbusRd(0x50, 0x00, 32)
    #print("DBG", res)
    logger.info("ID: 0x{:02x}".format(int(res[0])))
    logger.info("STATE: 0x{:02x}".format(int(res[3])))

    eqpt.smbusWr8(0x50, 0x7F, 0x00)
    res = eqpt.smbusRd(0x50, 0x81, 16)
    logger.info("MFR   : {}".format("".join([chr(t) for t in res])))
    res = eqpt.smbusRd(0x50, 0x94, 16)
    logger.info("MFR_PN: {}".format("".join([chr(t) for t in res])))
    res = eqpt.smbusRd(0x50, 0xA6, 16)
    logger.info("MFR_SN: {}".format("".join([chr(t) for t in res])))
    res = eqpt.smbusRd(0x50, 0x91, 3)
    logger.info("OUI   : {:02x}:{:02x}:{:02x}".format(int(res[0]), int(res[1]),
                                                      int(res[2])))

def mainApp():
  dictArgs = {}
  eqpt = labBase.getEqpt(cliArgs["driver"], "127.0.0.1", **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for MultiLane boards"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("driver", help="driver name")
  #parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("--act",
                      choices=("stop", "start", "pstart"),
                      default=None,
                      help="module action")
  parser.add_argument("--dump", action="store_true", default=False, help="dump module info")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
