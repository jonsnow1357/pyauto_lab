#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase
import pyauto_lab.equipment.Viavi

nRuns = 1
nMeas = 1

def _module_show_app(module):
  res = module.query("INST:LOAD?")
  logger.info("available apps:")
  for t in res:
    logger.info("  {}".format(t))
  res = module.query("INST:CAT?")
  logger.info("loaded: {}".format(res[0]))

def _module_show_status(module):
  res = module.query("OUTP:TEL:PHYS:LINE:TYPE?")
  logger.info("port: {}".format(res[0]))
  res = module.query("SOUR:DATA:TEL:PHYS:LINE:RATE?")
  logger.info("rate: {}".format(res[0]))
  res = module.query("OUTP:TEL:PHYS:LINE:OPT:STAT?")
  logger.info("TX state: {}".format(res[0]))
  res = module.query(":PHYS:TX:LINE:OPT:POW?")
  if (len(res) == 0):
    logger.info("TX power: INVALID")
  else:
    res = res[0].split(",")
    if (res[0] == "1"):
      logger.info("TX power: {} dBm".format(round(float(res[1]), 3)))
    else:
      logger.info("TX power: INVALID")

  res = module.query("PHYS:LINE:OPT:POW?")
  if (len(res) == 0):
    logger.info("RX power: INVALID")
  else:
    res = res[0].split(",")
    if (res[0] == "1"):
      logger.info("RX power: {} dBm".format(round(float(res[1]), 3)))
    else:
      logger.info("RX power: INVALID")

  res = module.query("PHYS:LINE:FOFF?")
  if (len(res) == 0):
    logger.info("RX offset: INVALID")
  else:
    res = res[0].split(",")
    if (res[0] == "1"):
      logger.info("RX offset: {} ppm".format(round(float(res[1]), 3)))
    else:
      logger.info("RX offset: INVALID")

  res = module.query("PHYS:ALL:CST:ALAR?")
  if (len(res) == 0):
    logger.info("RX alarm: INVALID")
  else:
    res = res[0].split(",")
    logger.info("RX alarm: {} lane(s) {}".format(res[0],
                                                 ["0x{:x}".format(int(t))
                                                  for t in res[1:]]))

def _module_TX_ON(module):
  module.query("OUTP:TEL:PHYS:LINE:OPT:STAT ON")

def _module_TX_OFF(module):
  module.query("OUTP:TEL:PHYS:LINE:OPT:STAT OFF")

def _module_cfg(module, mode):
  if (mode not in pyauto_lab.equipment.Viavi.lstCfg):
    logger.error("UNSUPPORTED mode {}".format(mode))
    return

  logger.info("configuring {}".format(mode))

  #module.write("INST:LOAD {}".format("New-Application"))
  #module.waitOnCmd()
  module.write("INST:CONF:EDIT:OPEN ON")

  if (mode == "ETH.100G"):
    module.write("INST:CONF:EDIT:DEV:MOD TERM")
    module.write("INST:CONF:EDIT:LAY:STAC PHYS_PCSL_MAC")

  module.write("INST:CONF:EDIT:APPL ON")
  module.waitOnCmd()

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()

  if (cliArgs["mod"] is None):
    logger.warning("MUST use a module for operation")
    return

  module = eqpt.getModule(cliArgs["mod"])
  logger.info("using module {}".format(cliArgs["mod"]))

  if (cliArgs["info"] is True):
    module.showInfo()
  if (cliArgs["cfg"] is not None):
    module.cfg(cliArgs["cfg"])
  if (cliArgs["on"] is True):
    module.setLaserOn()
  if (cliArgs["off"] is True):
    module.setLaserOff()
  if (cliArgs["go"] is True):
    module.stopMeasurement()
    module.startMeasurement()

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    res = module.getPHYInfo()
    logger.info("-- PHYS")
    for k, v in res.items():
      logger.info("{}: {}".format(k, v))
    logger.info("-- PCS")
    res = module.getPCSInfo()
    for k, v in res.items():
      logger.info("{}: {}".format(k, v))

def mainApp():
  dictArgs = {}
  eqpt = pyauto_lab.equipment.Viavi.ONT(cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for ONT"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("--info",
                      action="store_true",
                      default=False,
                      help="show chassis/module info")
  parser.add_argument("--mod", default=None, help="module ID")
  parser.add_argument("--on", action="store_true", default=False, help="module laser ON")
  parser.add_argument("--off", action="store_true", default=False, help="module laser OFF")
  parser.add_argument("--go",
                      action="store_true",
                      default=False,
                      help="restart measurement")
  parser.add_argument("--cfg", default=None, help="config module")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
