#!/usr/bin/env python
# template: lab
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_base.fs
import pyauto_base.bin
import pyauto_lab.equipment.base as labBase

nRuns = 1
nMeas = 1

def _I2C_dump(eqpt, i2cAddr, dictSz, path):
  with open(path, "wb") as f_out:
    for addr in range(0, dictSz["depth"], dictSz["page"]):
      if (addr == 0):
        if (dictSz["depth"] < 2**8):
          res = eqpt.i2cWrRd(i2cAddr, [0x00], dictSz["page"])
        elif (dictSz["depth"] < 2**16):
          res = eqpt.i2cWrRd(i2cAddr, [0x00, 0x00], dictSz["page"])
        else:
          raise NotImplementedError
      else:
        res = eqpt.i2cRd(i2cAddr, dictSz["page"])
      f_out.write(bytearray(res))
      if ((addr > 0) and (((addr * dictSz["width"]) % 1024) == 0)):
        print("-", end="")
        sys.stdout.flush()
    if ((dictSz["depth"] * dictSz["width"]) > 1024):
      print("-")
  logger.info("{} written".format(path))

def _I2C_load(eqpt, i2cAddr, dictSz, path):
  addr = 0
  cnt = 0
  with open(path, "rb") as f_in:
    while (True):
      buff = list(f_in.read(dictSz["page"]))
      if (len(buff) == 0):
        break
      if (len(buff) != dictSz["page"]):
        buff = buff + [0xff] * (dictSz["page"] - len(buff))
      if (dictSz["depth"] < 2**8):
        buff = pyauto_base.bin.int2bytes(addr, 1) + buff
      elif (dictSz["depth"] < 2**16):
        buff = pyauto_base.bin.int2bytes(addr, 2)[::-1] + buff
      else:
        raise NotImplementedError
      eqpt.i2cWr(i2cAddr, buff)
      time.sleep(0.008)  # T_WR delay
      addr += (dictSz["page"] // dictSz["width"])
      cnt += dictSz["page"]
      if (addr >= dictSz["depth"]):
        break
  logger.info("{} B written".format(cnt))

def _I2C_erase(eqpt, i2cAddr, dictSz):
  cnt = 0
  if (dictSz["width"] > 1):
    raise NotImplementedError
  for addr in range(0, dictSz["depth"], dictSz["page"]):
    buff = [0xff] * dictSz["page"]
    if (dictSz["depth"] < 2**8):
      buff = pyauto_base.bin.int2bytes(addr, 1) + buff
    elif (dictSz["depth"] < 2**16):
      buff = pyauto_base.bin.int2bytes(addr, 2)[::-1] + buff
    else:
      raise NotImplementedError
    eqpt.i2cWr(i2cAddr, buff)
    time.sleep(0.008)  # T_WR delay
    cnt += dictSz["page"]
    if ((addr > 0) and (((addr * dictSz["width"]) % 1024) == 0)):
      print("-", end="")
      sys.stdout.flush()
  logger.info("{} B written".format(cnt))

def _SPI_dump_microchip(eqpt, dictSz, path):
  with open(path, "wb") as f_out:
    for addr in range(0, dictSz["depth"], (dictSz["burst"] // dictSz["width"])):
      res = eqpt.spiXfer([0x06, addr] + (dictSz["burst"] * [0xFF]))
      f_out.write(bytearray(res)[(-1 * dictSz["burst"]):])
  logger.info("{} written".format(path))

def _SPI_load_microchip(eqpt, dictSz, path):
  addr = 0
  cnt = 0
  eqpt.spiXfer([0x04, 0xc0])  # EWEN instruction
  with open(path, "rb") as f_in:
    while (True):
      buff = bytearray(f_in.read(dictSz["width"]))
      if (len(buff) == 0):
        break
      if (len(buff) != dictSz["width"]):
        raise NotImplementedError
      eqpt.spiXfer([0x05, addr] + list(buff))
      time.sleep(0.006)  # T_WC delay
      addr += 1
      cnt += dictSz["width"]
      if (addr >= dictSz["depth"]):
        break
  eqpt.spiXfer([0x04, 0x00])  # EWDS instruction
  logger.info("{} B written".format(cnt))

def _SPI_erase_microchip(eqpt, dictSz):
  eqpt.spiXfer([0x04, 0xc0])  # EWEN instruction
  eqpt.spiXfer([0x04, 0x80])  # ERAL instruction
  time.sleep(0.01)  # T_EC delay
  eqpt.spiXfer([0x04, 0x00])  # EWDS instruction

"""
def _load_Silabs(eqpt, i2cAddr, fPath):
  pyauto_base.fs.chkPath_File(fPath)
  lstCmd = []
  with open(fPath, "r") as fIn:
    for ln in [t.strip("\n\r") for t in fIn.readlines()]:
      if (not ln.startswith("0x")):
        continue
      lstCmd.append(ln)

  logger.info("start Silabs programming ...")
  eqpt.cfgI2C(bPullUp=True)

  last_page = ""
  for cmd in lstCmd:
    addr, data = cmd.split(",")
    page = pyauto_base.bin.hexGetByte(1, addr)
    reg = pyauto_base.bin.hexGetByte(0, addr)
    if (page != last_page):
      eqpt.i2c_wr([i2cAddr, "0x01", page])
      last_page = page
    eqpt.i2c_wr([i2cAddr, reg, data])

  eqpt.HiZ()
  logger.info("DONE Silabs programming ...")
"""

# yapf: disable
_dict_I2C_MPN = {
    "cat24c32": {
        "i2c": {"speed": 400, "addr": 0x50},
        "size": {"width": 1, "depth": 4096, "page": 32},
        "call": {"dump": _I2C_dump, "load": _I2C_load, "erase": _I2C_erase, "cfg": None}
    },
}
_dict_SPI_MPN = {
    "93lc56b": {
        "spi": {"speed": 1000, "cspol": True, "cpol": False, "cpha": True},
        "size": {"width": 2, "depth": 128, "burst": 4},
        "call": {"dump": _SPI_dump_microchip, "load": _SPI_load_microchip, "erase": _SPI_erase_microchip, "cfg": None}
    },
}
# yapf: enable
_lst_MPN = list(_dict_I2C_MPN.keys()) + list(_dict_SPI_MPN.keys())

_I2C_addr_min = 0x07
_I2C_addr_max = 0x79
_I2C_dly_startup = 2
_SPI_dly_startup = 2

def _I2C_scan(eqpt):
  eqpt.i2cCfg(bPullUp=True)
  pyauto_base.misc.waitWithPrint(_I2C_dly_startup)  # wait for some I2C devices

  logger.info("== I2C scan")
  for addr in range(_I2C_addr_min, _I2C_addr_max):
    try:
      eqpt.i2cRd(addr, 1)
      logger.info("ACK from 0x{:02x}".format(addr))
    except labBase.LabEqptException:
      pass

def _SPI_test(eqpt):
  logger.info("== SPI test (loop)")

  eqpt.spiCfg()
  data = [0xff, 0x00, 0xaa, 0x55, 0x99, 0xc0, 0x0f, 0xfe]
  res = eqpt.spiXfer(data)
  if (res == data):
    logger.info("PASS: {}".format(["0x{:02x}".format(t) for t in res]))
  else:
    logger.info("FAIL: {}".format(["0x{:02x}".format(t) for t in res]))

def _dump(eqpt):
  path_out = os.path.join(pyauto_base.fs.mkOutFolder(), "eeprom.bin")
  if (cliArgs["mpn"] in _dict_I2C_MPN.keys()):
    ic = _dict_I2C_MPN[cliArgs["mpn"]]
    logger.info("== I2C dump")
    logger.info("-- addr 0x{:x}, size: {} x {} B ({} B page)".format(
        ic["i2c"]["addr"], ic["size"]["depth"], (ic["size"]["width"] * 8),
        ic["size"]["page"]))

    eqpt.i2cCfg(speed=ic["i2c"]["speed"], bPullUp=True)
    pyauto_base.misc.waitWithPrint(_I2C_dly_startup)  # wait for some I2C devices
    ic["call"]["dump"](eqpt, ic["i2c"]["addr"], ic["size"], path_out)
  if (cliArgs["mpn"] in _dict_SPI_MPN.keys()):
    ic = _dict_SPI_MPN[cliArgs["mpn"]]
    logger.info("== SPI dump")
    logger.info("-- size: {} x {} B (burst {})".format(ic["size"]["depth"],
                                                       (ic["size"]["width"] * 8),
                                                       ic["size"]["burst"]))
    eqpt.spiCfg(speed=ic["spi"]["speed"],
                bCSPOL=ic["spi"]["cspol"],
                bCPOL=ic["spi"]["cpol"],
                bCPHA=ic["spi"]["cpha"])
    pyauto_base.misc.waitWithPrint(_SPI_dly_startup)  # wait for some SPI devices
    ic["call"]["dump"](eqpt, ic["size"], path_out)

def _load(eqpt):
  pyauto_base.fs.chkPath_File(cliArgs["path"])
  if (os.stat(cliArgs["path"]).st_size == 0):
    msg = "{} IS EMPTY".format(cliArgs["path"])
    logger.error(msg)
    raise RuntimeError(msg)

  if (cliArgs["mpn"] in _dict_I2C_MPN.keys()):
    ic = _dict_I2C_MPN[cliArgs["mpn"]]
    logger.info("== I2C load")
    logger.info("-- addr 0x{:x}, size: {} x {} B ({} B page)".format(
        ic["i2c"]["addr"], ic["size"]["depth"], (ic["size"]["width"] * 8),
        ic["size"]["page"]))

    eqpt.i2cCfg(speed=ic["i2c"]["speed"], bPullUp=True)
    pyauto_base.misc.waitWithPrint(_I2C_dly_startup)  # wait for some I2C devices
    ic["call"]["load"](eqpt, ic["i2c"]["addr"], ic["size"], cliArgs["path"])
  if (cliArgs["mpn"] in _dict_SPI_MPN.keys()):
    ic = _dict_SPI_MPN[cliArgs["mpn"]]
    logger.info("== SPI load")
    logger.info("-- size: {} x {} B (burst {})".format(ic["size"]["depth"],
                                                       (ic["size"]["width"] * 8),
                                                       ic["size"]["burst"]))
    eqpt.spiCfg(speed=ic["spi"]["speed"],
                bCSPOL=ic["spi"]["cspol"],
                bCPOL=ic["spi"]["cpol"],
                bCPHA=ic["spi"]["cpha"])
    pyauto_base.misc.waitWithPrint(_SPI_dly_startup)  # wait for some SPI devices
    ic["call"]["load"](eqpt, ic["size"], cliArgs["path"])

def _erase(eqpt):
  if (cliArgs["mpn"] in _dict_I2C_MPN.keys()):
    ic = _dict_I2C_MPN[cliArgs["mpn"]]
    logger.info("== I2C erase")
    logger.info("-- addr 0x{:x}, size: {} x {} B ({} B page)".format(
        ic["i2c"]["addr"], ic["size"]["depth"], (ic["size"]["width"] * 8),
        ic["size"]["page"]))

    eqpt.i2cCfg(speed=ic["i2c"]["speed"], bPullUp=True)
    pyauto_base.misc.waitWithPrint(_I2C_dly_startup)  # wait for some I2C devices
    ic["call"]["erase"](eqpt, ic["i2c"]["addr"], ic["size"])
  if (cliArgs["mpn"] in _dict_SPI_MPN.keys()):
    ic = _dict_SPI_MPN[cliArgs["mpn"]]
    logger.info("== SPI erase")
    logger.info("-- size: {} x {} B (burst {})".format(ic["size"]["depth"],
                                                       (ic["size"]["width"] * 8),
                                                       ic["size"]["burst"]))
    eqpt.spiCfg(speed=ic["spi"]["speed"],
                bCSPOL=ic["spi"]["cspol"],
                bCPOL=ic["spi"]["cpol"],
                bCPHA=ic["spi"]["cpha"])
    pyauto_base.misc.waitWithPrint(_SPI_dly_startup)  # wait for some SPI devices
    ic["call"]["erase"](eqpt, ic["size"])

def _cfg(eqpt):
  if (cliArgs["mpn"] in _dict_I2C_MPN.keys()):
    logger.info("== I2C cfg")
    raise NotImplementedError
    #_load_Silabs(eqpt, "0xEE", "C:/WORK/projects/DCO_RFB/scripts/SiLabs/BRD_Si5341A-Registers.txt")
  if (cliArgs["mpn"] in _dict_SPI_MPN.keys()):
    logger.info("== SPI cfg")
    raise NotImplementedError

def _test(eqpt):
  if (cliArgs["action"] == "i2c_scan"):
    _I2C_scan(eqpt)
  elif (cliArgs["action"] == "spi_test"):
    _SPI_test(eqpt)
  elif (cliArgs["action"] == "dump"):
    _dump(eqpt)
  elif (cliArgs["action"] == "load"):
    _load(eqpt)
  elif (cliArgs["action"] == "erase"):
    _erase(eqpt)
  elif (cliArgs["action"] == "cfg"):
    _cfg(eqpt)
  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))

def mainApp():
  dictArgs = {}
  eqpt = labBase.getEqpt(cliArgs["driver"], cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for I2C/SPI dongle"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("driver", help="driver name")
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("action",
                      help="action",
                      choices=("info", "i2c_scan", "spi_test", "dump", "load", "erase",
                               "cfg"))
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("--mpn", choices=_lst_MPN, default="", help="Manufacturer PN")
  parser.add_argument("--addr", type=int, default=0x50, help="EEPROM address")
  parser.add_argument("--depth",
                      type=int,
                      default=0,
                      help="EEPROM depth (num of locations)")
  parser.add_argument("--width",
                      type=int,
                      default=0,
                      help="EEPROM width (data size in bytes)")
  parser.add_argument("--path", help="binary file path")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
