#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase
import pyauto_lab.equipment.HP

nRuns = 1
nMeas = 1

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    if (cliArgs["freq"] != 0.0):
      eqpt.CW(cliArgs["freq"] * 1e6, cliArgs["ampl"])
    if (cliArgs["start"]):
      eqpt.enable = True
    if (cliArgs["stop"]):
      eqpt.enable = False
    freq = eqpt.frequency
    ampl = eqpt.amplitude
    logger.info("CW freq: {} Hz, ampl: {} dBm".format(freq, ampl))

def mainApp():
  dictArgs = {}
  if (cliArgs["adapter"] is not None):
    dictArgs["adapter_name"] = cliArgs["adapter"]
  if (cliArgs["adapter"] == "prologix"):
    dictArgs["adapter_gpibAddr"] = 19
  eqpt = pyauto_lab.equipment.HP.HP_8648(cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for HP_8648"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-s",
                      "--start",
                      action="store_true",
                      default=False,
                      help="start <outId>")
  parser.add_argument("-p",
                      "--stop",
                      action="store_true",
                      default=False,
                      help="stop <outId>")
  parser.add_argument("-f", "--freq", type=float, default=0.0, help="frequency in MHz")
  parser.add_argument("-v", "--ampl", type=float, default=0.0, help="amplitude in dBm")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
