# run with: gnuplot -p thermal.gnuplot
set datafile separator ','
set key out autotitle columnhead noenhanced

#set terminal qt size 2000,500
#set terminal qt size 2560,1440 # QHD, 2.5K
#set terminal qt size 3200,1800 # QHD+
set terminal pngcairo size 3480,2160 # 4K
#set terminal pngcairo size 7680,4320 # 8K
set output "thermal.png"

set xdata time
set timefmt "%Y-%m-%dT%H:%M:%S"
set format x "%dT%H:%M"

set style line 100 lw 0.5 lc rgb "grey"
set grid ls 100
#set xtics 10
#set ytics 10
set xtics rotate # rotate labels on the x axis
#set key right center # legend placement

set style line 101 lw 4 lc rgb "red"
set style line 102 lw 4 lc rgb "blue"
plot \
  "thermal.csv" using 1:2 with lines ls 101, \
  "thermal.csv" using 1:3 with lines ls 102
