#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase
import pyauto_lab.equipment.Agilent

nRuns = 1
nMeas = 1

def _test_read_34903A(eqpt):
  cardId = "34903A"
  slotId = None
  dictInfo = eqpt.getInfo()
  for k, v in dictInfo["slots"].items():
    if (v == cardId):
      slotId = k
  if (slotId is None):
    logger.warning("CANNOT find any {}".format(cardId))
    return

  eqpt.config_channel(slotId, chId="1", measType="ON")
  eqpt.config_channel(slotId, chId="3", measType="ON")
  eqpt.config_channel(slotId, chId="5", measType="ON")
  eqpt.config_channel(slotId, chId="7", measType="ON")
  eqpt.show_config(slotId)

def _test_read_34904A(eqpt):
  cardId = "34904A"
  slotId = None
  dictInfo = eqpt.getInfo()
  for k, v in dictInfo["slots"].items():
    if (v == cardId):
      slotId = k
  if (slotId is None):
    logger.warning("CANNOT find any {}".format(cardId))
    return

  eqpt.config_channel(slotId, chId="11", measType="ON")
  eqpt.config_channel(slotId, chId="22", measType="ON")
  eqpt.config_channel(slotId, chId="33", measType="ON")
  eqpt.config_channel(slotId, chId="44", measType="ON")
  eqpt.show_config(slotId)

def _test_read_34905A(eqpt):
  cardId = "34905A"
  slotId = None
  dictInfo = eqpt.getInfo()
  for k, v in dictInfo["slots"].items():
    if (v == cardId):
      slotId = k
  if (slotId is None):
    logger.warning("CANNOT find any {}".format(cardId))
    return

  eqpt.config_channel(slotId, chId="14", measType="ON")
  eqpt.config_channel(slotId, chId="23", measType="ON")
  eqpt.show_config(slotId)

def _test_read_34907A(eqpt):
  cardId = "34907A"
  slotId = None
  dictInfo = eqpt.getInfo()
  for k, v in dictInfo["slots"].items():
    if (v == cardId):
      slotId = k
  if (slotId is None):
    logger.warning("CANNOT find any {}".format(cardId))
    return

  res = eqpt.query("DIGITAL:DATA:BYTE? (@{})".format(int(slotId) + 1))
  val = hex(int(float(res[0])))
  logger.info("DIN1: {}".format(val))
  res = eqpt.query("DIGITAL:DATA:BYTE? (@{})".format(int(slotId) + 2))
  val = hex(int(float(res[0])))
  logger.info("DIN2: {}".format(val))
  res = eqpt.query("TOTALIZE:DATA? (@{})".format(int(slotId) + 3))
  val = int(float(res[0]))
  logger.info("Totalize: {}".format(val))

  eqpt.write("SOURCE:VOLTAGE {},(@{})".format(3.3, int(slotId) + 4))
  eqpt.waitOnCmd()
  res = eqpt.query("SOURCE:VOLTAGE? (@{})".format(int(slotId) + 4))
  val = float(res[0])
  logger.info("OUT4: {} V".format(val))
  eqpt.write("SOURCE:VOLTAGE {},(@{})".format(-5.2, int(slotId) + 5))
  eqpt.waitOnCmd()
  res = eqpt.query("SOURCE:VOLTAGE? (@{})".format(int(slotId) + 5))
  val = float(res[0])
  logger.info("OUT5: {} V".format(val))

def _test_read_34908A(eqpt):
  cardId = "34908A"
  slotId = None
  dictInfo = eqpt.getInfo()
  for k, v in dictInfo["slots"].items():
    if (v == cardId):
      slotId = k
  if (slotId is None):
    logger.warning("CANNOT find any {}".format(cardId))
    return

  eqpt.config_channel(slotId, chId="1", measType="DC")
  eqpt.config_channel(slotId, chId="2", measType="AC")
  eqpt.config_channel(slotId, chId="3", measType="DC")
  eqpt.config_channel(slotId, chId="4", measType="AC")
  eqpt.config_channel(slotId, chId="5", measType="DC")
  eqpt.config_channel(slotId, chId="6", measType="AC")
  eqpt.config_channel(slotId, chId="7", measType="DC")
  eqpt.config_channel(slotId, chId="8", measType="AC")
  eqpt.show_config(slotId)

  s = int(slotId)
  chList = "(@{}:{})".format((s + 1), (s + 8))
  eqpt.write("ROUTE:SCAN {}".format(chList))
  eqpt.waitOnCmd()

  eqpt.write("INITIATE")
  eqpt.waitOnCmd(timeout=16)
  res = eqpt.query("FETCH?", TOmultiplier=2)
  res = [float(t) for t in res[0].split(",")]
  logger.info(res)

  eqpt.write("ROUTE:SCAN (@)")
  eqpt.waitOnCmd()

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()

  eqpt.setDateTime()
  dictInfo = eqpt.getInfo()
  for k, v in dictInfo["slots"].items():
    if (v is None):
      continue
    eqpt.config_slot(k)
    eqpt.show_config(k)

  #slotId = "200"
  #chId = "25"
  #eqpt.monitor_channel(slotId, chId, measType="temp_K")
  #for i in range(nMeas):
  #  logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas, pyauto_base.misc.getTimestamp()))
  #  res = eqpt.query("ROUTE:MONITOR:DATA?")
  #  logger.info("ch {}: {}".format((int(slotId) + int(chId)), float(res[0])))

  #_test_read_34903A(eqpt)
  #_test_read_34904A(eqpt)
  #_test_read_34905A(eqpt)
  #_test_read_34907A(eqpt)
  #_test_read_34908A(eqpt)

def mainApp():
  dictArgs = {}
  if (cliArgs["adapter"] == "prologix"):
    dictArgs["adapter_name"] = cliArgs["adapter"]
    dictArgs["adapter_gpibAddr"] = 9
  eqpt = pyauto_lab.equipment.Agilent.A_34970A(cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for 34970A"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
