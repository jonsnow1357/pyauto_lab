#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.plot
import pyauto_lab.equipment.base as labBase
import pyauto_lab.equipment.Tektronix

nRuns = 1
nMeas = 1

def _plot(eqpt):
  res = eqpt.query("DATA:ENCDG?")
  if (res[0] != "ASCI"):
    eqpt.write("DATA:ENCDG ASCII")
  res = eqpt.query("DATA:ENCDG?")
  if (res[0] != "ASCI"):
    msg = "ERROR setting ASCII format: {}".format(res[0])
    logger.error(msg)
    raise RuntimeError(msg)

  dictInfo = eqpt.getInfo()
  #print(dictInfo)
  res = eqpt.query("SELECT?")
  tmp = [int(t) for t in res[0].split(";")[:11]]
  enCh = tmp[:4]
  enMath = tmp[4:7]
  enRef = tmp[7:]
  if (sum(enMath) != 0):
    logger.warning("MATH active - WILL NOT capture")
  if (sum(enRef) != 0):
    logger.info("REF active - WILL NOT capture")

  ts = pyauto_base.misc.getDateTime()
  plot = pyauto_base.plot.GridPlot()
  res = eqpt.query("HORIZONTAL:MODE?")
  if (not res[0].startswith("MAI")):
    msg = "UNEXPECTED horizontal settings: {}".format(res)
    logger.error(msg)
    raise RuntimeError(msg)
  res = eqpt.query("HORIZONTAL:MAIN?")
  hscale = float(res[0])
  res = eqpt.query("HORIZONTAL:POSITION?")
  hpos = float(res[0])
  res = eqpt.query("HORIZONTAL:RECORDLENGTH?")
  hpoints = int(res[0])
  plot.setTime((hscale * (hpos - 50.0) / 10.0), hscale)

  eqpt.write("ACQUIRE:STATE STOP")

  for chEn, chId in zip(enCh, ["CH1", "CH2"]):
    if (chEn == 1):
      logger.info("{} active".format(chId))
      res = eqpt.query("{}:SCALE?".format(chId))
      vscale = float(res[0])
      res = eqpt.query("{}:POSITION?".format(chId))
      vpos = float(res[0])

      eqpt.write("DATA:SOURCE {}".format(chId))
      res = eqpt.query("WFMPRE:ENCDG?")
      if (res[0] != "ASC"):
        msg = "INCORRECT {} encoding: {}".format(chId, res)
        logger.error(msg)
        raise RuntimeError(msg)

      res = eqpt.query("WFMPRE:{}:NR_PT?".format(chId))
      vpoints = int(res[0])
      if (vpoints != hpoints):
        msg = "INCORRECT {} data points (1): {} != {}".format(chId, vpoints, hpoints)
        logger.error(msg)
        raise RuntimeError(msg)

      res = eqpt.query("WFMPRE:{}:PT_FMT?".format(chId))
      if (res[0] != "Y"):
        msg = "INCORRECT {} format: {}".format(chId, res)
        logger.error(msg)
        raise RuntimeError(msg)

      res = eqpt.query("WFMPRE:{}:WFID?".format(chId))
      logger.info(res)
      res = eqpt.query("CURVE?", TOmultiplier=16)
      data = [float(t) for t in res[0].split(",")]
      if (len(data) != vpoints):
        msg = "INCORRECT {} data points (2): {} != {}".format(chId, len(data), vpoints)
        logger.error(msg)
        raise RuntimeError(msg)

      res = eqpt.query("WFMPRE:{}:YOFF?".format(chId))
      yoff = float(res[0])
      res = eqpt.query("WFMPRE:{}:YMULT?".format(chId))
      ymult = float(res[0])
      res = eqpt.query("WFMPRE:{}:YZERO?".format(chId))
      yzero = float(res[0])
      for i in range(0, len(data)):
        data[i] = (data[i] - yoff) * ymult + yzero
      plot.addData(data, (-1.0 * vpos * vscale), vscale, "V", legend=chId)

  eqpt.write("ACQUIRE:STATE RUN")

  #plot.txt_ur = "upper right"
  #plot.txt_ul = "upper left"
  #plot.txt_ll = "lower left"
  plot.ts = ts
  plot.title = dictInfo["id"].split(",")[1]
  plot.show()

  dirOut = pyauto_base.fs.mkOutFolder()
  plot.fPath = os.path.join(dirOut, "{}_{}.png".format(plot.title.replace(" ", "_"), ts))
  plot.write()

def _reset(eqpt):
  eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  eqpt.waitOnCmd(timeout=8)

  for chId in ("CH1", "CH2"):
    eqpt.write("{}:BANDWIDTH FULL".format(chId))
    eqpt.write("{}:COUPLING DC".format(chId))
  eqpt.write("AUTOSET EXECUTE")
  eqpt.waitOnCmd(timeout=8)

def _test(eqpt):
  if (cliArgs["plot"]):
    _plot(eqpt)
    return

  _reset(eqpt)
  eqpt.chkID("TEKTRONIX,TDS 6[0-9ABC][0-9],.*")
  eqpt.write("MEASUREMENT:MEAS1:SOURCE CH1")
  eqpt.write("MEASUREMENT:MEAS1:TYPE FREQ")
  eqpt.write("MEASUREMENT:MEAS1:STATE ON")
  eqpt.write("MEASUREMENT:MEAS2:SOURCE CH1")
  eqpt.write("MEASUREMENT:MEAS2:TYPE PK2")
  eqpt.write("MEASUREMENT:MEAS2:STATE ON")
  eqpt.waitOnCmd()

  pyauto_base.misc.waitWithPrint(2)
  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    res = eqpt.query("MEASUREMENT:MEAS1:VALUE?")
    freq = float(res[0])
    res = eqpt.query("MEASUREMENT:MEAS2:VALUE?")
    pk2pk = float(res[0])
    logger.info("freq: {}, pk2pk: {}".format(freq, pk2pk))

def mainApp():
  dictArgs = {}
  if (cliArgs["adapter"] == "prologix"):
    dictArgs["adapter_name"] = cliArgs["adapter"]
    dictArgs["adapter_gpibAddr"] = 30
  eqpt = pyauto_lab.equipment.Tektronix.TDS600(cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for TDS600"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("--plot", action="store_true", default=False, help="get screenshot")
  parser.add_argument("--mkr", action="store_true", default=False, help="get markers")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
