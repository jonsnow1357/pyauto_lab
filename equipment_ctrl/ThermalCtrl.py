#!/usr/bin/env python
# template: lab
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

nRuns = 1
nMeas = 1
_fPathOut = os.path.join(pyauto_base.fs.mkOutFolder(), "thermal.csv")

def _clear_file():
  with open(_fPathOut, "w") as fOut:
    csvWr = csv.writer(fOut, lineterminator="\n")
    csvWr.writerow(["timestamp", "temp [C]", "set point [C]"])
    fOut.flush()

def _save_to_file(data):
  with open(_fPathOut, "a") as fOut:
    csvWr = csv.writer(fOut, lineterminator="\n")
    csvWr.writerow([pyauto_base.misc.getTimestamp(), str(data[0]), str(data[1])])
    fOut.flush()

def _profile_monitor(eqpt, seconds):
  tlim = datetime.timedelta(seconds=seconds)
  t0 = datetime.datetime.now()
  dt = datetime.datetime.now() - t0
  while (dt < tlim):
    [tp, sp, at_sp] = eqpt.chkStatus()
    _save_to_file([tp, sp])
    time.sleep(1)
    dt = datetime.datetime.now() - t0
    #print("DBG", dt, tlim)

def _profile(eqpt, argProfile):
  if (re.match(r"([\+\-0-9\.]+[Chm]?,)+", argProfile + ",") is None):
    msg = "INCORRECT profile '{}'".format(argProfile)
    logger.error(msg)
    raise RuntimeError(msg)

  _clear_file()
  pyauto_base.fs.cp(os.path.join("templates", "thermal.gnuplot"),
                    os.path.join(pyauto_base.fs.mkOutFolder(), "thermal.gnuplot"))

  for step in argProfile.split(","):
    if (step.endswith("C")):
      eqpt.setPoint = float(step[:-1])
      while (True):
        [tp, sp, at_sp] = eqpt.chkStatus()
        _save_to_file([tp, sp])
        time.sleep(1)
        if (at_sp):
          break
    elif (step.endswith("h")):
      #eqpt.tempRateDis()
      logger.info("wait {}".format(step))
      sec = int(step[:-1]) * 3600
      _profile_monitor(eqpt, sec)
      # eqpt.tempRateEn()
    elif (step.endswith("m")):
      #eqpt.tempRateDis()
      logger.info("wait {}".format(step))
      sec = int(step[:-1]) * 60
      _profile_monitor(eqpt, sec)
      # eqpt.tempRateEn()
    else:
      #eqpt.tempRateDis()
      logger.info("wait {}".format(step))
      sec = int(step)
      _profile_monitor(eqpt, sec)
      # eqpt.tempRateEn()

def _monitor(eqpt):
  _clear_file()
  pyauto_base.fs.cp(os.path.join("templates", "thermal.gnuplot"),
                    os.path.join(pyauto_base.fs.mkOutFolder(), "thermal.gnuplot"))

  while (True):
    [tp, sp, at_sp] = eqpt.chkStatus()
    _save_to_file([tp, sp])
    time.sleep(1)

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()

  #eqpt.showChannels()
  if hasattr(eqpt, "targetCh"):
    if (cliArgs["ch"] < 1):
      eqpt.showChannels()
      return
    else:
      eqpt.targetCh = cliArgs["ch"]
      logger.info("target ch is {}".format(eqpt.targetCh))

  if (cliArgs["start"]):
    eqpt.start()

  if (cliArgs["set"] is not None):
    eqpt.setPoint = cliArgs["set"]

  if (cliArgs["rate"] is not None):
    eqpt.tempRate = cliArgs["rate"]

  if (cliArgs["profile"] is not None):
    _profile(eqpt, cliArgs["profile"])

  if (cliArgs["stop"]):
    eqpt.stop()

  if (cliArgs["monitor"]):
    _monitor(eqpt)

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    logger.info("status: {}".format(eqpt.status))
    logger.info("temperature: {}".format(eqpt.temperature))
    logger.info("set point: {}".format(eqpt.setPoint))
    logger.info("temp rate: {}".format(eqpt.tempRate))

def mainApp():
  dictArgs = {}
  eqpt = labBase.getEqpt(cliArgs["driver"], cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for thermal chamber"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("driver", help="driver name")
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-s", "--start", action="store_true", default=False)
  parser.add_argument("-p", "--stop", action="store_true", default=False)
  parser.add_argument("-t", "--set", type=float, help="set point in C")
  parser.add_argument("-r", "--rate", type=float, help="rate in C/min, disable if <= 0")
  parser.add_argument("-m", "--monitor", action="store_true", default=False)
  parser.add_argument("-o", "--profile")
  parser.add_argument("--ch",
                      type=int,
                      default=1,
                      help="target CH if supported, list if -1")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
