#!/usr/bin/env python
# template: lab
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

nRuns = 1
nMeas = 1

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()

  if (cliArgs["image"] is not None):
    eqpt.writeImg(cliArgs["image"])
  if (cliArgs["data"] is not None):
    eqpt.writeCSV(cliArgs["data"])
  if (cliArgs["sweep"] is True):
    eqpt.write("SENSE:AVERAGE:CLEAR")
    eqpt.write("SENSE:SWEEP:MODE GROUPS")
    eqpt.waitOnCmd(timeout=120)

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    # eqpt.write("INST:SEL SA")
    # eqpt.waitOnCmd()

    #freq, pwr = eqpt.getPeak("650.0MHz", "50MHz", "10")
    #freq, pwr = eqpt.getPeak("664.0625MHz", "1KHz", "10")  # 100GAUI
    #freq, pwr = eqpt.getPeak("663.997940MHz", "1KHz", "10")  # 100GAUI_TI_EVB
    #freq, pwr = eqpt.getPeak("664.190826MHz", "1KHz", "10")  # GAUI SDK
    #freq, pwr = eqpt.getPeak("644.531265MHz", "1KHz", "10")  # CAUI
    #freq, pwr = eqpt.getPeak("1.84634368GHz", "10KHz", "10")
    #print("DBG", freq)
    #print("DBG", pwr)

    # if (cliArgs["phase"]):
    #   eqpt.write("INST:SEL PNOISE")
    #   eqpt.waitOnCmd()
    #
    #   pn = eqpt.getPhaseNoise(str(freq), "1KHz", "100MHz", "12KHz", "20MHz", "-60")
    #   #print("DBG", pn)

def mainApp():
  dictArgs = {}
  eqpt = labBase.getEqpt(cliArgs["driver"], cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for Spectrum Analyzer"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("driver", help="driver name")
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("--sweep", help="start sweep", action="store_true", default=False)
  parser.add_argument("--image", help="save <file_path> (eg. X:\\scope\\cature.png)")
  parser.add_argument("--data", help="save <file_path> (eg. X:\\scope\\cature.csv)")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
