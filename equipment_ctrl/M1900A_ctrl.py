#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse
import time

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase
import pyauto_lab.equipment.Anritsu

nRuns = 1
nMeas = 1

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()
  logger.info("Global output is {}".format(eqpt.globalEnable))
  eqpt.globalEnable = 1
  logger.info("Global output is {}".format(eqpt.globalEnable))
  time.sleep(2)
  eqpt.globalEnable = 0
  logger.info("Global output is {}".format(eqpt.globalEnable))
  # logger.info("Data output is {}".format(eqpt.Data.enable))
  # eqpt.Data.enable = 1
  # time.sleep(2)
  # logger.info("Data output is {}".format(eqpt.Data.enable))
  # eqpt.Data.enable = 0
  # logger.info("Data output is {}".format(eqpt.Data.enable))
  # logger.info("Data Amplitude is {}".format(eqpt.Data.amplitude))
  # eqpt.Data.amplitude = 0.9
  # logger.info("Data Amplitude is {}".format(eqpt.Data.amplitude))
  # time.sleep(2)
  # eqpt.Data.amplitude = 0.55
  # logger.info("Data Amplitude is {}".format(eqpt.Data.amplitude))
  # logger.info("Data Modulation is {}".format(eqpt.Data.modulation))
  # eqpt.Data.modulation = 'NRZ'
  # logger.info("Data Modulation is {}".format(eqpt.Data.modulation))
  # time.sleep(2)
  # eqpt.Data.modulation = 'PAM4'
  # logger.info("Data Modulation is {}".format(eqpt.Data.modulation))
  logger.info("Global output is {}".format(eqpt.Sj.enable))
  eqpt.Sj.enable = 0
  logger.info("Global output is {}".format(eqpt.Sj.enable))
  eqpt.Sj.enable = 1
  logger.info("Global output is {}".format(eqpt.Sj.enable))
  eqpt.Sj.enable = 0
  logger.info("Global output is {}".format(eqpt.Sj.enable))
  eqpt.Sj.frequency = 10e6
  eqpt.Sj.amplitude = 50e-3
  logger.info("Data Modulation is {}".format(eqpt.Data.modulation))

def mainApp():
  dictArgs = {}
  eqpt = pyauto_lab.equipment.Anritsu.M1900A(cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.build()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for M1900A"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
