#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_lab.config as lab_cfg
import pyauto_lab.equipment.base as labBase

def mainApp():
  logger.info("using config file: {}".format(os.path.realpath(cliArgs["cfg"])))
  eqptCfg = lab_cfg.LabSetupConfig()
  eqptCfg.loadCfgFile(cliArgs["cfg"])
  #eqptCfg.showInfo()

  for eqptInfo in eqptCfg.dictEqpt.values():
    logger.info("========")
    eqpt = labBase.getEqpt(eqptInfo.cls, eqptInfo.conn, eqptInfo.params)
    eqpt.connect()
    eqpt.showInfo()
    if (isinstance(eqpt, labBase.PowerSupply)):
      logger.info("INFO: {} output(s)".format(eqpt.nOut))
      for i in range(eqpt.nOut):
        res = eqpt.getOutput(i + 1)
        logger.info("  OUT{}: {} V, {} A".format((i + 1), res["voltage"], res["current"]))
    elif (isinstance(eqpt, labBase.Oscilloscope)):
      logger.info("{} channel(s)".format(eqpt.nCh))
      hs = eqpt.getHorizontal()
      for i in range(eqpt.nCh):
        vs, vo = eqpt.getVertical(i)
        lbl = eqpt.getLabel(i + 1)
        logger.info("  CH{}: scale_h {} s, scale_v {} V, offset_v {} V, lbl '{}'".format(
            i, hs[0], vs, vo, lbl))
    elif (isinstance(eqpt, labBase.ThermalChamber)):
      logger.info("status: {}".format(eqpt.status))
      logger.info("temperature: {}".format(eqpt.temperature))
      logger.info("set point: {}".format(eqpt.setPoint))
      logger.info("temp rate: {}".format(eqpt.tempRate))
    eqpt.showErrors()
    eqpt.disconnect()

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  appDir = os.getcwd()  # current folder
  appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "read ID information from equipment"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  parser.add_argument("-f", "--cfg", default=appCfgPath, help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
