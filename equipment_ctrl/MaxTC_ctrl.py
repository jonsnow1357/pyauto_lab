#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase
import pyauto_lab.equipment.MD

nRuns = 1
nMeas = 1
_fPathOut = os.path.join(pyauto_base.fs.mkOutFolder(), "maxtc.csv")

def _clear_file():
  with open(_fPathOut, "w") as fOut:
    csvWr = csv.writer(fOut, lineterminator="\n")
    csvWr.writerow(["timestamp", "temp [C]", "set point [C]"])
    fOut.flush()

def _save_to_file(data):
  with open(_fPathOut, "a") as fOut:
    csvWr = csv.writer(fOut, lineterminator="\n")
    csvWr.writerow([pyauto_base.misc.getTimestamp(), str(data[0]), str(data[1])])
    fOut.flush()

def _profile_monitor(eqpt, seconds):
  tlim = datetime.timedelta(seconds=seconds)
  t0 = datetime.datetime.now()
  dt = datetime.datetime.now() - t0
  while (dt < tlim):
    [t, s, c] = eqpt.chkStatus()
    _save_to_file([t, s])
    dt = datetime.datetime.now() - t0

def _profile(eqpt, argProfile):
  if (re.match(r"([\+\-0-9\.]+[Chm]?,)+", argProfile + ",") is None):
    msg = "INCORRECT profile '{}'".format(argProfile)
    logger.error(msg)
    raise RuntimeError(msg)

  #eqpt.start()
  _clear_file()
  for step in argProfile.split(","):
    if (step.endswith("C")):
      temp = float(step[:-1])
      eqpt.tempRateEn()
      eqpt.setPoint = temp
    elif (step.endswith("h")):
      eqpt.tempRateDis()
      logger.info("wait {}".format(step))
      sec = int(step[:-1]) * 3600
      _profile_monitor(eqpt, sec)
    elif (step.endswith("m")):
      eqpt.tempRateDis()
      logger.info("wait {}".format(step))
      sec = int(step[:-1]) * 60
      _profile_monitor(eqpt, sec)
    else:
      eqpt.tempRateDis()
      logger.info("wait {}".format(step))
      sec = int(step)
      _profile_monitor(eqpt, sec)
  #eqpt.stop()

def _monitor(eqpt):
  _clear_file()
  while (True):
    [t, s, c] = eqpt.chkStatus()
    _save_to_file([t, s])
    time.sleep(1)

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()

  if (cliArgs["start"]):
    eqpt.start()

  if (cliArgs["set"] is not None):
    eqpt.setPoint = cliArgs["set"]

  if (cliArgs["profile"] is not None):
    _profile(eqpt, cliArgs["profile"])

  if (cliArgs["stop"]):
    eqpt.stop()

  if (cliArgs["monitor"]):
    _monitor(eqpt)

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    logger.info("status: {}".format(eqpt.status))
    logger.info("temperature: {}".format(eqpt.temperature))

    #eqpt.profile
    #logger.info('Profile 1 is: {}'.format(eqpt.profile1))
    #eqpt.profile1 = True
    #logger.info('Profile 1 is: {}'.format(eqpt.profile1))
    #eqpt.setProfile1(iSetPoint=[25, 25, 25], iRate=[5, 5, 5], iSoak=[1, 1, 1])

def mainApp():
  dictArgs = {}
  eqpt = pyauto_lab.equipment.MD.MaxTC(cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for MaxTC"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-s", "--start", action="store_true", default=False)
  parser.add_argument("-p", "--stop", action="store_true", default=False)
  parser.add_argument("-t", "--set", type=float)
  parser.add_argument("-m", "--monitor", action="store_true", default=False)
  parser.add_argument("-r", "--profile")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
