#!/usr/bin/env python
# template: lab
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.convert
import pyauto_lab.equipment.base as labBase

nRuns = 1
nMeas = 1

def _labels(eqpt):
  lst_lbl = cliArgs["labels"].split(",")
  #print("DBG", lst_lbl)
  if (len(lst_lbl) < eqpt.nCh):
    lst_lbl += (eqpt.nCh - len(lst_lbl)) * [""]
  #print("DBG", lst_lbl)
  for i in range(eqpt.nCh):
    eqpt.setLabel(lst_lbl[i], (i + 1))
  return lst_lbl

def _show(eqpt):
  lst_ch = [int(ch) for ch in cliArgs["show"].split(",")]
  lst_ch = [t for t in lst_ch if ((t > 0) and (t < (eqpt.nCh + 1)))]
  for i in range(eqpt.nCh):
    if ((i + 1) in lst_ch):
      eqpt.showCh(i + 1)
    else:
      eqpt.hideCh(i + 1)

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()

  if (cliArgs["trigger"] is not None):
    eqpt.setTrigger(cliArgs["trigger"])
  if (cliArgs["horiz"] is not None):
    tmp = cliArgs["horiz"].split(",")
    if (len(tmp) == 1):
      scale = pyauto_base.convert.value2float(tmp[0])
      offset = 0.0
    elif (len(tmp) == 2):
      scale = pyauto_base.convert.value2float(tmp[0])
      offset = pyauto_base.convert.value2float(tmp[1])
    else:
      raise RuntimeError
    eqpt.setHorizontal(scale, offset)

  lst_lbl = []
  if (cliArgs["labels"] is not None):
    lst_lbl = _labels(eqpt)
  if (cliArgs["show"] is not None):
    _show(eqpt)
  if (cliArgs["image"] is not None):
    if (not cliArgs["image"].endswith(".png")):
      tmp = [lbl for lbl in lst_lbl if (lbl != "")]
      if (len(tmp) == 1):
        eqpt.writeImg(os.path.join(cliArgs["image"], "{}.png".format(tmp[0])))
      else:
        raise RuntimeError
    else:
      eqpt.writeImg(cliArgs["image"])
  if (cliArgs["csv"] is not None):
    eqpt.writeCSV(cliArgs["csv"])

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    scale, offset = eqpt.getHorizontal()
    if (offset > 1.0):
      logger.info("horiz: {} s, {} %".format(scale, offset))
    else:
      logger.info("horiz: {} s, {} s".format(scale, offset))
    for j in range(eqpt.nCh):
      scale, offset = eqpt.getVertical(j + 1)
      logger.info("vert CH{}: {} V, {} V".format((j + 1), scale, offset))

def mainApp():
  dictArgs = {}
  eqpt = labBase.getEqpt(cliArgs["driver"], cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for Oscilloscope"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("driver", help="driver name")
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("--show", help="show channels (comma separated)")
  parser.add_argument("--trigger", choices=labBase.lstOscTrigger, help="set trigger mode")
  parser.add_argument("--horiz", help="set horizontal scale to \"scale,offset\"")
  parser.add_argument("--labels", help="label channels (comma separated)")
  parser.add_argument("--image", help="save <file_path> (eg. X:\\scope\\cature.png)")
  parser.add_argument("--csv", help="save <file_path> (eg. X:\\scope\\cature.csv)")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
