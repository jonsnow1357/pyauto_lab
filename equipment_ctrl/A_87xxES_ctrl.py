#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.plot
import pyauto_lab.equipment.base as labBase
import pyauto_lab.equipment.Agilent
import pyauto_lab.data.file

nRuns = 1
nMeas = 1

def _plot(eqpt):
  res = eqpt.query("LOGM?")
  if (res[0] != "1"):
    msg = "display mode NOT LOGM"
    logger.error(msg)
    raise RuntimeError(msg)

  dictInfo = eqpt.getInfo()
  stf = float(eqpt.stf)
  spf = float(eqpt.spf)
  div = round(float(eqpt.div), 2)
  refp = float(eqpt.query("REFP?")[0])
  refv = float(eqpt.query("REFV?")[0])
  ref = round(((5.0 - refp) * div + refv), 2)
  #logger.info([stf, spf, div, refp, refv])

  eqpt.write("FORM4")
  res = eqpt.query("OUTPFORM")
  data = []
  for ln in res:
    tmp = [float(f) for f in ln.split(",")]
    data.append(tmp[0])
  #print(data)

  ts = pyauto_base.misc.getDateTime()
  plot = pyauto_base.plot.GridPlot(ndivX=10, ndivY=10)
  plot.setFreq(stf, spf, mode="start_stop")
  plot.addData(data, center=ref, scale=div, unit="dB")
  plot.txt_ur = "{} dB / div".format(div)
  plot.txt_ul = "REF={} dB".format(ref)
  #plot.txt_ll = "lower left"

  if (cliArgs["mkr"]):
    marker = []
    for q in ("MARK1", "MARK2", "MARK3", "MARK4", "MARK5"):
      eqpt.write("{}".format(q))
      res = eqpt.query("OUTPMARK")
      tmp = [float(f) for f in res[0].split(",")]
      marker.append([tmp[2], tmp[0]])
    #logger.info(marker)
    for mkr in marker:
      plot.addMarker(mkr[0], mkr[1])

  plot.ts = ts
  for q in ("S11", "S12", "S21", "S22"):
    res = eqpt.query("{}?".format(q))
    if (res[0] == "1"):
      plot.title = q
      break
  plot.show()

  dirOut = pyauto_base.fs.mkOutFolder()
  plot.fPath = os.path.join(dirOut, "{}_{}_{}".format(dictInfo["id"], plot.title, ts))
  plot.write()

# def _s2p(eqpt):
#   eqpt.write("FORM4;")
#   stf = float(eqpt.stf)
#   spf = float(eqpt.spf)
#   res = eqpt.query("POIN?;")
#   n_pts = int(float(res[0]))
#
#   freq_step = (spf - stf) / (n_pts - 1)
#   lstFreq = []
#   for i in range(0, n_pts):
#     lstFreq.append(stf + i * freq_step)
#
#   dirOut = pyauto_base.fs.mkOutFolder()
#   fOutPath = os.path.join(dirOut, "VNA_out.s2p")
#   tFile = pyauto_lab.data.file.TouchstoneFile(fOutPath)
#   tFile.optionLine = "# Hz S RI R 50"
#   tFile.setFreq(lstFreq)
#
#   for cmd in ("S11;", "S12;", "S21;", "S22;"):
#     logger.info("== {} {} point(s)".format(cmd, n_pts))
#     eqpt.write(cmd)
#     eqpt.write("SMIC;")
#     eqpt.write("OPC?;SING;")
#     eqpt.waitOnCmd(timeout=40)  # lots of points sometimes ...
#
#     tmp = eqpt._adapter.gpibEOM
#     eqpt._adapter.gpibEOM = None
#     res = eqpt.query("OUTPFORM;", TOmultiplier=2)
#     eqpt._adapter.gpibEOM = tmp
#
#     res = [t.split(",") for t in res]
#     tFile.setParam(int(cmd[1]), int(cmd[2]), res)
#   tFile.write()
#   eqpt.write("CONT;")

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()

  if (cliArgs["plot"]):
    _plot(eqpt)
    return
  if (cliArgs["s2p"]):
    #_s2p(eqpt)
    dirOut = pyauto_base.fs.mkOutFolder()
    fOutPath = os.path.join(dirOut, "VNA_out.s2p")
    eqpt.writeTouchstone(fOutPath)
    return

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))

def mainApp():
  dictArgs = {}
  if (cliArgs["adapter"] is not None):
    dictArgs["adapter_name"] = cliArgs["adapter"]
  if (cliArgs["adapter"] == "prologix"):
    dictArgs["adapter_gpibAddr"] = 16
  eqpt = pyauto_lab.equipment.Agilent.A_87xxES(cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for 87xxES"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("--s2p", action="store_true", default=False, help="get .s2p data")
  parser.add_argument("--plot", action="store_true", default=False, help="get screenshot")
  parser.add_argument("--mkr", action="store_true", default=False, help="get markers")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
