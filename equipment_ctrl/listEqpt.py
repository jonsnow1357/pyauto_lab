#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_lab.equipment.base as labBase

def mainApp():
  cnt = 1
  for eqptName in labBase.getEqptNames():
    eqpt = labBase.getEqpt(eqptName)
    lst_parents = [str(t) for t in eqpt.__class__.__mro__]
    lst_parents = [t for t in lst_parents if (t != "<class 'object'>")]
    lst_parents = [
        re.split(r"<class 'pyauto_lab\.equipment\.(.*)'>", t)[1] for t in lst_parents
    ]
    lst_parents = [t for t in lst_parents if ("base" in t)]
    if (len(lst_parents) > 1):
      lst_parents = [t for t in lst_parents if ("LabEqpt" not in t)]
    logger.info("{:<32} - {}".format(eqptName, lst_parents))
    cnt += 1
  logger.info("found {:d} equipment(s)".format(cnt))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "quick list of available equipments"
  parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",
  #                    choices=("info", ""))
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
