#!/usr/bin/env python
# template: lab
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

nRuns = 1
nMeas = 1

def _config(eqpt):
  lst_params = cliArgs["config"].split(",")
  if (len(lst_params) != 3):
    msg = "INCORRECT config: {}".format(cliArgs["config"])
    logger.error(msg)
    raise RuntimeError(msg)

  freq = int(float(lst_params[1]) * 1000)
  ampl = float(lst_params[2])
  eqpt.CW(freq, ampl, int(lst_params[0]))

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()

  if (cliArgs["config"] is not None):
    _config(eqpt)
  if (cliArgs["start"] is not None):
    eqpt.enable(cliArgs["start"])
  if (cliArgs["stop"] is not None):
    eqpt.disable(cliArgs["stop"])

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    unit = eqpt.getUnit()

    for out in range(eqpt.nOut):
      freq = eqpt.getFreq(outId=out)
      pwr = eqpt.getPwr(outId=out)
      # yapf: disable
      logger.info("OUT{}: {} {} @ {} MHz".format((out + 1), pwr, unit,
                                                 round((freq / 1e6), 3)))
      # yapf: enable

def mainApp():
  dictArgs = {}
  eqpt = labBase.getEqpt(cliArgs["driver"], cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for RF Signal Generator"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("driver", help="driver name")
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-s", "--start", type=int, help="start <outId>")
  parser.add_argument("-p", "--stop", type=int, help="stop <outId>")
  # yapf: disable
  parser.add_argument("-c", "--config",
                      help="<outId>,<freq_kHz>,<ampl_dBm>")
  # yapf: enable

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
