#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse
import random

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase
import pyauto_lab.equipment.TotalPhase

nRuns = 1
nMeas = 1

def _test_I2C(eqpt):
  eqpt.i2cCfg()
  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    lstTX = [
        "0x3", "0x4", "0x7", "0x0", "0x0", "0x0", "0x1", "0x0", "0x0", "0x0", "0x0", "0x1",
        "0xd", "0x0", "0x0", "0x0", "0x37", "0x1b", "0x0", "0x0", "0x43", "0x49", "0x53",
        "0x43", "0x4f", "0x2d", "0x46", "0x49", "0x4e", "0x49", "0x53", "0x41", "0x52",
        "0x20", "0x20", "0x20", "0x0", "0x0", "0x90", "0x65", "0x46", "0x54", "0x52",
        "0x4a", "0x38", "0x35", "0x31", "0x39", "0x50", "0x31", "0x42", "0x4e", "0x4c",
        "0x2d", "0x43", "0x36", "0x41", "0x20", "0x20", "0x20", "0x3", "0x52", "0x0", "0x74"
    ]
    lstRX = eqpt.rdI2C_fromAddr("0xA0", "0x00", 256)
    logger.info(lstRX)

    if (len(lstRX) != 256):
      msg = "UNEXPECTED length: {}".format(len(lstRX))
      logger.error(msg)
      raise RuntimeError(msg)
    for t, r in zip(lstTX, lstRX):
      if (t != r):
        msg = "{} != {}".format(t, r)
        logger.error(msg)
        raise RuntimeError(msg)

def _test_SPI(eqpt):
  eqpt.spiCfg(speed=4000)

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    lstTX = [hex(random.randint(0, 255)) for _ in range(16)]
    lstRX = eqpt.spiXfer(lstTX)
    logger.info(lstRX)

    if (len(lstRX) != 16):
      msg = "UNEXPECTED length: {}".format(len(lstRX))
      logger.error(msg)
      raise RuntimeError(msg)
    for t, r in zip(lstTX, lstRX):
      if (t != r):
        msg = "{} != {}".format(t, r)
        logger.error(msg)
        raise RuntimeError(msg)

def _test(eqpt):
  if (cliArgs["i2c"]):
    _test_I2C(eqpt)
  elif (cliArgs["spi"]):
    _test_SPI(eqpt)

def mainApp():
  dictArgs = {}
  eqpt = pyauto_lab.equipment.TotalPhase.Aardvark("", **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for Aardvark"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("--i2c", action="store_true", default=False, help="I2C EEPROM test")
  parser.add_argument("--spi", action="store_true", default=False, help="SPI loopback test")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
