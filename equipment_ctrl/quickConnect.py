#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase

nRuns = 1

def mainApp():
  dictArgs = {}
  if (cliArgs["adapter"] is not None):
    dictArgs["adapter_name"] = cliArgs["adapter"]
    if (cliArgs["class"] == "Keysight.N1000"):
      dictArgs["adapter_gpibAddr"] = 18
    elif (cliArgs["class"] == "JDSU.SB"):
      dictArgs["adapter_gpibAddr"] = 7
  eqpt = labBase.getEqpt(cliArgs["class"], cliArgs["conn"], dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "quick connect to an equipment"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("class", help="equipment class name")
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
