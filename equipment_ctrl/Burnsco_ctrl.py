#!/usr/bin/env python
# template:
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv
import argparse

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase
import pyauto_lab.equipment.Watlow

nRuns = 1
nMeas = 1
_fPathOut = os.path.join(pyauto_base.fs.mkOutFolder(), "burnsco.csv")

def _clear_file():
  with open(_fPathOut, "w") as fOut:
    csvWr = csv.writer(fOut, lineterminator="\n")
    csvWr.writerow(["timestamp", "temp [C]", "set point [C]"])
    fOut.flush()

def _save_to_file(data):
  with open(_fPathOut, "a") as fOut:
    csvWr = csv.writer(fOut, lineterminator="\n")
    csvWr.writerow([pyauto_base.misc.getTimestamp(), str(data[0]), str(data[1])])
    fOut.flush()

def _monitor(eqpt):
  _clear_file()
  while (True):
    curr_temp = eqpt.temperature
    set_point = eqpt.setPoint
    logger.info("temperature {} C / set point {} C".format(curr_temp, set_point))
    _save_to_file([curr_temp, set_point])
    time.sleep(1)

def _test(eqpt):
  #eqpt.RST()
  #pyauto_base.misc.waitWithPrint(4)
  #eqpt.waitOnCmd()

  if (cliArgs["start"]):
    eqpt.start()

  if (cliArgs["set"] is not None):
    eqpt.setPoint = cliArgs["set"]

  if (cliArgs["stop"]):
    eqpt.stop()

  if (cliArgs["monitor"]):
    _monitor(eqpt)

  for i in range(nMeas):
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas,
                                                      pyauto_base.misc.getTimestamp()))
    logger.info("status: {}".format(eqpt.status))
    logger.info("temperature: {}".format(eqpt.temperature))

def mainApp():
  dictArgs = {}
  eqpt = pyauto_lab.equipment.Watlow.F4T(cliArgs["conn"], **dictArgs)

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.showInfo()
    eqpt.showErrors()
    _test(eqpt)
    eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for Burnsco"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  parser.add_argument("-s", "--start", action="store_true", default=False)
  parser.add_argument("-p", "--stop", action="store_true", default=False)
  parser.add_argument("-t", "--set", type=float)
  parser.add_argument("-m", "--monitor", action="store_true", default=False)

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
